import Entity from './Entity';
import SharePlacement from './SharePlacement';
import Chooser from '../vendor/util/chooser';
import objectForLoop from '../vendor/util/objectHandle';
import getLastElementsInArray from '../vendor/util/getLastElementInArray';
// import Query from '../vendor/util/query';
import * as arrayHandle from '../vendor/util/arrayHandle';

class Share extends Entity {
  constructor(share) { // eslint-disable-line
    try {
      super(share);
      this.id = `${share.id}`;
      this.sharePlacements = share.sharePlacements || [];
      this.sharePlacements = this.sharePlacements.filter(sharePlacement => sharePlacement.placement); // do this for avoid placement = null cause error.
      this.type = share.type;
      this.format = share.format;
      this.structure = share.structure;
      this.zoneId = share.zoneId;
      this.siteId = share.siteId;
      this.totalShare = share.totalShare;
      this.isExtra = share.isExtra;
      this.isContainRelative = share.isContainRelative;
      // this.query = new Query(this.store, null, this.zoneId);
      this.currentTrack = null;
      this.isRotate = share.isRotate;
      this.isPageLoad = share.isPageLoad;
      this.rotateTime = share.rotateTime;
      this.relative = this.store.state.RELATIVE;
      this.isTemplate = share.isTemplate;
      this.template = share.template;
      this.reSelectRelativeFromZone = share.reSelectRelativeFromZone; // when this share is re-choose from zone for run right relative
      this.freeTurns = share.freeTurns; // pass from zone after compute history.
      this.only = share.only || false;
      this.tree = {
        zoneId: this.zoneId,
        shareId: this.id,
        siteId: this.siteId,
      };
      this.shareAppearWithCpd = share.shareAppearWithCpd;
      this.appeared = share.appeared;
      // turn position on share of placement = 0 if share type is single.
      if (this.type === 'single') {
        this.sharePlacements.map(sharePlacement => { // eslint-disable-line
          sharePlacement.positionOnShare = 0; // eslint-disable-line
          sharePlacement.placement.positionOnShare = 0; // eslint-disable-line
        });
      }
      this.passTree();
    } catch (error) {
      console.log('ArfError from share constructor', error);
    }
  }

  /**
   * pass detail to children
   */
  passTree() {
    for (let i = 0; i < this.sharePlacements.length; i += 1) {
      this.tree.shareId = this.id;
      this.sharePlacements[i].tree = {
        zoneId: this.tree.zoneId,
        shareId: this.tree.shareId,
        siteId: this.siteId,
      };
      this.sharePlacements[i].query = this.query;
    }
  }
  
  /**
   * compute Monopoly Percent
   * @param {*} position 
   * @param {*} avoidPlacementsId 
   * @returns {
          positionOnShare: position,
          turnCpd: number,
        }
   */
  cpdTurnInOnePosition(position = null, avoidPlacementsId = []) {
    try {
      const sharePlacements = this.allSharePlacements()
        .filter(item =>
           item.isAvailable()
           && (position ? item.positionOnShare === position : true)
           && avoidPlacementsId.indexOf(item.placement.id) < 0);
      if (sharePlacements.length === 0) {
        return {
          positionOnShare: position,
          turnCpd: 0,
        };
      }
      const positionCpds = [];
      for (let index = 0; index < sharePlacements.length; index += 1) {
        const sharePlacement = sharePlacements[index];
        const positionCpd = positionCpds.filter(i => i.positionOnShare === sharePlacement.positionOnShare)[0];
        if (!positionCpd) {
          positionCpds.push({
            positionOnShare: sharePlacement.positionOnShare,
            turnCpd: sharePlacement.placement.turnCpd(),
          });
        } else {
          positionCpd.turnCpd += sharePlacement.placement.turnCpd();
        }
      }

      let highestPositionCpd = positionCpds[0];
      for (let index = 0; index < positionCpds.length; index += 1) {
        const element = positionCpds[index];
        if (highestPositionCpd.turnCpd < element.turnCpd) highestPositionCpd = element;
      }

      return highestPositionCpd;
    } catch (error) {
      console.log('ArfError from share cpdTurnInOnePosition()', error);
      return 0;
    }
  }

  totalCpd() {
    let sharePlacements = (!this.isPageLoad) ? this.allSharePlacementsAvailable() : this.allSharePlacementsAvailableInPageLoad();
    // if none placements page load.
    if (sharePlacements.length === 0) sharePlacements = this.allSharePlacementsAvailable();
    const shareFormat = this.shareFormat();
    let result = 0;
    for (let index = 0; index < shareFormat.length; index += 1) {
      const positionOnShare = shareFormat[index].positionOnShare;
      const totalCPDPercent = this.cpdTurnInOnePosition(positionOnShare).turnCpd;
      result += totalCPDPercent;
    }
    return result;
  }

  heighestCpmWeight(position) {
    return this.allSharePlacements() // eslint-disable-line no-param-reassign
      .filter(sharePlacement => sharePlacement.isAvailable() && (sharePlacement.positionOnShare === position || !position))
      .reduce((result, sharePlacement) => {
        const weightSharePlacement = (sharePlacement.placement.revenueType === this.config.AdsType.revenueType.cpm.value ? sharePlacement.placement.weight : 0);
        if (result < weightSharePlacement) result = weightSharePlacement; // eslint-disable-line no-param-reassign
        return result;
      }, 0);
  }

  /**
   * get all Share-Placement
   * @returns {Array<Share>}
   */
  allSharePlacements() {
    try {
      const allSharePlacements = this.sharePlacements.map(item => (item instanceof SharePlacement ? item : new SharePlacement(item)));
      // allSharePlacements.sort((a, b) => a.positionOnShare - b.positionOnShare)
      return allSharePlacements;
    } catch (error) {
      console.log('ArfError from share allSharePlacements()', error);
      return [];
    }
  }

  forceSharePlacement() {
    return this.allSharePlacements().filter(item => item.isForce())[0];
  }

  allSharePlacementsAvailable() {
    try {
      return this.allSharePlacements().filter(item => item.isAvailable());
    } catch (error) {
      console.log('ArfError from share allSharePlacementsAvailable()', error);
      return [];
    }
  }

  /**
   * return all placement in pageload.
   * @param {int} positionOnShare
   */
  allSharePlacementsInPageLoad(positionOnShare) {
    try {
      if (!this.isPageLoad) return [];
      let result = [];
      const activePageLoad = this.store.state.PAGE_LOAD.activePageLoad;
      // console.log('checkActivePageload', activePageLoad.campaignId);
      const sharePlacementsAvailable = this.allSharePlacementsAvailable();
      const format = !positionOnShare ? this.shareFormat() : this.shareFormat().filter(position => position.positionOnShare === positionOnShare);
      // console.log('format', format);
      for (let i = 0; i < format.length; i += 1) {
        const sharePlacementsInPosition = sharePlacementsAvailable.filter(item => item.positionOnShare === format[i].positionOnShare);
        const inPageLoad = sharePlacementsInPosition.filter(item => item.placement.campaignId === activePageLoad.campaignId);
        if (inPageLoad.length > 0) {
          result = result.concat(inPageLoad);
        }
      }
      return result;
    } catch (error) {
      console.log('ArfError from share allSharePlacementsInPageLoad()', error);
      return [];
    }
  }

  allSharePlacementsAvailableInMonopolyCampaign() {
    try {
      let result = [];
      const { monopolyCampaign } = this.store.state.PAGE_LOAD;
      const sharePlacementsAvailable = this.allSharePlacementsAvailable();
      const format = this.shareFormat();
      // console.log('format', format);
      for (let i = 0; i < format.length; i += 1) {
        const sharePlacementsInPosition = sharePlacementsAvailable.filter(item => item.positionOnShare === format[i].positionOnShare);
        const inMonopolyCampaign = monopolyCampaign ? sharePlacementsInPosition.filter(item => item.placement.campaignId === monopolyCampaign.campaignId) : [];
        if (inMonopolyCampaign.length > 0) {
          result = result.concat(inMonopolyCampaign);
        }
      }
      console.log('allSharePlacementsAvailableInMonopolyCampaign', this.id, result);
      return result;
    } catch (error) {
      console.log('ArfError from share allSharePlacementsAvailableInMonopolyCampaign()', error);
      return [];
    }
  }

  isMonopoly() {
    const monopolyPlacements = this.allSharePlacementsAvailableInMonopolyCampaign();
    return monopolyPlacements.length > 0;
  }
  /**
   * return placement running on position, if position have placements in pageload -> return them, if not return placement running in this position.
   */
  allSharePlacementsAvailableInPageLoad() {
    try {
      let result = [];
      const { activePageLoad } = this.store.state.PAGE_LOAD;
      const sharePlacementsAvailable = this.allSharePlacementsAvailable();
      const format = this.shareFormat();
      // console.log('format', format);
      for (let i = 0; i < format.length; i += 1) {
        const sharePlacementsInPosition = sharePlacementsAvailable.filter(item => item.positionOnShare === format[i].positionOnShare);
        const inPageLoad = sharePlacementsInPosition.filter(item => item.placement.campaignId === activePageLoad.campaignId);
        if (inPageLoad.length > 0) {
          result = result.concat(inPageLoad);
        } else {
          result = result.concat(sharePlacementsInPosition);
        }
      }
      console.log('allSharePlacementsAvailableInPageLoad', this.id, result);
      return result;
    } catch (error) {
      console.log('ArfError from share allSharePlacementsAvailableInPageLoad()', error);
      return [];
    }
  }

  /**
   * get share format.
   */
  shareFormat() { // eslint-disable-line
    try {
      return this.type === this.config.AdsType.shareType.single ? [{
        positionOnShare: 0,
        area: {
          width: this.width,
          height: this.height,
        },
        totalShare: this.totalShare || 3,
      }] :
        this.format.split(',').map((item, index) => ({
          positionOnShare: index + 1,
          area: parseInt(item, 10),
          totalShare: this.totalShare || 3,
        }));
    } catch (error) {
      console.log('ArfError from share shareFormat()', error);
    }
  }

  /**
   * get history about shares previous.
   */
  sharesHistory(isGetAll) { // eslint-disable-line
    try {
      const trackLoad = this.store.getters.zoneTrackLoad(this.zoneId, this.totalShare);
      if (isGetAll) return trackLoad.turns;
      return trackLoad.turns.filter(item => item.shareId === this.id);
    } catch (error) {
      console.log('ArfError from share sharesHistory()', error);
    }
  }

  isAvailable() {
    try {
      const allSharePlacements = this.allSharePlacementsAvailable();
      const set1 = allSharePlacements.filter(sharePlacement => sharePlacement.positionOnShare !== 0);
      const set2 = allSharePlacements.filter(sharePlacement => sharePlacement.positionOnShare === 0);
      const shareFormat = this.type === this.config.AdsType.shareType.single ? [1] : this.format.split(',').map(item => parseInt(item, 10));
      // let set2Count = set2.length;
      // const result = shareFormat.reduce((res, item, index) => {
      //   const check1 = set1.length > 0 ? set1.filter(sharePlacement => sharePlacement.positionOnShare === (index + 1)).length > 0 : false;
      //   if (set2Count > 0) {
      //     set2Count -= 1;
      //   }
      //   return res !== 0 ? res && (check1 || set2.length > 0) : (check1 || set2.length > 0);
      // }, 0);
      const result = set1.length > 0 || set2.length > 0;
      console.log(`share ${this.id}: `, result, shareFormat, set1, set2);
      return result;
    } catch (error) {
      console.log('ArfError from share isAvailable()', error);
      return false;
    }
  }

  forceCpdFlag() {
    return this.store.state.CPD_FORCE;
  }

  cpmFlag() {
    return this.store.state.FULL_CPM;
  }

  isFullCpm() {
    return this.store.state.IS_FULL_CPM;
  }

  filterSharePlacement(sharePlacementsAvailable, highestPriorityRevenue, revenueTypes, avoidSharePlacementsId) {
    // console.log('filterSharePlacementResult', { sharePlacementsAvailable: sharePlacementsAvailable.length, highestPriorityRevenue, revenueTypes, avoidSharePlacementsId });
    return sharePlacementsAvailable.filter((sharePlacement) => {
      if (this.relative.length > 0 && highestPriorityRevenue.value === revenueTypes.relative.value && highestPriorityRevenue.value !== revenueTypes.pr.value) {
        return this.relative.filter((item) => {
          const placementRelative = sharePlacement.placement.getRelative();
          return item.campaignId === placementRelative.campaignId && item.relativeCode === placementRelative.relativeCode && item.check;
        }).length > 0;
      }
      return (
        /* get placement available have highest priority */
        sharePlacement.placementType() === highestPriorityRevenue.value &&
        /* remove placements rich limit or smt else reason. */
        avoidSharePlacementsId.indexOf(sharePlacement.placement.id) === -1);
    });
  }

  filterSharePlacementBaseOnRevenuePriority(sharePlacements, avoidRevenueTypes, avoidSharePlacementsId, positionOnShare, availableTurn, limitTurn, checkedData) {
    try {
      // const sharePlacementsAvailable = this.store.state.PAGE_LOAD.activePageLoad ? this.allSharePlacementsAvailableInPageLoad() : this.allSharePlacementsAvailable()
      // .filter(sharePlacement => sharePlacement.positionOnShare === positionOnShare);

      const sharePlacementInPosition = sharePlacements.filter(sharePlacement => sharePlacement.positionOnShare === positionOnShare);

      const sharePlacementsAvailable = sharePlacementInPosition;

      const revenueTypes = this.config.AdsType.revenueType;
      const cpdForceFlag = this.forceCpdFlag();
      // revenueTypes are checked.
      const checked = checkedData || [];
      let highestPriorityRevenue;
      // get highest priority revenue.
      objectForLoop(revenueTypes, (key) => { // eslint-disable-line
        if (checked.indexOf(key) !== -1 || avoidRevenueTypes.indexOf(key) !== -1) return 'continue';
        const revenueType = revenueTypes[key];
        if (!highestPriorityRevenue) highestPriorityRevenue = revenueType;
        else if (highestPriorityRevenue.priority > revenueType.priority) highestPriorityRevenue = revenueType;
      });

      let listSharePlacementUsable;

      if (!highestPriorityRevenue.isForce && availableTurn > 0) {
        let listRevenueNotForce = [];
        objectForLoop(revenueTypes, (key) => { // eslint-disable-line
          if (checked.indexOf(key) !== -1 || avoidRevenueTypes.indexOf(key) !== -1) return 'continue';
          const revenueType = revenueTypes[key];
          if (!revenueType.isForce && !revenueType.isBackup) listRevenueNotForce.push(revenueType);
        });
        if (this.relative.length === 0) listRevenueNotForce = listRevenueNotForce.filter(i => i.value !== revenueTypes.relative.value);
        // console.log('checklistRevenueNotForce', listRevenueNotForce);
        if (listRevenueNotForce.length > 0) {
          const cpdTurnInPosition = this.cpdTurnInOnePosition(positionOnShare, avoidSharePlacementsId).turnCpd;
          const isFullCpm = this.isFullCpm();

          if (cpdForceFlag && cpdTurnInPosition > 0) {
            for (let index = 0; index < listRevenueNotForce.length; index += 1) {
              const element = listRevenueNotForce[index];
              if (element.value === this.config.AdsType.revenueType.cpd.value) element.weight = 100;
              else element.weight = 0;
            }
          } else {
            let percentageOfCpdAppear = 0;
            const trueLimit = limitTurn <= this.store.state.TURNS_LEFT ? limitTurn : availableTurn;

            if ((trueLimit - (!isFullCpm ? 1 : 0)) <= cpdTurnInPosition && cpdTurnInPosition > 0 && cpdForceFlag !== false) {
              const cpdRevenue = listRevenueNotForce.filter(i => i.value === this.config.AdsType.revenueType.cpd.value);
              if (cpdRevenue.length > 0) listRevenueNotForce = cpdRevenue;
            }

            // if (this.cpmFlag() && listRevenueNotForce.length === 1 && listRevenueNotForce[0].value === this.config.AdsType.revenueType.cpd.value) {
            //   // listRevenueNotForce.push(this.config.AdsType.revenueType.pb);
            //   window.xxx = window.xxx || [];
            //   window.xxx.push({
            //     shareId: this.id,
            //     cpdFlag: cpdForceFlag,
            //     cpdTurnInPosition,
            //     availableTurn,
            //     limitTurn,
            //     fullCpmCounting,
            //     turnsLeft: this.store.state.TURNS_LEFT,
            //   });
            // }

            percentageOfCpdAppear = (cpdForceFlag === false && cpdTurnInPosition < trueLimit) ? 0 : cpdTurnInPosition / trueLimit;
            const ratio = (() => {
              if (trueLimit === this.totalShare) return 2 / 3;
              return 1;
            })();

            const truePercentOfCpd = percentageOfCpdAppear / ratio;
            const weight = 1 - truePercentOfCpd;
            for (let index = 0; index < listRevenueNotForce.length; index += 1) {
              const element = listRevenueNotForce[index];
              element.weight = 0; // init
              if (element.value === this.config.AdsType.revenueType.cpd.value) element.weight += truePercentOfCpd;
              else if (listRevenueNotForce.length - 1 > 0) element.weight = weight / (listRevenueNotForce.length - 1);
              else element.weight = weight;
            }
          }
          console.log('checklistRevenueNotForce', positionOnShare, listRevenueNotForce, isFullCpm);
          highestPriorityRevenue = new Chooser(listRevenueNotForce).baseOnField('weight');
        }
      }
      checked.push(highestPriorityRevenue.value);

      listSharePlacementUsable = this.filterSharePlacement(sharePlacementsAvailable, highestPriorityRevenue, revenueTypes, avoidSharePlacementsId);
      console.log('chooseCondition', !highestPriorityRevenue.isForce && availableTurn > 0, highestPriorityRevenue.isForce, availableTurn > 0, listSharePlacementUsable);
      // console.log('checkRevenuePriority', { positionOnShare, checked, sharePlacements, highestPriorityRevenue, listSharePlacementUsable, avoidSharePlacementsId });

      if (!listSharePlacementUsable.length > 0) {
        if (checked.length === Object.keys(revenueTypes).length) {
          listSharePlacementUsable = sharePlacementsAvailable;
          highestPriorityRevenue = null;
        } else return this.filterSharePlacementBaseOnRevenuePriority(sharePlacements, avoidRevenueTypes, avoidSharePlacementsId, positionOnShare, availableTurn, limitTurn, checked);
      }
      return {
        revenueType: highestPriorityRevenue,
        sharePlacements: listSharePlacementUsable,
      };
    } catch (error) {
      console.log('ArfError from share filterSharePlacementBaseOnRevenuePriority()', error);
      return {
        revenueType: null,
        sharePlacements: [],
      };
    }
  }

  /**
   * Pull out one placement randomly by its "weight"
   * @returns {Placement}
   */
  activeSharePlacement(sharePlacements, avoidRevenueTypes, avoidSharePlacementsId, positionOnShare, area, availableTurn, limitTurn) {
    try {
      const allSharePlacement = this.filterSharePlacementBaseOnRevenuePriority(sharePlacements, avoidRevenueTypes, avoidSharePlacementsId, positionOnShare, availableTurn, limitTurn);
      console.log('filterSharePlacementBaseOnRevenuePriority', allSharePlacement);
      /* if isResponsive = true , zone'll render without care width and height */
      const isResponsive = this.query.getZone('id', this.tree.zoneId)[0].isResponsive;
      const list = allSharePlacement.sharePlacements.filter(item => isResponsive || item.isFitArea(area));
      // console.log('listSharePlacementRunning', list);
      if (allSharePlacement.revenueType === this.config.AdsType.revenueType.cpd.value) {
        return new Chooser(list).baseOnWeight('cpdPercent');
      }
      return new Chooser(list).baseOnWeight('weight');
    } catch (error) {
      console.log('ArfError from share activeSharePlacement()', error);
      return null;
    }
  }

  turnsLeft(currentTrack) {
    return this.isPageLoad ? this.store.state.TURNS_LEFT : currentTrack.turnsLeft;
  }

  getTrack() {
    try {
      return this.store.getters.zoneTrackLoad(this.zoneId, this.totalShare);
    } catch (error) {
      console.log('ArfError from Share getTrack()', error);
      return [];
    }
  }

  limitTurn() {
    const tls = this.turnsLeft(this.getTrack());
    const isFullCpd = this.store.state.ZONE_FULL_CPD.indexOf(this.zoneId) > -1;
    if (this.only) return this.totalShare - this.appeared < tls ? this.totalShare - this.appeared : tls;
    const limitTurn = (this.shareFormat().reduce((res, format) => {
      const position = format.positionOnShare;
      const cpdTurn = this.cpdTurnInOnePosition(position).turnCpd;
      const cpmTurn = this.heighestCpmWeight(position) && !isFullCpd ? 1 : 0;
      const totalTurn = cpdTurn ? cpdTurn + cpmTurn : this.totalShare;
      if (res < totalTurn) res = totalTurn; // eslint-disable-line
      return res;
    }, 0) || this.totalShare) - (this.appeared);
    return limitTurn;
  }

  /**
   * Check for share type then return array of placements
   * @returns [Placement]
   */
  activeSharePlacements(isRotate, placementsLoaded) {
    try {
      const forceSharePlacement = this.forceSharePlacement();
      const shareFormat = this.shareFormat();
      const result = [];
      const history = this.sharesHistory(true);
      // const history = [];
      let sharePlacements;
      const sharePlacementInMonopolyCampaign = this.allSharePlacementsAvailableInMonopolyCampaign();
      if (sharePlacementInMonopolyCampaign.length > 0) {
        sharePlacements = sharePlacementInMonopolyCampaign;
      } else {
        sharePlacements = (isRotate || !this.isPageLoad) ? this.allSharePlacementsAvailable() : this.allSharePlacementsAvailableInPageLoad();
      }
      // if none placements page load.
      if (sharePlacements.length === 0) sharePlacements = this.allSharePlacementsAvailable();
      console.log('sharePlacements', this.id, sharePlacements, this.allSharePlacementsAvailable());

      for (let i = 0; i < shareFormat.length; i += 1) {
        let sharePlacementTemp = sharePlacements.map(x => x);
        const area = shareFormat[i].area;
        const positionOnShare = shareFormat[i].positionOnShare;
        const totalShare = shareFormat[i].totalShare;
        const avoidRevenueTypes = [];
        const avoidSharePlacementsId = [];
        let relativeSync = null;
        let relativeSharePlacement = [];
        let availableTurn = 0;
        let activeSharePlacement;
        /** force banner */
        if (forceSharePlacement && forceSharePlacement.positionOnShare === positionOnShare) {
          activeSharePlacement = forceSharePlacement;
          result.push(activeSharePlacement);
          continue;
        } else {
          if (!isRotate) {
            const prSharePlacement = sharePlacementTemp.filter(sharePlacement =>
              sharePlacement.positionOnShare === positionOnShare &&
              sharePlacement.placement.revenueType === this.config.AdsType.revenueType.pr.value);

            // if exits PR placement
            if (prSharePlacement.length > 0) {
              activeSharePlacement = new Chooser(prSharePlacement).baseOnWeight('weight');
            } else {
              const previousPlacements = getLastElementsInArray(history, totalShare)
              .map(item => item.placements)
              .filter(item => Array.isArray(item) && item.length > 0 && item.filter(a => a.position === positionOnShare))
              .map(item => item[0]);

              const totalCPDPercent = this.cpdTurnInOnePosition(positionOnShare).turnCpd;
              const totalPercentCPDloaded = previousPlacements.length > 0 ? previousPlacements.reduce((res, item) => {
                const placement = this.query.getPlacement('id', item.id)[0];
                if (placement) {
                  return res + placement.cpdPercent;
                }
                return res;
              }, 0) : 0;
              const turnsLeft = this.store.state.TURNS_LEFT;
              availableTurn = (turnsLeft > this.limitTurn() ? this.limitTurn() : turnsLeft) - (totalCPDPercent - totalPercentCPDloaded);
              console.log('LimitTurn', this.id, this.limitTurn(), turnsLeft);
              console.log('TotalShare', totalShare, this.freeTurns);
              console.log('totalCPDPercent in', positionOnShare, totalCPDPercent);
              console.log(`total Percent Load In Position ${i + 1}: ${totalPercentCPDloaded}`);
              console.log('AvailableTurn', availableTurn);
            // previousPlacements.map(item => this.query.getPlacement('id', item.placements[i])[0].cpdPercent);
            arrayHandle.uniqueItem(previousPlacements.map(i => i.id)).map((placementId) => { // eslint-disable-line
              const appear = previousPlacements.filter(item => item.id === placementId).length;
              const sharePlacement = sharePlacementTemp.filter(item => item.placement.id === placementId)[0];
              if (sharePlacement) {
                const cpdPercent = sharePlacement.placement.cpdPercent;
                if (sharePlacement.placement.revenueType === this.config.AdsType.revenueType.cpd.value && cpdPercent > 0 && appear >= cpdPercent) {
                  avoidSharePlacementsId.push(placementId);
                }
              }
            });

              if (this.relative.length > 0) {
                relativeSharePlacement = sharePlacements.filter(sharePlacement =>
                sharePlacement.positionOnShare === positionOnShare &&
                this.relative.filter(item =>
                  (item.campaignId === sharePlacement.placement.campaign.id) &&
                  (item.relativeCode === sharePlacement.placement.relative) &&
                  item.check).length > 0);
                if (relativeSharePlacement.length === 0) {
                  sharePlacementTemp = sharePlacementTemp.filter(sharePlacement => this.relative.filter((item) => {
                    const check = item.check;
                    if (check) return (sharePlacement.placement.relative === 0) || ((item.campaignId === sharePlacement.placement.campaign.id) && (item.relativeCode === sharePlacement.placement.relative));
                    return (item.campaignId !== sharePlacement.placement.campaign.id) || (item.relativeCode !== sharePlacement.placement.relative);
                  }).length > 0);
                }
                console.log('filterShareplacementRelative', this.zoneId, sharePlacementTemp, relativeSharePlacement, this.relative);
              }

              console.log('previousPlacements', previousPlacements, avoidSharePlacementsId);
            }
          } else if (isRotate && placementsLoaded.length > 0) {
            const cycleLength = sharePlacementTemp.filter(item => item.positionOnShare === positionOnShare).length;
            let placementsLoadedInPosition = placementsLoaded.filter(item => item.positionOnShare === positionOnShare);
            if (placementsLoadedInPosition.length >= cycleLength) {
              const lastItem = placementsLoadedInPosition[placementsLoadedInPosition.length - 1];
              placementsLoadedInPosition = [lastItem]; // eslint-disable-line
              placementsLoaded = placementsLoaded.filter(item => item.positionOnShare !== positionOnShare).push(lastItem); // eslint-disable-line
            }
            const cycle = getLastElementsInArray(placementsLoadedInPosition, cycleLength).map(item => item.id);
            sharePlacementTemp = sharePlacementTemp.filter(item => cycle.indexOf(item.id) === -1);
          }

          console.log('placementLoaded', placementsLoaded, sharePlacementTemp);
        }

        if (relativeSharePlacement.length > 0) {
          activeSharePlacement = this.activeSharePlacement(relativeSharePlacement, avoidRevenueTypes, [], positionOnShare, area, availableTurn, this.limitTurn());
          let currentRelative = null;
          if (!relativeSync) relativeSync = this.relative.filter(r => (r.campaignId === activeSharePlacement.placement.campaignId && r.check === true))[0];
          currentRelative = this.relative.filter(r => (r.campaignId === activeSharePlacement.placement.campaignId && (r.campaignId !== relativeSync.campaignId || r.relativeCode !== relativeSync.relativeCode)) && (r.check === true))[0];
          if (activeSharePlacement.placement.revenueType === this.config.AdsType.revenueType.cpd.value && avoidSharePlacementsId.indexOf(activeSharePlacement.placement.id) > -1) {
            // sync turn left
            const syncId = this.zoneId;
            const withId = relativeSync.zoneId;
            this.store.commit('SYNC_TURN', { syncId, withId });
            if (currentRelative) this.store.commit('SYNC_TURN', { syncId: currentRelative.zoneId, withId: relativeSync.zoneId });
          }
        } else {
          activeSharePlacement = this.activeSharePlacement(sharePlacementTemp, avoidRevenueTypes, avoidSharePlacementsId, positionOnShare, area, availableTurn, this.limitTurn());
          if (!activeSharePlacement) activeSharePlacement = this.activeSharePlacement(sharePlacementTemp, avoidRevenueTypes, [], positionOnShare, area, availableTurn, this.limitTurn());
        }

        if (activeSharePlacement) {
          activeSharePlacement.placement.sendRelativeCodeToState();
          result.push(activeSharePlacement);
        }
        // this job will do in placement component
        // if (!this.reSelectRelativeFromZone && activePlacement && activePlacement.placement.relative) {
        //   this.store.commit('SET_RELATIVE_PLACEMENT', activePlacement.placement.getRelative());
        // }
      }

      return result;
    } catch (error) {
      console.log('ArfError from share activeSharePlacements()', error);
      return [];
    }
  }
}

export default Share;
