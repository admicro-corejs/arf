import Entity from './Entity';
import Share from './Share';
import Banner from './Banner';
import Chooser from '../vendor/util/chooser';
import Logging from '../vendor/Logging';
import getLastElementsInArray from '../vendor/util/getLastElementInArray';
import { domain } from '../config';
import installScript from '../vendor/util/handleHTML/installScript';
import Query from '../vendor/util/query';
// import round from '../vendor/util/round';

class Zone extends Entity {
  constructor(zone) { // eslint-disable-line
    try {
      super(zone);
      this.id = `${zone.id}`;
      this.shares = zone.shares || [];
      this.globalFilters = zone.site.globalFilters;
      this.currentChannelLink = this.store.state.ENV.url;
      this.trackLoad = this.store.state.TRACK_LOAD;
      this.relative = this.store.state.RELATIVE;
      this.env = this.store.state.ENV;
      this.tree = { zoneId: this.id };
      this.isTemplate = zone.isTemplate;
      this.template = zone.template;
      this.site = zone.site;
      this.isPageLoad = zone.isPageLoad;
      this.isMobile = zone.isMobile;
      this.groupSSP = (zone.groupSSP && typeof zone.groupSSP === 'string') ? JSON.parse(zone.groupSSP) : zone.groupSSP; // da dc json parse o App.vue
      this.query = new Query(null, null, this);
      this.rawData = zone;
      this.passTree();
    } catch (err) {
      console.log('ArfError from zone constructor', err);
    }
  }

  /**
   * Check banner available of zone
   */
  commitBanner() {
    if (this.store.state.ZONE_BANNERS[this.id]) return;
    this.store.commit('BANNER_CHECK', { zoneId: this.id, totalShare: this.totalShare, banners: this.banners() });
  }

  /**
   * @returns list banners available
   */
  banners() {
    const allbannersid = this.shares.reduce((shareBanners, share) => {
      let currentShareBanners = shareBanners || [];
      if (!share.sharePlacements) return currentShareBanners;
      const sharePlacementBanners = share.sharePlacements.reduce((res, sharePlacement) => {
        try {
          const set = sharePlacement.placement.banners
          .map((i) => {
            const banner = i;
            banner.tree = { siteId: this.site.id, zoneId: this.id };
            return new Banner(i);
          })
          .filter(i => i.isAvailable())
          .map(i => i.id);
          let current = res || [];
          current = current.concat(set);
          return current;
        } catch (error) {
          console.log('Arf error from read banners in zonejs', error);
          return [];
        }
      }, 0);
      currentShareBanners = currentShareBanners.concat(sharePlacementBanners);
      return currentShareBanners;
    }, 0);
    return allbannersid;
  }

  /**
   * pass detail to children
   */
  passTree() {
    for (let i = 0; i < this.shares.length; i += 1) {
      this.shares[i].isPageLoad = this.isPageLoad;
      this.shares[i].siteId = this.site.id;
      this.shares[i].query = this.query;
    }
  }

  /**
   * @return {number} totalShare
   */
  get totalShare() {
    return this.isPageLoad ? this.store.state.TOTAL_SHARE : this.rawData.totalShare;
  }

  turnsLeft(currentTrack) {
    return this.isPageLoad ? this.store.state.TURNS_LEFT : currentTrack.turnsLeft;
  }

  /**
   * pass global filter to banner for checking
   */
  passGlobalFiltersToBanner() {
    try {
      this.shares.map(share => share.sharePlacements.filter(sharePlacement => sharePlacement.placement !== null)
        .map(sharePlacement => sharePlacement.placement.banners
          .map(banner => banner.globalFilters = this.globalFilters))) // eslint-disable-line
    } catch (err) {
      throw new Error(err);
    }
  }

  /**
   * Get all shares from this zone
   * @returns [Share]
   */
  allShares() {
    try {
      return this.shares.map(share => ((share instanceof Share) ? share : new Share(share, this.store)));
    } catch (error) {
      return [];
    }
  }

  allHistory() {
    try {
      return this.store.getters.zoneTrackLoad(this.zoneId, this.totalShare).turns;
    } catch (error) {
      console.log('ArfError from zone allHistory()', error);
      return [];
    }
  }

  getTrack() {
    try {
      return this.store.getters.zoneTrackLoad(this.id, this.totalShare);
    } catch (error) {
      console.log('ArfError from zone getTrack()', error);
      return [];
    }
  }

  forceCpdFlag() {
    return this.store.state.CPD_FORCE;
  }

  isFullCpm() {
    return this.store.state.IS_FULL_CPM;
  }

  /**
   * Filter share
   * @param {*} currentTrack 
   * @param {*} relativeDetails 
   * @param {*} isExtra 
   * @returns { values: shareAvailable, turnAvailable: number };
   */
  filterShare(currentTrack, relativeDetails, isExtra) {
    try {
      const context = this;
      const turnAvailable = this.totalShare;
      // let turnRest = turnAvailable;
      let shareAvailable = this.allShares().filter(item => item.isAvailable());
      if (isExtra) {
        return { values: shareAvailable, turnAvailable: null };
      }
      // if exits share monopoly return result.
      const monopolyShare = shareAvailable.filter(s => s.isMonopoly());
      if (monopolyShare.length > 0) shareAvailable = monopolyShare;

      const history = getLastElementsInArray(currentTrack.turns, turnAvailable);
      const turnsLeft = this.turnsLeft(currentTrack);
      console.log('shareAvailable', { shareAvailable, turnsLeft });
      let totalStaticTurnsLeft = 0;
      let freeTurns = 0;
      let result = [];
      let relativeShares = [];
      const pageLoadShares = shareAvailable.filter(share => share.allSharePlacementsInPageLoad().length > 0);
      console.log('pageLoadShare', this.id, pageLoadShares);
      if (pageLoadShares.length > 0) shareAvailable = pageLoadShares;


      // sort array for prioritize share cpd
      // shareAvailable = shareAvailable.sort((a, b) => b.cpdTurnInOnePosition().turnCpd - a.cpdTurnInOnePosition().turnCpd);
      for (let i = 0, length = shareAvailable.length; i < length; i += 1) {
        const share = shareAvailable[i];
        const shareCpdTurn = share.cpdTurnInOnePosition().turnCpd;
        const shareAppearWithCpd = history.filter(item => item.shareId === share.id)
        .reduce((res, turn) => {
          const isCpd = turn.placements.reduce((res2, placementDetail) => {
            const placementId = placementDetail.id;
            const placement = context.query.getPlacement('id', placementId)[0];
            return res2 || (placement ? placement.revenueType === this.config.AdsType.revenueType.cpd.value && placement.cpdPercent > 0 : false);
          }, 0);
          if (isCpd) return res + 1;
          return res;
        }, 0);
        const shareAppear = history.filter(item => item.shareId === share.id).length;
        share.shareAppearWithCpd = shareAppearWithCpd;
        share.appeared = shareAppear;
        const checkPR = share.allSharePlacements().filter(x => x.isAvailable() && !x.placement.isRunThisCampaignType(this.config.AdsType.revenueType.default.value))
          .reduce((res, item2) => res || item2.placement.isPlacementType(this.config.AdsType.revenueType.pr.value), 0);
        if (checkPR) {
          result = [share];
          break; /* if share contain PR placement then stop push share to result */
        }

        if (shareCpdTurn) {
          const cpdLeft = shareCpdTurn - shareAppearWithCpd;
          totalStaticTurnsLeft += (cpdLeft > 0 ? cpdLeft : 0);
        }

        if (shareCpdTurn && shareAppearWithCpd < shareCpdTurn) {
          result.push(share);
        }

        // if free turn > 0 => add share have none cpd like cpm.
        freeTurns = turnsLeft < totalStaticTurnsLeft ? 0 : (turnsLeft - totalStaticTurnsLeft);
        if (i === length - 1 && freeTurns > 0) { // in the last of loop, check if have free turn => add share
          shareAvailable.forEach((item) => { // eslint-disable-line
            // if (item.cpdTurnInOnePosition().turnCpd === 0 && result.filter(x => x.id === item.id).length === 0) {
            //   result.push(item);
            // }
            // const format = item.shareFormat();
            const allSharePlacements = !this.isPageLoad ? item.allSharePlacementsAvailable() : item.allSharePlacementsAvailableInPageLoad();
            const check = allSharePlacements.length > 0;
            // for (let index = 0; index < format.length; index += 1) {
            //   const position = format[index].positionOnShare;
            //   const sharePlacementSuit = allSharePlacements.filter(sharePlacement => sharePlacement.positionOnShare === position && sharePlacement.placementType() !== this.config.AdsType.revenueType.cpd.value);
            //   if (sharePlacementSuit.length === 0) check = false;
            // }
            if (check && result.filter(x => x.id === item.id).length === 0) {
              result.push(item);
            }
          });
          // shareAvailable.filter(item => (item.cpdTurnInOnePosition().turnCpd === 0 ? result.push(item) : '')); // eslint-disable-line
        }
        // if freeturns === 0 , ít mean need to re-charge.
        // if (freeTurns === 0) {
        //   result.push(share);
        // }
        console.log('ShareFilter', {
          share, shareCpdTurn, freeTurns, shareAppearWithCpd, checkPR, turnsLeft, totalStaticTurnsLeft, sharePlacements: share.allSharePlacements().filter(x => x.isAvailable() && !x.placement.isRunThisCampaignType(this.config.AdsType.revenueType.default.value)),
        });
      }

      // if after choose share and result is empty -> choose random share available
      if (result.length === 0 && freeTurns > 0) {
        // const share = new Chooser(shareAvailable).random();
        // result.push(share);
        result = shareAvailable;
      }

      console.log('filterShare', result, freeTurns);
      if (relativeDetails.length > 0) {
        for (let i = 0; i < relativeDetails.length; i += 1) {
          const key = relativeDetails[i];
          relativeShares = result.filter(share => share.allSharePlacements().reduce((res, sharePlacement) => res ||
            (sharePlacement.placement.campaignId === key.campaignId && sharePlacement.placement.relative === key.relativeCode && key.check), 0));
          if (relativeShares.length === 0) {
            console.log('rechooseShareRelativeIfRelativeRunWithCpd');
            relativeShares = shareAvailable.filter(share => share.allSharePlacements().reduce((res, sharePlacement) => res ||
            (sharePlacement.placement.revenueType === this.config.AdsType.revenueType.cpd.value && sharePlacement.placement.campaignId === key.campaignId && sharePlacement.placement.relative === key.relativeCode && key.check), 0));
          }
          console.log('runRelative', this.id, key, relativeShares);
          if (relativeShares.length > 0) {
            result = relativeShares;
            break;
          }
        }
      }

      result.forEach((share) => { // eslint-disable-line no-param-reassign
        share.freeTurns = freeTurns; // eslint-disable-line no-param-reassign
      });

      return { values: result, turnAvailable: null };
    } catch (error) {
      console.log('ArfError from zone fillterShare()', error);
      return { values: [], turnAvailable: null };
    }
  }

  /**
   * force display share
   * @param {*} avoidShares 
   * @returns 
   */
  forceShare(avoidShares) {
    return this.allShares().filter(share => share.forceSharePlacement() && (Array.isArray(avoidShares) ? avoidShares.indexOf(share.id) === -1 : true))[0];
  }

  /**
   * Get a active share randomly by its weight
   * @return {Share}
   */
  activeShare(avoidShares, isExtra, shareId) {
    try {
      /** force banner */
      const forceShare = this.forceShare(avoidShares);
      if (forceShare) return forceShare;

      const currentTrack = this.getTrack();

      const sharesAvailable = this.allShares()
      .filter(share => (Array.isArray(avoidShares) ? avoidShares.indexOf(share.id) === -1 : true));
      if (sharesAvailable.length === 0) return null;

      if (shareId) {
        const shareForExtra = sharesAvailable.filter(s => s.id === shareId)[0];
        if (shareForExtra) return shareForExtra;
      }

      console.log('currentTrack', currentTrack);
      const filterShare = this.filterShare(currentTrack, this.relative, isExtra);
      filterShare.values = filterShare.values.filter(share => (Array.isArray(avoidShares) ? avoidShares.indexOf(share.id) === -1 : true));
      /*  */
      const totalCpdRunning = filterShare.values.reduce((res, share) => res + share.totalCpd(), 0);
      this.store.commit('TOTAL_CPD_RUNNING', {
        zoneId: this.id,
        cpd: totalCpdRunning,
      });
      console.log('TotalCpd', this.id, totalCpdRunning);
      /* */
      let activeShare = sharesAvailable[0];
      if (filterShare.values.length === 1) {
        activeShare = filterShare.values[0];
        activeShare.only = true;
      } else if (currentTrack.nextTurn.shareId === '') {
        let totalWeight = 1; // eslint-disable-line
        let totalCpmWeight = 0;
        let skipCpmWeight = 0;
        // calculator weight of share if share have none monopoly placemnet(sum of CPM placement weight, pb = 0).
        filterShare.values.map((share) => { // eslint-disable-line array-callback-return
          const shareCpdWeight = share.cpdTurnInOnePosition().turnCpd / this.totalShare;
          const shareCpmWeight = share.heighestCpmWeight();
          totalCpmWeight += shareCpmWeight;
          console.log('checkCPMWeight', share.id, shareCpmWeight, totalCpmWeight);
          if (shareCpdWeight > 0) {
            totalWeight -= shareCpdWeight;
          }
        });
        totalWeight = parseFloat(totalWeight.toFixed(1));

        const cpdForce = this.forceCpdFlag();
        const isFullCpm = this.isFullCpm();
        const percentageAppearCpdForceAvailable = parseFloat((2 / 3).toPrecision(10));
        const percentageAppearCpdForceUnavailable = parseFloat((1 / 3).toPrecision(10));
        for (let index = 0; index < filterShare.values.length; index += 1) {
          const share = filterShare.values[index];
          let shareCpdWeight = share.cpdTurnInOnePosition().turnCpd / this.totalShare;
          const shareCpmWeight = share.heighestCpmWeight();
          // if (cpdForce === false && shareCpdWeight > 0) {
          //   shareCpmWeight += shareCpdWeight;
          // }
          const shareAppearWithCpd = share.shareAppearWithCpd;
          console.log('checkShareCpdWeight', this.id, share.id, { shareCpdWeight, shareCpmWeight, shareAppearWithCpd }, this.totalShare);
          const cpdForceEffect = share.cpdTurnInOnePosition().turnCpd > shareAppearWithCpd;
          if (!cpdForceEffect) {
            shareCpdWeight = 0; // if cpd in share not available => cpd weight = 0.
          }
          if (totalWeight === 0 && cpdForceEffect) {
            share.weight = shareCpdWeight;
            this.store.commit('ZONE_FULL_CPD', this.id);
            continue;
          }
          // share.weight = shareCpdWeight;// eslint-disable-line no-param-reassign
          /* eslint-disable no-param-reassign */
          console.log('cpdFlag', cpdForce, cpdForceEffect, totalCpmWeight);
          if (cpdForce === true || cpdForce === null || totalCpmWeight === 0) {
            share.weight = 0;
            if (cpdForceEffect && cpdForce === true) {
              share.weight = shareCpdWeight;
              skipCpmWeight += shareCpmWeight;
              // totalCpmWeight -= shareCpmWeight;
            } else if (cpdForce === true && shareCpdWeight > 0) {
              share.weight = 0;
            } else if (cpdForce === true && shareCpdWeight === 0) {
              share.weight = (shareCpmWeight * totalWeight) / totalCpmWeight;
            }

            if (shareCpdWeight > 0 && (cpdForce === null || totalCpmWeight === 0)) {
              share.weight = shareCpdWeight;
              skipCpmWeight += shareCpmWeight;
              // totalCpmWeight -= shareCpmWeight;
            } else if (cpdForce === null) {
              share.weight = totalCpmWeight === 0 ? 0 : (shareCpmWeight * totalWeight) / (totalCpmWeight - skipCpmWeight);
            }

            share.weight = parseFloat(share.weight.toPrecision(10));
            // const trueCpdWeight = share.weight * (percentageAppearCpdForceAvailable + (totalCpmWeight === 0 ? percentageAppearCpdForceUnavailable : 0));
            // const trueCpmWeight = totalCpmWeight ? (percentageAppearCpdForceUnavailable * shareCpmWeight) / totalCpmWeight : 0;
            // const deltaWeight = share.weight - (trueCpdWeight + trueCpmWeight);
            // console.log('checkComputeWeight', { deltaWeight, id: share.id, weight: share.weight, percentageAppearCpdForceAvailable, percentageAppearCpdForceUnavailable, shareCpmWeight, totalCpmWeight, skipCpmWeight });
            console.log('weightBefore', share.weight, percentageAppearCpdForceUnavailable, shareCpmWeight, percentageAppearCpdForceAvailable);
            share.weight = parseFloat(((share.weight - (shareCpdWeight <= 0 ? percentageAppearCpdForceUnavailable * (shareCpmWeight !== 0 && totalCpmWeight !== 0 ? shareCpmWeight / totalCpmWeight : 0) : 0)) / percentageAppearCpdForceAvailable).toPrecision(10));
            if (!isFullCpm
              && totalCpmWeight > 0
              && totalWeight > 0
              && share.cpdTurnInOnePosition().turnCpd > 0
              && ((this.turnsLeft(this.getTrack()) - 1) <= (share.cpdTurnInOnePosition().turnCpd - shareAppearWithCpd))) {
              share.weight = 9999;
            }
            console.log('weightAffter', share.weight, isFullCpm, share.cpdTurnInOnePosition().turnCpd, shareAppearWithCpd, this.turnsLeft(this.getTrack()), ((this.turnsLeft(this.getTrack()) - 1) <= (share.cpdTurnInOnePosition().turnCpd - shareAppearWithCpd)));
            if (share.weight < 0) share.weight = 0;
          } else if (cpdForce === false) {
            share.weight = shareCpmWeight !== 0 && totalCpmWeight !== 0 ? shareCpmWeight / totalCpmWeight : 0;
          }
          if (share.limitTurn() === 0) share.weight = 0;
          console.log('bindShareWeightForCPMShare', { zoneId: this.id, id: share.id, weight: share.weight, totalWeight, shareCpmWeight, totalCpmWeight, skipCpmWeight, shareCpdWeight, cpdForceEffect });
        }
        /* eslint-enable no-param-reassign */
        console.log('xxx', filterShare.values.map(share => share.weight));
        activeShare = new Chooser(filterShare.values).baseOnWeight('weight');

        // activeShare = new Chooser(filterShare.values).random();
      } else {
        activeShare = filterShare.values.filter(item => item.id === currentTrack.nextTurn.shareId)[0];
      }
      /* avoid active share = null */
      if (!activeShare && !avoidShares) {
        activeShare = new Chooser(this.allShares()).random();
      }
      console.log('activeShare', activeShare);
      /* add more detail to activeShare */
      if (activeShare) {
        activeShare.currentTrack = currentTrack;
        activeShare.totalShare = this.totalShare;
        activeShare.env = this.env;
        activeShare.isPageLoad = this.isPageLoad;
      }

      // this.clearHistoryNotUse(currentTrack);
      return activeShare;
    } catch (error) {
      console.log('ArfError from zone activeShare()', error);
      return null;
    }
  }

    /**
 * init lib nessesary.
 * @param {function} done : callback when library loaded.
 */
  async initLibrary(done) { // eslint-disable-line
    const isMobile = this.isMobile;
    const admcorearfUrl = `${domain.domainCore.url}${domain.domainCore.admcorearf}`;
    const mbCoreUrl = `${domain.domainCore.url}${domain.domainCore.mb_core}`;
    // const isExecuted = Array.from(document.getElementsByTagName('script')).filter(item => item.src === admcorearfUrl || item.src === mbCoreUrl).length > 0;
    // console.log('isMobile', isMobile);
    if (isMobile) {
      if (!window.zoneM && this.store.state.REMOTE_SCRIPT_REGISTER.indexOf(mbCoreUrl) === -1) {
        this.store.commit('REMOTE_SCRIPT_REGISTER', { url: mbCoreUrl, remove: false });
        await installScript(null, mbCoreUrl, true, null, this.store);
      } else {
        await new Promise((resolve) => {
          let timesCheck = 0;
          const timing = setInterval(() => {
            if (window.zoneM || timesCheck >= 100) {
              clearInterval(timing);
              resolve();
            }
            timesCheck += 1;
          }, 100);
        });
        this.store.commit('REMOTE_SCRIPT_REGISTER', { url: mbCoreUrl, remove: true });
      }
    } else if (!window.ADS_CHECKER) {
      if (this.store.state.REMOTE_SCRIPT_REGISTER.indexOf(admcorearfUrl) === -1) {
        this.store.commit('REMOTE_SCRIPT_REGISTER', { url: admcorearfUrl, remove: false });
        await installScript(null, admcorearfUrl, true, null, this.store);
      }
      await new Promise((resolve) => {
        let timesCheck = 0;
        if (window.ADS_CHECKER) {
          resolve();
        } else {
          console.warn('running into interval', this.id);
          const timing = setInterval(() => {
            const value = window.ADS_CHECKER;
            if (value) {
              clearInterval(timing);
              resolve(value);
            }

            if (timesCheck >= 100) {
              clearInterval(timing);
              resolve();
            }
            timesCheck += 1;
          }, 100);
        }
      });
      this.store.commit('REMOTE_SCRIPT_REGISTER', { url: admcorearfUrl, remove: true });
    }
    // console.warn(`checkLoadLibrary ${this.id}`, window._admloadJs);
    if (done) done();
  }

  /**
   * create log url
   * @returns logging url
   */
  loggingUrl() {
    const logData = {};
    logData[this.config.loggingConfig.zone.param.dmn] = encodeURIComponent(this.currentChannelLink);
    logData[this.config.loggingConfig.zone.param.zid] = this.id;
    logData[this.config.loggingConfig.zone.param.pgid] = this.store.state.LOAD_ID;
    logData[this.config.loggingConfig.zone.param.uid] = this.store.state.UID;
    return new Logging(this.config.loggingConfig).zoneLoggingUrl(logData);
  }

  /**
   * Send log
   */
  logging() {
    if (this.store.state.IS_PREVIEW) return;
    new Logging(this.config.loggingConfig).logging(this.loggingUrl());
  }
}

export default Zone;
