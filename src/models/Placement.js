import Entity from './Entity';
import Banner from './Banner';
import checkTime from '../vendor/util/dateTime';
import Chooser from '../vendor/util/chooser';
// import Query from '../vendor/util/query';
import * as urlUtil from '../vendor/util/url';
import getLastElementsInArray from '../vendor/util/getLastElementInArray';

class Placement extends Entity {
  constructor(placement) {
    super(placement);
    this.id = `${placement.id}`;
    this.banners = placement.banners;
    this.startTime = placement.startTime;
    this.endTime = placement.endTime;
    this.relative = placement.relative;
    this.cpdPercent = placement.cpdPercent;
    this.isRotate = placement.isRotate;
    this.campaign = placement.campaign;
    this.shareId = placement.shareId;
    this.positionOnShare = placement.positionOnShare; // it will be pass by SharePlacement.
    this.campaignId = placement.campaignId;
    this.zoneId = placement.zoneId;
    this.shareId = placement.shareId;
    this.rotateTime = placement.rotateTime;
    this.bannerInvolves = placement.bannerInvolves;
    this.tree = this.tree || {};
    // this.query = this.tree.zoneId ? new Query(this.store, null, this.tree.zoneId) : new Query(this.store);
    this.passTree();
  }

  weightValue(weightName) {
    if (this.banners.length === 1) return this.banners[0][weightName];
    return super.weightValue(weightName);
  }

  passTree() {
    this.tree.placementId = this.id;
    for (let i = 0; i < this.banners.length; i += 1) {
      this.banners[i].tree = { zoneId: this.tree.zoneId, shareId: this.tree.shareId, sharePlacementId: this.tree.sharePlacementId, placementId: this.tree.placementId, siteId: this.tree.siteId };
      this.banners[i].query = this.query;
      this.banners[i].positionOnShare = this.positionOnShare;
    }
  }

  /**
   * Get all banners from this placement
   * @returns [Banner]
   */
  allBanners() {
    try {
      return this.banners.map(banner => new Banner(banner, this.store));
    } catch (err) {
      return [];
    }
  }

  getRelativeInSamePosition(shareData, isCheckPosition) {
    try {
      const relatives = [];
      const share = shareData || this.query.getShare('id', this.tree.shareId)[0];
      const sharePlacements = share.sharePlacements;
      const placements = sharePlacements.map(sp => (sp.placement instanceof Placement ? sp.placement : new Placement(sp.placement)));
      const placementsAvailable = placements.filter(p => (p.positionOnShare === this.positionOnShare || !isCheckPosition) && p.isAvailable() && p.relative && p.relative !== 0);
      for (let index = 0; index < placementsAvailable.length; index += 1) {
        const placement = placementsAvailable[index];
        const relative = placement.getRelative();
        relatives.push(relative);
      }
      return relatives;
    } catch (error) {
      console.log('ArfError From getRelativeInSamePosition in Placement.js', error);
      return [];
    }
  }

  getRelativeInSamePositionInZone() {
    try {
      const zone = this.query.getZone('id', this.tree.zoneId)[0];
      const shares = zone.shares;
      return shares.reduce((res, share, index) => {
        if (index === 0) {
          if (share.id === this.tree.shareId) return this.getRelativeInSamePosition(share, true);
          return this.getRelativeInSamePosition(share, false);
        }
        if (share.id === this.tree.shareId) return res.concat(this.getRelativeInSamePosition(share, true));
        return res.concat(this.getRelativeInSamePosition(share, false));
      }, 0);
    } catch (error) {
      console.log('ArfError From getRelativeInSamePositionInZone in Placement.js', error);
      return [];
    }
  }

  /**
   * filter banner inside Placement
   * @param lastBanners
   * @returns {Placement.allBanners}
   */
  filterBanner(lastBanners) {
    try {
      // if (this.revenueType === this.config.AdsType.revenueType.passBack.value) {
      //   return this.allBanners();
      // }
      let allBanners;
      if (this.allBanners().length > 1 && Array.isArray(lastBanners)) {
        allBanners = this.allBanners().filter(item => lastBanners.indexOf(item.id) === -1);
      } else {
        allBanners = this.allBanners();
      }
      const result = allBanners.filter(x => x.isAvailable(this.placementType()));
      return result;
    } catch (err) {
      console.log('ArfError from placement.filterBanner', err);
      return [];
    }
  }

  /**
   * check placement campaign type
   * @param type
   * @returns {boolean}
   */
  isRunThisCampaignType(type) {
    return this.campaign.revenueType === type;
  }

  getRelative() {
    return {
      zoneId: this.tree.zoneId,
      campaignId: this.campaign.id,
      relativeCode: this.relative,
      placementId: this.id,
    };
  }

  sendRelativeCodeToState() {
    // if (!this.relative) return;
    const relatives = this.getRelativeInSamePositionInZone();
    const relative = this.getRelative();
    if (relative && relative.relativeCode) {
      relative.check = true;
      this.store.commit('SET_RELATIVE_PLACEMENT', relative);

      const relativesFalse = relatives.filter(item =>
       (item.campaignId !== relative.campaignId
      && item.relativeCode !== relative.relativeCode));

      relativesFalse.map((item) => { // eslint-disable-line
        item.check = false; // eslint-disable-line
        this.store.commit('SET_RELATIVE_PLACEMENT', item);
      });
    } else {
      relatives.map((item) => { // eslint-disable-line
        item.check = false; // eslint-disable-line
        this.store.commit('SET_RELATIVE_PLACEMENT', item);
      });
    }
  }

  /**
   * check revenue type of placement
   * @param type
   * @returns {boolean}
   */
  isPlacementType(type) {
    return this.revenueType === type;
  }

  forceBanner(bannerList) {
    return bannerList.filter(banner => banner.isForce());
  }

  isForce() {
    return this.forceBanner(this.allBanners()).length > 0;
  }

  /**
   * Get active banner by its weight
   * @returns {Banner}
   */
  activeBanner(context, bannersLoaded) {
    try {
      context = context || []; // eslint-disable-line
      let bannerAvailable;

      const forceBanners = this.forceBanner(this.allBanners());
      if (forceBanners.length > 0) {
        bannerAvailable = forceBanners; // force banner.
      } else {
        bannerAvailable = this.filterBanner();
      }
      // console.log('checkBannerList', this.id, bannerAvailable);

      if (context.indexOf('rotate') > -1 && Array.isArray(bannersLoaded) && bannersLoaded.length > 0) {
        const cycleLength = bannerAvailable.length;
        if (bannersLoaded.length >= cycleLength) {
          const lastItem = bannersLoaded[bannersLoaded.length - 1];
          if (lastItem) bannersLoaded = [lastItem]; // eslint-disable-line
        }
        const cycle = getLastElementsInArray(bannersLoaded, cycleLength).map(item => item.id);
        bannerAvailable = bannerAvailable.filter(item => cycle.indexOf(item.id) === -1);
        /* filter banner involve with order-priority */
      } else if (context.indexOf('rotate') === -1 && Array.isArray(bannersLoaded) && bannersLoaded.length > 0) {
        const revenueTypeOrder = [
          this.config.AdsType.revenueType.cpm.value,
          this.config.AdsType.revenueType.pb.value,
          this.config.AdsType.revenueType.default.value,
          this.config.AdsType.revenueType.cpd.value,
        ];
        let bannerInvolves = [];


        for (let i = 0; i < revenueTypeOrder.length; i += 1) {
          const revenueType = revenueTypeOrder[i];
          const bannerWithRevenueTypes = this.bannerInvolves.filter(banner => banner.revenueType === revenueType
            && bannersLoaded.map(loadedItem => loadedItem.id).indexOf(banner.id) === -1);

          if (bannerWithRevenueTypes.length > 0) {
            bannerInvolves = bannerWithRevenueTypes;
            break;
          }
        }
        /* end filter banner involve */
        bannerAvailable = bannerAvailable.concat(bannerInvolves.map(item => (item instanceof Banner ? item : new Banner(item))))
        .filter(item => bannersLoaded.map(loadedItem => loadedItem.id).indexOf(item.id) === -1);
        console.log('ReDrawPlacementChecking', { id: this.id, bannerInvolves, bannersLoaded, bannerAvailable });
      }
      if (bannerAvailable.length > 0) {
        return new Chooser(bannerAvailable).baseOnWeight('weight');
      }

      // default banner here
      return false;
    } catch (err) {
      console.log('Error from ActiveBanner in Placement.js', err);
      return false;
    }
  }

  isInPageLoad() {
    return this.campaignId === this.store.PAGE_LOAD.activePageLoad;
  }

  placementType() {
    if (this.campaign.revenueType === this.config.AdsType.revenueType.default.value) {
      return this.config.AdsType.revenueType.default.value;
    }
    return this.revenueType;
  }

  isFitArea(area) { // eslint-disable-line
    return true;
    // try {
    //   if (!this.tree.zoneId) return true;
    //   const zone = this.query.getZone(this.tree.zoneId)[0];
    //   if (zone && (zone.isMobile || zone.isResponsive)) {
    //     return true;
    //   }
    // /* */
    // if (!area) area = { width: this.width, height: this.height }; // eslint-disable-line
    //   const roundingError = this.config.roundingError;
    //   if (!isNaN(area)) {
    //     const share = this.query.getShare('id', this.shareId)[0];
    //     if (!share) return true;
    //     const totalPart = (share.type === this.config.AdsType.shareType.single ? [1] : share.format.split(','))
    //     .reduce((total, item) => total + parseInt(item, 10), 0);
    //     const placementArea = ((this.width * this.height) * totalPart) / (share.width * share.height);
    //     const deviation = (Math.abs(placementArea - area) / area) <= roundingError; // muc do chenh lech dien tich +-20%
    //     console.log('checkPlacementArea', area, placementArea, deviation);
    //     return deviation;
    //   }
    //   const deviation = (((area.width * area.height) - (this.width * this.height)) / (area.width * area.height)) <= roundingError;
    //   console.log('checkPlacementArea', area, deviation);
    //   return deviation;
    // } catch (error) {
    //   console.log('ArfError from Placement.isFitArea', error);
    //   return true;
    // }
  }

  turnCpd() {
    // if (this.cpdPercent > 0) {
    //   return round((totalShare * this.cpdPercent) / 100);
    // }
    // return 0;
    if (this.placementType() === this.config.AdsType.revenueType.default.value) return 0;
    return this.cpdPercent;
  }

  isAvailable() {
    const startTimeCampaign = this.campaign.startTime;
    const endTimeCampaign = this.campaign.endTime;
    const countBanner = this.filterBanner().length;
    const isFitArea = this.isFitArea();
    const ignoretime = urlUtil.getParameterByName('ignoretime') || true;
    // console.log('checkPlacementAvailable', { id: this.id,
    //   result: this.campaign.revenueType === this.config.AdsType.revenueType.default.value ||
    //   (checkTime(startTimeCampaign, endTimeCampaign) && checkTime(this.startTime, this.endTime) && countBanner > 0 && isFitArea),
    //   countBanner,
    //   timeCamp: checkTime(startTimeCampaign, endTimeCampaign),
    //   timePlacement: checkTime(this.startTime, this.endTime),
    //   isFitArea,
    //   revenueType: this.campaign.revenueType === this.config.AdsType.revenueType.default.value ? this.config.AdsType.revenueType.default.value : this.revenueType });
    return this.campaign.revenueType === this.config.AdsType.revenueType.default.value ||
      ((ignoretime || (checkTime(startTimeCampaign, endTimeCampaign) && checkTime(this.startTime, this.endTime))) && countBanner > 0 && isFitArea);
  }
}

export default Placement;
