import store from '../store';
// import config from '../config/constants';
import config from '../config';

class Entity {
  constructor(entity) {
    this.id = entity.id;
    this.width = entity.width;
    this.height = entity.height;
    this.css = entity.css;
    this.weight = entity.weight;
    this.css = entity.css;
    this.outputCss = entity.outputCss;
    this.tree = entity.tree;
    this.query = entity.query;
    this.store = store;
    this.config = config;
    this.revenueType = entity.revenueType;
  }

  /**
   * get weight
   * Its can be override.
   */
  weightValue(weightName) {
    return this[weightName];
  }
}

export default Entity;
