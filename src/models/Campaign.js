import checkTime from '../vendor/util/dateTime';

export default class Campaign {
  constructor(campaign) {
    this.id = campaign.id;
    this.startTime = campaign.startTime;
    this.endTime = campaign.endTime;
    this.revenueType = campaign.revenueType;
  }

  isAvailable() {
    return checkTime(this.startTime, this.endTime);
  }
}
