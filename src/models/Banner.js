import Entity from './Entity';
import { MultipleBannerTerm } from '../vendor/BannerTerm/';
import Logging from '../vendor/Logging';
// import Query from '../vendor/util/query';
import Macro from '../vendor/Macro';
import getlValueFromChannelVariable from '../config/channelVariable';
import * as urlUtil from '../vendor/util/url';
// import installScript from '../vendor/util/handleHTML/installScript';

class Banner extends Entity {
  constructor(banner) {
    super(banner);
    this.id = `${banner.id}`;
    this.html = banner.html;
    this.isIFrame = banner.isIFrame;
    this.imageUrl = banner.imageUrl;
    this.optionBanners = banner.optionBanners;
    this.bannerType = banner.bannerType;
    this.placementId = banner.placementId;
    this.currentChannelLink = this.store.state.ENV.url;
    this.url = banner.url;
    this.thirdPartyUrl = banner.thirdPartyUrl;
    // this.query = this.tree.zoneId ? new Query(this.store, null, this.tree.zoneId) : new Query(this.store);
    this.renderFrom = null;
    this.bannerHtmlType = banner.bannerHtmlType;
    this.isStandAlone = banner.isStandAlone;
    this.positionOnShare = banner.positionOnShare; // it will be pass by Placement
    /* fields(views, trueView, click): present for banner log value */
    this.views = banner.views;
    this.target = banner.target || '_blank';
    this.trueView = banner.trueView;
    this.click = banner.click;
    this.isDocumentWrite = banner.isDocumentWrite;
    // temp data for check for ssp coz when force ssp cause change this.html so function isForceSsp return wrong result.
    this.isForceSSpTemp = null;
    this.vastTagsUrl = banner.vastTagsUrl;
     /* */
  }

  isForce() {
    return this.checkForce();
  }

  isMobile() {
    const zoneBelongTo = this.query.getZone('id', this.tree.zoneId)[0];
    if (zoneBelongTo) {
      return zoneBelongTo.isMobile;
    }
    return false;
  }

  isResponsive() {
    const zoneBelongTo = this.query.getZone('id', this.tree.zoneId)[0];
    if (zoneBelongTo) {
      return zoneBelongTo.isResponsive;
    }
    return false;
  }

  sspTracking() {
    return urlUtil.getParameterByName('ssptk') || urlUtil.getParameterByName('ssppage');
  }

  isForceSSp() {
    if (this.isForceSSpTemp) return this.isForceSSpTemp;
    const ssptk = urlUtil.getParameterByName('ssptk');
    const sspPage = urlUtil.getParameterByName('ssppage');
    console.log('sspTracking', ssptk);
    const result = this.html.indexOf(`sspbid_${ssptk}"`) !== -1 || this.html.indexOf(`ssppagebid_${sspPage}"`) !== -1 || this.html.indexOf(`adslot_${sspPage}`) !== -1;
    this.isForceSSpTemp = result;
    return result;
  }

  checkForce() {
    const bannerIdFromUrl = urlUtil.getParameterByName('bz');
    const zoneIdFromUrl = urlUtil.getParameterByName('z');
    const sspTracking = this.sspTracking();
    const isForceSSp = this.isForceSSp();
    const zsp = urlUtil.getHashtag().split('_')[0].replace('#', '');
    if (sspTracking && isForceSSp) {
      const i = urlUtil.getParameterByName('i');
      const type = urlUtil.getParameterByName('type');

      window.banTrackingParam = {
        i,
        type,
        sspid: sspTracking,
        z: this.tree ? this.tree.zoneId : null,
        bz: this.id,
        zsp,
      };

      if (this.isMobile()) {
        this.html = `<div id="bansptracking_${this.sspTracking()}" data-rel="/mbdata_${this.sspTracking()}.js"></div><div id="adm_zone${this.sspTracking()}"  data-rel="/mbdata_${this.sspTracking()}.js"></div>`;
      } else {
        this.html = `<div id="bansptracking_${this.sspTracking()}">`;
      }
      return isForceSSp;
    }
    return bannerIdFromUrl
      && zoneIdFromUrl
      && bannerIdFromUrl === this.id
      && zoneIdFromUrl === (this.tree ? this.tree.zoneId : null);
  }

  get cacheBuster() {
    return this.store.state.LOAD_ID;
  }

  getEnv() {
    try {
      return this.store.state.ENV;
    } catch (error) { // error when render directly from banner.
      console.log('ArfError from getEnv in banner.js', error);
      return {
        isPreviewMode: true,
      };
    }
  }

  isAvailable(revenueType) {
    const env = this.getEnv();
    if (!this.store.state.IS_PREVIEW) {
      // get result check from history.
      // const queryResult = this.store.state.CHANNEL_FILTER[this.id];
      // if (queryResult !== undefined) {
      //   return queryResult;
      // }

      let result;
      if (revenueType === this.config.AdsType.revenueType.default.value) {
        result = true;
      } else {
        /* [global filter]: commit result to store to check one times on page */
        const isPassGlobalFilter = this.store.getters.getGlobalFilterResult(this.tree.siteId, this.tree.zoneId);
        // if (!isPassGlobalFilter) {
        //   const listGlobalFilterAvailable = this.store.state.GLOBAL_FILTER.filter(item => item.siteId === this.tree.siteId);
        //   isPassGlobalFilter = new MultipleBannerTerm(listGlobalFilterAvailable, env, '&&').isPass;
        //   console.log('checkGlobalFilterResult', { isPassGlobalFilter, siteId: this.tree.siteId }, this.id, this.tree, this.query);
        //   this.store.commit('GLOBAL_FILTER_RESULT', { isPassGlobalFilter, siteId: this.tree.siteId });
        // }
        // console.log('globalFilters', this.store.state.GLOBAL_FILTER, isPassGlobalFilter);
        /* end block [global filter] */

        const isPassChannel = new MultipleBannerTerm(this.optionBanners, env).isPass;

        result = isPassGlobalFilter && isPassChannel;
      }

      // save the result to avoid re-check.
      const filterResult = {
        bannerId: this.id,
        result,
      };
      this.store.commit('PUSH_CHANNEL_FILTER', filterResult);

      // console.log(`bannerchannel ${this.id}: ${result}`, revenueType, this.optionBanners);
      return result;
    }
    return true;
  }

  bannerContent() {
    try {
      if (this.isStandAlone && (this.bannerType.isUpload && this.bannerType.value === 'img')) {
        return `<a ${this.url ? `href="${this.clickUrlUnEsc()}"` : ''} target="${this.target}">
          <img src="${this.imageUrl}">
        </a>`;
      } else if ((this.bannerType.isUpload && this.bannerType.value === 'img')) {
        return `<a ${this.url ? `href="${this.clickUrlUnEsc()}"` : ''} target="${this.target}">
                  <img${(this.isMobile()) ? ' style="width: 100%;"' : ''} src="${this.imageUrl}">
                </a>`;
      }
      return new Macro(this.html, this).replaceMacro();
    } catch (error) {
      console.log('Error from banner Model : BannerContent function', error);
      return '';
    }
  }

  loggingUrl(cov, config, isEncodeUrlClick, index = 0) {
    const placement = this.tree ? this.query.getPlacement('id', this.tree.placementId)[0] : null;
    const campaign = placement ? placement.campaign : null;
    const campaignId = campaign ? campaign.id : null;

    const logData = {};
    logData[this.config.loggingConfig.banner.param.dmn] = encodeURIComponent(this.currentChannelLink);
    logData[this.config.loggingConfig.banner.param.zid] = this.tree ? this.tree.zoneId : null;
    logData[this.config.loggingConfig.banner.param.pli] = this.placementId;
    logData[this.config.loggingConfig.banner.param.items] = this.id;
    logData[this.config.loggingConfig.banner.param.cmpg] = campaignId;
    logData[this.config.loggingConfig.banner.param.pgid] = this.store.state.LOAD_ID;
    logData[this.config.loggingConfig.banner.param.uid] = this.store.state.UID;
    logData[this.config.loggingConfig.banner.param.cov] = cov;
    logData[this.config.loggingConfig.banner.param.cat] = getlValueFromChannelVariable();
    if (cov === 1) {
      let urls = [];
      try {
        urls = JSON.parse(this.url);
      } catch (error) {
        urls = [this.url];
      }
      const url = new Macro(urls[index], this).replaceMacro();
      logData[this.config.loggingConfig.banner.param.re] = isEncodeUrlClick ? encodeURIComponent(url) : url;
    }
    const loggingConfig = config || this.config.loggingConfig;
    return new Logging(loggingConfig).bannerLoggingUrl(logData);
  }

  logging(cov) {
    if (this.store.state.IS_PREVIEW) return;
    // cov=0 OR cov=-1: view placement, banner.
    // cov=1: click
    // cov=2: true view
    // cov=3: load box when using rotate.
    const loggingConfig = this.config.loggingConfig;
    let thirdPartyUrlList = [];
    try {
      thirdPartyUrlList = JSON.parse(this.thirdPartyUrl);
    } catch (error) {
      thirdPartyUrlList = [{ url: this.thirdPartyUrl, cov: 0 }];
    }
    const self = this;
    if (thirdPartyUrlList && thirdPartyUrlList.length > 0) { // send 3rd tracking
      thirdPartyUrlList.filter(i => i.cov === cov).forEach((thirdPartyUrl) => {
        const img = new Image();
        const src = new Macro(thirdPartyUrl.url, self).replaceMacro();
        img.src = src;
      });
    }
    new Logging(loggingConfig).logging(this.loggingUrl(cov));
  }

  clickUrlEsc(index) {
    return encodeURIComponent(this.loggingUrl(1, null, true, index));
  }

  clickUrlUnEsc(index) {
    return this.loggingUrl(1, null, true, index);
  }

  // log for expand
  get expandLog() {
    return this.loggingUrl(10, null, true);
  }

  get vast() {
    try {
      if (this.vastTagsUrl === '') return '';
      const uid = this.store.state.UID;
      const pageId = window.__admloadPageId;
      console.log('checkpageId', window.__admloadPageId);
      let dgid = '';
      if (typeof window.ADS_CHECKER !== 'undefined') {
        dgid = window.__admloadPageIdc;
        // const uid = window.ADM_AdsTracking.get("__uid");
        // return `${this.vastTagsUrl}&domain=${encodeURIComponent(this.currentChannelLink)}&zoneId=${this.tree ? this.tree.zoneId : null}&category=${getlValueFromChannelVariable()}&pgid=${pageId}&uid=${uid}&dgid=${dgid}`;
      }
      if (typeof window.ADM_CHECKER !== 'undefined') {
        dgid = window.__admloadPageRdIdc;
        // const uid = window.ADM_CHECKER.__uid;
        // return `${this.vastTagsUrl}&domain=${encodeURIComponent(this.currentChannelLink)}&zoneId=${this.tree ? this.tree.zoneId : null}&category=${getlValueFromChannelVariable()}&pgid=${pageId}&uid=${uid}&dgid=${dgid}`;
      }
      return encodeURIComponent(`${this.vastTagsUrl}&domain=${encodeURIComponent(this.currentChannelLink)}&zoneId=${this.tree ? this.tree.zoneId : null}&category=${getlValueFromChannelVariable()}&pgid=${pageId}&uid=${uid}&dgid=${dgid}`);
    } catch (error) {
      console.log(error);
      return '';
    }
  }
}

export default Banner;
