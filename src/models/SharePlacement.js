import Placement from './Placement';
import Entity from './Entity';

export default class SharePlacement extends Entity {
  constructor(sharePlacement) { // eslint-disable-line
    try {
      super(sharePlacement);
      this.id = sharePlacement.id;
      this.positionOnShare = sharePlacement.positionOnShare;
      this.placementId = sharePlacement.placementId;
      this.shareId = sharePlacement.shareId;
      this.passTree(sharePlacement);
      /**
       * check Placement data
       * @type {Placement}
       */
      let placement = null;
      if (sharePlacement.placement instanceof Placement) {
        placement = sharePlacement.placement;
        placement.shareId = sharePlacement.shareId;
      } else if (sharePlacement.placement) {
        sharePlacement.placement.positionOnShare = this.positionOnShare; // eslint-disable-line
        placement = new Placement(sharePlacement.placement, this.store);
        placement.shareId = sharePlacement.shareId;
      }
      this.placement = placement;
      this.weight = placement.weight;
      this.cpdPercent = placement.cpdPercent;
    } catch (error) {
      console.log('ArfError from sharePlacement constructor', error);
    }
  }

  /**
   * pass detail to children
   */
  passTree(sharePlacement) {
    if (!sharePlacement.placement) return;
    this.tree.sharePlacementId = this.id;
    sharePlacement.placement.tree = {
      zoneId: this.tree.zoneId,
      shareId: this.tree.shareId,
      sharePlacementId: this.tree.sharePlacementId,
      siteId: this.tree.siteId,
    }; // eslint-disable-line
    sharePlacement.placement.query = this.query; // eslint-disable-line
  }

  isFitArea(area) {
    if (!this.placement) return false;
    return this.placement.isFitArea(area);
  }

  isForce() {
    if (!this.placement) return false;
    return this.placement.isForce();
  }

  area() {
    if (!this.placement) return null;
    return {
      width: this.placement.width,
      height: this.placement.height
    };
  }

  isInPageLoad() {
    if (!this.placement) return false;
    return this.placement.isInPageLoad();
  }

  placementType() {
    if (!this.placement) return null;
    return this.placement.placementType();
  }

  isAvailable() {
    if (!this.placement) return false;
    return this.placement && this.placement.isAvailable();
  }
}
