/* eslint-disable no-shadow, no-param-reassign */
import vue from 'vue';
import vuex from 'vuex';
import { MultipleBannerTerm } from '@/vendor/BannerTerm/';
import getlValueFromChannelVariable from './config/channelVariable';
import TrackLoad from './vendor/ArfStorage/storage/TrackLoad/main';
import getCurrentChannelUrl from './vendor/util/Term/getCurrentChannelUrl';
import ArfStorage from './vendor/ArfStorage/ArfStorage';
import config, { variables } from './config';
import Choose from './vendor/util/chooser';
import objectForLoop from './vendor/util/objectHandle';
// import objectForLoop from './vendor/util/objectHandle';

vue.use(vuex);

const currentChannel = getlValueFromChannelVariable() || getCurrentChannelUrl('pageUrl');
const state = {
  CURRENT_CHANNEL: currentChannel,
  ENV: null,
  CONFIG: {},
  ORDER_RENDER: [],
  MESSAGE: [],
  ZONE_DATA: {},
  BANNER_DATA: {},
  TRACKING_VIEW: [],
  BANNER_RENDERED: {},
  RELATIVE: [],
  PAGE_LOAD: {
    CHANNEL: currentChannel,
    IS_SET: false,
    TOTAL_PAGELOAD: 3,
    CAMPAIGNS: [],
    EXIST_MONOPOLY: false,
    activePageLoad: {},
    monopolyCampaign: {},
  },
  // PAGE_LOAD_TEMP: {
  //   CHANNEL: currentChannel,
  //   IS_SET: false,
  //   TOTAL_PAGELOAD: 3,
  //   EXIST_MONOPOLY: false,
  //   CAMPAIGNS: [],
  //   activePageLoad: {},
  //   monopolyCampaign: {},
  // },
  REMOTE_SCRIPT_REGISTER: [],
  REMOTE_SCRIPT_LOADED: [],
  REDRAW_SIGN: [],
  ADD_SHARE_SIGN: [],
  TRACK_LOAD: {},
  LOAD_ID: null,
  UID: null,
  GLOBAL_FILTER: [],
  ARF_CONFIG: config,
  IS_PREVIEW: false,
  CORE_LOADED: [],
  CHANNEL_FILTER: {},
  OPTION_CHANNEL_FILTER: {},
  GLOBAL_FILTER_RESULT: {},
  CPD_FORCE: null,
  FULL_CPM: null,
  IS_FULL_CPM: false,
  TURNS_LEFT: 3,
  TOTAL_SHARE: 3,
  TOTAL_CPD_RUNNING: [],
  ZONE_BANNERS: {},
  ZONE_FULL_CPD: [],
};

const arfStorage = new ArfStorage();

/**
 * region: page load id and uid.
 */
if (!state.LOAD_ID) {
  const timeStamp = new Date().getTime();
  const randomNumber = Math.floor(Math.random() * 1000000);
  state.LOAD_ID = `${timeStamp}${randomNumber}`;
}
let uid = arfStorage.getCookie('_uidcms');
if (!uid) {
  uid = state.LOAD_ID;
  arfStorage.setCookie('_uidcms', uid, 3e6, '/', `.${window.location.hostname}`);
}
if (!state.UID) state.UID = uid;
console.log('checkUid', uid);

/**
 * endregion
 */

/*
Init Data for VueX State
 */
const trackLoad = new TrackLoad(arfStorage, state.CURRENT_CHANNEL);
// let isSavingTrackLoad = false;
// const taskSaves = [];
state.TRACK_LOAD = trackLoad;
/**
 * Read Page load config from storage
 */
const pageloadConfigStr = arfStorage.getStorage(config.cookieConfig.pageLoad.name); // eslint-disable-line
let pageloadConfig = pageloadConfigStr ? JSON.parse(arfStorage.getStorage(config.cookieConfig.pageLoad.name)) : state.PAGE_LOAD; // eslint-disable-line
if (trackLoad.pageload.length >= pageloadConfig.TOTAL_PAGELOAD) {
  trackLoad.clearPageload();
}
// if (!pageloadConfig.monopolyCampaign) pageloadConfig = state.PAGE_LOAD;
// if (state.CURRENT_CHANNEL !== pageloadConfig.CHANNEL) {
//   state.PAGE_LOAD = {
//     CHANNEL: getlValueFromChannelVariable() || getCurrentChannelUrl('pageUrl'),
//     IS_SET: false,
//     TOTAL_PAGELOAD: 3,
//     CAMPAIGNS: [],
//     EXIST_MONOPOLY: false,
//     activePageLoad: {},
//     monopolyCampaign: {},
//   };
// } else {
//     // Re-save pageload if pageload config has change in current channel.
//   setTimeout(() => {
//     if (JSON.stringify(state.PAGE_LOAD.CAMPAIGNS) !== JSON.stringify(state.PAGE_LOAD_TEMP.CAMPAIGNS) || state.PAGE_LOAD.TOTAL_PAGELOAD !== state.PAGE_LOAD_TEMP.TOTAL_PAGELOAD) {
//       console.log('Re-SavePageloadConfig');
//       arfStorage.setStorage(config.cookieConfig.pageLoad.name, JSON.stringify(state.PAGE_LOAD_TEMP), '', '/', state.CURRENT_CHANNEL); // eslint-disable-line
//     }
//   }, 3000);
//   state.PAGE_LOAD = pageloadConfig;
// }

// const pageLoadData = new PageLoadData(arfStorage, state.CURRENT_CHANNEL_LINK);
// state.PAGE_LOAD = pageLoadData.data;
/*
end region : Init
 */

 /**
  * Set CPM full page flag.
  */
let isCreateFullCpm = (() => {
  try {
    return JSON.parse(arfStorage.getStorage('_cpmfullpage')) || false;
  } catch (error) {
    console.log('Arf Error from SetCMPFlag in store', error);
    return false;
  }
})();
state.IS_FULL_CPM = isCreateFullCpm;


let turnsLeft = (() => {
  try {
    return JSON.parse(arfStorage.getStorage('_tl')) || pageloadConfig.TOTAL_PAGELOAD || 3;
  } catch (error) {
    console.log('Arf Error from SetCMPFlag in store', error);
    return 3;
  }
})();
state.TURNS_LEFT = turnsLeft;

if (turnsLeft === pageloadConfig.TOTAL_PAGELOAD) {
  objectForLoop(trackLoad.history, (zoneId) => {
    try {
      const track = trackLoad.readZone(zoneId);
      if (track.isPageLoad) {
        track.turns = [];
        console.log('isClearFromStart');
      }
    } catch (error) {
      trackLoad.clearTrack();
    }
  });
  trackLoad.clearPageload();
  isCreateFullCpm = false;
}

let cpmFlag;
function setCPMFlag() {
  if (turnsLeft === pageloadConfig.TOTAL_PAGELOAD) { // reset counting if starting new cycle.
    state.IS_FULL_CPM = false;
    isCreateFullCpm = false;
    arfStorage.setStorage('_cpmfullpage', false, '', '/', state.CURRENT_CHANNEL);
  }

  const cpmFullCase = [{ value: true, wei: isCreateFullCpm ? 0 : 1 / 3 }, { value: false, wei: isCreateFullCpm ? 9999 : 2 / 3 }];
  const chooserCpmFlag = new Choose(cpmFullCase);
  cpmFlag = chooserCpmFlag.baseOnField('wei');
  if (turnsLeft === 1 && !isCreateFullCpm) {
    cpmFlag = { value: true, wei: isCreateFullCpm ? 0 : 1 / 3 };
  }
  state.FULL_CPM = cpmFlag.value;
  console.log('setCPMFlag', state.FULL_CPM);
  if (cpmFlag.value) {
    state.IS_FULL_CPM = true;
    state.CPD_FORCE = false;
    arfStorage.setStorage('_cpmfullpage', cpmFlag.value, '', '/', state.CURRENT_CHANNEL);
    isCreateFullCpm = cpmFlag.value;
  }
}
setCPMFlag();

turnsLeft -= 1;
if (turnsLeft <= 0) turnsLeft = 3;
arfStorage.setStorage('_tl', turnsLeft, '', '/', state.CURRENT_CHANNEL);

let isChangeState = false;
function changeStateIfChangeChannel() {
  if (isChangeState) return;
  isChangeState = true;
  state.TURNS_LEFT = 3;
  turnsLeft = 3;
  isCreateFullCpm = false;
  setCPMFlag();
  turnsLeft -= 1;
  arfStorage.setStorage('_tl', turnsLeft, '', '/', state.CURRENT_CHANNEL);
}
const getters = {
  zoneTrackLoad() {
    return (zoneId, totalShare) => {
      const data = trackLoad.readZone(zoneId, totalShare);
      // if zoneId not in data, readZone function'll init new zone data, so must change state
      // state.TRACK_LOAD_CURRENT = trackLoad.data;
      return data;
    };
  },
  relative(stateData) {
    return stateData.RELATIVE;
  },
  isScriptExecuted(state) {
    return uri => state.REMOTE_SCRIPT_LOADED.filter(item => item === uri).length > 0;
  },
  getMessage(state) {
    return state.MESSAGE;
  },
  getGlobalFilterResult(state) {
    return (siteId, zoneId) => {
      try {
        // const globalfilterContainListZone = state.GLOBAL_FILTER.filter(item => item.siteId === siteId && item.listZoneId).reduce((res, gf) => {
        //   const { listZoneId } = gf;
        //   const listZoneIdArray = listZoneId.split(',');
        //   return res.concat(listZoneIdArray);
        // }, []);

        // if (globalfilterContainListZone.indexOf(zoneId) === -1) return state.GLOBAL_FILTER_RESULT[siteId];
        if (state.ENV.isUpdate) {
          const url = getCurrentChannelUrl('pageUrl');
          const referrerUrl = getCurrentChannelUrl();
          const currentChannel = getlValueFromChannelVariable() || getCurrentChannelUrl('pageUrl');
          state.env.url = url;
          state.env.referrerUrl = referrerUrl;
          state.env.currentChannel = currentChannel;
          // eslint-disable-next-line no-new
          new MultipleBannerTerm(state.GLOBAL_FILTER, state.ENV, '&&');
          state.env.isUpdate = false;
        }
        let globalfilterResult = true;
        for (let index = 0; index < state.GLOBAL_FILTER.length; index += 1) {
          const globalfilter = state.GLOBAL_FILTER[index];
          const { listZoneId, conditionZone } = globalfilter;
          const listZoneIdArray = listZoneId ? listZoneId.split(',') : [];
          const extraCondition = (conditionZone === 'except' && listZoneIdArray.indexOf(zoneId) > -1) || (conditionZone === 'include' && listZoneIdArray.indexOf(zoneId) < 0);
          // if (listZoneIdArray.length === 0) globalfilterResult = globalfilterResult && globalfilter.isPass;
          // else if ((conditionZone === 'except' && listZoneIdArray.indexOf(zoneId) > -1) || (conditionZone === 'include' && listZoneIdArray.indexOf(zoneId) < 0)) globalfilterResult = globalfilterResult && true;
          // else globalfilterResult = globalfilterResult && globalfilter.isPass;
          globalfilterResult = globalfilterResult && (globalfilter.isPass || extraCondition);
        }

        return globalfilterResult;
      } catch (error) {
        return state.GLOBAL_FILTER_RESULT[siteId];
      }
    };
  },
};

const actions = {
  trackingZone(context, zone) {
    console.log('checkStausZoneRender', zone);
    window[variables.admGroupPage] = window[variables.admGroupPage] || [];

    if (!(zone.id
        && zone.addslot
        && zone.listzone)) {
      return;
    }

    if (window[variables.admGroupPage].filter(item => item.addslot === zone.addslot).length === 0) {
      const newAdmGroupPage = {};
      newAdmGroupPage.listzone = zone.listzone;
      newAdmGroupPage.loaded = {};
      newAdmGroupPage.addslot = zone.addslot;
      window[variables.admGroupPage].push(newAdmGroupPage);
    }

    for (let i = 0; i < window[variables.admGroupPage].length; i += 1) {
      const admGroupPage = window[variables.admGroupPage][i];

      if (admGroupPage.addslot === zone.addslot) {
        admGroupPage.loaded[zone.id] = zone.status;
        window[variables.admGroupPage][i] = admGroupPage;
      }
    }
  },
};

const mutations = {
  INIT_ENV(state, env) {
    if (state.ENV && !env.isUpdate === true) return;
    state.ENV = env;
    state.ENV.variables = state.ENV.variables || {};
  },
  SET_ENV_VARIABLE(state, variable) {
    state.ENV.variables[variable.name] = variable.value;
  },
  SET_ENV_PROPERTY(state, key, value) {
    state.ENV.key = value;
  },
  RENEW_CPM_CPD_FLAG(state, isCountTurnsLeft = true) {
    console.log('renewFlag');
    if (!isCountTurnsLeft) turnsLeft = 3;
    state.TURNS_LEFT = isCountTurnsLeft ? turnsLeft : 3;
    state.FULL_CPM = null;
    state.CPD_FORCE = null;
    cpmFlag = null;
    setCPMFlag();
    if (cpmFlag.value) {
      state.IS_FULL_CPM = true;
    }
    if (isCountTurnsLeft) {
      turnsLeft -= 1;
      if (turnsLeft <= 0) turnsLeft = 3;
    }
    console.log('checkCpmFlag', cpmFlag.value);
  },
  /*
  *****setup zone data****
   */
  INIT_ZONE_DATA(state, data) {
    state.ZONE_DATA = data;
  },
  PUSH_ZONE(state, zone) {
    try {
      const elId = zone.propsData.model.id;
      if (!state.ZONE_DATA[elId]) {
        state.ZONE_DATA[elId] = zone;
      }
    } catch (error) {
      console.log('Error from PUSH_ZONE in store', error);
    }
  },
  PUSH_ORDER_RENDER(state, zoneId) {
    state.ORDER_RENDER.push(zoneId);
  },
  CORE_LOADED(state, core) {
    try {
      state.CORE_LOADED.push(core);
    } catch (error) {
      console.log('Error from CORE_LOADED in store', error);
    }
  },
  ZONE_FULL_CPD(state, zoneId) {
    state.ZONE_FULL_CPD.push(zoneId);
  },
  SET_ACTIVE_SHARE(state, activeShare) {
    state.ZONE_DATA[activeShare.zoneId].activeShare = activeShare;
  },
  REMOVE_ZONE(state, zoneId) {
    delete state.ZONE_DATA[zoneId];
  },
  REMOTE_SCRIPT_REGISTER(state, data) {
    if (data.remove) {
      const index = state.REMOTE_SCRIPT_REGISTER.indexOf(data.url);
      state.REMOTE_SCRIPT_REGISTER.splice(index, 1);
    } else {
      state.REMOTE_SCRIPT_REGISTER.push(data.url);
    }
  },
  /*
  ******setup Banner data for view****
   */
  PUSH_BANNER(state, banner) {
    try {
      const el = (typeof banner.el === 'string' ? document.getElementById(banner.el.replace(/(#)/, '')) : banner.el)
      || document.getElementById(banner.propsData.model.id);
      state.BANNER_DATA[el.id] = banner;
    } catch (error) {
      //
    }
  },
  REMOVE_BANNER(state, bannerId) {
    delete state.BANNER_DATA[bannerId];
  },
  /*
  ******setup state page load***
  */
  SET_ACTIVE_PAGE_LOAD(state, activePageLoad) {
    console.log('runCommitActivePageLoad', state.PAGE_LOAD.activePageLoad);
    activePageLoad.loadId = state.LOAD_ID;
    if (state.PAGE_LOAD.activePageLoad.loadId === state.LOAD_ID) return;
    state.PAGE_LOAD.activePageLoad = activePageLoad;
    trackLoad.pushPageloadLog(activePageLoad); // push to trackload
    trackLoad.write();
    window._ADMpageloadAds = window._ADMpageloadAds ? `${window._ADMpageloadAds},${activePageLoad.campaignId}` : activePageLoad.campaignId;
  },

  PUSH_CONFIG_PAGELOAD(state, pageConfig) {
    try {
      console.log('checkPUSH_CONFIG_PAGELOAD', pageConfig, (!state.PAGE_LOAD.IS_SET && typeof pageConfig === 'object') && typeof pageConfig.existMonopoly === 'undefined');
      if ((!state.PAGE_LOAD.IS_SET && typeof pageConfig === 'object') && typeof pageConfig.existMonopoly === 'undefined') {
        console.log('TestPushPageload', pageConfig);
        state.PAGE_LOAD.TOTAL_PAGELOAD = pageConfig.totalPageload;
        pageConfig.campaigns.forEach((campaign) => {
          if (state.PAGE_LOAD.CAMPAIGNS.filter(item => item.campaignId === campaign.campaignId).length === 0) state.PAGE_LOAD.CAMPAIGNS.push(campaign);
        });
        state.PAGE_LOAD.IS_SET = false;
      }

      if (pageConfig.existMonopoly && !state.PAGE_LOAD.monopolyCampaign.campaignId) {
        state.PAGE_LOAD.EXIST_MONOPOLY = pageConfig.existMonopoly;
        state.PAGE_LOAD.monopolyCampaign = pageConfig.monopolyCampaign;
      }

      arfStorage.setStorage(config.cookieConfig.pageLoad.name, JSON.stringify(state.PAGE_LOAD), '', '/', state.CURRENT_CHANNEL); // eslint-disable-line
    } catch (error) {
      console.log('ArfError from PUSH_CONFIG_PAGELOAD in store', error);
    }
  },
  /*
  ******Track Load Data in each Zone*****
   */
  BANNER_CHECK(state, data) {
    const { zoneId, totalShare, banners } = data;
    const zoneData = trackLoad.readZone(zoneId, totalShare);
    const checker = (s1, s2) => {
      for (let i = 0; i < s1.length; i += 1) {
        const e = s1[i];
        const check = s2.indexOf(e) !== -1;
        if (!check) return false;
      }
      return s1.length > 0 || s2.length === 0;
    };
    const check = checker(zoneData.banners, banners);
    if (!check) {
      changeStateIfChangeChannel();
      trackLoad.init(data);
    }
    state.ZONE_BANNERS[zoneId] = banners;
  },
  TRACK_TURN(state, data) {
    const { zoneId, turn, totalShare, isPageLoad } = data;
    if (!turn) return;
    const zoneData = trackLoad.readZone(zoneId, totalShare);
    const formatData = trackLoad.formatZoneData(zoneData, state.PAGE_LOAD.TOTAL_PAGELOAD);
    formatData.turns.push(turn);
    formatData.isPageLoad = isPageLoad;
    formatData.countTurnsLeft(isPageLoad ? state.TURNS_LEFT : null);
    trackLoad.changeInStore(zoneId, formatData);
    // console.log('checkTurn', zoneId, turn, zoneData.turns, totalShare);
  },
  SAVE_TRACK_LOAD_DATA(state) {
    console.log('runSaveTrackLoad', state.TRACK_LOAD);
    trackLoad.write();
  },
  SYNC_TURN(state, data) {
    console.log('runSyncTurnLeftRelative', data);
    const { syncId, withId } = data;
    const from = trackLoad.readZone(syncId);
    const to = trackLoad.readZone(withId);
    from.turnsLeft = to.turnsLeft;
  },
  /**
 * ***Save Banners are rendered****
 */
  SAVE_BANNER_RENDERED(state, banner) {
    state.BANNER_RENDERED[banner.bannerId] = banner;
  },
  /**
   * Relative placement
   */
  SET_RELATIVE_PLACEMENT(state, relativeDetail) {
    const isPush = state.RELATIVE.filter(item =>
      item.relativeCode === relativeDetail.relativeCode
      && item.campaignId === relativeDetail.campaignId).length !== 0;
    if (!isPush) {
      state.RELATIVE.push(relativeDetail);
    }
  },
  SET_GLOBAL_FILTER(state, globalFilters) {
    try {
      globalFilters.map(item => (state.GLOBAL_FILTER.filter(it => it.id === item.id).length === 0 ? state.GLOBAL_FILTER.push(item) : ''));
    } catch (err) {
      console.log('ArfError from push globalfilter to store', err);
    }
  },
  /**
   * REMOTE_SCRIPT_LOADED
   */
  PUSH_REMOTE_SCRIPT_LOADED(state, uri) {
    // const isExist = state.REMOTE_SCRIPT_LOADED.filter(item => item === uri).length > 0;
    // if (!isExist && uri) state.REMOTE_SCRIPT_LOADED.push(uri);
    if (uri) state.REMOTE_SCRIPT_LOADED.push(uri);
  },
  /**
   * re-draw placement sign: placement 'll re-render if this Array have it's id.
   */
  PUSH_REDRAW_SIGN(state, placementId) {
    if (placementId) state.REDRAW_SIGN.push({ placementId });
  },
  /**
   * setup preview mode.
   */
  SET_PREVIEW_MODE(state, isPreview) {
    state.IS_PREVIEW = isPreview;
  },
  /**
   * Run more share in zone.
   */
  PUSH_ADD_SHARE_SIGN(state, zoneId) {
    if (zoneId) state.ADD_SHARE_SIGN.push(zoneId);
  },

  PUSH_MESSAGE(state, message) {
    state.MESSAGE.push(message);
  },
  PUSH_CHANNEL_FILTER(state, filterResult) {
    state.CHANNEL_FILTER[filterResult.bannerId] = filterResult.result;
  },
  PUSH_OPTION_CHANNEL_FILTER(state, option) {
    state.OPTION_CHANNEL_FILTER[option.id] = option;
  },
  GLOBAL_FILTER_RESULT(state, result) {
    state.GLOBAL_FILTER_RESULT[result.siteId] = result.isPassGlobalFilter;
  },
  CPD_FORCE(state, isForce) {
    if (cpmFlag.value) return;
    if (state.CPD_FORCE === null) {
      state.CPD_FORCE = isForce;
    }
  },
  FULL_CPM(state, data) {
    state.FULL_CPM = state.FULL_CPM && data;
  },
  TOTAL_CPD_RUNNING(state, data) {
    state.TOTAL_CPD_RUNNING.push(data);
  },
  PUSH_TRACKING_VIEW(state, data) {
    if (typeof data === 'object' && data.bid && data.callback && data.type) {
      state.TRACKING_VIEW.push(data);
    }
  },
};

export default new vuex.Store({
  state,
  mutations,
  getters,
  actions,
});
