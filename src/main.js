import instance from './Instance';
import * as detail from './detail';
import * as utils from './utils';
import Logging from './vendor/Logging';
import { getCurrentBrowser } from './vendor/util/browserChecking';

if (document.location.href === 'https://cafef.vn/' || document.location.href === 'http://cafef.vn/') {
  const logData = {};
  logData[instance.config.loggingConfig.zone.param.dmn] = encodeURIComponent(instance.env.url);
  logData[instance.config.loggingConfig.zone.param.zid] = 'kdsjflp3';
  logData[instance.config.loggingConfig.zone.param.pgid] = instance.store.state.LOAD_ID;
  logData[instance.config.loggingConfig.zone.param.uid] = instance.store.state.UID;
  const logging = new Logging(instance.config.loggingConfig);
  const url = logging.zoneLoggingUrl(logData);
  logging.logging(url);

  const currentBrowser = getCurrentBrowser();
  if (currentBrowser === 'firefox') {
    logData[instance.config.loggingConfig.zone.param.zid] = 'ketf80ne';
  } else if (currentBrowser === 'safari') {
    logData[instance.config.loggingConfig.zone.param.zid] = 'ketfa49v';
  } else {
    logData[instance.config.loggingConfig.zone.param.zid] = 'ketjptd4';
  }
  logging.logging(logging.zoneLoggingUrl(logData));
}
/**
 * Init queues
 * @type {Array}
 */
window.__meta_keywords = (function () {
  const metaKeywords = Array.from(document.getElementsByTagName('meta'))
  .filter(m => m.name.indexOf('keywords') > -1)
  .reduce((k, i) => {
    if (!k) return i.content;
    return `${k},${i.content}`;
  }, 0);

  return metaKeywords;
}());
window.__admisInIframe = window.__admisInIframe || (function () {
  try {
    return window.self !== window.top;
  } catch (a) {
    return !0;
  }
}());
window.__admURL = window.__admURL || (function () {
  return ((window.__admisInIframe && (document.referrer !== '')) ? document.referrer : document.URL).replace(/'/g, '');
}());
window.arfAsync = window.arfAsync || [];
window._ADMpageloadAds = window._ADMpageloadAds || ''; // array show current pageload.
/** */
window.admicroAD = window.admicroAD || {};
window.admicroAD.unit = window.admicroAD.unit || [];
/** */
window.arfZonesQueue = window.arfZonesQueue || [];
window.arfZonesQueue = window.arfZonesQueue.filter(zone => zone.propsData.model);
window.isArfInitLibraryDone = window.isArfInitLibraryDone || false;
window._ArfListZoneDisable = window._ArfListZoneDisable || [];
window.arf = window.arf || {};
window.arf.env = window.arf.env || {}; // setup env for arf, eg. : variables,..etc.
window.isArfLoaded = true;

// instance vue for each zone
window.vueInstance = window.vueInstance || [];

// receieve msg from popup window or iframe.
window.addEventListener('message', instance.receiveMessage, false);

// const instance = new Instance();

const Zone = (zoneData, isPreviewMode) => instance.renderZone(zoneData, isPreviewMode);

const Banner = (banner, isPreviewMode) => instance.renderBanner(banner, isPreviewMode, true);

const pushZone = async param => instance.pushZone(param); // param : zoneId || zoneData

const redrawPlacement = idOfBannerCode => instance.redrawPlacement(idOfBannerCode);

const drawMoreShare = zoneId => instance.drawMoreShare(zoneId);


instance.initLibrary(() => {
  pushZone();

  instance.observerQueue(window.arfZonesQueue, () => {
    pushZone();
  });

  (async function () {
    // use lastZoneId for fix render zone many times on time. (all process target on first element)
    let lastZoneId;
    while (window.arfAsync.length > 0) {
      const zoneId = window.arfAsync.shift();
      if (lastZoneId === zoneId) {
        await new Promise((resolve) => { // eslint-disable-line
          setTimeout(() => {
            resolve();
          }, 1000);
        });
      }
      lastZoneId = zoneId;
      pushZone(zoneId);
    }

    // observer arfZonesQueue
    instance.observerQueue(window.arfAsync, () => {
      while (window.arfAsync.length > 0) {
        const zoneId = window.arfAsync.shift();
        pushZone(zoneId);
      }
    });
  }());
});

const refresh = (zoneIdList) => {
  Array.from(document.querySelectorAll('div[id*="zone-"]')).forEach((zone) => {
    const id = zone.id.replace('zone-', '');
    if (zoneIdList && !(Array.isArray(zoneIdList) && zoneIdList.includes(id))) return;
      const el = document.createElement('div');
      el.id = id;
      zone.replaceWith(el);
      arfAsync.push(id);
     });
 };
// console.log('taskRender', instance.tasks);
// run all tasks are waited coz library have not init yet.
// instance.executeAllTasks();
// observer task array, execute everytime push task.
// instance.observeTask();
console.log('checkInstance', instance);
export {
  Zone,
  Banner,
  pushZone,
  redrawPlacement,
  drawMoreShare,
  detail,
  utils,
  refresh,
};
