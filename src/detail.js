import getVariable from './vendor/util/getVariable';
import store from './store';


export function getCurrentPageLoad() { // eslint-disable-line
  try {
    return store.state.PAGE_LOAD.activePageLoad;
  } catch (error) {
    console.log('ArfError from Detail', error);
    return {};
  }
}

export function getValue(name) {
  return getVariable(name, store.state.ENV);
}

export function cpdFlag() {
  return store.state.CPD_FORCE;
}

export function cpmFlag() {
  return store.state.FULL_CPM;
}

export function getState() {
  return store.state;
}

export function registerViewTracking(data) {
  store.commit('PUSH_TRACKING_VIEW', data);
}
