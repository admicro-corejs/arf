import installScript from './vendor/util/handleHTML/installScript';
import storageType from './vendor/env/storageTypes';
import ArfStorage from './vendor/ArfStorage';
import {
  writeDomAsync,
} from './vendor/WriteDomAsync';
import ViewTracking from './vendor/ViewTracking/entry';


const storage = new ArfStorage();

export async function loadScript(url, callback) { // eslint-disable-line
  await installScript(null, url, true, 0, null, false);
  if (callback) callback();
}
/**
 *
 * @param {*} el: wrapper
 * @param {*} content: html content
 * @param {*} options: {avoidConflictDocWrite: boolean, error(e), done()}
 */
export function WriteDomAsync(el, content, options) {
  writeDomAsync(el, content, options);
}

/**
 * windowObject: 'windowObject',
  cookie: 'cookie',
  localStorage: 'localStorage',
 */
export function getValue(name, type) {
  let value = null;
  if (type === storageType.cookie) {
    value = storage.getCookie(name);
  } else if (type === storageType.localStorage) {
    value = storage.getStorage(name);
  } else if (type === storageType.windowObject) {
    value = window[name];
  }

  return value;
}

export function startStopBanner(iframe) {
  const objetcMonitor = ViewTracking(iframe);
  const ifrmWindow = iframe.contentWindow;
  objetcMonitor.checkView({
    inView() {
      ifrmWindow.postMessage('start', '*');
    },
    outView() {
      ifrmWindow.postMessage('pause', '*');
    },
    config: {
      percentageLimit: 0.25,
      timeLimit: 100,
      interval: 100,
    },
  });
}
