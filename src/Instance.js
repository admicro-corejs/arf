import Vue from 'vue';
import { MultipleBannerTerm } from '@/vendor/BannerTerm/';
import getlValueFromChannelVariable from './config/channelVariable';
import './vendor/polyfills/replaceWithPolyfill';
import './vendor/polyfills/childElementCount';
import store from './store';
import Arf from './Arf';
import ViewTracking from './vendor/ViewTracking/entry';
import {
  writeDomAsync,
} from './vendor/WriteDomAsync';
import config, {
  siteConfig,
  domain,
} from './config';
import getCurrentChannelUrl from './vendor/util/Term/getCurrentChannelUrl';
import * as url from './vendor/util/url';
import installScript from './vendor/util/handleHTML/installScript';
// import DetectDevice from './vendor/util/detectDevices';
// import Zone from './models/Zone';
// import objectForLoop from './vendor/util/objectHandle';
import { getCurrentBrowser } from './vendor/util/browserChecking';
import setEnvVariable from './vendor/env/SetEnvVariable';
import messageHandler from './vendor/util/messageHub/messageHandler';
import Query from './vendor/util/query';
import Placement from './models/Placement';

Vue.prototype.$viewTracking = ViewTracking;
Vue.prototype.$writeDomAsync = writeDomAsync;
Vue.prototype.$arfConfig = config;

window.arfTesting = window.arfTesting || {}; // init env for testing

class Task {
  constructor(data) {
    this.invoke = data ? data.invoke : null;
    this.index = data ? data.index : 0;
    this.immediatetly = data ? data.immediatetly : false;
    this.isExecuted = false;
    this.waitTime = data ? (data.waitTime || 0) : 0;
    this.timeout = data ? (data.timeout || 3000) : 3000;
  }

  exec() {
    return new Promise((resolve) => {
      if (!this.isExecuted) {
        setTimeout(() => {
          this.invoke(resolve);
        }, this.waitTime);

        setTimeout(() => {
          resolve();
        }, this.timeout);
        this.isExecuted = true; // eslint-disable-line no-param-reassign
      }
    });
  }
}

class TaskManager {
  constructor() {
    this.tasks = [];
    this.isProcessing = 0;
    this.waitTimeout = 0;
    this.callbackStart = null;
    this.isWaitForTimeout = false;
    this.observerTask();
  }

  turnOnWait(timeout) {
    this.waitTimeout = timeout || 5000;
  }

  push(task) {
    if (!(task instanceof Task)) {
      task = new Task(task); // eslint-disable-line no-param-reassign
    }

    if (task.immediatetly) {
      task.exec();
    } else {
      this.tasks.push(task);
    }

    this.tasks.sort((a, b) => (a.index - b.index));
  }

  async execSync(isReRunning) {
    this.isProcessing = 1;
    await this.waitTime();
    if (this.callbackStart && !isReRunning) this.callbackStart();
    for (let index = 0; index < this.tasks.length; index += 1) {
      const task = this.tasks[index];
      if (task.isExecuted) continue;
      // if (index < 2) task.exec(); // uu tien 3 zone dau tien push.
      await task.exec(); // eslint-disable-line
    }

    if (this.tasks.filter(task => !task.isExecuted).length > 0) {
      await this.execSync(1);
    }
    this.isProcessing = 0;
  }

  async execAsync(isReRunning) {
    this.isProcessing = 1;
    await this.waitTime();
    if (this.callbackStart && !isReRunning) this.callbackStart();
    for (let index = 0; index < this.tasks.length; index += 1) {
      const task = this.tasks[index];
      if (task.isExecuted) continue;
      task.exec();
    }

    if (this.tasks.filter(task => !task.isExecuted).length > 0) {
      await this.execAsync(1);
    }
    this.isProcessing = 0;
  }

  async waitTime() {
    if (this.isWaitForTimeout) return;
    await new Promise((resolve) => {
      setTimeout(() => {
        resolve();
        this.isWaitForTimeout = true;
      }, this.waitTimeout);
    });
  }

  observerTask() {
    const tm = this;
    const vm = new Vue({ // eslint-disable-line no-new,no-unused-vars
      data: {
        tasks: tm.tasks,
        isProcessing: tm.isProcessing,
      },
      watch: {
        async tasks() {
          if (tm.isProcessing) return;
          console.log('checkRunTask');
          await tm.execAsync();
        },
      },
    });

    // setTimeout(() => { // after 15s stop watch to save perfomance.
    //   vm.$destroy();
    // }, 15000);
  }
}


class Instance {
  constructor(env) {
    const envTemp = env || {
      domain: url.getOrigin(window.__admURL || location.href),
      url: getCurrentChannelUrl('pageUrl'),
      referrerUrl: getCurrentChannelUrl(),
      force: [],
      domainCore: domain.domainCore.url,
      domainLog: domain.domainLog.url,
      currentChannel: getlValueFromChannelVariable() || getCurrentChannelUrl('pageUrl'),
    };
    this.env = envTemp;
    this.store = store;
    store.commit('INIT_ENV', this.env);
    this.isSetupPageload = false;
    this.isSetupArfEnv = false;
    this.config = config;
    this.listInstance = [];
    this.tasks = new TaskManager();

    this.vueInstance = new Vue({ // eslint-disable-line no-new
      el: document.createElement('div'),
      // data: {
      //   arfs: [],
      // },
      // watch: {
      //   arfs() {
      //     this.$emit('pushData', this.arfs.shift());
      //   },
      // },
      store,
      render: h => h(Arf),
    });
    // const urlParams = this.getParamUrl();
    domain.domainCore.url = envTemp.domainCore;
    domain.domainLog.url = envTemp.domainLog;
  }

  /**
   * init lib nessesary.
   * @param {function} done : callback when library loaded.
   */
  async initLibrary(done) {
    // const device = new DetectDevice();
    // const isMobile = device.phone || device.tablet;
    // const admcorearfUrl = `${domain.domainCore.url}${domain.domainCore.admcorearf}`;
    // const mbCoreUrl = `${domain.domainCore.url}${domain.domainCore.mb_core}`;
    // const isExecuted = Array.from(document.getElementsByTagName('script')).filter(item => item.src === admcorearfUrl || item.src === mbCoreUrl).length > 0;
    // console.log('isMobile', isMobile);
    // if (!isExecuted) {
    //   if (isMobile) {
    //     await installScript(null, mbCoreUrl, null, store);
    //   } else if (!window.ADS_CHECKER) await installScript(null, admcorearfUrl, null, store);
    // }

    // const tagsponsorzPC = `${domain.domainCore.url}${domain.domainCore.tagsponsorzPC}`;
    // const tagsponsorzMB = `${domain.domainCore.url}${domain.domainCore.tagsponsorzMB}`;

    // const height = window.screen.availHeight;
    // const width = window.screen.availWidth;

    // let OSName = 'Unknown OS';
    // if (navigator.appVersion.indexOf('Windows') !== -1 && navigator.appVersion.indexOf('Phone')) OSName = 'Windows';
    // if (navigator.appVersion.indexOf('Macintosh') !== -1) OSName = 'MacOS';
    // const isPC = ['Windows', 'MacOS'].indexOf(OSName) > -1;
    // const isMobile = (width <= 812 || height <= 812) && !isPC;
    // // let OSName = 'Unknown OS';
    // // if (navigator.appVersion.indexOf('Win') !== -1) OSName = 'Windows';
    // const isMatchDomain = this.env.domain.indexOf('tuoitre.vn') > -1;
    // if (isMobile && isMatchDomain) {
    //   await installScript(null, tagsponsorzMB, true, null, this.store);
    // } else if (isMatchDomain) {
    //   await installScript(null, tagsponsorzPC, true, null, this.store);
    // }

    const currentBrowser = getCurrentBrowser();
    if (currentBrowser === 'firefox') {
      window.chkAdmblockAds = true;
      await installScript(null, 'https://static.contineljs.com/core/corecontineljs.js', true, null, store);
    }
    if (done) done();
  }

  checkBlockZone(zoneId) {
    const listZoneDisable = window[siteConfig.zonesDisable.name] || [];
    const listZoneHidden = window[siteConfig.zoneHidden.name] || [];
    return listZoneDisable.indexOf(zoneId) !== -1 && listZoneHidden.indexOf(zoneId) !== -1;
  }

  async pushZone(param) {
    // if param is object =>  param is zoneData.
    if (typeof param === 'object') {
      const zoneData = param;
      // store.commit('PUSH_ZONE', zoneData);
      this.renderZone(zoneData);
    } else {
      // param is not object => param is zoneId
      const zoneId = param;
      if (store.state.ZONE_DATA[zoneId]) {
        // this block code for re-render zone if nesscesary. ex: lightbox.
        try {
          this.vueInstance.$children[0].$children.filter(item => item.id === zoneId).forEach((zoneComponent) => {
            zoneComponent.$destroy();
          }); // destroy zone instance for re-render.
          this.renderZone(store.state.ZONE_DATA[zoneId], null, zoneId, true);
        } catch (error) {
          console.log('ArfError from re-render zone in PushZone function', error);
        }
      } else {
        if (zoneId) {
          /* stop render if zone contain inside _ArfListZoneDisable */
          if (this.checkBlockZone(zoneId)) return;

          const demoMode = url.getParameterByName('arfdemo'); // turn on demo mode
          let isDemo = false;
          if (demoMode) {
            isDemo = demoMode.toString().indexOf(zoneId) > -1;
          }
          const dataUrl = `${domain.domainCore.url}/cms/arf-${zoneId}${isDemo ? '-demo' : ''}.min.js`;
          installScript(null, dataUrl, true, null, store);
        }

        while (Array.isArray(window.arfZonesQueue) && window.arfZonesQueue.length > 0) {
          const zoneData = window.arfZonesQueue.shift();

          /* stop render if zone contain inside _ArfListZoneDisable */
          // if (this.checkBlockZone(zoneData.propsData.model.id)) continue;

          /* get pageload config when push zone */
          this.setupSiteConfig(zoneData);
          /* wait 300ms for setup pageload */
          // store.commit('PUSH_ZONE', zoneData);
          this.renderZone(zoneData);
        }
      }
    }
  }

  setupEnvVariable(channels) {
    for (let index = 0; index < channels.length; index += 1) {
      const channel = channels[index];
      const variablesArray = JSON.parse(channel.storageType);
      setEnvVariable(variablesArray);
    }
  }

  redrawPlacement(elId) {
    console.log('callRedrawPlacement', elId);
    try {
      let placementId = (document.getElementById(elId)).parentElement.parentElement.parentElement.getAttribute('id');
      if (placementId.indexOf('placement-') !== -1) {
        placementId = placementId.replace('placement-', '');
      }
      store.commit('PUSH_REDRAW_SIGN', placementId);
    } catch (error) {
      //
    }
  }

  drawMoreShare(elId) {
    console.log('callDrawMoreShare', elId);
    try {
      if (typeof elId === 'string') {
        const slotId = (document.getElementById(elId)).parentElement.getAttribute('id');
        const zoneId = slotId.split('-')[2];
        store.commit('PUSH_ADD_SHARE_SIGN', zoneId);
      }

      if (typeof elId === 'object') {
        store.commit('PUSH_ADD_SHARE_SIGN', elId);
      }
    } catch (error) {
      //
    }
  }

  renderBanner(data, isPreviewMode, isStandAlone) {
    // if data undefined or null => not run.
    if (!data.propsData.model) return;
    // if (!data.propsData.model.env) data.propsData.model.env = this.env; // eslint-disable-line
    data.propsData.model.isStandAlone = isStandAlone; // eslint-disable-line
    store.commit('SET_PREVIEW_MODE', isPreviewMode || false);
    // data.propsData.model.env.isPreviewMode = isPreviewMode || false; // eslint-disable-line
    store.commit('PUSH_BANNER', data);

    // all task'll run when library're inited.
    const task = new Task({
      immediatetly: true,
      invoke: () => {
        this.render(data, 'banner');
      },
    });
    // task.immediatetly = true;
    // task.invoke = () => {
    //   this.render(data, 'banner');
    // };
    this.tasks.push(task);
  }

  renderZone(data, isPreviewMode, elRenderTo, RenderImmediatetly) {
    // if data undefined or null => not run.
    if (!data.propsData.model) return;
    store.commit('PUSH_ORDER_RENDER', data.propsData.model.id);
    // if (!data.propsData.model.env) data.propsData.model.env = this.env; // eslint-disable-line

    store.commit('PUSH_ZONE', data);
    store.commit('SET_PREVIEW_MODE', isPreviewMode || false);
    // data.propsData.model.env.isPreviewMode =  isPreviewMode || false; // eslint-disable-line

    // all task'll run when library're inited.
    const taskIndex = store.state.ORDER_RENDER.indexOf(data.propsData.model.id);

    const task = new Task({
      index: taskIndex,
      immediatetly: RenderImmediatetly || false,
      invoke: (resolve) => {
        this.render(data, 'zone', elRenderTo, resolve);
      },
    });

    this.tasks.push(task);
    // if (store.state.ORDER_RENDER.indexOf(data.propsData.model.id) < 3) {
    //   this.tasks.push(task);
    // } else {
    //   task.timeout = 1000; // add timeout execute render tasks for optimize speed render, too much zone concurrent render cause slow.
    //   this.tasks.push(task);
    // }
  }

  observerQueue(queue, callback) {
    return new Vue({ // eslint-disable-line no-new
      data: {
        queue,
      },
      watch: {
        queue(value) {
          if (callback) {
            callback(value);
          }
        },
      },
    });
  }

  getPlaceToRender(data, elRenderTo) {
    if (elRenderTo) {
      const el = document.getElementById(elRenderTo);
      if (el) return el;
      // const elAdm = document.getElementById(`admzone${elRenderTo}`);
      // if (elAdm) elResult = elAdm;
      // const elAdmSlot = document.getElementById(`adm-slot-${elRenderTo}`);
      // if (elAdmSlot) elResult = elAdmSlot;
    }
    const el = document.getElementById(data.propsData.model.id);
    if (el) return el;

    // const elAdm = document.getElementById(`admzone${data.propsData.model.id}`);
    // if (elAdm) return elAdm;

    const elAdmSlot = document.getElementById(`admzone${data.propsData.model.id}`) || document.getElementById(`adm-slot-${data.propsData.model.id}`);
    if (elAdmSlot) {
      const elTemp = document.createElement('div');
      elTemp.id = data.propsData.model.id;
      elAdmSlot.appendChild(elTemp);
      return elTemp;
    }

    return document.getElementById(data.propsData.model.id) || document.getElementById(`admzone${data.propsData.model.id}`) || document.getElementById(`adm-slot-${data.propsData.model.id}`);
  }

  render(data, type, elRenderTo, resolve) {
    const limit = 6; // unit: seconds
    // only render when parent don't set display none.
    let i = 0;
    const wait = setInterval(() => {
      i += 100;
      if (i > limit * 1000) {
        clearInterval(wait);
        if (resolve) resolve();
      }

      const el = this.getPlaceToRender(data, elRenderTo);

      if (!el) return;
      const parentEl = el.parentElement;

      if (parentEl.style.display !== 'none') {
        clearInterval(wait);
        this.vueInstance.$emit('pushData', {
          el,
          type,
          data,
          hookToRenderNextZone: resolve,
        });
        // this.vueInstance.$data.arfs.push({
        //   el,
        //   type,
        //   data,
        //   hookToRenderNextZone: resolve,
        // });
      }
    }, 100);
  }

  getParamUrl() {
    const urlParams = [];
    location.search.replace(
      new RegExp('([^?=&]+)(=([^&]*))?', 'g'),
      ($0, $1, $2, $3) => {
        urlParams.push({
          name: $1,
          value: $3,
        });
      },
    );
    return urlParams;
  }

  setupSiteConfig(zoneData) {
    try {
      if (!this.isSetupArfEnv) {
        const channels = zoneData.propsData.model.site.channels;
        this.setupEnvVariable(channels);
        this.isSetupArfEnv = true;
      }
      this.setGlobalfilter(zoneData.propsData.model);
      // this.setupGroupSsp(zoneData.propsData.model);
      this.pushConfigMonopolyCampaign(zoneData);
      // push pageload config to store
      const campaignPageload = this.pushConfigPageload(zoneData);
      // wait if exist pageLoad
      if (campaignPageload.length > 0) {
        this.tasks.turnOnWait();
        this.tasks.callbackStart = () => {
          if (!this.isSetupPageload) {
            const activePageLoad = this.setupPageLoad();
            this.isSetupPageload = !!activePageLoad.campaignId; // this flag for choose until can choose active pageload.
          }
        };
      }
    } catch (error) {
      console.log('ArfError from setupSiteConfig', error);
      this.isSetupPageload = false;
    }
  }

  filterCampaign(zoneData, campaigns) {
    try {
      if (campaigns.length === 0) return [];

      let campaignIdsAvailable = [];
      let placements = [];

      campaigns.forEach((campaign) => {
        const placementsInCampaign = new Query(null, zoneData).getPlacement('campaignId', campaign.campaignId);
        placements = placements.concat(placementsInCampaign);
      });

      placements = placements.map(placement => (placement instanceof Placement ? placement : new Placement(placement)));
      placements = placements.filter((placement) => {
        placement.tree = placement.tree || {}; // eslint-disable-line
        placement.tree.siteId = zoneData.propsData.model.site.id; // eslint-disable-line
        placement.tree.zoneId = zoneData.propsData.model.id; // eslint-disable-line
        placement.passTree();
        return placement.isAvailable();
      });

      campaignIdsAvailable = placements.map(p => p.campaignId);
      const result = campaigns
        .filter(c => campaignIdsAvailable.indexOf(c.campaignId) !== -1)
        .filter((c) => {
          if (c.options) {
            const options = (JSON.parse(c.options)).filter(o => o.domain === store.state.ENV.domain);
            return options.length > 0 && new MultipleBannerTerm(options, store.state.env, '||').isPass;
          }
          return true;
        });
      console.log('checkFilterCampaign', campaigns, result);
      return result;
    } catch (error) {
      console.log('ArfError from filterCampaign in Instance', error);
      return [];
    }
  }

  pushConfigMonopolyCampaign(zoneData) {
    try {
      const zoneDataTemp = JSON.parse(JSON.stringify(zoneData));
      console.log('runPushConfigMonopolyCampaign');
      const monopolyCampaignsConfig = zoneDataTemp.propsData.model.site.campaignMonopoly;
      let monopolyCampaigns = monopolyCampaignsConfig.campaigns;
      // if (monopolyCampaigns.length === 0) return;
      monopolyCampaigns = this.filterCampaign(zoneDataTemp, monopolyCampaigns);
      const pageloadConfig = zoneDataTemp.propsData.model.site.pageLoad || {};
      pageloadConfig.existMonopoly = monopolyCampaigns.length > 0;
      pageloadConfig.monopolyCampaign = monopolyCampaigns[0];
      console.log('existMonopoly', zoneDataTemp.propsData.model.id, monopolyCampaigns.length > 0);
      store.commit('PUSH_CONFIG_PAGELOAD', pageloadConfig);
    } catch (error) {
      console.log('ArfError from pushConfigMonopolyCampaign', error);
    }
  }

  pushConfigPageload(zoneData) {
    try {
      const zoneDataTemp = JSON.parse(JSON.stringify(zoneData));
      if (store.state.PAGE_LOAD.EXIST_MONOPOLY) return [];
      if (!zoneDataTemp.propsData.model.isPageLoad) return []; // if zone not run with pageLoad.
      const pageloadConfig = zoneDataTemp.propsData.model.site.pageLoad;
      if (!pageloadConfig) return [];
      let { campaigns } = pageloadConfig;

      campaigns = this.filterCampaign(zoneDataTemp, campaigns);
      console.log('checkCampaignPageLoad', campaigns, zoneDataTemp);
      pageloadConfig.campaigns = campaigns;
      store.commit('PUSH_CONFIG_PAGELOAD', pageloadConfig);
      return campaigns;
    } catch (error) {
      console.log('ArfError from pushConfigPageload', error);
      return [];
    }
  }

  setupPageLoad() {
    try {
      const campaigns = store.state.PAGE_LOAD.CAMPAIGNS;
      let activePageLoad = {
        campaignId: '',
      };

      const totalPageload = store.state.PAGE_LOAD.TOTAL_PAGELOAD;
      console.log('totalPageload', campaigns, totalPageload);
      if (campaigns && campaigns.length > 0) {
        // const history = store.state.TRACK_LOAD.history;
        const pageloadData = {};
        // objectForLoop(history, (zoneId) => { // eslint-disable-line
        //   const zoneTrack = history[zoneId];
        //   if (zoneTrack) {
        //     const zoneHistory = zoneTrack.turns;
        //     const index = -Math.max(zoneHistory.length % totalPageload, 0);
        //     const twoTurnsPrevious = index ? zoneHistory.slice(index) : [];
        //     campaigns.map((pageLoad) => { // eslint-disable-line
        //       pageloadData[pageLoad.campaignId] = {};
        //       let times = 0;
        //       twoTurnsPrevious.map((turn) => { // eslint-disable-line
        //         if (turn.pageLoadId === pageLoad.campaignId) {
        //           times += 1;
        //         }
        //       });
        //       pageloadData[pageLoad.campaignId].times = times;
        //     });
        //   }
        //   return 'break';
        // });
        const pageloadHistory = store.state.TRACK_LOAD.pageload;
        campaigns.map((pageLoad) => { // eslint-disable-line
          pageloadData[pageLoad.campaignId] = {};
          let times = 0;
          pageloadHistory.map((pl) => { // eslint-disable-line
            if (pl.campaignId === pageLoad.campaignId) {
              times += 1;
            }
          });
          pageloadData[pageLoad.campaignId].times = times;
        });
        console.log('checkNewPageLoadData', pageloadData, pageloadHistory);
        // remove campaign rich limit
        const filterLoad = campaigns.filter((item) => {
          let appear;
          try {
            appear = pageloadData[item.campaignId].times;
          } catch (err) {
            appear = 0;
          }
          if (appear >= item.pageLoad) {
            return false;
          }
          return true;
        });
        // choose active page load.
        if (filterLoad.length > 0) {
          console.log('filterLoad', filterLoad);
          activePageLoad = filterLoad[Math.floor(Math.random() * filterLoad.length)];
        }
        // console.warn('checkNewActivePageLoad', activePageLoad);
        console.log('curentPageLoad', activePageLoad);
        store.commit('SET_ACTIVE_PAGE_LOAD', activePageLoad);
        return activePageLoad;
      }
      return activePageLoad;
    } catch (err) {
      console.log('ArfError from setupPageLoad', err);
      return {
        campaignId: '',
      };
    }
  }

  // setupGroupSsp(zone) {
  //   try {
  //     const groupSSP = (zone.groupSSP && typeof zone.groupSSP === 'string') ? JSON.parse(zone.groupSSP) : zone.groupSSP;
  //     if (!groupSSP) return;
  //     const zoneDetail = {};
  //     zoneDetail.addslot = groupSSP.addslot;
  //     zoneDetail.listzone = groupSSP.listzone;

  //     store.dispatch('trackingZone', zoneDetail);
  //   } catch (error) {
  //     console.log('ArfError from setupGroupSsp', error);
  //   }
  // }

  setGlobalfilter(zone) {
    const siteId = zone.site.id;
    if (store.state.GLOBAL_FILTER_RESULT[siteId]) return;
    const globalFilters = zone.site.globalFilters;
    store.commit('SET_GLOBAL_FILTER', globalFilters);

    const isPassGlobalFilter = new MultipleBannerTerm(globalFilters, store.state.ENV, '&&').isPass;
    store.commit('GLOBAL_FILTER_RESULT', { isPassGlobalFilter, siteId });
  }

  receiveMessage(event) {
    try {
      if (event.origin !== 'https://static-cmsads.admicro.vn' && event.origin !== document.location.origin) return;
      // console.log('checkDataFromMsg', event);

      if (event.data.channel && event.data.channel === 'cms-arf') {
        const {
          data,
          source,
        } = event;
        data.source = source;
        messageHandler.pushMessage(data);
        store.commit('PUSH_MESSAGE', data.scopes);
      }
    } catch (error) {
      //
    }
  }
}

export default new Instance(window.arfTesting.env);
