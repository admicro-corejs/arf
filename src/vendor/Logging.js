import objectForLoop from '../vendor/util/objectHandle';
/**
 * @config:
 * const logging = {
  url: 'http://lg1.logging.admicro.vn',
  zone: {
    path: 'advbcms',
    param: { dmn: 'dmn', zid: 'zid' },
  },
  banner: {
    path: 'cpx_cms',
    param: { dmn: 'dmn', zid: 'zid', pli: 'pli', cmpg: 'cmpg', items: 'items', cov: 'cov', pgid: 'pgid', uid: 'uid' },
  },
};
 */
export default class Logging {
  constructor(config) {
    this.config = config;
  }

  createLinkLogging(path, params) {
    const paramsString = params.reduce((linkLog, param) => (linkLog ? `${linkLog}&${param.name}=${param.value}` : `?${param.name}=${param.value}`), 0);
    const linkLog = `${this.config.url}/${path}${paramsString}`;
    return linkLog;
  }

  logging(linkLog) {
    const img = new Image();
    img.src = linkLog;
  }

  /**
   *
   * @param {*} type : 'banner' / 'zone'
   * @param {*} params
   */
  createParamsArray(type, params) {
    const data = [];
    objectForLoop(this.config[type].param, (param) => { // eslint-disable-line
      if (param === 're' && params.cov !== 1) return 'continue';
      data.push({ name: param, value: params[param] });
    });
    console.log('loggingParam', data);
    return data;
  }

  /**
   * input {dmn, zoneId, placementId, bannerId, campaignId, cov, re}
   */
  bannerLoggingUrl(params) {
    const data = this.createParamsArray('banner', params);
    const linkLog = this.createLinkLogging(this.config.banner.path, data);
    console.log('BannerLog', linkLog);
    return linkLog;
  }

  zoneLoggingUrl(params) {
    const data = this.createParamsArray('zone', params);
    const linkLog = this.createLinkLogging(this.config.zone.path, data);
    console.log('ZoneLog', linkLog);
    return linkLog;
  }
}
