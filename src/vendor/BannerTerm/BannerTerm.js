import Channel from './Channel/Channel';
import OptionsBanner from './Channel/OptionBanner';
import Entity from './Entity';
import config from '../../config/AdsType';
import store from '../../store';

export default class BannerTerm extends Entity {
  constructor(conditon, env, defaultLogical) {
    super(conditon);
    let isPass;
    this.defaultLogical = defaultLogical;
    /**
     * init data for check.
     */
    const init = new OptionsBanner(conditon, env, this.defaultLogical);

    if (init.getType() === config.bannerCondition.type.channel) { // eslint-disable-line
      /* this block resolve for channel */
      // const resultFromStore = store.state.OPTION_CHANNEL_FILTER[conditon.id];
      const resultFromStore = null;
      if (resultFromStore) {
        isPass = resultFromStore.isPass;
      } else {
        isPass = new Channel(conditon, env).isPass();
        if (this.comparison === '!=') isPass = !isPass;
      }

      store.commit('PUSH_OPTION_CHANNEL_FILTER', { id: conditon.id, isPass });
    } else if (init.getType() === config.bannerCondition.type.pageUrl) {
      isPass = init.multiPageUrl();
    } else if (init.getType() === config.bannerCondition.type.referringPage) {
      isPass = init.multiReferringPage();
    } else if (init.getType() === config.bannerCondition.type.variable) {
      isPass = init.multiVariable();
    } else {
      isPass = true;
    }

    this.isPass = isPass;
    this.logical = init.getLogicalExpression();
  }
}
