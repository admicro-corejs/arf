export default class Entity {
  constructor(channel) {
    this.id = channel.id;
    this.type = channel.type;
    this.value = channel.value;
    this.logical = channel.logical;
    this.comparison = channel.comparison;
    this.optionBannerChannels = channel.optionBannerChannels;
    this.startTime = channel.startTime;
    this.endTime = channel.endTime;
    this.isUrlPattern = channel.isUrlPattern;
  }
}
