import config from '../../../config/AdsType';
import OptionBanner from './OptionBanner';
import Entity from '../Entity';

/**
 * This class for check banner term with type = 'channel'
 */
export default class Channel extends Entity {
  constructor(channel, env) {
    super(channel);
    this.env = env;
  }
  listConditions() {
    try {
      return this.optionBannerChannels.map(optionBannerChannel =>
        optionBannerChannel.channel.optionChannels.map(condition =>
          (condition instanceof OptionBanner ? condition : new OptionBanner(condition, this.env))));
      // return this.optionBannerChannels[0].channel.optionChannels.map(condition =>
      //   (condition instanceof OptionBanner ? condition : new OptionBanner(condition, this.env)));
    } catch (err) {
      console.log('ArfError from list condition', err);
      return [];
    }
  }

  /**
   * check pass conditions.
   */
  isPass() {
    let result = false;
    const listConditions = this.listConditions();
    for (let index = 0; index < listConditions.length; index += 1) {
      const condition = listConditions[index];
      result = result || this.isPassOptionBannerChannel(condition);
    }
    return result;
  }

  isPassOptionBannerChannel(optionBannerChannel) {
    const listConditions = optionBannerChannel;
    if (listConditions.length > 0) {
      let result = true;
      for (let i = 0; i < listConditions.length; i += 1) {
        let resultTemp = true;
        const condition = listConditions[i];
        if (condition.getType() === config.bannerCondition.type.pageUrl) {
          resultTemp = condition.multiPageUrl();
        } else if (condition.getType() === config.bannerCondition.type.referringPage) {
          resultTemp = condition.multiReferringPage();
        } else if (condition.getType() === config.bannerCondition.type.variable) {
          resultTemp = condition.multiVariable();
        } else {
          resultTemp = true;
        }

        if (i > 0) {
          result += condition.getLogicalExpression() + resultTemp;
        } else {
          result = resultTemp;
        }
      }
      console.log(this.id, result);
      return window.eval(result);
    }
    return true;
  }
}
