import CheckPathLogic from '../../util/Term/checkPathLogic';
import getPath2Check from '../../util/Term/getPath2Check';
import config from '../../../config/AdsType';
import checkTime from '../../util/dateTime';
import Entity from '../Entity';
import getVariable from '../../util/getVariable';

export default class OptionChannel extends Entity {
  constructor(channel, env, defaultLogical) {
    super(channel);
    this.env = env;
    this.defaultLogical = defaultLogical;
    this.url = env ? env.url : null;
    this.referrerUrl = env ? env.referrerUrl : null;
    this.name = channel.name;
    this.globalVariables = channel.globalVariables;
    this.optionChannelType = channel.optionChannelType;
    this.logicalSelect = channel.logicalSelect;
  }
  /**
   * get Type of Banner Condition.
   */
  getType() {
    if (this.isChannel) return config.bannerCondition.type.channel;
    else if (this.isVariable) return config.bannerCondition.type.variable;
    else if (this.isUrl) return config.bannerCondition.type.pageUrl;
    else if (this.isReffererUrl) return config.bannerCondition.type.referringPage;
    return null;
  }

  get isChannel() {
    return this.type && config.bannerCondition.type.channel.indexOf(this.type.toLowerCase()) !== -1;
  }

  get isVariable() {
    if (!this.optionChannelType) {
      return this.type && config.bannerCondition.type.variable.indexOf(this.type.toLowerCase()) !== -1;
    }
    return this.optionChannelType.isVariable === true || (this.optionChannelType.isMultiSelect && this.optionChannelType.isGlobal);
  }

  get isUrl() {
    if (!this.optionChannelType) {
      return this.type && config.bannerCondition.type.pageUrl.indexOf(this.type.toLowerCase()) !== -1;
    }
    return this.optionChannelType.isInputLink === true && config.bannerCondition.type.pageUrl.indexOf(this.optionChannelType.name.toLowerCase()) !== -1;
  }

  get isReffererUrl() {
    if (!this.optionChannelType) {
      return this.type && config.bannerCondition.type.referringPage.indexOf(this.type.toLowerCase()) !== -1;
    }
    return this.optionChannelType.isInputLink === true && config.bannerCondition.type.referringPage.indexOf(this.optionChannelType.name.toLowerCase()) !== -1;
  }

  get getGlobalVariables() {
    return this.globalVariables ? this.globalVariables.split(',') : [];
  }

  // in the case multi-select option of variable, this.comparison's reponsibility is nagative or positive
  get isNegativeForVariable() {
    if (this.logicalSelect && this.comparison && this.comparison === '!=') {
      return true;
    }
    return false;
  }

  get getComparison() {
    if (this.logicalSelect) {
      return this.logicalSelect.split(',');
    }
    return [this.comparison];
  }
  /**
   * get Values of conditions.
   */
  get getValues() {
    return this.value ? this.value.split(',') : this.value;
  }

  get isAvailable() {
    return checkTime(this.startTime, this.endTime);
  }

  /**
   * get logical expression to combie with another condition.
   */
  getLogicalExpression() {
    if (this.logical) return this.logical === 'and' ? '&&' : '||';
    return this.defaultLogical;
  }

  /**
   * get global variale value.
   */
  getGlobalVariableValue(globalVariable) {
    if (globalVariable) {
      const value = getVariable(globalVariable, this.env);
      if (value !== null && value !== undefined) {
        return decodeURIComponent(value.toString());
      }
      return undefined;
    }
    return undefined;
  }

  getPageUrl() {
    return this.url || getPath2Check('pageUrl');
  }

  getReferrerUrl() {
    return this.referrerUrl || getPath2Check();
  }

  /**
   * check condition URL.
   * @param {*} globalVariableValue
   * @param {*} value
   */
  pageUrl(url, comparison, value) {
    if (!this.isAvailable) return true;
    const urlTemp = url || this.getPageUrl();
    return (new CheckPathLogic(comparison, value).isPassUrl(urlTemp));
  }

  /**
   * check condition multile URLs.
   */
  multiPageUrl(url) {
    const urlTemp = url || this.getPageUrl();
    const values = this.getValues;
    const comparion = this.getComparison[0];
    let result = false;
    for (let i = 0; i < values.length; i += 1) {
      if (i > 0) result = result || this.pageUrl(urlTemp, comparion, values[i]);
      else result = this.pageUrl(urlTemp, comparion, values[i]);
    }
    return result;
  }

  /**
   * check condition reference Page.
   * @param {*} referrerUrl
   * @param {*} value this for compare
   */
  referringPage(referrerUrl, comparison, value) {
    if (!this.isAvailable) return true;
    const referrerTemp = referrerUrl || this.getReferrerUrl();
    return (new CheckPathLogic(comparison, value).isPassUrl(referrerTemp));
  }

  /**
   * check condition multile multiple Reference Page.
   */
  multiReferringPage(referrerUrl) {
    const referrerTemp = referrerUrl || this.getReferrerUrl();
    const values = this.getValues;
    const comparion = this.getComparison[0];
    let result = false;
    for (let i = 0; i < values.length; i += 1) {
      if (i > 0) result = result || this.referringPage(referrerTemp, comparion, values[i]);
      else result = this.referringPage(referrerTemp, comparion, values[i]);
    }
    return result;
  }

  /**
   * check pass variable.
   * @param {*} globalVariableValue
   * @param {*} value
   */
  variable(realGlobalVariableValue, comparsion, expectedValue) {
    if (!this.isAvailable) return true;

    let globalVariableValueTemp;
    let expectedValueTemp;

    if (!isNaN(expectedValue) && !isNaN(realGlobalVariableValue)) {
      expectedValueTemp = parseFloat(expectedValue);
      globalVariableValueTemp = parseFloat(realGlobalVariableValue);
    } else {
      expectedValueTemp = expectedValue;
      globalVariableValueTemp = realGlobalVariableValue;
    }
    if (expectedValueTemp === 'undefined') {
      expectedValueTemp = undefined;
    }
    // console.log('checking Comparison', globalVariableValueTemp, comparsion, expectedValueTemp, new CheckPathLogic(comparsion, expectedValueTemp).isPassVariable(globalVariableValueTemp));
    return new CheckPathLogic(comparsion, expectedValueTemp).isPassVariable(globalVariableValueTemp);
  }

  /**
   * check pass multiple variable.
   */
  multiVariable() {
    const globalVariables = this.getGlobalVariables;
    let result = false;
    for (let index = 0; index < globalVariables.length; index += 1) {
      const globalVariableName = globalVariables[index];
      const realGlobalVariableValue = this.getGlobalVariableValue(globalVariableName);
      const comparison = this.getComparison[index];
      const expectedValue = this.getValues[index];
      // console.log('checkCompareVariable', { globalVariableName, expectedValue, comparison, realGlobalVariableValue, result: this.variable(realGlobalVariableValue, comparison, expectedValue) });
      if (index > 0) result = result || this.variable(realGlobalVariableValue, comparison, expectedValue);
      else result = this.variable(realGlobalVariableValue, comparison, expectedValue);
    }
    if (this.isNegativeForVariable) result = !result;
    // console.log('checkComaprison', this.isNegativeForVariable, result);
    return result;
  }

  /**
   * check pass category condition.
   */
  category() {
    if (!this.isAvailable) return true;
    const values = this.getValues;
    let result = false;
    for (let i = 0; i < values.length; i += 1) {
      if (i > 0) result = result || true;
      else result = true;
    }
    return result;
  }
}
