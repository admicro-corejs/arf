import BannerTerm from './BannerTerm';

export default class MultipleBannerTerm {
  constructor(optionBanners, env, defaultLogical) {
    this.optionBanners = optionBanners;
    this.defaultLogical = defaultLogical;
    this.env = env;
  }

  get isPass() {
    try {
      let isPass;

      if (this.optionBanners && this.optionBanners.length > 0) {
        let strCheck;
        for (let i = 0; i < this.optionBanners.length; i += 1) {
          const check = new BannerTerm(this.optionBanners[i], this.env, this.defaultLogical);
          if (i > 0) strCheck += check.logical + check.isPass;
          else strCheck = check.isPass;
          this.optionBanners[i].isPass = check.isPass;
        }
        // console.log('strCheck', strCheck, this.optionBanners);
        isPass = window.eval(strCheck);
      } else {
        isPass = true;
      }
      return isPass;
    } catch (error) {
      console.log('ArfError from BannerTerm', error);
      return false;
    }
  }
}
