import * as macroConfig from '../config/macroConfig';

export default class Macro {
  constructor(stringInput, obj) {
    this.stringInput = stringInput;
    this.obj = obj;
  }

  get macroTemplate() {
    return macroConfig.getAll();
  }

  getAllMacroInString() {
    const set1 = this.stringInput.match(/%%(.+?)%%/g) || [];
    const set2 = this.stringInput.match(/\[(.+?)\]/g) || [];
    const result = set1.concat(set2);
    return result;
  }

  macroValidate(macro) {
    return this.macroTemplate.filter(item => item.macro === macro).length > 0;
  }

  /**
   *  this @function just get macro value from object , which contain macro value like : Zone, Share, Placement, Banner.
   * @param {*} macro
   */
  getValueOfMacro(macro, custom) {
    if (custom && custom.filter(item => item.macro === macro)[0]) {
      return custom.filter(item => item.macro === macro)[0].value;
    }
    const macroObj = this.macroTemplate.filter(item => item.macro === macro)[0];
    if (!macroObj) return null;
    const type = macroObj.type;
    if (macroObj.value) {
      return macroObj.value();
    }
    if (Array.isArray(type)) {
      let value = null;
      const context = this;
      for (let index = 0; index < type.length; index += 1) {
        const key = type[index];
        if (typeof (value || context.obj)[key] === 'function') {
          value = (value || context.obj)[key].bind(context.obj)();
        } else {
          value = (value || context.obj)[key];
        }
      }
      return value;
    }
    const value = (this.obj[type] !== undefined || this.obj[type] !== null) ? this.obj[type] : '';
    if (typeof value === 'function') {
      const index = this.getIndex(macro);
      return (value.bind(this.obj))(index);
    }
    // console.log('MarcoField', macro, type, value);
    return value;
  }

  getIndex(macro) {
    try {
      const indexReg = /(?:\[)(.+?)(?:\])/g;
      const index = indexReg.exec(macro)[1];
      return index;
    } catch (error) {
      return 0;
    }
  }

  replaceMacro(isRemoveMacro, isEncode, custom) {
    try {
      const allMacro = this.getAllMacroInString();
      console.log('macro', this.obj.id, allMacro);
      let str = this.stringInput;
      for (let i = 0; i < allMacro.length; i += 1) {
        const macro = allMacro[i];
        if (!this.macroValidate(macro)) continue;
        const link = isRemoveMacro ? '' : this.getValueOfMacro(allMacro[i], custom);
        str = str.replace(macro, isEncode ? encodeURIComponent(link) : link);
      }
      return str;
    } catch (error) {
      console.log('ArfError from Marco Replace funrion', error);
      return this.stringInput;
    }
  }
}
