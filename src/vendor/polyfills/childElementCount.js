/* eslint-disable */
(function(constructor) {
  if (constructor &&
      constructor.prototype &&
      constructor.prototype.childElementCount == null) {
    Object.defineProperty(constructor.prototype, 'childElementCount', {
      get: function() {
        try {
          let i = 0;
          let count = 0;
          let node;
          let nodes = this.childNodes;
          while (node = nodes[i++]) {
            if (node.nodeType === 1) count++;
          }
          return count;
        } catch (error) {
          return 0;
        }
      }
    });
  }
})(window.Node || window.Element);