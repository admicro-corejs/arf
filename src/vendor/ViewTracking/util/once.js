/**
 * @function
 * @name once
 * @memberof twynUtils
 *
 * @param {function} callback The function to proxy.
 *
 * @returns {function} A proxy function that will only be invoked once.
 *
 * @description Returns a function that is restricted to invoking `callback`
 * once. Repeat calls to the function return the value of the first call.
 *
 * @example
 *
 * var calculateExpensiveNumber = function() { ... };
 * var expensiveNumber = once(calculateExpensiveNumber);
 *
 * var a = expensiveNumber() * 3.1415 + expensiveNumber();
 * // => exensiveNumber is actually invocing `calculateExpensiveNumber` only once
 *
 */
export default function once(callback) {
  let called = false, cache  // eslint-disable-line
  return () => {
    if (!called) {
      cache = callback(...arguments); // eslint-disable-line
      called = true;
    }
    return cache;
  };
}
