import isIframeContext from './isIframeContext';

export default function isFriendlyIframeContext(win) {
  try {
    if (!isIframeContext(win)) {
      return false;
    }
    const selfLocation = win.self.location;
    const topLocation = win.top.location;

    return selfLocation.protocol === topLocation.protocol &&
      selfLocation.host === topLocation.host &&
      selfLocation.port === topLocation.port;
  } catch (e) {
    return false;
  }
}
