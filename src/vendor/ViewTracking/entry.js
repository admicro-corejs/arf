import ViewTracking from './viewtracking';
import Utils from './utils';
import Strategy from './monitor/strategy/StrategyBase';
import CompositeStrategy from './monitor/strategy/CompositeStrategy';
import PollingStrategy from './monitor/strategy/PollingStrategy';
import EventStrategy from './monitor/strategy/EventStrategy';

import VisMon from './monitor/monitor';
import VisState from './helper/VisState';

ViewTracking.prototype.monitor = function (config) {
  return new VisMon(this, config);
};

function createInnerMonitor (outerMonitor, callback, config) {
  let timeElapsed = 0,
    timeStarted = null,
    timeLimit = config.timeLimit,
    percentageLimit = config.percentageLimit,
    interval = config.interval;
  return VisMon.Builder(outerMonitor.visobj()).strategy(new PollingStrategy({
    interval,
  })).on('update', (monitor) => {
    const percentage = monitor.state().percentage;
    if (percentageLimit > percentage) timeStarted = null; else {
      const now = Utils.now();
      timeStarted = timeStarted || now, timeElapsed = now - timeStarted;
    }
    timeElapsed >= timeLimit && (monitor.stop(), outerMonitor.stop(), callback(monitor));
  }).on('stop', () => {
    timeStarted = null;
  }).build();
};

function onPercentageTimeTestPassed (visobj, callback, config) {
  const _config = Utils.defaults(config, {
      percentageLimit: 1,
      timeLimit: 1e3,
      interval: 100,
      strategy: undefined,
    });
  const hiddenLimit = Math.max(_config.percentageLimit - 0.001, 0);
  let innerMonitor = null;
  const outerMonitor = VisMon.Builder(new ViewTracking(visobj.element(), {
      hidden: hiddenLimit,
      referenceWindow: visobj.referenceWindow(),
    })).set('strategy', _config.strategy).on('visible', (monitor) => {
      innerMonitor === null && (innerMonitor = createInnerMonitor(monitor, callback, _config)),
        innerMonitor.start();
    }).on('hidden', () => {
      innerMonitor !== null && innerMonitor.stop();
    }).on('stop', () => {
      innerMonitor !== null && innerMonitor.stop();
    }).build();
  return outerMonitor.start(), function () {
    outerMonitor.stop(), innerMonitor = null;
  };
};

ViewTracking.prototype.onPercentageTimeTestPassed = function (callback, config) {
  return onPercentageTimeTestPassed(this, callback, config);
};

function checkView(visobj, objParam) {
  const config = objParam.config || {
    percentageLimit: 0.5,
    timeLimit: 1000,
    interval: 100,
  };
  const inView = objParam.inView;
  const outView = objParam.outView;
  let isTrack = false;
  const monitor = visobj.monitor({
    visible() {
      if (!isTrack) {
        isTrack = true;
        visobj.onPercentageTimeTestPassed((monitor) => {
          inView(monitor);
          isTrack = false;
        }, config);
      }
    },
    hidden(monitor) {
      outView(monitor);
    },
  });
  monitor.start();
  return monitor;
}

ViewTracking.prototype.checkView = function (inViewCallBack, outViewCallBack, config) {
  return checkView(this, inViewCallBack, outViewCallBack, config);
};
/*
 * backward compatibility <1.0.0
*/

/*
 * ViewTracking should be able to be called without
 * "new" operator. e.g. ViewTracking(myElement)
 * */
function ViewTrackingWrapper(element, config) {
  return new ViewTracking(element, config);
}

Strategy.CompositeStrategy = CompositeStrategy;
Strategy.PollingStrategy = PollingStrategy;
Strategy.EventStrategy = EventStrategy;
VisMon.Strategy = Strategy;

ViewTrackingWrapper.Utils = Utils;
ViewTrackingWrapper.VisMon = VisMon;
ViewTrackingWrapper.VisState = VisState;

/**
 * @static
 * @method
 * @name of
 * @memberof ViewTracking
 *
 * @returns {ViewTracking} An initialized ViewTracking object.
 *
 * @description Constructs and returns a ViewTracking object.
 * This is syntactic sugar for `new ViewTracking(..)`.
 *
 * @example
 *
 * var myElement = document.getElementById('myElement');
 * var visElement = ViewTracking.of(myElement);
 *
 */
ViewTrackingWrapper.of = (element, config) => ViewTrackingWrapper(element, config);
ViewTrackingWrapper.fn = ViewTracking.prototype;

export default ViewTrackingWrapper;
