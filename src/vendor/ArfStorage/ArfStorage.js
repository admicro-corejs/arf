import LocalStorage from './LocalStorage';
import * as hash from './hash';

export default class ArfStorage {
  constructor() {
    if (window.checkLocalStorage === undefined) {
      window.checkLocalStorage = (function () {
        try {
          window.localStorage.setItem('_arfStorage', '');
          window.localStorage.removeItem('_arfStorage');
        } catch (a) {
          return !1;
        }
        return !0;
      }());
    }
    if (window.checkSessionStorage === undefined) {
      window.checkSessionStorage = (function () {
        try {
          window.sessionStorage.setItem('_arfStorage', '');
          window.sessionStorage.removeItem('_arfStorage');
        } catch (a) {
          return !1;
        }
        return !0;
      }());
    }
    this.isEncrypt = true;
    this.localStorage = new LocalStorage();
  }

  setStorage(name, value, expires, path, domain, secure) {
    if (this.isEncrypt && (name !== '__R' || name !== '__RC')) {
        value = hash.encryptCookieJson(value); // eslint-disable-line
    }
    if (window.checkLocalStorage) {
      this.localStorage.setItem(name, value, expires);
      if (name === '__R' || name === '__RC') {
        this.setCookie(name, value, expires, path, domain, secure);
      }
    } else {
      this.setCookie(name, value, expires, path, domain, secure);
    }
  }

  getStorage(name) {
    let value;
    if (window.checkLocalStorage) {
      if (name === '__R' || name === '__RC') {
        return this.getCookie(name);
      }
      value = this.localStorage.getItem(name);
    } else {
      value = this.getCookie(name);
    }
    value = value.replace(/([,;]|)+(timestamp)\w+([,;])/g, '');
    value = this.isEncrypt ? hash.decryptCookieJson(value) : value;
    return value;
  }
  setCookie(name, value, expires, path, domain, secure) {
    const pathTmp = (path === '') ? '/' : path;
    let expiresTmp = expires;
    if (expires === 0 || expires === '') {
      expiresTmp = (new Date(+(new Date()) + (864 * 1e5))).toGMTString();
    } else {
      expiresTmp = (new Date(+(new Date()) + (expires * 6e4))).toGMTString();
    }
    const r = [`${name}=${decodeURIComponent(value)}`];
    const s = {
      expires: expiresTmp,
      path: pathTmp,
      domain,
    };
    function addtos(i) {
      r.push(`${i}=${s[i]}`);
    }

    Object.keys(s).map(addtos);
    const res = secure && r.push('secure');
    document.cookie = r.join(';');
    return (
      res,
      document.cookie,
      true);
  }
  getCookie(cname) {
    if (document.cookie.length > 0) {
      let cstart = document.cookie.indexOf(`${cname}=`);
      if (cstart !== -1) {
        cstart = cstart + cname.length + 1;
        let cend = document.cookie.indexOf(';', cstart);
        if (cend === -1) cend = document.cookie.length;
        return decodeURIComponent(document.cookie.substring(cstart, cend));
      }
    }
    return '';
  }
  subCookie(cookie, type, start) {
    const cstart = cookie.indexOf(type, start);
    const cookLen = cookie.length - 1;
    if (cstart !== -1) {
      let cend = cookie.indexOf(';', cstart);
      if (cend === -1) {
        cend = cookLen;
      }
      return cookie.substring(cstart, cend);
    }
    return '';
  }
}
