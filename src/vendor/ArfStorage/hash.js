import * as encrypt from '../encrypt';

const key = '7Ziv7Zir7Ziq7Zit7Zis';
export function encryptCookieJson(cookie) {
  return encrypt.encode(cookie, key, 54814);
}
export function decryptCookieJson(cookieEncrypted) {
  return encrypt.decode(cookieEncrypted, key, 54814);
}
