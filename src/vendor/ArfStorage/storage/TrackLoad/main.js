import config from '../../../../config/cookieConfig';
import ChannelDetail from './ChannelDetail';

export default class TrackLoad {
  constructor(arfStorage, currentChannelLink) {
    this.arfStorage = arfStorage;
    this.trackLoadConfig = config.trackLoad;
    this.expired = 30; // 30 minutes

    this.currentChannelLink = currentChannelLink;
    let cookieTime = this.arfStorage.getStorage(this.trackLoadConfig.name);
    if (cookieTime === '') {
      cookieTime = {};
    } else {
      try {
        cookieTime = JSON.parse(cookieTime);
      } catch (err) {
        cookieTime = {};
      }
    }
    this.history = cookieTime.history || {};
    this.pageload = Array.isArray(cookieTime.pageload) ? cookieTime.pageload : [];
    this.timestamp = cookieTime.timestamp || this.getTimestamp();
    if (this.getExpired() <= this.getTimestamp()) {
      this.history = {};
      this.pageload = [];
      this.timestamp = this.getTimestamp();
    }
  }
  getTimestamp() {
    const current = new Date();
    return current.getTime();
  }
  getExpired() {
    const ts = new Date(this.timestamp);
    return ts.setMinutes(ts.getMinutes() + this.expired);
  }
  // getDomain() {
  //   const arr = this.currentChannelLink.split('/');
  //   const result = `${arr[0]}//${arr[2]}`;
  //   return result;
  // }
  init(data) {
    const { zoneId, totalShare, banners } = data;
    const hisory = new ChannelDetail(data || { banners, totalShare });
    this.history[zoneId] = hisory;
  }

  pushPageloadLog(data) {
    try {
      const { campaignId, pageLoad, loadId } = data;
      this.pageload.push({
        campaignId,
        pageLoad,
        loadId,
      });
    } catch (error) {
      console.log('Arf Error from push pageload history (pushPageloadLog)', error);
    }
  }

  clearPageload() {
    this.pageload = [];
  }

  changeInStore(zoneId, data) {
    for (const key in this.history[zoneId]) { // eslint-disable-line
      if (this.history[zoneId].hasOwnProperty(key)) { // eslint-disable-line
        const value = data[key];
        this.history[zoneId][key] = value;
      } else {
        delete this.history[zoneId][key];
      }
    }
  }

  readZone(zoneId, totalShare) {
    if (this.history[zoneId] === undefined) this.history[zoneId] = {}; // eslint-disable-line
    let currentTrack = this.history[zoneId];
    if (currentTrack === undefined) {
      const initChannelDetail = new ChannelDetail(totalShare ? { totalShare } : {});
      this.history[zoneId] = initChannelDetail; // eslint-disable-line
      currentTrack = initChannelDetail;
    }
    const currentTotalShare = currentTrack.totalShare;
    // if total share change, re-init channel details
    if (totalShare && totalShare !== currentTotalShare) {
      const initChannelDetail = new ChannelDetail({ totalShare });
      this.history[zoneId] = initChannelDetail; // eslint-disable-line
      currentTrack = initChannelDetail;
    }
    return new ChannelDetail(currentTrack);
  }

  formatZoneData(data, totalPageload) {
    return data instanceof ChannelDetail ? data : new ChannelDetail(data, totalPageload);
  }

  write() {
    const strCookieTime = JSON.stringify({ channel: this.currentChannelLink, history: this.history, pageload: this.pageload, timestamp: this.timestamp });
    console.log('TrackLoad', strCookieTime);
    this.arfStorage.setStorage(this.trackLoadConfig.name, strCookieTime, '', '/', this.currentChannelLink);
  }

  clearTrack() {
    this.arfStorage.setStorage(this.trackLoadConfig.name, '', '', '/', this.currentChannelLink);
  }
}
