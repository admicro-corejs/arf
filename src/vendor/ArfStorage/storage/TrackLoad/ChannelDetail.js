import Turn from './Turn';

export default class ChannelDetail {
  constructor(detail, totalPageload) {
    this.turns = detail.turns || [];
    this.nextTurn = detail.nextTurn || new Turn();
    this.totalShare = detail.totalShare || 3;
    this.turnsLeft = detail.turnsLeft || this.totalShare;
    this.totalPageload = totalPageload || 3;
    this.isPageLoad = detail.isPageLoad || true;
    this.banners = detail.banners || [];

    if (detail.turnsLeft <= 0) {
      this.turnsLeft = this.totalShare;
      if (this.isPageLoad) this.turns = [];
    }
    if ((this.turns.length >= this.getLowestCommonMultiple())) {
      this.turns = [];
    }
  }

  addTurn(turn) {
    const turnData = turn instanceof Turn ? turn : new Turn(turn);
    this.turns.push(turnData);
  }

  getLowestCommonMultiple() {
    const greatestCommonDivisor = (() => {
      let a = Math.abs(this.totalShare);
      let b = Math.abs(this.totalPageload);
      if (a === 0 || b === 0) return a + b;
      while (a !== b) {
        if (a > b) a -= b;
        else b -= a;
      }
      return a;
    })();
    return (Math.abs(this.totalShare) * Math.abs(this.totalPageload)) / greatestCommonDivisor;
  }

  countTurnsLeft(turnLeft) {
    console.log('checkTurnLeft', { private: this.turnsLeft, global: turnLeft });
    this.turnsLeft = turnLeft !== null && turnLeft !== undefined ? turnLeft : this.turnsLeft;
    this.turnsLeft -= 1;
    return this.turnsLeft;
  }
}
