export default class Turn {
  constructor(placements, shareId, structure, pageLoadId, cpmFlag) {
    this.placements = placements || [];
    this.shareId = shareId || '';
    this.structure = structure || [];
    this.pageLoadId = pageLoadId || '';
    this.cpmFlag = cpmFlag || false;
  }
}
