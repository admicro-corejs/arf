export default class MessageEvent {
  constructor(name, scopes, data) {
    this.name = name;
    this.data = data;
    this.scopes = scopes;
    this.isResnponse = false;
    this.channel = 'cms-arf';
  }
}
