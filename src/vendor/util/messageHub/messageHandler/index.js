import globalHandler from './globalMsg';
import iframeSize from './iframeSize';

class MessageHandler {
  constructor() {
    this.messages = [];
  }

  handler(msgData, ...extentDatas) {
    if (msgData.scopes.indexOf('global') !== -1) {
      console.log('hanndleGlobal');
      globalHandler(msgData);
    } else if (msgData.name.indexOf('bodySize') !== -1) {
      iframeSize(extentDatas[0], msgData);
    }
  }

  /**
   *
   * @param {*} args : [0] bannerID, [1]: target Iframe
   */
  handlerAll(...args) {
    const msgsFilter = this.messages.filter(item => (item.scopes.indexOf(args[0]) !== -1 || item.scopes.indexOf('global') !== -1));
    if (msgsFilter.length === 0) return;
    for (let index = 0; index < msgsFilter.length; index += 1) {
      const msg = msgsFilter[index];
      this.handler(msg, args[1]);
      this.reponseMessage(index, msg, args[0]);
    }
    console.log('run handle msg', this.messages);
  }

  pushMessage(msg) {
    this.messages.push(msg);
  }

  reponseMessage(index, msg, bannerId) {
    msg.response.push(bannerId);
    this.messages.splice(index, 1, msg);
  }
}

export default new MessageHandler();

// export default function messageHandler(msgData, ...extentDatas) {
//   if (msgData.scopes.indexOf('global') !== -1) {
//     globalHandler(msgData);
//   } else if (msgData.name.indexOf('bodySize') !== -1) {
//     iframeSize(extentDatas[0], msgData);
//   }
// }
