import MessageEvent from '../MessageEvent';

export default function handler(data) {
  if (data.name === 'parent') {
    const msg = new MessageEvent('parent', [], '');
    data.source.postMessage(msg, '*');
  }
}
