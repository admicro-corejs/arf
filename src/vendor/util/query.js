export default class Query {
  constructor(store, zoneRawData, zoneData, zoneId) {
    this.store = store;
    if (store && !zoneId) {
      this.data = store.state.ZONE_DATA;
    } else if (zoneRawData) {
      this.data = {};
      this.data[zoneRawData.id] = zoneRawData;
    } else if (zoneData) {
      const raw = {
        propsData: {
          model: zoneData,
        },
      };
      this.data = {};
      this.data[zoneData.id] = raw;
    } else {
      this.data = [store.state.ZONE_DATA[zoneId]];
    }
  }

  getZone(key, value) {
    try {
      const result = [];
      for (const zone in this.data) { // eslint-disable-line
        if (this.data[zone].propsData.model[key] === value) {
          result.push(this.data[zone].propsData.model);
        }
      }
      return result;
    } catch (error) {
      console.log('ArfError from Query.getZone', error);
      return [];
    }
  }

  getShare(key, value) {
    try {
      const result = [];
      for (const zone in this.data) { // eslint-disable-line
        const shares = this.data[zone].propsData.model.shares;
        if (shares.length > 0) {
          for (let i = 0, length = shares.length; i < length; i += 1) {
            const share = shares[i];
            if (!share) continue;
            let valueTemp = '';
            if (key === 'id') {
              valueTemp = (value.length > 8 && value[0] === 's') ? value.substring(1) : value;
            } else {
              valueTemp = value;
            }
            if (share && share[key] === valueTemp) {
              result.push(share);
            }
          }
        }
      }
      return result;
    } catch (error) {
      return [];
    }
  }

  getShareplacement(key, value) {
    try {
      const result = [];

      for (const zone in this.data) { // eslint-disable-line
        const shares = this.data[zone].propsData.model.shares;
        if (shares.length > 0) {
          for (let i = 0, numberOfShare = shares.length; i < numberOfShare; i += 1) {
            const share = shares[i];
            if (!share) continue;
            share.sharePlacements = share.sharePlacements || [];
            for (let j = 0, numberOfPlacement = share.sharePlacements.length; j < numberOfPlacement; j += 1) {
              const sharePlacements = share.sharePlacements[j];
              if (!sharePlacements) continue;
              if (sharePlacements && sharePlacements[key] === value) {
                result.push(sharePlacements);
              }
            }
          }
        }
      }

      return result;
    } catch (error) {
      return [];
    }
  }

  getPlacement(key, value) {
    try {
      const result = [];
      for (const zone in this.data) { // eslint-disable-line
        const shares = this.data[zone].propsData.model.shares;
        if (shares.length > 0) {
          for (let i = 0, numberOfShare = shares.length; i < numberOfShare; i += 1) {
            const share = shares[i];
            if (!share) continue;
            share.sharePlacements = share.sharePlacements || [];
            for (let j = 0, numberOfPlacement = share.sharePlacements.length; j < numberOfPlacement; j += 1) {
              const placement = share.sharePlacements[j].placement;
              if (!placement) continue;
              let valueTemp = '';
              if (key === 'id') {
                valueTemp = (value.length > 8 && value[0] === 'p') ? value.substring(1) : value;
              } else {
                valueTemp = value;
              }
              if (placement[key] && placement[key] === valueTemp) {
                result.push(placement);
              }
            }
          }
        }
      }

      return result;
    } catch (error) {
      return [];
    }
  }

  getBanner(key, value) {
    try {
      const result = [];

      for (const zone in this.data) { // eslint-disable-line
        const shares = this.data[zone].propsData.model.shares;
        if (shares.length > 0) {
          for (let i = 0, numberOfShare = shares.length; i < numberOfShare; i += 1) {
            const share = shares[i];
            if (!share) continue;
            share.sharePlacements = share.sharePlacements || [];
            for (let j = 0, numberOfPlacement = share.sharePlacements.length; j < numberOfPlacement; j += 1) {
              const sharePlacement = share.sharePlacements[j];
              if (!sharePlacement) continue;
              const banners = sharePlacement.placement.banners;
              for (let k = 0; k < banners.length; k += 1) {
                const banner = banners[i];
                if (!banner) continue;
                let valueTemp = '';
                if (key === 'id') {
                  valueTemp = (value.length > 8 && value[0] === 'b') ? value.substring(1) : value;
                } else {
                  valueTemp = value;
                }
                if (banner && banner[key] === valueTemp) {
                  result.push(banner);
                }
              }
            }
          }
        }
      }

      return result;
    } catch (error) {
      return [];
    }
  }

  getFunction(name) {
    if (name === 'banner') {
      return this.getBanner;
    } else if (name === 'placement') {
      return this.getPlacement;
    } else if (name === 'share') {
      return this.getShare;
    }
    return this.getZone;
  }
}
