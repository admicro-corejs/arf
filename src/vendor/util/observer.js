export default class observer {
  constructor(callback) {
    this.observerConfig = {
      attributes: true, childList: true, characterData: true, subtree: true,
    };
    this.mutation = MutationObserver ? new MutationObserver(callback) : () => {};
  }

  start(target, config) {
    this.mutation.observe(target, config || this.observerConfig);
  }

  startAfter(target, time) {
    setTimeout(() => {
      this.mutation.observe(target, this.observerConfig);
    }, time);
  }

  disconnect() {
    this.mutation.disconnect();
  }

  disconnectAfter(time, callback) {
    setTimeout(() => {
      this.mutation.disconnect();
      if (callback) callback();
    }, time);
  }
}
