// get elements of last block cycle.
export default function getLastElementsInArray(array, numberOfElements) {
  const index = -Math.max(array.length % (numberOfElements), 0);
  const result = index ? array.slice(index) : [];
  return result;
}
