export function getCurrentBrowser() {
  let tem;
  let M;
  const ua = navigator.userAgent;
  M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
  if (/trident/i.test(M[1])) {
    tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
    return `IE ${tem[1] || ''}`;
  }
  if (M[1] === 'Chrome') {
    tem = ua.match(/\b(OPR|Edge|Edg)\/(\d+)/);
    if (tem != null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
  }
  M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
  tem = ua.match(/version\/(\d+)/i);
  if (tem != null) {
    M.splice(1, 1, tem[1]);
  }
  const currentBrowser = M.join(' ').substring(0, (M.join(' ').indexOf(' '))).toLowerCase();
  return currentBrowser;
}

export function checkBrowser(s) {
  let browser = s;
  browser = (typeof (browser) === 'undefined' ||
    browser === 'undefined' ||
    browser == null ||
    browser === '') ? 0 : browser;
  browser = `,${browser},`.toLowerCase();
  return (browser !== ',,' && browser !== ',0,') ? (`${browser}`.indexOf(this.getCurrentBrowser()) !== -1) : true;
}
