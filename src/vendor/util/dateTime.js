// import xDate from '../xDate';
class DateTime {
  now() {
    return (new Date()).getTime();
  }

  Value(dateTimeStr) {
    try {
      return new Date(dateTimeStr);
    } catch (error) {
      return 0;
    }
  }
}

export default function checkTime(startTime, endTime) {
  const dateTime = new DateTime();
  const startTimeCheck = startTime ? dateTime.Value(startTime) <= dateTime.now() : true;
  const endTimeCheck = endTime ? dateTime.Value(endTime) >= dateTime.now() : true;
  return startTimeCheck && endTimeCheck;
}
