export default function getCurrentDomain(type) {
  const url = document.URL;
  const ref = document.referrer;
  let http = (type === 'pageUrl') ? url.replace(/\?i=([0-9]+)&bz=([0-9]+)&z=([0-9]+)#([0-9_0-9]+)/g, '') : ref.replace(/\?i=([0-9]+)&bz=([0-9]+)&z=([0-9]+)#([0-9_0-9]+)/g, '');
  const arrUrlReg = http.match(/([^|]+)/i);
  if (arrUrlReg) {
    http = `${arrUrlReg[0]}`;
  }
  return http.toLowerCase();
}
