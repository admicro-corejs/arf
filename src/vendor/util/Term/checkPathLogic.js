// import getPath2Check from './getPath2Check';
import stringPosition from './stringPosition';
import regExpFromString from './regExpFromString';

export default class checkPathLogic {
  constructor(comparison, valueCompare) {
    this.comparison = comparison;
    this.valueCompare = valueCompare;
  }
  isPassVariable(globalVariable) {
    let value;
    if (!isNaN(globalVariable) || globalVariable === undefined || globalVariable === null) value = globalVariable;
    else value = decodeURIComponent(`${globalVariable}`);
    return this.checking(value);
  }

  /* using for page URL or Referrer URL */
  isPassUrl(url) {
    const decodeUrl = decodeURIComponent(url);
    // console.log('compareURL', this.valueCompare, this.comparison, decodeUrl, this.checking(decodeUrl));
    return this.checking(decodeUrl);
  }

  checking(value) {
    // console.log('CheckingCompare', value, this.comparison, this.valueCompare);
    switch (this.comparison) {
      case '>':
        return (value > this.valueCompare);
      case '>=':
        return (value >= this.valueCompare);
      case '<':
        return (value < this.valueCompare);
      case '<=':
        return (value <= this.valueCompare);
      case '==':
        return (value == this.valueCompare); // eslint-disable-line
      case '!=':
        return (value != this.valueCompare); // eslint-disable-line
      case '=~':
        return stringPosition(value, this.valueCompare, 0);
      case '!~':
        return !stringPosition(value, this.valueCompare, 0);
      case '=x': {
        // const regConfig = this.valueCompare.match(/[^(/)]*[^(/)]/g);
        // const reg = new RegExp(regConfig[0], regConfig[1]);
        const reg = regExpFromString(this.valueCompare);
        return reg.test(value);
      }
      case '!x': {
        // const regConfig = this.valueCompare.match(/[^(/)]*[^(/)]/g);
        // const reg = new RegExp(regConfig[0], regConfig[1]);
        const reg = regExpFromString(this.valueCompare);
        return !reg.test(value);
      }
      default:
        return false;
    }
  }
}

