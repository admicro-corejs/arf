export default function stringPosition(path, data, offset) {
  const haystack = (`${path}`).toLowerCase();
  const needle = (`${data}`).toLowerCase();
  return haystack.indexOf(needle, offset) !== -1;
}
