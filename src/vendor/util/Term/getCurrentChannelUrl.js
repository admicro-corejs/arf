import getCurrentDomain from './getCurrentDomain';

export default function getCurrentChannelUrl(type) {
  const url = getCurrentDomain(type);
  // return encodeURIComponent(url.match(/([^http://])([^?]*)/)[0]);
  return url;
}
