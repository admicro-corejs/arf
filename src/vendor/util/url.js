export function getParameterByName(name, url) {
  if (!url) url = window.location.href; // eslint-disable-line
  name = name.replace(/[[\]]/g, '\\$&'); // eslint-disable-line
  const regex = new RegExp(`[?&]${name}(=([^&#]*)|&|#|$)`);
  const results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

export function getHashtag() {
  return window.location.hash;
}

export function getOrigin(url) {
  const pathArray = url.split('/');
  const protocol = pathArray[0];
  const host = pathArray[2];
  const origin = `${protocol}//${host}`;
  return origin;
}
