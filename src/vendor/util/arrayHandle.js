// flatten array
// export function flatten(arr) {
//   return arr.reduce((acc, val) => acc.concat(Array.isArray(val) ? flatten(val) : val), []);
// }
// Clone Array that don't reference to Array copy.
// export function cloneArray(arr) {
//   let copy;
//   if (arr === null || typeof arr !== 'object') return arr;

//   // Handle Array
//   if (arr instanceof Array) {
//     copy = [];
//     for (let i = 0, len = arr.length; i < len; i += 1) {
//       copy[i] = cloneArray(arr[i]);
//     }
//     return copy;
//   }

//   throw new Error('Unable to copy array!.');
// }

export function uniqueItem(arr) { // eslint-disable-line
  const n = {};
  const r = [];
  for (let i = 0; i < arr.length; i += 1) {
    if (!n[arr[i]]) {
      n[arr[i]] = true;
      r.push(arr[i]);
    }
  }
  return r;
}
// permute item in array and return a array store permutations
// export function permuteArray(array) {
//   const permArr = [];
//   const usedChars = [];
//   const UniqueItem = (arr) => {
//     const n = {};
//     const r = [];
//     for (let i = 0; i < arr.length; i += 1) {
//       if (!n[arr[i]]) {
//         n[arr[i]] = true;
//         r.push(arr[i]);
//       }
//     }
//     return r;
//   };
//   const permute = (input) => {
//     let i;
//     let ch;
//     for (i = 0; i < input.length; i += 1) {
//       ch = input.splice(i, 1)[0];
//       usedChars.push(ch);
//       if (input.length === 0) {
//         permArr.push(usedChars.slice());
//       }
//       permute(input);
//       input.splice(i, 0, ch);
//       usedChars.pop();
//     }
//     return UniqueItem(permArr);
//   };
//   return permute(array);
// }

// export function kCombinations(set, k) {
//   let i;
//   let j;
//   let combs;
//   let head;
//   let tailcombs;

//   // There is no way to take e.g. sets of 5 elements from
//   // a set of 4.
//   if (k > set.length || k <= 0) {
//     return [];
//   }

//   // K-sized set has only one K-sized subset.
//   if (k === set.length) {
//     return [set];
//   }

//   // There is N 1-sized subsets in a N-sized set.
//   if (k === 1) {
//     combs = [];
//     for (i = 0; i < set.length; i += 1) {
//       combs.push([set[i]]);
//     }
//     return combs;
//   }

//   combs = [];
//   for (i = 0; i < (set.length - k) + 1; i += 1) {
//     // head is a list that includes only our current element.
//     head = set.slice(i, i + 1);
//     // We take smaller combinations from the subsequent elements
//     tailcombs = this.kCombinations(set.slice(i + 1), k - 1);
//     // For each (k-1)-combination we join it with the current
//     // and store it to the set of k-combinations.
//     for (j = 0; j < tailcombs.length; j += 1) {
//       combs.push(head.concat(tailcombs[j]));
//     }
//   }
//   return combs;
// }
// create these combinations k-set.length of array set => collection into array
// export function combinations(set) {
//   let kCombs;
//   const combs = [];

//   // Calculate all non-empty k-combinationsr
//   for (let k = 1; k <= set.length; k += 1) {
//     kCombs = this.kCombinations(set, k);
//     for (let i = 0; i < kCombs.length; i += 1) {
//       combs.push(kCombs[i]);
//     }
//   }
//   return combs;
// }

// export function checkTwoArrayEqual(arr1, arr2) {
//   if (arr1.length !== arr2.length) {
//     return false;
//   }
//   return JSON.stringify(arr1) === JSON.stringify(arr2);
// }

// export function getIntersect(arr1, arr2) {
//   const r = [];
//   const o = {};
//   let l = arr2.length;
//   let i;
//   let v;
//   for (i = 0; i < l; i += 1) {
//     o[arr2[i]] = true;
//   }
//   l = arr1.length;
//   for (i = 0; i < l; i += 1) {
//     v = arr1[i];
//     if (v in o) {
//       r.push(v);
//     }
//   }
//   return r;
// }
