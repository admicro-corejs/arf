export default class Chooser {
  constructor(set) {
    this.set = set;
  }

  setChecking() {
    return this.set.length > 0;
  }

  baseOnWeight(weightName) {
    if (!this.setChecking()) return null;
    const randomNumber = Math.random() * 100;
    // function weightValue() belong to objects in /models folder.
    const ratio = this.set.reduce((acc, item) => (item.weightValue(weightName) + acc), 0) / 100;
    if (!ratio) {
      return this.random();
    }
    const result = this.set.reduce((acc, item) => {
      const nextRange = acc + (item.weightValue(weightName) / ratio);

      if (typeof acc === 'object') {
        return acc;
      }

      if (randomNumber >= acc && randomNumber < nextRange) {
        return item;
      }

      return nextRange;
    }, 0);
    return result;
  }

  baseOnField(weightName) {
    if (!this.setChecking()) return null;
    const randomNumber = Math.random() * 100;
    // function weightValue() belong to objects in /models folder.
    const ratio = this.set.reduce((acc, item) => (item[weightName] + acc), 0) / 100;
    if (!ratio) {
      return this.random();
    }
    const result = this.set.reduce((acc, item) => {
      const nextRange = acc + (item[weightName] / ratio);

      if (typeof acc === 'object') {
        return acc;
      }

      if (randomNumber >= acc && randomNumber < nextRange) {
        return item;
      }

      return nextRange;
    }, 0);
    return result;
  }

  random() {
    if (!this.setChecking()) return null;
    return this.set[Math.floor(Math.random() * this.set.length)];
  }
}
