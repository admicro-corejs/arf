export default function objectForLoop(object, doSomething) {
  for (const key in object) { // eslint-disable-line
    let option;
    if (Object.prototype.hasOwnProperty.call(object, key) || {}.hasOwnProperty.call(object, key)) {
      option = doSomething(key);
    }
    if (option === 'break') break;
    if (option === 'continue') continue;
  }
}
