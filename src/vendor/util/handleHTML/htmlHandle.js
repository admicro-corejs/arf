import ScriptHandle from './scriptHandle';

class HtmlHandle {
  constructor(html, el, store) {
    this.html = html;
    this.el = el;
    this.store = store;
  }

  createStackHtml() {
    const result = [];
    const htmlCreateMacro = this.html.replace(/<script\b[^>]*>([\s\S]*?)<\/script>/gm, '%%script_tag%%');
    const htmls = htmlCreateMacro.split(/(%%script_tag%%=?)/).filter(item => item !== '' && item !== '\n');
    const scripts = this.html.match(/<script\b[^>]*>([\s\S]*?)<\/script>/gm);

    for (let index = 0; index < htmls.length; index += 1) {
      const element = htmls[index];
      if (element === '%%script_tag%%') {
        const script = scripts.shift();
        if (script) result.push({ html: script, type: 'script' });
      } else {
        result.push({ html: element, type: 'normal' });
      }
    }

    return result;
  }
  // Visualization of position names
  //   <!-- beforebegin -->
  // <p>
  //   <!-- afterbegin -->
  //   foo
  //   <!-- beforeend -->
  // </p>
  // <!-- afterend -->
  async injecttHTML(position) {
    if (!this.el) return;
    const stack = this.createStackHtml();
    console.log('checkStack', stack);

    const wrap = this.el;
    if (wrap) {
      let lastIdMocking;
      for (let index = 0; index < stack.length; index += 1) {
        const idMock = `mocking${Math.floor(Math.random() * 100000000)}`;
        const mock = `<div id="${idMock}"></div>`;
        const element = stack[index];
        const nextPosition = index === 0 ? position : 'afterend';
        if (index < stack.length - 1) element.html = `${element.html}\n${mock}`;
        const mockEl = lastIdMocking ? document.getElementById(lastIdMocking) : lastIdMocking;
        if (element.type === 'script') {
          const scriptHandle = new ScriptHandle(element.html, this.store);
          await scriptHandle.executeJS(); // eslint-disable-line
          if (mockEl) {
            mockEl.insertAdjacentHTML(nextPosition, element.html);
            mockEl.remove();
          } else this.el.insertAdjacentHTML(nextPosition, element.html);
        } else if (mockEl) {
          mockEl.insertAdjacentHTML(nextPosition, `${element.html}`);
          mockEl.remove();
        } else this.el.insertAdjacentHTML(nextPosition, element.html);
        lastIdMocking = idMock;
      }
    }
  }

  async injecttHTMLBeforeBegin() {
    await this.injecttHTML('beforebegin');
  }

  async injecttHTMLAfterBegin() {
    await this.injecttHTML('afterbegin');
  }

  async injecttHTMLBeforeEnd() {
    await this.injecttHTML('beforeend');
  }

  async injecttHTMLAfterEnd() {
    await this.injecttHTML('afterend');
  }


}

export default function main(html, el) {
  return new HtmlHandle(html, el);
}
