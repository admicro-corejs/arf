// import installScript from './installScript';

class ScriptHandle {
  constructor(html, elOrId) {
    this.scriptType = {
      evalScript: 1,
      scriptUrl: 2,
    };
    this.html = html;
    if (elOrId && typeof elOrId === 'string') {
      this.el = document.getElementById(elOrId);
    } else {
      this.el = elOrId;
    }
  }

  /**
   * @function getScriptTag : get script from html string
   * @returns {Array} script and script type.
  */
  getScriptTag() {
    let element = this.html;
    const scripts = [];
    const trim = (str) => {
      let strTemp = str;
      strTemp = strTemp.replace(/^\s+/, '');
      for (let i = strTemp.length - 1; i >= 0; i -= 1) {
        if (/\S/.test(strTemp.charAt(i))) {
          strTemp = strTemp.substring(0, i + 1);
          break;
        }
      }
      return strTemp;
    };
    // boc tach script
    const allScriptTag = this.html.match(/<script\b[^>]*>([\s\S]*?)<\/script>/gm);

    if (allScriptTag) {
      let jsCodeInsideScriptTag = '';
      for (let i = 0, len = allScriptTag.length; i < len; i += 1) {
        const script = {
          type: '',
          value: '',
          index: i,
          attributes: [],
        };
        element = element.replace(allScriptTag[i], '');
        const attributes = this.getAttributes(allScriptTag[i]);
        script.attributes = attributes;
        script.html = allScriptTag[i];

        const srcAttribute = attributes.filter(att => att[0] === 'src')[0];
        if (srcAttribute) {
          script.type = this.scriptType.scriptUrl;
          scripts.push(script);
        } else {
          jsCodeInsideScriptTag = allScriptTag[i].replace(/<script\b[^>]*>([\s\S]*?)<\/script>/gm, '$1');
          if (trim(jsCodeInsideScriptTag) !== '') {
            script.type = this.scriptType.evalScript;
            script.value = trim(jsCodeInsideScriptTag);
            scripts.push(script);
          }
        }

        // const srcAttributeSingleQuote = allScriptTag[i].match(/(src='([^"]*)')|(src="([^"]*)")/gi);
        // if (srcAttributeSingleQuote) {
        //   const linkSrc = srcAttributeSingleQuote[0].replace(/src='([^"]*)'/gi, '$1');
        //   script.type = this.scriptType.scriptUrl;
        //   script.value = linkSrc;

        //   scripts.push(script);
        // }
      }
    }
    return scripts;
  }

  getAttributes(scriptTag) {
    const regexForRemoveUnnessesaryChar = />[\s\S]*?<\/script>/gm;
    const summary = scriptTag.replace(regexForRemoveUnnessesaryChar, '');
    const regex = /(([a-z]*?)=("|')(.*?)("|'))|\w+/gm;
    const separateAttributes = summary.match(regex);
    separateAttributes.shift();

    if (!separateAttributes) return [];

    return separateAttributes.map((item) => {
      const separateKeyVaue = item.match(/(([a-z]).*?(?=(=")|(=')))|([^"=\s]=?)(.*?)(?="$)|([^'=\s]=?)(.*?)(?='$)/gm);
      if (!separateKeyVaue) {
        return [item, true];
      }
      return separateKeyVaue;
    });
  }

  installScript(attributes, scriptElement) {
    let isResolved = false;
    const timeout = 5000;
    return new Promise((resolve) => {
      if (attributes.filter(item => item[0] === 'async' && (item[1] === true || item[1] === 'true')).length > 0) resolve('installScriptDone');
      setTimeout(() => {
        if (!isResolved) resolve('timeout');
      }, timeout);
      const newScriptTag = scriptElement || document.createElement('script');
      newScriptTag.type = 'text/javascript';
      const onloadValue = attributes.filter(item => item[0] === 'onload')[0];
      newScriptTag.onload = function () {
        isResolved = true;
        resolve('installScriptDone');
        if (onloadValue) window.eval(onloadValue[1]);
        newScriptTag.remove();
      };
      newScriptTag.onerror = function () {
        isResolved = true;
        resolve('installScriptDone');
        if (onloadValue) window.eval(onloadValue[1]);
        newScriptTag.remove();
      };
      for (let index = 0; index < attributes.length; index += 1) {
        const att = attributes[index];
        if (att[0] === 'onload') continue;
        newScriptTag[att[0]] = att[1];
      }
      document.head.appendChild(newScriptTag);
      // const firstScriptTag = document.getElementsByTagName('script')[0];
      // firstScriptTag.parentNode.insertBefore(newScriptTag, firstScriptTag);
      // if (!el) {
      //   const firstScriptTag = document.getElementsByTagName('script')[0];
      //   firstScriptTag.parentNode.insertBefore(newScriptTag, firstScriptTag);
      // } else {
      //   el.appendChild(newScriptTag);
      // }
    });
  }

  /**
   * Excute js from script tag contain : script from url, script inside tag.
   * @param {*} el : place to install script.
   * @param {*} executedTime : time to execute js from url (after onload), default value = 0ms.
   */
  async executeJS(callback) {
    const scripts = this.getScriptTag();
    // console.log('evalScript', scripts);
    for (let index = 0; index < scripts.length; index += 1) {
      const script = scripts[index];
      try {
        if (script.type === this.scriptType.scriptUrl) {
          await this.installScript(script.attributes); // eslint-disable-line no-await-in-loop
        } else {
          window.eval(script.value);
        }
      } catch (error) {
        window.console.error(error);
      }
      if (callback) callback(script.html);
    }
  }
}

export default ScriptHandle;
