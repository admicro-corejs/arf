/**
 * @function {installScript} excute js from url
 * @param {*} el : place to install script.
 * @param {*} url : url of script.
 * @param {*} executedTime : time to execute.
 */
export default async function installScript(el, url, isAsync, executedTime, store, isReInstall) {
  const asyncFlag = (typeof isAsync === 'boolean') ? isAsync : true;
  return new Promise((resolve) => {
    if (!isReInstall && store) {
      const isExecutedScript = store.getters.isScriptExecuted(url);
      if (isExecutedScript) {
        setTimeout(() => {
          resolve('installScriptDone');
        }, ((executedTime) || 0));
        return;
      }
    }
    const newScriptTag = document.createElement('script');
    if (!el) {
      const firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(newScriptTag, firstScriptTag);
    } else {
      el.appendChild(newScriptTag);
    }
    newScriptTag.type = 'text/javascript';
    newScriptTag.onload = function () {
      setTimeout(() => {
        resolve('installScriptDone');
        if (store) store.commit('PUSH_REMOTE_SCRIPT_LOADED', url); // commit script to store
      }, ((executedTime) || 0));
    };
    newScriptTag.onerror = function () {
      setTimeout(() => {
        resolve('installScriptDone');
        if (store) store.commit('PUSH_REMOTE_SCRIPT_LOADED', url); // commit script to store
      }, ((executedTime) || 0));
    };
    newScriptTag.async = asyncFlag;
    newScriptTag.src = url;
  });
}
