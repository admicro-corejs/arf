// import * as macroConfig from '../../../config/macroConfig';
// import * as screen from '../../util/screen';
import Macro from '../../Macro';
// import msgHandler from '../../util/messageHub/messageHandler';
// import MessageEvent from '../messageHub/MessageEvent';
import Observer from '../observer';

export class IframeHandler { // eslint-disable-line
  /**
   * @param {ElementHTML} target: element wrap iframe
   * @param {*} bannerObject: banner Onject
   * @param {*} onload: onload callback
   * @param {*} isMobile
   */
  constructor(bannerObject, onload, isMobile) {
    this.bannerComponent = bannerObject;
    this.bannerObject = bannerObject.current;
    this.isMobile = isMobile;
    this.src = this.getIframeSrc();
    // this.addBannerId(this.bannerObject.id);
    // this.addParamMacro();
    this.replaceMacro();
    this.el = this.createIframe(onload);
    this.observe = null;
  }

  getIframeSrc() {
    const iframe = [];
    // boc tach script
    const allIframeTag = this.bannerObject.html.match(/<iframe\b[^>]*>([\s\S]*?)<\/iframe>/gm);

    if (allIframeTag) {
      for (let i = 0, len = allIframeTag.length; i < len; i += 1) {
        let IfraneSrc = '';

        const srcAttribute = allIframeTag[i].match(/src="([^"]*)"/gi);
        if (srcAttribute) {
          const linkSrc = srcAttribute[0].replace(/src="([^"]*)"/gi, '$1');
          IfraneSrc = linkSrc;

          iframe.push(IfraneSrc);
        }
      }
    }
    return iframe[0];
  }

  // configIframe(targetIfrm) {
  //   try {
  //     // new iFrmFunc.ConfigIframe(targetIfrm).setId(this.id);
  //     msgHandler.handlerAll(this.bannerObject.id, targetIfrm);
  //     this.$store.watch(state => state.MESSAGE, () => {
  //       this.hanndleIframeMsg(targetIfrm);
  //     });
  //   } catch (error) {
  //     console.log('ArfError from configIframe', error);
  //   }
  // }

  createIframe(onloadCallback) {
    const iframe = document.createElement('iframe');
    iframe.id = `iframe${this.bannerComponent.current.tree ? `-${this.bannerComponent.current.tree.zoneId}` : ''}-${this.bannerComponent.current.id}`;
    iframe.src = this.src;
    iframe.onload = onloadCallback;
    iframe.frameBorder = 0;
    iframe.marginWidth = 0;
    iframe.marginHeight = 0;
    iframe.scrolling = 'no'; // Prevent iframe body scrolling
    iframe.width = this.isMobile ? this.bannerComponent.$el.clientWidth : this.bannerObject.width;
    // const msg = new MessageEvent('responsive');
    // this.bannerComponent.$el.onresize = () => {
    //   console.log('onResize', this.bannerComponent.$el.clientWidth);
    //   iframe.width = this.isMobile ? this.bannerComponent.$el.clientWidth : this.bannerObject.width;
    //   // iframe.contentWindow.postMessage(msg, '*');
    // };
    // this.bannerComponent.$el.addEventListener('onresize', () => {
    //   console.log('onResize', this.bannerComponent.$el.clientWidth);
    //   iframe.width = this.isMobile ? this.bannerComponent.$el.clientWidth : this.bannerObject.width;
    // });

    // if (window.ResizeObserver) {
    //   const ro = new window.ResizeObserver(() => {
    //     console.log('onResize', this.bannerComponent.$el.clientWidth);
    //     iframe.width = this.isMobile ? this.bannerComponent.$el.clientWidth : this.bannerObject.width;
    //   });
    //   ro.observe(this.bannerComponent.$el);
    //   setTimeout(() => { ro.disconnect(); }, 5000);
    // } else {
    //   const observer = new Observer(() => {
    //     console.log('onResize', this.bannerComponent.$el.clientWidth);
    //     iframe.width = this.isMobile ? this.bannerComponent.$el.clientWidth : this.bannerObject.width;
    //   });
    //   observer.start(this.bannerComponent.$el, {
    //     attributes: true,
    //   });
    //   observer.disconnectAfter(3000);
    // }
    iframe.height = this.bannerObject.height;
    iframe.style.display = 'block';
    return iframe;
  }

  // addParamToSrc(param, value) {
  //   if (this.src.indexOf('?') === -1) {
  //     this.src += `?${param}=${value}`;
  //   } else {
  //     this.src += `&${param}=${value}`;
  //   }
  //   console.log('checkSrc', this.src);
  // }

  // addBannerId(bid) {
  //   const bannerId = this.getParameterByName('arfbid');
  //   if (!bannerId) {
  //     this.addParamToSrc('arfbid', bid);
  //   }
  // }

  // addParamMacro() {
  //   const allMacro = macroConfig.getAll();
  //   for (let index = 0; index < allMacro.length; index += 1) {
  //     const macroValue = allMacro[index];
  //     if (this.bannerObject.html.indexOf(macroValue.macro) !== -1) {
  //       if (!this.getParameterByName(macroValue.param)) {
  //         this.addParamToSrc(macroValue.param, macroValue.macro);
  //       }
  //       this.bannerObject.html.replace(macroValue.macro, '');
  //     }
  //   }
  // }

  replaceMacro() {
    this.src = new Macro(this.src, this.bannerObject).replaceMacro(false, false);
    // console.log('checkSrcAfterReplaceMacro', this.src);
  }

  // getParameterByName(name, url) {
  //   if (!url) url = window.location.href; // eslint-disable-line
  //   name = name.replace(/[[\]]/g, '\\$&'); // eslint-disable-line
  //   const regex = new RegExp(`[?&]${name}(=([^&#]*)|&|#|$)`);
  //   const results = regex.exec(url);
  //   if (!results) return null;
  //   if (!results[2]) return '';
  //   return decodeURIComponent(results[2].replace(/\+/g, ' '));
  // }

  watch() {
    if (window.ResizeObserver) {
      const ro = new window.ResizeObserver(() => {
        // console.log('onResize', this.bannerComponent.$el.clientWidth);
        this.el.width = this.isMobile ? this.bannerComponent.$el.clientWidth : this.bannerObject.width;
      });
      ro.observe(this.bannerComponent.$el);
      this.observe = ro;
    } else {
      const observer = new Observer(() => {
        // console.log('onResize', this.bannerComponent.$el.clientWidth);
        this.el.width = this.isMobile ? this.bannerComponent.$el.clientWidth : this.bannerObject.width;
      });
      observer.start(this.bannerComponent.$el, {
        attributes: true,
      });
      this.observe = observer;
    }
  }

  stopWatch() {
    if (this.observe) this.observe.disconnect();
  }
}
