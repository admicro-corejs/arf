export default function resizeIFrameToFitContent(iFrame) {
  console.log(
    iFrame.contentWindow.document.body.scrollWidth,
    iFrame.contentWindow.document.body.scrollHeight,
  );
  /* eslint-disable */
  iFrame.width = iFrame.contentWindow.document.body.scrollWidth;
  iFrame.height = iFrame.contentWindow.document.body.scrollHeight;
  /* eslint-enable */
  return {
    width: iFrame.width,
    height: iFrame.height,
  };
}
