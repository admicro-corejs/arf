export default function round(number) {
  const decimal = number % 1;
  if (decimal >= 0.5) {
    return (number - decimal) + 1;
  }
  return (number - decimal);
}
