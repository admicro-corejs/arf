import store from '@/store';

export default function getVariable(name, env) {
  const value = window[name];
  if (value !== null && value !== undefined) return value;
  if (env) return (env.variables ? env.variables[name] : null);
  // return '';
  return (store.state.ENV.variables ? store.state.ENV.variables[name] : null);
}
