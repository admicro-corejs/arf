const key = 'a25qbmtjY2g=';
function base64Encode(str) {
// first we use encodeURIComponent to get percent-encoded UTF-8,
  // then we convert the percent encodings into raw bytes which
  // can be fed into btoa.
  return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, (match, p1) => String.fromCharCode(`0x${p1}`)));
}
function base6Decode(str) {
  // Going backwards: from bytestream, to percent-encoding, to original string.
  return decodeURIComponent(atob(str).split('').map(c => `%${(`00${c.charCodeAt(0).toString(16)}`).slice(-2)}`).join(''));
}
function encodeWithKey(s, k) {
  let enc = '';
  let str = '';
  // make sure that input is string
  str = s.toString();
  for (let i = 0; i < str.length; i += 1) {
    // create block
    const a = str.charCodeAt(i);
    // bitwise XOR
    const b = a ^ k; // eslint-disable-line
    enc += String.fromCharCode(b);
  }
  return enc;
}
export function encode(str, k1, k2) {
  const value = encodeWithKey(str, k1 && k2 ? encodeWithKey(base6Decode(k1), k2) : encodeWithKey(base6Decode(key), 90));
  return base64Encode(value);
}
export function decode(decryptStr, k1, k2) {
  const value = base6Decode(decryptStr);
  return encodeWithKey(value, k1 && k2 ? encodeWithKey(base6Decode(k1), k2) : encodeWithKey(base6Decode(key), 90));
}
