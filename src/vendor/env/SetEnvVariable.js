/**
 * This for get variable for arf and set them into store.ENV.variables variables object.
 */
import store from '../../store';
import ArfStorage from '../ArfStorage';
import storageType from './storageTypes';


const storage = new ArfStorage();

function setValue(name, value) {
  store.commit('SET_ENV_VARIABLE', { name, value });
}

function getValue(name) {
  return store.state.ENV.variables[name];
}

export default function setEnvVariable(variableArray) {
  for (let index = 0; index < variableArray.length; index += 1) {
    const variable = variableArray[index];
    const type = variable.type;
    const name = variable.valueName;
    let value = null;
    if (type === storageType.cookie) {
      value = storage.getCookie(name);
    } else if (type === storageType.localStorage) {
      value = storage.getStorage(name);
    } else if (type === storageType.windowObject) {
      value = window[name];
    }

    if (!getValue(name) && value) {
      setValue(name, value);
    }
  }
}
