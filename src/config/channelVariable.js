const listVariable = [
  '_ADM_Channel',
];

export default function getlValueFromChannelVariable() {
  for (let index = 0; index < listVariable.length; index += 1) {
    const element = listVariable[index];
    if (window[element]) {
      return window[element];
    }
  }
  return null;
}
