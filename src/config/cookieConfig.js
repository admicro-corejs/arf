import unit from './unit';

function convertTime(objectExpired) {
  const { value } = objectExpired;
  const unit2 = objectExpired.unit;
  return value || unit2;
}


const cookie = {
  pageLoad: {
    name: '_pls',
  },
  trackLoad: {
    name: '_tilo',
    expire: convertTime({ value: 30, unit: unit.minute }),
  },
  frequency: {
    name: '_frq',
    expire: convertTime({ value: 10, unit: unit.day }),
  },
  uid: {
    name: '_uidcms',
    expire: convertTime({ value: 5, unit: unit.year }),
  },
};

export default cookie;
