const macros = [
  { macro: '%%WIDTH%%', type: 'width', param: 'width' },
  { macro: '%%HEIGHT%%', type: 'height', param: 'height' },
  { macro: '%%CLICK_URL_ESC%%', type: 'clickUrlEsc', param: 'cue' },
  { macro: '%%CLICK_URL_UNESC%%', type: 'clickUrlUnEsc', param: 'cuu' },
  { macro: '%%CACHEBUSTER%%', type: 'cacheBuster', param: 'ord', value: () => `${(new Date()).getTime()}` },
  { macro: '%%ARF_CONTENT%%', type: 'adsFormat' },
  { macro: '%%TRUE_VIEW_REPORT%%', type: 'trueView' },
  { macro: '%%VIEW_REPORT%%', type: 'views' },
  { macro: '%%CLICK_REPORT%%', type: 'click' },
  { macro: '%%ZONE_ID%%', type: ['tree', 'zoneId'] },
  { macro: '%%POSITION_ON_SHARE%%', type: 'positionOnShare' },
  { macro: '%%VIEW_EXPAND%%', type: 'expandLog' },
  { macro: '%%VAST_TAGS_URL%%', type: 'vast' },
  { macro: '%%NO_BRAND%%', type: 'domain', value: () => false },
  { macro: '[timestamp]', value: () => `${(new Date()).getTime()}` },
];

export function getAll() {
  return macros;
}

export function getMacroByMacroText(macro) {
  return macros.filter(item => item.macro === macro)[0];
}

export function getMacroByType(type) {
  return macros.filter(item => item.type === type)[0];
}
