const zonesDisable = {
  name: '_ArfListZoneDisable',
};

const zoneHidden = {
  name: '_ADM_ZONE_HIDE',
};

export default { zonesDisable, zoneHidden };

