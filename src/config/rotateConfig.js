const rotate = {
  share: {
    default: 30000,
    minimum: 10000,
  },
  placement: {
    default: 20000,
    minimum: 5000,
  },
};

export default rotate;
