import { domainLog } from './domain';

const logging = {
  url: domainLog.url, /* link test log: https://checklog20180414114019.azurewebsites.net/Home || https://lg1.logging.admicro.vn */
  zone: {
    path: 'advbcms',
    param: {
      dmn: 'dmn', zid: 'zid', pgid: 'pgid', uid: 'uid',
    },
  },
  banner: {
    path: 'cpx_cms',
    /**
     * dmn=${domain}
     * zid=${zoneID}
     * pli=${placementId}
     * cmpg=${campaignID}
     * items=${bannerId}
     * cov=${cov}
     * cat: channel variable.
     * pgid: page load id.
     * uid: user id.
     *
     * Log Detail:
     * cov = 0 : log view
     * cov = 1 : log click
     * cov = 2 : log true view
     * cov = 3 : log load extra like rotate,..
     * cov = 4 : log load faild
     * cov = 5 : log banner render done.
     * cov = 6 : other region: load slow.
     * cov = 7 : log banner callback.
     * cov = 10: log expand.
     * LOG video inside banner html 25% - cov = 11, 50% = cov = 12, 75% - cov = 13, 100% - cov = 14, start - cov = 15
     * cov = 77 : log banner render from callback
     */
    param: {
      dmn: 'dmn', zid: 'zid', pli: 'pli', cmpg: 'cmpg', items: 'items', cat: 'cat', cov: 'cov', pgid: 'pgid', uid: 'uid', re: 're',
    },
  },
};

export default logging;
