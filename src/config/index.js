import AdsType from './AdsType';
import cookieConfig from './cookieConfig';
import loggingConfig from './loggingConfig';
import rotateConfig from './rotateConfig';
import roundingError from './roundingErrorConfig';
import siteConfig from './siteConfig';
import domain from './domain';
import variables from './variablesName';

export { AdsType, cookieConfig, loggingConfig, rotateConfig, roundingError, siteConfig, domain, variables };
export default { AdsType, cookieConfig, loggingConfig, rotateConfig, roundingError, siteConfig, domain, variables };
