const domainCore = {
  url: 'https://media1.admicro.vn',
  arf: '/cms/Arf.min.js',
  admcorearf: '/core/admcorearf.js',
  mb_core: '/core/mb_core.js',
  bantrackingmb: '/core/bantrackingmb.js',
  bantracking: '/core/bantracking.js',
  tagsponsorzPC: '/js_boxapp/tagsponsorz_473687.js',
  tagsponsorzMB: '/js_boxapp/tagsponsorz_473688.js',
};

const domainLog = {
  url: 'https://lg1.logging.admicro.vn',
};

export { domainCore, domainLog };
export default { domainCore, domainLog };
