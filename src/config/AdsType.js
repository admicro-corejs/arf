const shareType = {
  single: 'single',
  multiple: 'multiple',
};
// Revenue type are chosen by priority and number of pieces.
// if flag 'isForce' is true so this revenueType is force appear,
// if it available(highest priotity and available pieces).
const revenueType = {
  pageLoad: {
    value: 'pageLoad',
    pieces: 3,
    priority: 2,
    isForce: true,
  },
  pr: {
    value: 'pr',
    pieces: 1,
    priority: 1,
    isForce: true,
  },
  cpd: {
    value: 'cpd',
    pieces: 3,
    priority: 3,
    isForce: false,
  },
  relative: {
    value: 'relative',
    priority: 4,
    isForce: false,
  },
  cpm: {
    value: 'cpm',
    priority: 5,
    isForce: false,
  },
  pb: {
    value: 'pb',
    priority: 6,
    isForce: false,
    isBackup: true,
  },
  default: {
    value: 'default',
    priority: 7,
    isForce: false,
    isBackup: true,
  },
};

const bannerCondition = {
  type: {
    channel: ['channel'].map(item => item.toLocaleLowerCase()),
    variable: ['variable', 'globalVariable'].map(item => item.toLocaleLowerCase()),
    pageUrl: ['pageUrl', 'Site Page-URL', 'url'].map(item => item.toLocaleLowerCase()),
    referringPage: ['referringPage', 'Site - Referring Page'].map(item => item.toLocaleLowerCase()),
    category: ['category'].map(item => item.toLocaleLowerCase()),
  },
};

export default {
  shareType,
  revenueType,
  bannerCondition,
};
