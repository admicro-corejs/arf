const dom = {
  beforeMount() {
    // Attach styles & scripts before rendering for better ads displaying performances
    this.createOutputCss(() => {
      this.attachStyles();
    });
    // this.attachScripts();
  },

  methods: {
    /**
     * Attach entity's styles to header
     */
    attachStyles() {
      if (!this.outputCss) return;

      const head = document.head || document.getElementsByTagName('head')[0];
      const style = document.createElement('style');

      style.id = `style-${this.$options.name}-${this.id || this.current.id}`;
      style.type = 'text/css';
      const cssText = this.outputCss || '';
      // cssText = cssText.replace(/(share-)/g, '');
      if (style.styleSheet) {
        style.styleSheet.cssText = cssText;
      } else {
        style.appendChild(document.createTextNode(cssText));
      }
      // console.log('attach', cssText, this.outputCss);
      head.appendChild(style);
    },

    createOutputCss(callback) {
      if (!this.current.css) return;
      const componentType = this.$options.name;
      const outputCss = this.current.outputCss || `#${componentType}-${this.current.id}{
        ${this.current.css}
      }`;
      this.$data.outputCss = outputCss;
      callback();
    },

    /**
     * Attach entity's scripts to header
     */
    // attachScripts() {
    //   if (!this.scripts) return;

    //   const body = document.body || document.getElementsByTagName('body')[0];
    //   const script = document.createElement('script');

    //   script.id = `scripts-${this.id}`;
    //   script.type = 'text/javascript';
    //   script.innerHTML = this.scripts;

    //   body.appendChild(script);
    // },
  },

};

export default dom;
