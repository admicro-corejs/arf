import htmlHandle from '../vendor/util/handleHTML/htmlHandle';
import * as macroConfig from '../config/macroConfig';
import Macro from '../vendor/Macro';

export default {
  data() {
    return {
      macroTemplate: macroConfig.getMacroByType('adsFormat').macro,
    };
  },

  mounted() {
    // this.setupFormat();
  },

  methods: {
    async setupFormat() {
      try {
        if (!(this.current.isTemplate || (this.current.isTemplate && this.current.template))) return;
        console.log(`runSetupTemplate ${this.current.id}`, this.current.isTemplate, this.current.template);

        const placeToReplace = this.createPlaceToReplace();
        const html = new Macro(this.current.template, this).replaceMacro(false, false, [{ macro: this.macroTemplate, value: `<div id="${placeToReplace}"></div>` }]);

        await htmlHandle(html, this.$el, this.$store).injecttHTMLAfterEnd();

        // reder content into layout template.
        // const elId = this.getElementLayout();
        // const wrap = document.getElementById(elId);
        // if (elId && wrap) wrap.appendChild(this.$el);
        const elToReplace = document.getElementById(placeToReplace);
        elToReplace.replaceWith(this.$el);
      } catch (error) {
        console.log('ArfError from AdsTemplate', error);
      } // eslint-disable-line
    },
    createPlaceToReplace() {
      const componentName = this.$options.name;
      const id = `${this.current.id}-${componentName}-template`;
      // const htmlRemoveMacro = this.current.template.replace(this.macroTemplate, `<div id="${id}"></div>`);
      return id;
    },
  },
};
