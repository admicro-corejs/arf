# arf-base

> A Vue.js project for rendering advertisement and it serve for AdsServing tools.

## Build Setup

``` bash
# install dependencies
npm install

# build for library in production and develop
npm run build
```

Contact: tamleminh@admicro.vn
