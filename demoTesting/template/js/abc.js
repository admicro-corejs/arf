/* eslint-disable */

const linkCore = 'https://adi.admicro.vn/adt/cpc/tvcads/files/js/core_vplus/vppc_foru.js';
const listBox = [42495, 42496, 42497];
const listData = generateListData(listBox);

function createLinkData(uid) {
  return 'http://nspapi.aiservice.vn/request/view-plus/client?guid='+uid+'&domain=GenK&boxid=5&url=http://genk.vn/news-20171117143218157.chn';
}

function getUid() {
  sessionStorage.removeItem('sttBoxforu');
  var uid = ADM_AdsTracking.get("__uid");
  console.log('uid', uid);
  if(!uid) {
    inszone1Uid=0;
    return;
  }
  return uid;
}

function generateListData(boxes) {
  const result = [];
  while (boxes.length > 0) {
    const boxId = boxes.shift();
    const uid = getUid();
    const link = createLinkData(uid);
    result.push({link, boxId});
  }
  console.log('listData', result);
  return result;
}

function loadWithinOrder() {
  const data = listData.shift();
  console.log('loading...', data);
  _admloadJs(data.link, () => {
    boxForu(GenK_Box_5,data.boxId);
    if (listData.length > 0) {
      loadWithinOrder();
    }
  });
}

_admloadJs(linkCore, () => {
  loadWithinOrder();
});


