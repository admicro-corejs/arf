// /* eslint-disable */
// import Instance from '../../../src/Instance';

// var mochaAsync = (fn) => {
//   return done => {
//     fn.call().then(done, err => {
//       done(err);
//     });
//   };
// };

// describe('1. Instance', () => {
//   beforeEach(() => {
//     var fixture = '<zone id="jtzk0s2r"></zone>';

//     document.body.insertAdjacentHTML(
//       'afterbegin',
//       fixture);


//     Instance.pushZone({
//       el: document.getElementById("jtzk0s2r") || document.getElementById("admzonejtzk0s2r") || document.getElementById("adm-slot-jtzk0s2r"),
//       propsData: {
//         model: {
//           "id": "jtzk0s2r",
//           "name": "test_980x90",
//           "height": 90,
//           "width": 980,
//           "css": "",
//           "outputCss": "",
//           "groupSSP": "",
//           "isResponsive": true,
//           "isMobile": false,
//           "isTemplate": false,
//           "template": "",
//           "totalShare": 3,
//           "status": "active",
//           "isPageLoad": true,
//           "isZoneVideo": false,
//           "positionOnSite": 0,
//           "timeBetweenAds": 0,
//           "siteId": "jtzk0h50",
//           "shares": [{
//             "id": "jtzk0sfj",
//             "css": "",
//             "outputCss": "",
//             "width": 980,
//             "height": 90,
//             "classes": "",
//             "isRotate": false,
//             "rotateTime": 20,
//             "type": "single",
//             "format": "",
//             "zoneId": "jtzk0s2r",
//             "isTemplate": false,
//             "template": "",
//             "offset": "",
//             "isAdPod": false,
//             "duration": "",
//             "sharePlacements": [{
//               "id": "jtzk2k6y",
//               "positionOnShare": 0,
//               "placementId": "jtzk2bec",
//               "shareId": "jtzk0sfj",
//               "time": "[{\"startTime\":\"2019-04-01T17:00:00.000Z\",\"endTime\":null}]",
//               "placement": {
//                 "id": "jtzk2bec",
//                 "width": 80,
//                 "height": 500,
//                 "startTime": "2019-04-01T17:00:00.000Z",
//                 "endTime": null,
//                 "weight": 1,
//                 "revenueType": "cpm",
//                 "cpdPercent": 0,
//                 "isRotate": false,
//                 "rotateTime": 20,
//                 "price": 0,
//                 "relative": 0,
//                 "campaignId": "jtzk1hhp",
//                 "campaign": {
//                   "id": "jtzk1hhp",
//                   "startTime": "2019-04-01T17:00:00.000Z",
//                   "endTime": null,
//                   "views": 0,
//                   "viewPerSession": 0,
//                   "timeResetViewCount": 0,
//                   "settingBudgetCPM": "",
//                   "settingBudgetCPC": "",
//                   "weight": 0,
//                   "revenueType": "cpd",
//                   "pageLoad": 0,
//                   "totalCPM": 0,
//                   "isRunBudget": false,
//                   "expense": 0,
//                   "maxCPMPerDay": 0
//                 },
//                 "banners": [{
//                   "id": "jtzk2byv",
//                   "html": "",
//                   "width": 80,
//                   "height": 500,
//                   "keyword": "",
//                   "isMacro": false,
//                   "weight": 1,
//                   "placementId": "jtzk2bec",
//                   "imageUrl": "https://static-cmsads.admicro.vn/cmsads/2019/04/5435-1554195805328.jpeg",
//                   "bannerHtmlTypeId": "jhk08image",
//                   "url": "",
//                   "target": "_blank",
//                   "bannerTypeId": "56c53393-914e-4182-9d7c-54bbbd4b304e",
//                   "isIFrame": false,
//                   "isDefault": false,
//                   "isRelative": false,
//                   "videoType": "",
//                   "clickThroughUrl": "",
//                   "vastTagsUrl": "",
//                   "vastVideoUrl": "",
//                   "videoUrl": "",
//                   "duration": 0,
//                   "thirdPartyTracking": "",
//                   "thirdPartyUrl": "",
//                   "isDocumentWrite": true,
//                   "skipOffset": 0,
//                   "isAddLinkSSP": false,
//                   "optionBanners": [],
//                   "bannerType": {
//                     "id": "56c53393-914e-4182-9d7c-54bbbd4b304e",
//                     "name": "Img",
//                     "value": "img",
//                     "isUpload": true,
//                     "isVideo": false
//                   },
//                   "bannerHtmlType": null
//                 }]
//               }
//             }]
//           }],
//           "site": {
//             "id": "jtzk0h50",
//             "domain": "http://testcore.vn",
//             "pageLoad": 0,
//             "isRunBannerDefault": false,
//             "globalFilters": [],
//             "channels": [{
//               "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
//               "name": "Location",
//               "isGlobal": true,
//               "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]"
//             }]
//           }
//         }
//       }
//     });
//   });

//   it('should render correct zone', mochaAsync(async () => {
//     await new Promise((resolve) => {
//       setTimeout(() => {
//         resolve();
//       }, 1000);
//     });
//     expect(document.querySelector('#zone-jtzk0s2r')).to.not.equal(null);
//   }));

//   it('should render correct share', mochaAsync(async () => {
//     await new Promise((resolve) => {
//       setTimeout(() => {
//         resolve();
//       }, 1300);
//     });
//     expect(document.querySelector('#share-jtzk0sfj')).to.not.equal(null);
//   }));

//   it('should render correct placement', mochaAsync(async () => {
//     await new Promise((resolve) => {
//       setTimeout(() => {
//         resolve();
//       }, 1500);
//     });
//     expect(document.querySelector('#placement-jtzk2bec')).to.not.equal(null);
//   }));

//   it('should render correct banner', mochaAsync(async () => {
//     await new Promise((resolve) => {
//       setTimeout(() => {
//         resolve();
//       }, 1600);
//     });
//     expect(document.querySelector('#slot-1-jtzk0s2r-jtzk2byv')).to.not.equal(null);
//   }));
// });

