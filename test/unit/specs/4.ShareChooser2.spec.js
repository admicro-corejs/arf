// /* eslint-disable */
// import store from '../../../src/store';
// import Zone from '../../../src/models/Zone';

// const zoneData = {
//   "id": "k0m2f0zb",
//   "name": "zone_300x600",
//   "height": 600,
//   "width": 300,
//   "css": "",
//   "outputCss": "",
//   "groupSSP": "",
//   "isResponsive": true,
//   "isMobile": false,
//   "isTemplate": false,
//   "template": "",
//   "totalShare": 3,
//   "status": "active",
//   "isPageLoad": true,
//   "isZoneVideo": false,
//   "positionOnSite": "0",
//   "timeBetweenAds": 0,
//   "siteId": "k0m2d1xt",
//   "shares": [{
//     "id": "k0m2f1d3",
//     "css": "",
//     "outputCss": "",
//     "width": 300,
//     "height": 600,
//     "classes": "",
//     "isRotate": false,
//     "rotateTime": 0,
//     "type": "single",
//     "format": "",
//     "zoneId": "k0m2f0zb",
//     "isTemplate": false,
//     "template": "",
//     "offset": "",
//     "isAdPod": false,
//     "duration": "",
//     "sharePlacements": [{
//       "id": "k0m2my2u",
//       "positionOnShare": 0,
//       "placementId": "k0m2mlw8",
//       "shareId": "k0m2f1d3",
//       "time": "[{\"startTime\":\"2019-09-15T17:00:00.000Z\",\"endTime\":null}]",
//       "placement": {
//         "id": "k0m2mlw8",
//         "width": 300,
//         "height": 600,
//         "startTime": "2019-09-15T17:00:00.000Z",
//         "endTime": null,
//         "weight": 1,
//         "revenueType": "cpm",
//         "cpdPercent": 0,
//         "isRotate": false,
//         "rotateTime": 20,
//         "price": 0,
//         "relative": 0,
//         "campaignId": "k0m2lo4r",
//         "campaign": {
//           "id": "k0m2lo4r",
//           "startTime": "2019-09-15T17:00:00.000Z",
//           "endTime": null,
//           "views": 0,
//           "viewPerSession": 0,
//           "timeResetViewCount": 0,
//           "settingBudgetCPM": "",
//           "settingBudgetCPC": "",
//           "weight": 0,
//           "revenueType": "cpd",
//           "pageLoad": 0,
//           "totalCPM": 0,
//           "isRunMonopoly": false,
//           "optionMonopoly": "",
//           "isRunBudget": false,
//           "expense": 0,
//           "maxCPMPerDay": 0
//         },
//         "banners": [{
//           "id": "k0m2mm5f",
//           "html": "",
//           "width": 300,
//           "height": 600,
//           "keyword": "",
//           "isMacro": false,
//           "weight": 1,
//           "placementId": "k0m2mlw8",
//           "imageUrl": "https://cms.surfcountor.com/cmsads/2019/09/admi-1568617943224.jpeg",
//           "bannerHtmlTypeId": "jhk08image",
//           "url": "",
//           "target": "_blank",
//           "bannerTypeId": "56c53393-914e-4182-9d7c-54bbbd4b304e",
//           "isIFrame": false,
//           "isDefault": false,
//           "isRelative": false,
//           "videoType": "",
//           "clickThroughUrl": "",
//           "vastTagsUrl": "",
//           "vastVideoUrl": "",
//           "videoUrl": "",
//           "duration": 0,
//           "thirdPartyTracking": "",
//           "thirdPartyUrl": "",
//           "isDocumentWrite": true,
//           "skipOffset": 0,
//           "isAddLinkSSP": false,
//           "optionBanners": [],
//           "bannerType": {
//             "id": "56c53393-914e-4182-9d7c-54bbbd4b304e",
//             "name": "Img",
//             "value": "img",
//             "isUpload": true,
//             "isVideo": false
//           },
//           "bannerHtmlType": null
//         }]
//       }
//     }]
//   }, {
//     "id": "k0m2flr9",
//     "css": "> :first-child {\n   padding-bottom: 10px;\n}",
//     "outputCss": "#share-k0m2flr9 > :first-child {\n  padding-bottom: 10px;\n}\n",
//     "width": 300,
//     "height": 600,
//     "classes": "",
//     "isRotate": false,
//     "rotateTime": 0,
//     "type": "multiple",
//     "format": "1,1",
//     "zoneId": "k0m2f0zb",
//     "isTemplate": false,
//     "template": "",
//     "offset": "",
//     "isAdPod": false,
//     "duration": null,
//     "sharePlacements": [{
//       "id": "k0m2r475",
//       "positionOnShare": 1,
//       "placementId": "k0m2qvcu",
//       "shareId": "k0m2flr9",
//       "time": "[{\"startTime\":\"2019-09-15T17:00:00.000Z\",\"endTime\":null}]",
//       "placement": {
//         "id": "k0m2qvcu",
//         "width": 300,
//         "height": 250,
//         "startTime": "2019-09-15T17:00:00.000Z",
//         "endTime": null,
//         "weight": 1,
//         "revenueType": "cpm",
//         "cpdPercent": 0,
//         "isRotate": false,
//         "rotateTime": 20,
//         "price": 0,
//         "relative": 0,
//         "campaignId": "k0m2lo4r",
//         "campaign": {
//           "id": "k0m2lo4r",
//           "startTime": "2019-09-15T17:00:00.000Z",
//           "endTime": null,
//           "views": 0,
//           "viewPerSession": 0,
//           "timeResetViewCount": 0,
//           "settingBudgetCPM": "",
//           "settingBudgetCPC": "",
//           "weight": 0,
//           "revenueType": "cpd",
//           "pageLoad": 0,
//           "totalCPM": 0,
//           "isRunMonopoly": false,
//           "optionMonopoly": "",
//           "isRunBudget": false,
//           "expense": 0,
//           "maxCPMPerDay": 0
//         },
//         "banners": [{
//           "id": "k0m2qvml",
//           "html": "",
//           "width": 300,
//           "height": 250,
//           "keyword": "",
//           "isMacro": false,
//           "weight": 1,
//           "placementId": "k0m2qvcu",
//           "imageUrl": "https://cms.surfcountor.com/cmsads/2019/09/cpm1-1568618144442.png",
//           "bannerHtmlTypeId": "jhk08image",
//           "url": "",
//           "target": "_blank",
//           "bannerTypeId": "56c53393-914e-4182-9d7c-54bbbd4b304e",
//           "isIFrame": false,
//           "isDefault": false,
//           "isRelative": false,
//           "videoType": "",
//           "clickThroughUrl": "",
//           "vastTagsUrl": "",
//           "vastVideoUrl": "",
//           "videoUrl": "",
//           "duration": 0,
//           "thirdPartyTracking": "",
//           "thirdPartyUrl": "",
//           "isDocumentWrite": true,
//           "skipOffset": 0,
//           "isAddLinkSSP": false,
//           "optionBanners": [],
//           "bannerType": {
//             "id": "56c53393-914e-4182-9d7c-54bbbd4b304e",
//             "name": "Img",
//             "value": "img",
//             "isUpload": true,
//             "isVideo": false
//           },
//           "bannerHtmlType": null
//         }]
//       }
//     }, {
//       "id": "k0m2rewy",
//       "positionOnShare": 2,
//       "placementId": "k0m2pj5y",
//       "shareId": "k0m2flr9",
//       "time": "[{\"startTime\":\"2019-09-15T17:00:00.000Z\",\"endTime\":null}]",
//       "placement": {
//         "id": "k0m2pj5y",
//         "width": 300,
//         "height": 250,
//         "startTime": "2019-09-15T17:00:00.000Z",
//         "endTime": null,
//         "weight": 1,
//         "revenueType": "cpd",
//         "cpdPercent": 1,
//         "isRotate": false,
//         "rotateTime": 20,
//         "price": 0,
//         "relative": 0,
//         "campaignId": "k0m2lo4r",
//         "campaign": {
//           "id": "k0m2lo4r",
//           "startTime": "2019-09-15T17:00:00.000Z",
//           "endTime": null,
//           "views": 0,
//           "viewPerSession": 0,
//           "timeResetViewCount": 0,
//           "settingBudgetCPM": "",
//           "settingBudgetCPC": "",
//           "weight": 0,
//           "revenueType": "cpd",
//           "pageLoad": 0,
//           "totalCPM": 0,
//           "isRunMonopoly": false,
//           "optionMonopoly": "",
//           "isRunBudget": false,
//           "expense": 0,
//           "maxCPMPerDay": 0
//         },
//         "banners": [{
//           "id": "k0m2pjfv",
//           "html": "",
//           "width": 300,
//           "height": 250,
//           "keyword": "",
//           "isMacro": false,
//           "weight": 1,
//           "placementId": "k0m2pj5y",
//           "imageUrl": "https://cms.surfcountor.com/cmsads/2019/09/1.pn-1568618078124.png",
//           "bannerHtmlTypeId": "jhk08image",
//           "url": "",
//           "target": "_blank",
//           "bannerTypeId": "56c53393-914e-4182-9d7c-54bbbd4b304e",
//           "isIFrame": false,
//           "isDefault": false,
//           "isRelative": false,
//           "videoType": "",
//           "clickThroughUrl": "",
//           "vastTagsUrl": "",
//           "vastVideoUrl": "",
//           "videoUrl": "",
//           "duration": 0,
//           "thirdPartyTracking": "",
//           "thirdPartyUrl": "",
//           "isDocumentWrite": true,
//           "skipOffset": 0,
//           "isAddLinkSSP": false,
//           "optionBanners": [],
//           "bannerType": {
//             "id": "56c53393-914e-4182-9d7c-54bbbd4b304e",
//             "name": "Img",
//             "value": "img",
//             "isUpload": true,
//             "isVideo": false
//           },
//           "bannerHtmlType": null
//         }]
//       }
//     }]
//   }, {
//     "id": "k0m2isjf",
//     "css": "> :first-child {\n        padding-bottom: 10px;\n}\n> :last-child {\n     padding-top: 10px;\n}\n",
//     "outputCss": "#share-k0m2isjf > :first-child {\n  padding-bottom: 10px;\n}\n#share-k0m2isjf > :last-child {\n  padding-top: 10px;\n}\n",
//     "width": 300,
//     "height": 600,
//     "classes": "",
//     "isRotate": false,
//     "rotateTime": 0,
//     "type": "multiple",
//     "format": "1,1,1",
//     "zoneId": "k0m2f0zb",
//     "isTemplate": false,
//     "template": "",
//     "offset": "",
//     "isAdPod": false,
//     "duration": null,
//     "sharePlacements": [{
//       "id": "k0m301xp",
//       "positionOnShare": 1,
//       "placementId": "k0m2zgwa",
//       "shareId": "k0m2isjf",
//       "time": "[{\"startTime\":\"2019-09-15T17:00:00.000Z\",\"endTime\":null}]",
//       "placement": {
//         "id": "k0m2zgwa",
//         "width": 300,
//         "height": 150,
//         "startTime": "2019-09-15T17:00:00.000Z",
//         "endTime": null,
//         "weight": 1,
//         "revenueType": "cpm",
//         "cpdPercent": 0,
//         "isRotate": false,
//         "rotateTime": 20,
//         "price": 0,
//         "relative": 0,
//         "campaignId": "k0m2lo4r",
//         "campaign": {
//           "id": "k0m2lo4r",
//           "startTime": "2019-09-15T17:00:00.000Z",
//           "endTime": null,
//           "views": 0,
//           "viewPerSession": 0,
//           "timeResetViewCount": 0,
//           "settingBudgetCPM": "",
//           "settingBudgetCPC": "",
//           "weight": 0,
//           "revenueType": "cpd",
//           "pageLoad": 0,
//           "totalCPM": 0,
//           "isRunMonopoly": false,
//           "optionMonopoly": "",
//           "isRunBudget": false,
//           "expense": 0,
//           "maxCPMPerDay": 0
//         },
//         "banners": [{
//           "id": "k0m2zha8",
//           "html": "",
//           "width": 300,
//           "height": 150,
//           "keyword": "",
//           "isMacro": false,
//           "weight": 1,
//           "placementId": "k0m2zgwa",
//           "imageUrl": "https://cms.surfcountor.com/cmsads/2019/09/cpm_-1568618536741.png",
//           "bannerHtmlTypeId": "jhk08image",
//           "url": "",
//           "target": "_blank",
//           "bannerTypeId": "56c53393-914e-4182-9d7c-54bbbd4b304e",
//           "isIFrame": false,
//           "isDefault": false,
//           "isRelative": false,
//           "videoType": "",
//           "clickThroughUrl": "",
//           "vastTagsUrl": "",
//           "vastVideoUrl": "",
//           "videoUrl": "",
//           "duration": 0,
//           "thirdPartyTracking": "",
//           "thirdPartyUrl": "",
//           "isDocumentWrite": true,
//           "skipOffset": 0,
//           "isAddLinkSSP": false,
//           "optionBanners": [],
//           "bannerType": {
//             "id": "56c53393-914e-4182-9d7c-54bbbd4b304e",
//             "name": "Img",
//             "value": "img",
//             "isUpload": true,
//             "isVideo": false
//           },
//           "bannerHtmlType": null
//         }]
//       }
//     }, {
//       "id": "k0m306dh",
//       "positionOnShare": 2,
//       "placementId": "k0m2wbbg",
//       "shareId": "k0m2isjf",
//       "time": "[{\"startTime\":\"2019-09-15T17:00:00.000Z\",\"endTime\":null}]",
//       "placement": {
//         "id": "k0m2wbbg",
//         "width": 300,
//         "height": 150,
//         "startTime": "2019-09-15T17:00:00.000Z",
//         "endTime": null,
//         "weight": 1,
//         "revenueType": "cpd",
//         "cpdPercent": 1,
//         "isRotate": false,
//         "rotateTime": 20,
//         "price": 0,
//         "relative": 0,
//         "campaignId": "k0m2lo4r",
//         "campaign": {
//           "id": "k0m2lo4r",
//           "startTime": "2019-09-15T17:00:00.000Z",
//           "endTime": null,
//           "views": 0,
//           "viewPerSession": 0,
//           "timeResetViewCount": 0,
//           "settingBudgetCPM": "",
//           "settingBudgetCPC": "",
//           "weight": 0,
//           "revenueType": "cpd",
//           "pageLoad": 0,
//           "totalCPM": 0,
//           "isRunMonopoly": false,
//           "optionMonopoly": "",
//           "isRunBudget": false,
//           "expense": 0,
//           "maxCPMPerDay": 0
//         },
//         "banners": [{
//           "id": "k0m2wboc",
//           "html": "",
//           "width": 300,
//           "height": 150,
//           "keyword": "",
//           "isMacro": false,
//           "weight": 1,
//           "placementId": "k0m2wbbg",
//           "imageUrl": "https://cms.surfcountor.com/cmsads/2019/09/cpd_-1568618437769.png",
//           "bannerHtmlTypeId": "jhk08image",
//           "url": "",
//           "target": "_blank",
//           "bannerTypeId": "56c53393-914e-4182-9d7c-54bbbd4b304e",
//           "isIFrame": false,
//           "isDefault": false,
//           "isRelative": false,
//           "videoType": "",
//           "clickThroughUrl": "",
//           "vastTagsUrl": "",
//           "vastVideoUrl": "",
//           "videoUrl": "",
//           "duration": 0,
//           "thirdPartyTracking": "",
//           "thirdPartyUrl": "",
//           "isDocumentWrite": true,
//           "skipOffset": 0,
//           "isAddLinkSSP": false,
//           "optionBanners": [],
//           "bannerType": {
//             "id": "56c53393-914e-4182-9d7c-54bbbd4b304e",
//             "name": "Img",
//             "value": "img",
//             "isUpload": true,
//             "isVideo": false
//           },
//           "bannerHtmlType": null
//         }]
//       }
//     }, {
//       "id": "k0m30am2",
//       "positionOnShare": 3,
//       "placementId": "k0m2w0rv",
//       "shareId": "k0m2isjf",
//       "time": "[{\"startTime\":\"2019-09-15T17:00:00.000Z\",\"endTime\":null}]",
//       "placement": {
//         "id": "k0m2w0rv",
//         "width": 300,
//         "height": 150,
//         "startTime": "2019-09-15T17:00:00.000Z",
//         "endTime": null,
//         "weight": 1,
//         "revenueType": "cpd",
//         "cpdPercent": 1,
//         "isRotate": false,
//         "rotateTime": 20,
//         "price": 0,
//         "relative": 0,
//         "campaignId": "k0m2lo4r",
//         "campaign": {
//           "id": "k0m2lo4r",
//           "startTime": "2019-09-15T17:00:00.000Z",
//           "endTime": null,
//           "views": 0,
//           "viewPerSession": 0,
//           "timeResetViewCount": 0,
//           "settingBudgetCPM": "",
//           "settingBudgetCPC": "",
//           "weight": 0,
//           "revenueType": "cpd",
//           "pageLoad": 0,
//           "totalCPM": 0,
//           "isRunMonopoly": false,
//           "optionMonopoly": "",
//           "isRunBudget": false,
//           "expense": 0,
//           "maxCPMPerDay": 0
//         },
//         "banners": [{
//           "id": "k0m2w17x",
//           "html": "",
//           "width": 300,
//           "height": 150,
//           "keyword": "",
//           "isMacro": false,
//           "weight": 1,
//           "placementId": "k0m2w0rv",
//           "imageUrl": "https://cms.surfcountor.com/cmsads/2019/09/cpd_-1568618445479.png",
//           "bannerHtmlTypeId": "jhk08image",
//           "url": "",
//           "target": "_blank",
//           "bannerTypeId": "56c53393-914e-4182-9d7c-54bbbd4b304e",
//           "isIFrame": false,
//           "isDefault": false,
//           "isRelative": false,
//           "videoType": "",
//           "clickThroughUrl": "",
//           "vastTagsUrl": "",
//           "vastVideoUrl": "",
//           "videoUrl": "",
//           "duration": 0,
//           "thirdPartyTracking": "",
//           "thirdPartyUrl": "",
//           "isDocumentWrite": true,
//           "skipOffset": 0,
//           "isAddLinkSSP": false,
//           "optionBanners": [],
//           "bannerType": {
//             "id": "56c53393-914e-4182-9d7c-54bbbd4b304e",
//             "name": "Img",
//             "value": "img",
//             "isUpload": true,
//             "isVideo": false
//           },
//           "bannerHtmlType": null
//         }]
//       }
//     }]
//   }],
//   "site": {
//     "id": "k0m2d1xt",
//     "domain": "http://testcore_tam.vn",
//     "pageLoad": 0,
//     "isRunBannerDefault": false,
//     "isNoBrand": true,
//     "globalFilters": [],
//     "channels": [],
//     "campaignMonopoly": {
//       "campaigns": []
//     }
//   }
// }

// const env = {
//   "url": "https://autopro.com.vn/",
//   "domain": "https://autopro.com.vn",
//   "referrerUrl": "https://autopro.com.vn/",
//   "force": [],
//   "domainCore": "https://media1.admicro.vn",
//   "domainLog": "https://lg1.logging.admicro.vn",
//   "variables": {
//     "_ADM_Channel": "%2Fhome%2F",
//     "__RC": "4",
//     "__R": "1",
//   },
// };

// store.state.GLOBAL_FILTER_RESULT = {
//   '15': true
// };
// store.commit('INIT_ENV', env);
// const zone = new Zone(zoneData);
// var mochaAsync = (fn) => {
//   return done => {
//     fn.call().then(done, err => {
//       done(err);
//     });
//   };
// };


// describe('4. Check Fair random in choose Share', function () {
//   this.timeout(90000);

//   it('should random correct ratio', mochaAsync(async () => {
//     const times = 10000;
//     let share_k0m2isjf = 0;
//     let share_k0m2flr9 = 0;
//     let share_k0m2f1d3 = 0;
//     let isError = false;
//     for (let index = 0; index < times; index++) {
//       const activeShare = zone.activeShare();
//       if (activeShare.id === 'k0m2isjf') {
//         share_k0m2isjf++;
//       } else if (activeShare.id === 'k0m2flr9') {
//         share_k0m2flr9++;
//       } else if (activeShare.id === 'k0m2f1d3') {
//         share_k0m2f1d3++;
//       } else {
//         isError = true;
//         break;
//       }
//       store.commit('RENEW_CPM_CPD_FLAG');
//     }

//     const ratio_share_k0m2isjf = share_k0m2isjf / times;
//     const ratio_share_k0m2flr9 = share_k0m2flr9 / times;
//     const ratio_share_k0m2f1d3 = share_k0m2f1d3 / times;

//     const error = 0.03;
//     const expected1 = (ratio_share_k0m2isjf < (0.33 + error) && ratio_share_k0m2isjf > (0.33 - error));
//     const expected2 = (ratio_share_k0m2flr9 < (0.33 + error) && ratio_share_k0m2flr9 > (0.33 - error));
//     const expected3 = (ratio_share_k0m2f1d3 < (0.33 + error) && ratio_share_k0m2f1d3 > (0.33 - error));

//     describe(`ratio_share_k0m2isjf (${share_k0m2isjf}): ${ratio_share_k0m2isjf}`, () => {
//       it('checking', () => {
//         expect(expected1).to.equal(true);
//       });
//     });
//     describe(`ratio_share_k0m2flr9 (${share_k0m2flr9}): ${ratio_share_k0m2flr9}`, () => {
//       it('checking', () => {
//         expect(expected2).to.equal(true);
//       });
//     });
//     describe(`ratio_share_k0m2f1d3 (${share_k0m2f1d3}): ${ratio_share_k0m2f1d3}`, () => {
//       it('checking', () => {
//         expect(expected2).to.equal(true);
//       });
//     });
//     const result = expected1 && expected2 && expected3 && !isError;

//     expect(result).to.equal(true);
//   }));
// });
