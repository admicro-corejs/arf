import ArfTesting from './ArfTesting';

const zoneData = {
    "id": "96",
    "name": "1_LeaderBoard(980x90)",
    "height": 90,
    "width": 980,
    "css": "    max-width: 1160px;\nmargin: 0 auto;",
    "outputCss": "#zone-96 {\n  max-width: 1160px;\n  margin: 0 auto;\n}\n",
    "groupSSP": "",
    "isResponsive": true,
    "isMobile": false,
    "isTemplate": false,
    "template": "",
    "totalShare": 3,
    "status": "active",
    "isPageLoad": true,
    "isZoneVideo": false,
    "siteId": "21",
    "shares": [{
        "id": "jtgyrgpv",
        "css": "",
        "outputCss": "",
        "width": 980,
        "height": 90,
        "classes": "",
        "isRotate": false,
        "rotateTime": 0,
        "type": "single",
        "format": "",
        "zoneId": "96",
        "isTemplate": false,
        "template": "",
        "offset": "",
        "isAdPod": false,
        "duration": null,
        "sharePlacements": [{
            "id": "jto5ctob",
            "positionOnShare": 0,
            "placementId": "165440",
            "shareId": "jtgyrgpv",
            "time": "[{\"startTime\":\"1989-12-31T17:00:00.000Z\",\"endTime\":null}]",
            "placement": {
                "id": "165440",
                "width": 728,
                "height": 90,
                "startTime": "1989-12-31T17:00:00.000Z",
                "endTime": null,
                "weight": 1,
                "revenueType": "cpd",
                "cpdPercent": 2,
                "isRotate": false,
                "rotateTime": 0,
                "relative": 0,
                "campaignId": "1027935",
                "campaign": {
                    "id": "1027935",
                    "startTime": "1989-12-31T17:00:00.000Z",
                    "endTime": null,
                    "weight": 0,
                    "revenueType": "cpm",
                    "pageLoad": 0,
                    "totalCPM": 0,
                    "isRunMonopoly": false,
                    "optionMonopoly": null,
                    "isRunBudget": false
                },
                "banners": [{
                    "id": "165440",
                    "html": "<script type=\"text/javascript\">\r\ngoogle_ad_client = \"ca-pub-0204890140576859\";\r\ngoogle_ad_slot = \"4863976245\";\r\ngoogle_ad_width = 728;\r\ngoogle_ad_height = 90;\r\n</script>\r\n<script type=\"text/javascript\"\r\nsrc=\"//pagead2.googlesyndication.com/pagead/show_ads.js\">\r\n</script>",
                    "width": 728,
                    "height": 90,
                    "keyword": "",
                    "isMacro": false,
                    "weight": 1,
                    "placementId": "165440",
                    "imageUrl": "",
                    "url": "",
                    "target": "_blank",
                    "isIFrame": true,
                    "isDefault": false,
                    "isRelative": false,
                    "vastTagsUrl": "",
                    "thirdPartyTracking": "",
                    "thirdPartyUrl": null,
                    "isDocumentWrite": true,
                    "optionBanners": [{
                        "id": "jto5d14p",
                        "logical": "and",
                        "bannerId": "165440",
                        "comparison": "==",
                        "value": "",
                        "type": "channel",
                        "optionBannerChannels": [{
                            "id": "jto5d3xh",
                            "optionBannerId": "jto5d14p",
                            "channelId": "jto51rbc",
                            "channel": {
                                "id": "jto51rbc",
                                "name": "nuoc_ngoai",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "jto524zp",
                                    "name": "Location",
                                    "logical": "and",
                                    "comparison": "==",
                                    "value": "101",
                                    "globalVariables": "__RC",
                                    "channelId": "jto51rbc",
                                    "valueSelect": "__RC>=101",
                                    "logicalSelect": ">=",
                                    "optionChannelType": {
                                        "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
                                        "name": "Location",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": true,
                                        "isGlobal": true,
                                        "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]"
                                    }
                                }, {
                                    "id": "ju3js01b",
                                    "name": "Location",
                                    "logical": "and",
                                    "comparison": "!=",
                                    "value": "undefined",
                                    "globalVariables": "__RC",
                                    "channelId": "jto51rbc",
                                    "valueSelect": "__RC==undefined",
                                    "logicalSelect": "==",
                                    "optionChannelType": {
                                        "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
                                        "name": "Location",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": true,
                                        "isGlobal": true,
                                        "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]"
                                    }
                                }]
                            }
                        }]
                    }],
                    "bannerType": {
                        "name": "Script",
                        "value": "script",
                        "isUpload": false,
                        "isVideo": false
                    },
                    "bannerHtmlType": {
                        "name": "PR Tracking",
                        "value": "pr-tracking",
                        "weight": 99
                    }
                }]
            }
        }, {
            "id": "jvhkkjml",
            "positionOnShare": 0,
            "placementId": "565291",
            "shareId": "jtgyrgpv",
            "time": "[{\"startTime\":\"1989-12-31T17:00:00.000Z\",\"endTime\":null}]",
            "placement": {
                "id": "565291",
                "width": 1040,
                "height": 250,
                "startTime": "1989-12-31T17:00:00.000Z",
                "endTime": null,
                "weight": 1,
                "revenueType": "cpd",
                "cpdPercent": 3,
                "isRotate": false,
                "rotateTime": 0,
                "relative": 0,
                "campaignId": "1296111",
                "campaign": {
                    "id": "1296111",
                    "startTime": "1989-12-31T10:00:00.000Z",
                    "endTime": null,
                    "weight": 0,
                    "revenueType": "cpd",
                    "pageLoad": 0,
                    "totalCPM": 0,
                    "isRunMonopoly": false,
                    "optionMonopoly": null,
                    "isRunBudget": false
                },
                "banners": [{
                    "id": "565291",
                    "html": "<script>\n  (function() {\n    var admid=window.frameElement.id;\n    var b = parent.document.getElementById(admid);\n    b.style.width = \"100%\";\n    var f = b.parentNode.id;\n    var a = parent.wPrototype.getElementWidth(f);\n    srcimg = \"//adi.admicro.vn/adt/cpc/tvcads/files/images/vib_191118/1040x90.png\";\n    var b = document,\n        c = navigator.userAgent + \"\";\n    var d = '<div id=\"adstop\" style=\"position:relative;overflow:hidden\">',\n        d = -1 != c.indexOf(\"Android\") || -1 != c.indexOf(\"iPad\") || -1 != c.indexOf(\"iPhone\") ? d + ('<img src=\"' + srcimg + '\" border=\"0\"/><a href=\"%%CLICK_URL_UNESC%%\" target=\"_blank\" style=\"position:absolute;top:0;left:0;width:1040px;height:250px;display:block;z-index:9999;\"><span></span></a>') : d + ('<iframe id=\"demo_iframe\" src=\"//adi.admicro.vn/adt/banners/nam2015/4043/min_html5/thuongphamthuy/2018_11_16/1040x250/1040x250/1040x250.html?url=%%CLICK_URL_ESC%%\" width=\"1040\" frameborder=\"0\" scrolling=\"no\" height=\"250\"></iframe>');\n    b.write(d + \"</div>\");\n  }\n  )();\n</script>",
                    "width": 1040,
                    "height": 250,
                    "keyword": "",
                    "isMacro": false,
                    "weight": 1,
                    "placementId": "565291",
                    "imageUrl": "",
                    "url": "https://vib.com.vn/wps/portal/vn/ca-nhan?utm_source=AdMicro&utm_medium=CPD&utm_campaign=",
                    "target": "",
                    "isIFrame": true,
                    "isDefault": false,
                    "isRelative": false,
                    "vastTagsUrl": "",
                    "thirdPartyTracking": "",
                    "thirdPartyUrl": "",
                    "isDocumentWrite": true,
                    "optionBanners": [{
                        "id": "jvhklf10",
                        "logical": "and",
                        "bannerId": "565291",
                        "comparison": "==",
                        "value": "",
                        "type": "channel",
                        "optionBannerChannels": [{
                            "id": "jvhkli30",
                            "optionBannerId": "jvhklf10",
                            "channelId": "1834",
                            "channel": {
                                "id": "1834",
                                "name": "cafef.vn/vib",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "jtgyrtpk",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "cafef.vn/vib.html",
                                    "globalVariables": "",
                                    "channelId": "1834",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jtgyrtpm",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "cafef.vn/vib/",
                                    "globalVariables": "",
                                    "channelId": "1834",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xlj",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "cafef.vn/vib.html",
                                    "globalVariables": "",
                                    "channelId": "1834",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xll",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "cafef.vn/vib/",
                                    "globalVariables": "",
                                    "channelId": "1834",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }]
                            }
                        }]
                    }],
                    "bannerType": {
                        "name": "Script",
                        "value": "script",
                        "isUpload": false,
                        "isVideo": false
                    },
                    "bannerHtmlType": {
                        "name": "PR Tracking",
                        "value": "pr-tracking",
                        "weight": 99
                    }
                }]
            }
        }, {
            "id": "jvhkogg8",
            "positionOnShare": 0,
            "placementId": "565829",
            "shareId": "jtgyrgpv",
            "time": "[{\"startTime\":\"1989-12-31T17:00:00.000Z\",\"endTime\":null}]",
            "placement": {
                "id": "565829",
                "width": 1041,
                "height": 252,
                "startTime": "1989-12-31T17:00:00.000Z",
                "endTime": null,
                "weight": 1,
                "revenueType": "cpd",
                "cpdPercent": 3,
                "isRotate": false,
                "rotateTime": 0,
                "relative": 0,
                "campaignId": "1298716",
                "campaign": {
                    "id": "1298716",
                    "startTime": "1989-12-31T17:00:00.000Z",
                    "endTime": null,
                    "weight": 0,
                    "revenueType": "cpd",
                    "pageLoad": 0,
                    "totalCPM": 0,
                    "isRunMonopoly": false,
                    "optionMonopoly": null,
                    "isRunBudget": false
                },
                "banners": [{
                    "id": "565829",
                    "html": "",
                    "width": 1041,
                    "height": 252,
                    "keyword": "",
                    "isMacro": false,
                    "weight": 1,
                    "placementId": "565829",
                    "imageUrl": "//adi.admicro.vn/images/2018/12/top-banner_cafef_1544501769.jpg",
                    "url": "https://uudaicanhan.vpbank.com.vn/deluxe-savings?utm_source=AdMicro&utm_medium=CPD&utm_campaign=",
                    "target": "_blank",
                    "isIFrame": false,
                    "isDefault": false,
                    "isRelative": false,
                    "vastTagsUrl": "",
                    "thirdPartyTracking": "",
                    "thirdPartyUrl": null,
                    "isDocumentWrite": true,
                    "optionBanners": [{
                        "id": "jvhkomgs",
                        "logical": "and",
                        "bannerId": "565829",
                        "comparison": "==",
                        "value": "",
                        "type": "channel",
                        "optionBannerChannels": [{
                            "id": "jvhkopzp",
                            "optionBannerId": "jvhkomgs",
                            "channelId": "1836",
                            "channel": {
                                "id": "1836",
                                "name": "cafef.vn/vpbank",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "jtgyrtpo",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "cafef.vn/vpbank.html",
                                    "globalVariables": "",
                                    "channelId": "1836",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }]
                            }
                        }]
                    }],
                    "bannerType": {
                        "name": "Img",
                        "value": "img",
                        "isUpload": true,
                        "isVideo": false
                    },
                    "bannerHtmlType": null
                }]
            }
        }, {
            "id": "k0eo5zz0",
            "positionOnShare": 0,
            "placementId": "k0eo5zyc",
            "shareId": "jtgyrgpv",
            "time": "[{\"startTime\":\"2019-09-11T02:53:19.067Z\",\"endTime\":null}]",
            "placement": {
                "id": "k0eo5zyc",
                "width": 980,
                "height": 90,
                "startTime": "1989-12-31T17:00:00.000Z",
                "endTime": null,
                "weight": 1,
                "revenueType": "cpm",
                "cpdPercent": 0,
                "isRotate": false,
                "rotateTime": 0,
                "relative": 0,
                "campaignId": "jxvllgyh",
                "campaign": {
                    "id": "jxvllgyh",
                    "startTime": "1989-12-31T17:00:00.000Z",
                    "endTime": null,
                    "weight": 0,
                    "revenueType": "cpm",
                    "pageLoad": 0,
                    "totalCPM": 0,
                    "isRunMonopoly": false,
                    "optionMonopoly": "",
                    "isRunBudget": false
                },
                "banners": [{
                    "id": "k0eo5zym",
                    "html": "<div id=\"ssppagebid_4063\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:4063,w:980,h:90, group: \"\"}); });} else {admsspPosition({sspid:4063,w:980,h:90, group: \"\"});}</script>",
                    "width": 980,
                    "height": 90,
                    "keyword": "",
                    "isMacro": false,
                    "weight": 1,
                    "placementId": "k0eo5zyc",
                    "imageUrl": "",
                    "url": "",
                    "target": "_blank",
                    "isIFrame": false,
                    "isDefault": false,
                    "isRelative": false,
                    "vastTagsUrl": "",
                    "thirdPartyTracking": "",
                    "thirdPartyUrl": "",
                    "isDocumentWrite": false,
                    "optionBanners": [{
                        "id": "k1j2l2wc",
                        "logical": "and",
                        "bannerId": "k0eo5zym",
                        "comparison": "==",
                        "value": "",
                        "type": "channel",
                        "optionBannerChannels": [{
                            "id": "k1j2l3hc",
                            "optionBannerId": "k1j2l2wc",
                            "channelId": "k1j2cz4m",
                            "channel": {
                                "id": "k1j2cz4m",
                                "name": "cafef_dulieu",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "k1j2doji",
                                    "name": "du-lieu",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/du-lieu/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "k1j2cz4m",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "k1j2p1t6",
                                    "name": "hastc",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/hastc/detail/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "k1j2cz4m",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "k1j2qnmq",
                                    "name": "hose",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/hose/detail/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "k1j2cz4m",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "k1j2rebc",
                                    "name": "upcom",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/upcom/detail/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "k1j2cz4m",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "k1j4s29m",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "http://s.cafef.vn/du-lieu.chn",
                                    "globalVariables": "",
                                    "channelId": "k1j2cz4m",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }]
                            }
                        }]
                    }],
                    "bannerType": {
                        "name": "Script",
                        "value": "script",
                        "isUpload": false,
                        "isVideo": false
                    },
                    "bannerHtmlType": {
                        "name": "PR Tracking",
                        "value": "pr-tracking",
                        "weight": 99
                    }
                }]
            }
        }, {
            "id": "k2qduc2m",
            "positionOnShare": 0,
            "placementId": "k2qdqdxs",
            "shareId": "jtgyrgpv",
            "time": "[{\"startTime\":\"2019-11-08T17:00:00.000Z\",\"endTime\":\"2019-12-06T16:59:59.000Z\"}]",
            "placement": {
                "id": "k2qdqdxs",
                "width": 1160,
                "height": 90,
                "startTime": "2019-11-08T17:00:00.000Z",
                "endTime": "2019-12-06T16:59:59.000Z",
                "weight": 1,
                "revenueType": "cpd",
                "cpdPercent": 1,
                "isRotate": false,
                "rotateTime": 0,
                "relative": 0,
                "campaignId": "k2qdkfc7",
                "campaign": {
                    "id": "k2qdkfc7",
                    "startTime": "2019-11-07T17:00:00.000Z",
                    "endTime": null,
                    "weight": 0,
                    "revenueType": "cpd",
                    "pageLoad": 0,
                    "totalCPM": 0,
                    "isRunMonopoly": false,
                    "optionMonopoly": "",
                    "isRunBudget": false
                },
                "banners": [{
                    "id": "k2qdqefb",
                    "html": "<script>\n  (function() {\n    var admid=window.frameElement.id;\n    var b = parent.document.getElementById(admid);\n    b.style.width = \"100%\";\n    var f = b.parentNode.id;\n    var a = parent.wPrototype.getElementWidth(f);\n    \n\tvar image_url = 'https://adi.admicro.vn/adt/banners/nam2015/4043/min_html5/huyphanquoc/2019_11_09/nasun/screenshot/nasun__bg_replay.jpg';\n\tvar banner_url = '//adi.admicro.vn/adt/banners/nam2015/4043/min_html5/tranglehuyen/2019_10_30/NaSun_1160x90/NaSun_1160x90/NaSun_1160x90.html';\n    var banner_width = 1160;\n    var banner_height = 90;\n    \n\tvar b = document,\n        c = navigator.userAgent + \"\";\n    var d = '<div id=\"adstop\" style=\"position:relative;overflow:hidden\">',\n        d = -1 != c.indexOf(\"Android\") || -1 != c.indexOf(\"iPad\") || -1 != c.indexOf(\"iPhone\") ? d + ('<img src=\"' + image_url + '\" border=\"0\"/><a href=\"%%CLICK_URL_UNESC%%\" target=\"_blank\" style=\"position:absolute;top:0;left:0;width:'+banner_width+'px;height:'+banner_height+'px;display:block;z-index:9999;\"><span></span></a>') : d + ('<iframe id=\"demo_iframe\" src=\"'+banner_url+'?url=%%CLICK_URL_ESC%%\" width=\"'+banner_width+'\" frameborder=\"0\" scrolling=\"no\" height=\"'+banner_height+'\"></iframe>');\n    b.write(d + \"</div>\");\n    \n\twindow.setTimeout(function() {\n      var a = parent.wPrototype.getElementWidth(f),\n          a = 980 > a ? 980 : a,\n          b = document.getElementById(\"adstop\"),\n          a = Math.floor((banner_width - a) / 2);\n      -1 != c.indexOf(\"Android\") || -1 != c.indexOf(\"iPad\") || -1 != c.indexOf(\"iPhone\") ? b.style.marginLeft = \"-\" + (0 > a ? 0 : a) + \"px\" : b.style.marginLeft = \"-\" + (0 > a ? 0 : a) + \"px\"\n    }, 100)}\n  )();\n</script>\n\n",
                    "width": 1160,
                    "height": 90,
                    "keyword": "",
                    "isMacro": false,
                    "weight": 1,
                    "placementId": "k2qdqdxs",
                    "imageUrl": "",
                    "url": "https://nasunpaint.vn/chuong-trinh-khuyen-mai-son-nasun?utm_source=admicro&utm=CPD%20kinhdoanhtaichinh",
                    "target": "",
                    "isIFrame": true,
                    "isDefault": false,
                    "isRelative": false,
                    "vastTagsUrl": "",
                    "thirdPartyTracking": "",
                    "thirdPartyUrl": "",
                    "isDocumentWrite": true,
                    "optionBanners": [{
                        "id": "k2qdv1ve",
                        "logical": "and",
                        "bannerId": "k2qdqefb",
                        "comparison": "==",
                        "value": "",
                        "type": "channel",
                        "optionBannerChannels": [{
                            "id": "k2qdv3tf",
                            "optionBannerId": "k2qdv1ve",
                            "channelId": "1075",
                            "channel": {
                                "id": "1075",
                                "name": "Cafef.vn-Doanh Nghiá»‡p     ",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "jto394jm",
                                    "name": "doanh-nghiep",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/doanh-nghiep/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "1075",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xje",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/doanh-nghiep.chn",
                                    "globalVariables": "",
                                    "channelId": "1075",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xjg",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "doanh-nghiep",
                                    "globalVariables": "",
                                    "channelId": "1075",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }]
                            }
                        }, {
                            "id": "k2qdv3tg",
                            "optionBannerId": "k2qdv1ve",
                            "channelId": "951",
                            "channel": {
                                "id": "951",
                                "name": "cafef.vn - Thoi su",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "juay0zx8",
                                    "name": "thoi-su",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/thoi-su/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "951",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xja",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/thoi-su",
                                    "globalVariables": "",
                                    "channelId": "951",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xjc",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/thoi-su/",
                                    "globalVariables": "",
                                    "channelId": "951",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }]
                            }
                        }, {
                            "id": "k2qdv43m",
                            "optionBannerId": "k2qdv1ve",
                            "channelId": "1077",
                            "channel": {
                                "id": "1077",
                                "name": "Cafef.vn-HĂ ng hĂ³a nguyĂªn liá»‡u",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "jvbu4xjo",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/hang-hoa-nguyen-lieu.chn",
                                    "globalVariables": "",
                                    "channelId": "1077",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xjq",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/hang-hoa-nguyen-lieu/",
                                    "globalVariables": "",
                                    "channelId": "1077",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }]
                            }
                        }, {
                            "id": "k2qdv46j",
                            "optionBannerId": "k2qdv1ve",
                            "channelId": "203",
                            "channel": {
                                "id": "203",
                                "name": "Cafef.vn - KTvimo-DL-DMdautu",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "jvbu4xg6",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/Danh-muc-dau-tu",
                                    "globalVariables": "",
                                    "channelId": "203",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xg8",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/vi-mo-dau-tu.chn",
                                    "globalVariables": "",
                                    "channelId": "203",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xgc",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/du-lieu.chn",
                                    "globalVariables": "",
                                    "channelId": "203",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jxlj188c",
                                    "name": "vi-mo-dau-tu",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/vi-mo-dau-tu/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "203",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }]
                            }
                        }, {
                            "id": "k2rlr4fe",
                            "optionBannerId": "k2qdv1ve",
                            "channelId": "215",
                            "channel": {
                                "id": "215",
                                "name": "Doanh Nghiep - Thi TrÆ°á»ng - Vi_mo",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "jxcmzd31",
                                    "name": "doanh-nghiep",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/doanh-nghiep/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "215",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jxcmzd9w",
                                    "name": "thi-truong",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/thi-truong/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "215",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jxcmzdca",
                                    "name": "vi-mo-dau-tu",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/vi-mo-dau-tu/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "215",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }]
                            }
                        }]
                    }],
                    "bannerType": {
                        "name": "Script",
                        "value": "script",
                        "isUpload": false,
                        "isVideo": false
                    },
                    "bannerHtmlType": {
                        "name": "PR Tracking",
                        "value": "pr-tracking",
                        "weight": 99
                    }
                }]
            }
        }, {
            "id": "k2t7vxjw",
            "positionOnShare": 0,
            "placementId": "k2t7v8wx",
            "shareId": "jtgyrgpv",
            "time": "[{\"startTime\":\"2019-11-10T17:00:00.000Z\",\"endTime\":\"2019-12-01T16:59:59.000Z\"}]",
            "placement": {
                "id": "k2t7v8wx",
                "width": 1160,
                "height": 250,
                "startTime": "2019-11-10T17:00:00.000Z",
                "endTime": "2019-12-01T16:59:59.000Z",
                "weight": 1,
                "revenueType": "cpd",
                "cpdPercent": 1,
                "isRotate": false,
                "rotateTime": 20,
                "relative": 0,
                "campaignId": "k2t7qbac",
                "campaign": {
                    "id": "k2t7qbac",
                    "startTime": "2019-11-09T17:00:00.000Z",
                    "endTime": null,
                    "weight": 0,
                    "revenueType": "cpd",
                    "pageLoad": 0,
                    "totalCPM": 0,
                    "isRunMonopoly": false,
                    "optionMonopoly": "",
                    "isRunBudget": false
                },
                "banners": [{
                    "id": "k2t7v9mo",
                    "html": "<script>\n  (function() {\n    var admid=window.frameElement.id;\n    var b = parent.document.getElementById(admid);\n    b.style.width = \"100%\";\n    var f = b.parentNode.id;\n    var a = parent.wPrototype.getElementWidth(f);\n    \n\tvar image_url = 'https://adi.admicro.vn/adt/banners/nam2015/4043/min_html5/huyphanquoc/2019_11_10/flamingo-2/screenshot/flamingo__bg_replay.jpg';\n\tvar banner_url = 'https://adi.admicro.vn/adt/banners/nam2015/4043/min_html5/huyennguyenthithu/2019_11_09/1160x90/1160x90/1160x90_1160_250_right.html';\n    var banner_width = 1160;\n    var banner_height = 250;\n    \n\tvar b = document,\n        c = navigator.userAgent + \"\";\n    var d = '<div id=\"adstop\" style=\"position:relative;overflow:hidden\">',\n        d = -1 != c.indexOf(\"Android\") || -1 != c.indexOf(\"iPad\") || -1 != c.indexOf(\"iPhone\") ? d + ('<img src=\"' + image_url + '\" border=\"0\"/><a href=\"%%CLICK_URL_UNESC%%\" target=\"_blank\" style=\"position:absolute;top:0;left:0;width:'+banner_width+'px;height:'+banner_height+'px;display:block;z-index:9999;\"><span></span></a>') : d + ('<iframe id=\"demo_iframe\" src=\"'+banner_url+'?url=%%CLICK_URL_ESC%%\" width=\"'+banner_width+'\" frameborder=\"0\" scrolling=\"no\" height=\"'+banner_height+'\"></iframe>');\n    b.write(d + \"</div>\");\n    \n\twindow.setTimeout(function() {\n      var a = parent.wPrototype.getElementWidth(f),\n          a = 980 > a ? 980 : a,\n          b = document.getElementById(\"adstop\"),\n          a = Math.floor((banner_width - a) / 2);\n      -1 != c.indexOf(\"Android\") || -1 != c.indexOf(\"iPad\") || -1 != c.indexOf(\"iPhone\") ? b.style.marginLeft = \"-\" + (0 > a ? 0 : a) + \"px\" : b.style.marginLeft = \"-\" + (0 > a ? 0 : a) + \"px\"\n    }, 100)}\n  )();\n</script>\n\n",
                    "width": 1160,
                    "height": 250,
                    "keyword": "",
                    "isMacro": false,
                    "weight": 1,
                    "placementId": "k2t7v8wx",
                    "imageUrl": "",
                    "url": "http://bds.flamingodailai.com/flamingo-night-street/?utm_source=AdMicro&utm_medium=CPD&utm_campaign=Flamingo",
                    "target": "",
                    "isIFrame": true,
                    "isDefault": false,
                    "isRelative": false,
                    "vastTagsUrl": "",
                    "thirdPartyTracking": "",
                    "thirdPartyUrl": "",
                    "isDocumentWrite": true,
                    "optionBanners": [{
                        "id": "k2t7y56v",
                        "logical": "and",
                        "bannerId": "k2t7v9mo",
                        "comparison": "==",
                        "value": "",
                        "type": "channel",
                        "optionBannerChannels": [{
                            "id": "k2t7y68a",
                            "optionBannerId": "k2t7y56v",
                            "channelId": "1351",
                            "channel": {
                                "id": "1351",
                                "name": "Cafef.vn - BDS-CK-TCNH-TCQC",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "jxbjmgri",
                                    "name": "bat-dong-san",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/bat-dong-san/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "1351",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jxblg1do",
                                    "name": "thi-truong-chung-khoan",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/thi-truong-chung-khoan/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "1351",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jxblg1fd",
                                    "name": "tai-chinh-quoc-te",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/tai-chinh-quoc-te/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "1351",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jxblg1fe",
                                    "name": "tai-chinh-ngan-hang",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/tai-chinh-ngan-hang/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "1351",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }]
                            }
                        }]
                    }],
                    "bannerType": {
                        "name": "Script",
                        "value": "script",
                        "isUpload": false,
                        "isVideo": false
                    },
                    "bannerHtmlType": {
                        "name": "PR Tracking",
                        "value": "pr-tracking",
                        "weight": 99
                    }
                }]
            }
        }, {
            "id": "k31h50sf",
            "positionOnShare": 0,
            "placementId": "k31h4k4c",
            "shareId": "jtgyrgpv",
            "time": "[{\"startTime\":\"2019-11-17T17:00:00.000Z\",\"endTime\":\"2019-12-15T16:59:59.000Z\"}]",
            "placement": {
                "id": "k31h4k4c",
                "width": 1160,
                "height": 250,
                "startTime": "2019-11-17T17:00:00.000Z",
                "endTime": "2019-12-15T16:59:59.000Z",
                "weight": 1,
                "revenueType": "cpd",
                "cpdPercent": 1,
                "isRotate": false,
                "rotateTime": 20,
                "relative": 0,
                "campaignId": "k31h1o6q",
                "campaign": {
                    "id": "k31h1o6q",
                    "startTime": "2019-11-15T17:00:00.000Z",
                    "endTime": null,
                    "weight": 0,
                    "revenueType": "cpd",
                    "pageLoad": 0,
                    "totalCPM": 0,
                    "isRunMonopoly": false,
                    "optionMonopoly": "",
                    "isRunBudget": false
                },
                "banners": [{
                    "id": "k31h4krq",
                    "html": "<script>\n  (function() {\n    var admid=window.frameElement.id;\n    var b = parent.document.getElementById(admid);\n    b.style.width = \"100%\";\n    var f = b.parentNode.id;\n    var a = parent.wPrototype.getElementWidth(f);\n    \n\tvar image_url = 'https://adi.admicro.vn/adt/banners/nam2015/4043/min_html5/huyphanquoc/2019_11_18/flamingo/screenshot/flamingo__bg_replay.jpg';\n\tvar banner_url = 'https://adi.admicro.vn/adt/banners/nam2015/4043/min_html5/huyennguyenthithu/2019_10_30/1160x90-1/1160x90/1160x90_1160_250_right.html';\n    var banner_width = 1160;\n    var banner_height = 250;\n    \n\tvar b = document,\n        c = navigator.userAgent + \"\";\n    var d = '<div id=\"adstop\" style=\"position:relative;overflow:hidden\">',\n        d = -1 != c.indexOf(\"Android\") || -1 != c.indexOf(\"iPad\") || -1 != c.indexOf(\"iPhone\") ? d + ('<img src=\"' + image_url + '\" border=\"0\"/><a href=\"%%CLICK_URL_UNESC%%\" target=\"_blank\" style=\"position:absolute;top:0;left:0;width:'+banner_width+'px;height:'+banner_height+'px;display:block;z-index:9999;\"><span></span></a>') : d + ('<iframe id=\"demo_iframe\" src=\"'+banner_url+'?url=%%CLICK_URL_ESC%%\" width=\"'+banner_width+'\" frameborder=\"0\" scrolling=\"no\" height=\"'+banner_height+'\"></iframe>');\n    b.write(d + \"</div>\");\n    \n\twindow.setTimeout(function() {\n      var a = parent.wPrototype.getElementWidth(f),\n          a = 980 > a ? 980 : a,\n          b = document.getElementById(\"adstop\"),\n          a = Math.floor((banner_width - a) / 2);\n      -1 != c.indexOf(\"Android\") || -1 != c.indexOf(\"iPad\") || -1 != c.indexOf(\"iPhone\") ? b.style.marginLeft = \"-\" + (0 > a ? 0 : a) + \"px\" : b.style.marginLeft = \"-\" + (0 > a ? 0 : a) + \"px\"\n    }, 100)}\n  )();\n</script>\n\n",
                    "width": 1160,
                    "height": 250,
                    "keyword": "",
                    "isMacro": false,
                    "weight": 1,
                    "placementId": "k31h4k4c",
                    "imageUrl": "",
                    "url": "http://bds.flamingodailai.com/the-legend-villas?utm_source=AdMicro&utm_medium=CPD&utm_campaign=Flamingo",
                    "target": "",
                    "isIFrame": true,
                    "isDefault": false,
                    "isRelative": false,
                    "vastTagsUrl": "",
                    "thirdPartyTracking": "",
                    "thirdPartyUrl": "",
                    "isDocumentWrite": true,
                    "optionBanners": [{
                        "id": "k31h5rac",
                        "logical": "and",
                        "bannerId": "k31h4krq",
                        "comparison": "==",
                        "value": "",
                        "type": "channel",
                        "optionBannerChannels": [{
                            "id": "k31h5sbl",
                            "optionBannerId": "k31h5rac",
                            "channelId": "1351",
                            "channel": {
                                "id": "1351",
                                "name": "Cafef.vn - BDS-CK-TCNH-TCQC",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "jxbjmgri",
                                    "name": "bat-dong-san",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/bat-dong-san/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "1351",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jxblg1do",
                                    "name": "thi-truong-chung-khoan",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/thi-truong-chung-khoan/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "1351",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jxblg1fd",
                                    "name": "tai-chinh-quoc-te",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/tai-chinh-quoc-te/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "1351",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jxblg1fe",
                                    "name": "tai-chinh-ngan-hang",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/tai-chinh-ngan-hang/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "1351",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }]
                            }
                        }]
                    }],
                    "bannerType": {
                        "name": "Script",
                        "value": "script",
                        "isUpload": false,
                        "isVideo": false
                    },
                    "bannerHtmlType": {
                        "name": "PR Tracking",
                        "value": "pr-tracking",
                        "weight": 99
                    }
                }]
            }
        }, {
            "id": "k3f2a06y",
            "positionOnShare": 0,
            "placementId": "k3f28r5b",
            "shareId": "jtgyrgpv",
            "time": "[{\"startTime\":\"2019-11-25T17:00:00.000Z\",\"endTime\":\"2019-12-02T16:59:59.000Z\"}]",
            "placement": {
                "id": "k3f28r5b",
                "width": 1040,
                "height": 250,
                "startTime": "2019-11-25T17:00:00.000Z",
                "endTime": "2019-12-02T16:59:59.000Z",
                "weight": 1,
                "revenueType": "cpd",
                "cpdPercent": 1,
                "isRotate": false,
                "rotateTime": 20,
                "relative": 0,
                "campaignId": "k3f267tl",
                "campaign": {
                    "id": "k3f267tl",
                    "startTime": "2019-11-25T17:00:00.000Z",
                    "endTime": null,
                    "weight": 0,
                    "revenueType": "cpd",
                    "pageLoad": 0,
                    "totalCPM": 0,
                    "isRunMonopoly": false,
                    "optionMonopoly": "",
                    "isRunBudget": false
                },
                "banners": [{
                    "id": "k3f28s3m",
                    "html": "<script>\n  (function() {\n    var admid=window.frameElement.id;\n    var b = parent.document.getElementById(admid);\n    b.style.width = \"100%\";\n    var f = b.parentNode.id;\n    var a = parent.wPrototype.getElementWidth(f);\n    \n\tvar image_url = 'https://adi.admicro.vn/adt/banners/nam2015/4043/min_html5/huyphanquoc/2019_11_26/thaodien/screenshot/thaodien__bg_replay.jpg';\n\tvar banner_url = 'https://adi.admicro.vn/adt/banners/nam2015/4043/min_html5/linhtranthuy/2019_11_25/1040x250/1040x250/1040x250.html';\n    var banner_width = 1040;\n    var banner_height = 250;\n\t\n    var b = document,\n        c = navigator.userAgent + \"\";\n    var d = '<div id=\"adstop\" style=\"position:relative;overflow:hidden\">',\n        d = -1 != c.indexOf(\"Android\") || -1 != c.indexOf(\"iPad\") || -1 != c.indexOf(\"iPhone\") ? d + ('<img src=\"' + image_url + '\" border=\"0\"/><a href=\"%%CLICK_URL_UNESC%%\" target=\"_blank\" style=\"position:absolute;top:0;left:0;width:' + banner_width + 'px;height:' + banner_height + 'px;display:block;z-index:9999;\"><span></span></a>') : d + ('<iframe id=\"demo_iframe\" src=\"'+banner_url+'?url=%%CLICK_URL_ESC%%\" width=\"'+banner_width+'\" frameborder=\"0\" scrolling=\"no\" height=\"'+banner_height+'\"></iframe>');\n    b.write(d + \"</div>\");\n  }\n  )();\n</script>",
                    "width": 1040,
                    "height": 250,
                    "keyword": "",
                    "isMacro": false,
                    "weight": 1,
                    "placementId": "k3f28r5b",
                    "imageUrl": "",
                    "url": "https://www.q2thaodien.com.vn/vi/san-pham/?utm_source=AdMicro&utm_medium=CPD&utm_campaign=Thaodien",
                    "target": "",
                    "isIFrame": true,
                    "isDefault": false,
                    "isRelative": false,
                    "vastTagsUrl": "",
                    "thirdPartyTracking": "",
                    "thirdPartyUrl": "",
                    "isDocumentWrite": true,
                    "optionBanners": [{
                        "id": "k3f2adjk",
                        "logical": "and",
                        "bannerId": "k3f28s3m",
                        "comparison": "==",
                        "value": "",
                        "type": "channel",
                        "optionBannerChannels": [{
                            "id": "k3f2aeci",
                            "optionBannerId": "k3f2adjk",
                            "channelId": "93",
                            "channel": {
                                "id": "93",
                                "name": "Cafef.vn_Home",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "jto42coj",
                                    "name": "home",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/home/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "93",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xfj",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "http://cafef.vn/home/",
                                    "globalVariables": "",
                                    "channelId": "93",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xfq",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "==",
                                    "value": "http://cafef.vn/",
                                    "globalVariables": "",
                                    "channelId": "93",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }]
                            }
                        }]
                    }],
                    "bannerType": {
                        "name": "Script",
                        "value": "script",
                        "isUpload": false,
                        "isVideo": false
                    },
                    "bannerHtmlType": {
                        "name": "PR Tracking",
                        "value": "pr-tracking",
                        "weight": 99
                    }
                }]
            }
        }, {
            "id": "k3g5a83h",
            "positionOnShare": 0,
            "placementId": "k3g51f68",
            "shareId": "jtgyrgpv",
            "time": "[{\"startTime\":\"2019-11-26T17:00:00.000Z\",\"endTime\":\"2019-12-03T16:59:59.000Z\"}]",
            "placement": {
                "id": "k3g51f68",
                "width": 1160,
                "height": 90,
                "startTime": "2019-11-26T17:00:00.000Z",
                "endTime": "2019-12-03T16:59:59.000Z",
                "weight": 1,
                "revenueType": "cpd",
                "cpdPercent": 1,
                "isRotate": false,
                "rotateTime": 20,
                "relative": 0,
                "campaignId": "k3ds89eb",
                "campaign": {
                    "id": "k3ds89eb",
                    "startTime": "2019-11-24T17:00:00.000Z",
                    "endTime": null,
                    "weight": 0,
                    "revenueType": "cpd",
                    "pageLoad": 0,
                    "totalCPM": 0,
                    "isRunMonopoly": false,
                    "optionMonopoly": "",
                    "isRunBudget": false
                },
                "banners": [{
                    "id": "k3g51fp5",
                    "html": "<script>\n  (function() {\n    var admid=window.frameElement.id;\n    var b = parent.document.getElementById(admid);\n    b.style.width = \"100%\";\n    var f = b.parentNode.id;\n    var a = parent.wPrototype.getElementWidth(f);\n    \n\tvar image_url = 'https://adi.admicro.vn/adt/banners/nam2015/4043/min_html5/huyphanquoc/2019_11_27/sakos-2/screenshot/sakos__bg_replay.jpg';\n\tvar banner_url = 'https://adi.admicro.vn/adt/banners/nam2015/4043/min_html5/dieplamthi/2019_11_26/1160x90_sakos/1160x90_sakos/1160x90_sakos.html';\n    var banner_width = 1160;\n    var banner_height = 90;\n    \n\tvar b = document,\n        c = navigator.userAgent + \"\";\n    var d = '<div id=\"adstop\" style=\"position:relative;overflow:hidden\">',\n        d = -1 != c.indexOf(\"Android\") || -1 != c.indexOf(\"iPad\") || -1 != c.indexOf(\"iPhone\") ? d + ('<img src=\"' + image_url + '\" border=\"0\"/><a href=\"%%CLICK_URL_UNESC%%\" target=\"_blank\" style=\"position:absolute;top:0;left:0;width:'+banner_width+'px;height:'+banner_height+'px;display:block;z-index:9999;\"><span></span></a>') : d + ('<iframe id=\"demo_iframe\" src=\"'+banner_url+'?url=%%CLICK_URL_ESC%%\" width=\"'+banner_width+'\" frameborder=\"0\" scrolling=\"no\" height=\"'+banner_height+'\"></iframe>');\n    b.write(d + \"</div>\");\n    \n\twindow.setTimeout(function() {\n      var a = parent.wPrototype.getElementWidth(f),\n          a = 980 > a ? 980 : a,\n          b = document.getElementById(\"adstop\"),\n          a = Math.floor((banner_width - a) / 2);\n      -1 != c.indexOf(\"Android\") || -1 != c.indexOf(\"iPad\") || -1 != c.indexOf(\"iPhone\") ? b.style.marginLeft = \"-\" + (0 > a ? 0 : a) + \"px\" : b.style.marginLeft = \"-\" + (0 > a ? 0 : a) + \"px\"\n    }, 100)}\n  )();\n</script>\n\n",
                    "width": 1160,
                    "height": 90,
                    "keyword": "",
                    "isMacro": false,
                    "weight": 1,
                    "placementId": "k3g51f68",
                    "imageUrl": "",
                    "url": "https://sakos.vn/khuyen-mai/?utm_source=AdMicro&utm_medium=CPD&utm_campaign=sakos",
                    "target": "",
                    "isIFrame": true,
                    "isDefault": false,
                    "isRelative": false,
                    "vastTagsUrl": "",
                    "thirdPartyTracking": "",
                    "thirdPartyUrl": "",
                    "isDocumentWrite": true,
                    "optionBanners": [{
                        "id": "k3g5c006",
                        "logical": "and",
                        "bannerId": "k3g51fp5",
                        "comparison": "==",
                        "value": "",
                        "type": "channel",
                        "optionBannerChannels": [{
                            "id": "k3g5c1vo",
                            "optionBannerId": "k3g5c006",
                            "channelId": "1075",
                            "channel": {
                                "id": "1075",
                                "name": "Cafef.vn-Doanh Nghiá»‡p     ",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "jto394jm",
                                    "name": "doanh-nghiep",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/doanh-nghiep/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "1075",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xje",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/doanh-nghiep.chn",
                                    "globalVariables": "",
                                    "channelId": "1075",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xjg",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "doanh-nghiep",
                                    "globalVariables": "",
                                    "channelId": "1075",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }]
                            }
                        }, {
                            "id": "k3g5c25n",
                            "optionBannerId": "k3g5c006",
                            "channelId": "1077",
                            "channel": {
                                "id": "1077",
                                "name": "Cafef.vn-HĂ ng hĂ³a nguyĂªn liá»‡u",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "jvbu4xjo",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/hang-hoa-nguyen-lieu.chn",
                                    "globalVariables": "",
                                    "channelId": "1077",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xjq",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/hang-hoa-nguyen-lieu/",
                                    "globalVariables": "",
                                    "channelId": "1077",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }]
                            }
                        }, {
                            "id": "k3g5c25o",
                            "optionBannerId": "k3g5c006",
                            "channelId": "203",
                            "channel": {
                                "id": "203",
                                "name": "Cafef.vn - KTvimo-DL-DMdautu",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "jvbu4xg6",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/Danh-muc-dau-tu",
                                    "globalVariables": "",
                                    "channelId": "203",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xg8",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/vi-mo-dau-tu.chn",
                                    "globalVariables": "",
                                    "channelId": "203",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xgc",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/du-lieu.chn",
                                    "globalVariables": "",
                                    "channelId": "203",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jxlj188c",
                                    "name": "vi-mo-dau-tu",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/vi-mo-dau-tu/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "203",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }]
                            }
                        }, {
                            "id": "k3g5c25p",
                            "optionBannerId": "k3g5c006",
                            "channelId": "951",
                            "channel": {
                                "id": "951",
                                "name": "cafef.vn - Thoi su",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "juay0zx8",
                                    "name": "thoi-su",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/thoi-su/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "951",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xja",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/thoi-su",
                                    "globalVariables": "",
                                    "channelId": "951",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xjc",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/thoi-su/",
                                    "globalVariables": "",
                                    "channelId": "951",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }]
                            }
                        }]
                    }],
                    "bannerType": {
                        "name": "Script",
                        "value": "script",
                        "isUpload": false,
                        "isVideo": false
                    },
                    "bannerHtmlType": {
                        "name": "PR Tracking",
                        "value": "pr-tracking",
                        "weight": 99
                    }
                }]
            }
        }]
    }, {
        "id": "jy3v6tyj",
        "css": "> :first-child {\n    float: left;\n    width: 496px;\n}\n> :last-child {\n float: right;\n    width: 496px;\n}\n",
        "outputCss": "#share-jy3v6tyj > :first-child {\n  float: left;\n  width: 496px;\n}\n#share-jy3v6tyj > :last-child {\n  float: right;\n  width: 496px;\n}\n",
        "width": 980,
        "height": 90,
        "classes": "",
        "isRotate": false,
        "rotateTime": 0,
        "type": "multiple",
        "format": "1,1",
        "zoneId": "96",
        "isTemplate": false,
        "template": "",
        "offset": "",
        "isAdPod": false,
        "duration": null,
        "sharePlacements": [{
            "id": "k0g8732q",
            "positionOnShare": 1,
            "placementId": "k0g86oby",
            "shareId": "jy3v6tyj",
            "time": "[{\"startTime\":\"2019-09-11T17:00:00.000Z\",\"endTime\":null}]",
            "placement": {
                "id": "k0g86oby",
                "width": 496,
                "height": 90,
                "startTime": "2019-09-11T17:00:00.000Z",
                "endTime": null,
                "weight": 1,
                "revenueType": "pb",
                "cpdPercent": 0,
                "isRotate": false,
                "rotateTime": 20,
                "relative": 0,
                "campaignId": "1297841",
                "campaign": {
                    "id": "1297841",
                    "startTime": "2018-09-13T17:00:00.000Z",
                    "endTime": null,
                    "weight": 0,
                    "revenueType": "cpd",
                    "pageLoad": 0,
                    "totalCPM": 0,
                    "isRunMonopoly": false,
                    "optionMonopoly": "",
                    "isRunBudget": false
                },
                "banners": [{
                    "id": "k0g86ozu",
                    "html": "",
                    "width": 496,
                    "height": 89,
                    "keyword": "",
                    "isMacro": false,
                    "weight": 1,
                    "placementId": "k0g86oby",
                    "imageUrl": "https://static-cmsads.admicro.vn/cmsads/2019/10/468x-1571219449901.jpeg",
                    "url": "https://rongbay.com/Ha-Noi/Lao-dong-pho-thong-c69.html?utm_source=AdMicro",
                    "target": "_blank",
                    "isIFrame": false,
                    "isDefault": false,
                    "isRelative": false,
                    "vastTagsUrl": "",
                    "thirdPartyTracking": "",
                    "thirdPartyUrl": "",
                    "isDocumentWrite": true,
                    "optionBanners": [],
                    "bannerType": {
                        "name": "Img",
                        "value": "img",
                        "isUpload": true,
                        "isVideo": false
                    },
                    "bannerHtmlType": null
                }]
            }
        }, {
            "id": "k31g5jzn",
            "positionOnShare": 2,
            "placementId": "k31g4f1u",
            "shareId": "jy3v6tyj",
            "time": "[{\"startTime\":\"2019-11-24T17:00:00.000Z\",\"endTime\":\"2019-11-25T09:10:50.776Z\"},{\"startTime\":\"2019-11-25T09:10:50.776Z\",\"endTime\":\"2019-12-01T16:59:59.000Z\"}]",
            "placement": {
                "id": "k31g4f1u",
                "width": 496,
                "height": 90,
                "startTime": "2019-11-24T17:00:00.000Z",
                "endTime": "2019-12-01T16:59:59.000Z",
                "weight": 1,
                "revenueType": "cpd",
                "cpdPercent": 1,
                "isRotate": false,
                "rotateTime": 0,
                "relative": 0,
                "campaignId": "k2sg81lk",
                "campaign": {
                    "id": "k2sg81lk",
                    "startTime": "2019-11-09T17:00:00.000Z",
                    "endTime": null,
                    "weight": 0,
                    "revenueType": "cpd",
                    "pageLoad": 0,
                    "totalCPM": 0,
                    "isRunMonopoly": false,
                    "optionMonopoly": "",
                    "isRunBudget": false
                },
                "banners": [{
                    "id": "k31g4fd9",
                    "html": "<script>\n  (function() {\n    var admid=window.frameElement.id;\n    var b = parent.document.getElementById(admid);\n    b.style.width = \"100%\";\n    var f = b.parentNode.id;\n    var a = parent.wPrototype.getElementWidth(f);\n    \n\tvar image_url = 'https://adi.admicro.vn/adt/banners/nam2015/4043/min_html5/huyphanquoc/2019_11_16/simland/screenshot/simland_1.jpg';\n\tvar banner_url = 'https://adi.admicro.vn/adt/banners/nam2015/4043/min_html5/tranglehuyen/2019_11_16/496x90/496x90/496x90.html';\n    var banner_width = 496;\n    var banner_height = 90;\n\t\n    var b = document,\n        c = navigator.userAgent + \"\";\n    var d = '<div id=\"adstop\" style=\"position:relative;overflow:hidden\">',\n        d = -1 != c.indexOf(\"Android\") || -1 != c.indexOf(\"iPad\") || -1 != c.indexOf(\"iPhone\") ? d + ('<img src=\"' + image_url + '\" border=\"0\"/><a href=\"%%CLICK_URL_UNESC%%\" target=\"_blank\" style=\"position:absolute;top:0;left:0;width:' + banner_width + 'px;height:' + banner_height + 'px;display:block;z-index:9999;\"><span></span></a>') : d + ('<iframe id=\"demo_iframe\" src=\"'+banner_url+'?url=%%CLICK_URL_ESC%%\" width=\"'+banner_width+'\" frameborder=\"0\" scrolling=\"no\" height=\"'+banner_height+'\"></iframe>');\n    b.write(d + \"</div>\");\n  }\n  )();\n</script>",
                    "width": 496,
                    "height": 90,
                    "keyword": "",
                    "isMacro": false,
                    "weight": 1,
                    "placementId": "k31g4f1u",
                    "imageUrl": "",
                    "url": "http://sim-island.com.vn/?utm_source=AdMicro&utm_medium=CPD&utm_campaign=Sim-Island",
                    "target": "",
                    "isIFrame": true,
                    "isDefault": false,
                    "isRelative": false,
                    "vastTagsUrl": "",
                    "thirdPartyTracking": "",
                    "thirdPartyUrl": "",
                    "isDocumentWrite": true,
                    "optionBanners": [{
                        "id": "k3e8j9iz",
                        "logical": "and",
                        "bannerId": "k31g4fd9",
                        "comparison": "==",
                        "value": "",
                        "type": "channel",
                        "optionBannerChannels": [{
                            "id": "k3e8jag6",
                            "optionBannerId": "k3e8j9iz",
                            "channelId": "93",
                            "channel": {
                                "id": "93",
                                "name": "Cafef.vn_Home",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "jto42coj",
                                    "name": "home",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/home/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "93",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xfj",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "http://cafef.vn/home/",
                                    "globalVariables": "",
                                    "channelId": "93",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xfq",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "==",
                                    "value": "http://cafef.vn/",
                                    "globalVariables": "",
                                    "channelId": "93",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }]
                            }
                        }]
                    }],
                    "bannerType": {
                        "name": "Script",
                        "value": "script",
                        "isUpload": false,
                        "isVideo": false
                    },
                    "bannerHtmlType": {
                        "name": "PR Tracking",
                        "value": "pr-tracking",
                        "weight": 99
                    }
                }]
            }
        }]
    }, {
        "id": "k0g3n3na",
        "css": "",
        "outputCss": "",
        "width": 0,
        "height": 0,
        "classes": "",
        "isRotate": false,
        "rotateTime": 0,
        "type": "single",
        "format": "",
        "zoneId": "96",
        "isTemplate": false,
        "template": "",
        "offset": "",
        "isAdPod": false,
        "duration": "",
        "sharePlacements": [{
            "id": "k0g4o31l",
            "positionOnShare": 0,
            "placementId": "566501",
            "shareId": "k0g3n3na",
            "time": "[{\"startTime\":\"1989-12-31T17:00:00.000Z\",\"endTime\":null}]",
            "placement": {
                "id": "566501",
                "width": 0,
                "height": 0,
                "startTime": "1989-12-31T17:00:00.000Z",
                "endTime": null,
                "weight": 1,
                "revenueType": "cpm",
                "cpdPercent": 0,
                "isRotate": false,
                "rotateTime": 0,
                "relative": 0,
                "campaignId": "jxvllgyh",
                "campaign": {
                    "id": "jxvllgyh",
                    "startTime": "1989-12-31T17:00:00.000Z",
                    "endTime": null,
                    "weight": 0,
                    "revenueType": "cpm",
                    "pageLoad": 0,
                    "totalCPM": 0,
                    "isRunMonopoly": false,
                    "optionMonopoly": "",
                    "isRunBudget": false
                },
                "banners": [{
                    "id": "566501",
                    "html": "<div id=\"ssppagebid_562\"></div><script>if(typeof(admsspPosition)==\"undefined\"){_admloadJs(\"//media1.admicro.vn/core/ssppage.js\",function(){admsspPosition({sspid:562,w:0,h:0,group:\"562,563,566,567,568\"});});}else{admsspPosition({sspid:562,w:0,h:0,group:\"562,563,566,567,568\"});}</script>",
                    "width": 0,
                    "height": 0,
                    "keyword": "",
                    "isMacro": false,
                    "weight": 1,
                    "placementId": "566501",
                    "imageUrl": "",
                    "url": "",
                    "target": "_blank",
                    "isIFrame": false,
                    "isDefault": false,
                    "isRelative": false,
                    "vastTagsUrl": "",
                    "thirdPartyTracking": "",
                    "thirdPartyUrl": null,
                    "isDocumentWrite": true,
                    "optionBanners": [{
                        "id": "jvvswv71",
                        "logical": "or",
                        "bannerId": "566501",
                        "comparison": "==",
                        "value": "",
                        "type": "channel",
                        "optionBannerChannels": [{
                            "id": "jvvswz0q",
                            "optionBannerId": "jvvswv71",
                            "channelId": "1830",
                            "channel": {
                                "id": "1830",
                                "name": "Thá»‹ TrÆ°á»ng",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "jtji6vc9",
                                    "name": "thá»‹ trÆ°á»ng",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/thi-truong/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "1830",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }]
                            }
                        }]
                    }],
                    "bannerType": {
                        "name": "Script",
                        "value": "script",
                        "isUpload": false,
                        "isVideo": false
                    },
                    "bannerHtmlType": {
                        "name": "PR Tracking",
                        "value": "pr-tracking",
                        "weight": 99
                    }
                }, {
                    "id": "566513",
                    "html": "<div id=\"ssppagebid_1736\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1736,w:0,h:0, group:\"405,1736,1737,1738\"}); });} else {admsspPosition({sspid:1736,w:0,h:0, group:\"405,1736,1737,1738\"});}</script>",
                    "width": 0,
                    "height": 0,
                    "keyword": "",
                    "isMacro": false,
                    "weight": 1,
                    "placementId": "566501",
                    "imageUrl": "",
                    "url": "",
                    "target": "_blank",
                    "isIFrame": false,
                    "isDefault": false,
                    "isRelative": false,
                    "vastTagsUrl": "",
                    "thirdPartyTracking": "",
                    "thirdPartyUrl": null,
                    "isDocumentWrite": true,
                    "optionBanners": [{
                        "id": "jvvswvh2",
                        "logical": "or",
                        "bannerId": "566513",
                        "comparison": "==",
                        "value": "",
                        "type": "channel",
                        "optionBannerChannels": [{
                            "id": "jw0fb410",
                            "optionBannerId": "jvvswvh2",
                            "channelId": "1388",
                            "channel": {
                                "id": "1388",
                                "name": "CafeF- Sá»‘ng",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "jto41202",
                                    "name": "song",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/song/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "1388",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xkz",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/song",
                                    "globalVariables": "",
                                    "channelId": "1388",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }]
                            }
                        }]
                    }],
                    "bannerType": {
                        "name": "Script",
                        "value": "script",
                        "isUpload": false,
                        "isVideo": false
                    },
                    "bannerHtmlType": {
                        "name": "PR Tracking",
                        "value": "pr-tracking",
                        "weight": 99
                    }
                }, {
                    "id": "566529",
                    "html": "<div id=\"ssppagebid_1759\"></div><script>if(typeof(admsspPosition)==\"undefined\"){_admloadJs(\"//media1.admicro.vn/core/ssppage.js\",function(){admsspPosition({sspid:1759,w:0,h:0,group:\"1760,1761,1762,1759,399\"});});}else{admsspPosition({sspid:1759,w:0,h:0,group:\"1760,1761,1762,1759,399\"});}</script>",
                    "width": 0,
                    "height": 0,
                    "keyword": "",
                    "isMacro": false,
                    "weight": 1,
                    "placementId": "566501",
                    "imageUrl": "",
                    "url": "",
                    "target": "_blank",
                    "isIFrame": false,
                    "isDefault": false,
                    "isRelative": false,
                    "vastTagsUrl": "",
                    "thirdPartyTracking": "",
                    "thirdPartyUrl": null,
                    "isDocumentWrite": true,
                    "optionBanners": [{
                        "id": "jvvswvr0",
                        "logical": "or",
                        "bannerId": "566529",
                        "comparison": "==",
                        "value": "",
                        "type": "channel",
                        "optionBannerChannels": [{
                            "id": "jvvswzmx",
                            "optionBannerId": "jvvswvr0",
                            "channelId": "951",
                            "channel": {
                                "id": "951",
                                "name": "cafef.vn - Thoi su",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "juay0zx8",
                                    "name": "thoi-su",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/thoi-su/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "951",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xja",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/thoi-su",
                                    "globalVariables": "",
                                    "channelId": "951",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xjc",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/thoi-su/",
                                    "globalVariables": "",
                                    "channelId": "951",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }]
                            }
                        }]
                    }],
                    "bannerType": {
                        "name": "Script",
                        "value": "script",
                        "isUpload": false,
                        "isVideo": false
                    },
                    "bannerHtmlType": {
                        "name": "PR Tracking",
                        "value": "pr-tracking",
                        "weight": 99
                    }
                }, {
                    "id": "566540",
                    "html": "<div id=\"ssppagebid_1776\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1776,w:0,h:0, group:\"1776,1777,1778,404\"}); });} else {admsspPosition({sspid:1776,w:0,h:0, group:\"1776,1777,1778,404\"});}</script>",
                    "width": 0,
                    "height": 0,
                    "keyword": "",
                    "isMacro": false,
                    "weight": 1,
                    "placementId": "566501",
                    "imageUrl": "",
                    "url": "",
                    "target": "_blank",
                    "isIFrame": false,
                    "isDefault": false,
                    "isRelative": false,
                    "vastTagsUrl": "",
                    "thirdPartyTracking": "",
                    "thirdPartyUrl": null,
                    "isDocumentWrite": true,
                    "optionBanners": [{
                        "id": "jvvswvw2",
                        "logical": "or",
                        "bannerId": "566540",
                        "comparison": "==",
                        "value": "",
                        "type": "channel",
                        "optionBannerChannels": [{
                            "id": "jw0f51lr",
                            "optionBannerId": "jvvswvw2",
                            "channelId": "1076",
                            "channel": {
                                "id": "1076",
                                "name": "Cafef.vn_Kinh táº¿ vÄ© mĂ´",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "jto3f4vc",
                                    "name": "vi-mo-dau-tu",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/vi-mo-dau-tu/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "1076",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }]
                            }
                        }]
                    }],
                    "bannerType": {
                        "name": "Script",
                        "value": "script",
                        "isUpload": false,
                        "isVideo": false
                    },
                    "bannerHtmlType": {
                        "name": "PR Tracking",
                        "value": "pr-tracking",
                        "weight": 99
                    }
                }, {
                    "id": "566552",
                    "html": "<div id=\"ssppagebid_1799\"></div><script>if(typeof(admsspPosition)==\"undefined\"){_admloadJs(\"//media1.admicro.vn/core/ssppage.js\",function(){admsspPosition({sspid:1799,w:0,h:0,group:\"403,1799,1800,1801,1802\"});});}else{admsspPosition({sspid:1799,w:0,h:0,group:\"403,1799,1800,1801,1802\"});}</script>",
                    "width": 0,
                    "height": 0,
                    "keyword": "",
                    "isMacro": false,
                    "weight": 1,
                    "placementId": "566501",
                    "imageUrl": "",
                    "url": "",
                    "target": "_blank",
                    "isIFrame": false,
                    "isDefault": false,
                    "isRelative": false,
                    "vastTagsUrl": "",
                    "thirdPartyTracking": "",
                    "thirdPartyUrl": null,
                    "isDocumentWrite": true,
                    "optionBanners": [{
                        "id": "jvvsww4z",
                        "logical": "or",
                        "bannerId": "566552",
                        "comparison": "==",
                        "value": "",
                        "type": "channel",
                        "optionBannerChannels": [{
                            "id": "jw0exjfc",
                            "optionBannerId": "jvvsww4z",
                            "channelId": "1146",
                            "channel": {
                                "id": "1146",
                                "name": "cafef.vn - TĂ i chĂ­nh quá»‘c táº¿",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "jto3v6o4",
                                    "name": "tai-chinh-quoc-te",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/tai-chinh-quoc-te/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "1146",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }]
                            }
                        }]
                    }],
                    "bannerType": {
                        "name": "Script",
                        "value": "script",
                        "isUpload": false,
                        "isVideo": false
                    },
                    "bannerHtmlType": {
                        "name": "PR Tracking",
                        "value": "pr-tracking",
                        "weight": 99
                    }
                }, {
                    "id": "566567",
                    "html": "<div id=\"ssppagebid_1816\"></div><script>if(typeof(admsspPosition)==\"undefined\"){_admloadJs(\"//media1.admicro.vn/core/ssppage.js\",function(){admsspPosition({sspid:1816,w:0,h:0,group:\"402,1816,1817,1818,1819\"});});}else{admsspPosition({sspid:1816,w:0,h:0,group:\"402,1816,1817,1818,1819\"});}</script>",
                    "width": 0,
                    "height": 0,
                    "keyword": "",
                    "isMacro": false,
                    "weight": 1,
                    "placementId": "566501",
                    "imageUrl": "",
                    "url": "",
                    "target": "_blank",
                    "isIFrame": false,
                    "isDefault": false,
                    "isRelative": false,
                    "vastTagsUrl": "",
                    "thirdPartyTracking": "",
                    "thirdPartyUrl": null,
                    "isDocumentWrite": true,
                    "optionBanners": [{
                        "id": "jvvswwdm",
                        "logical": "or",
                        "bannerId": "566567",
                        "comparison": "==",
                        "value": "",
                        "type": "channel",
                        "optionBannerChannels": [{
                            "id": "jw0ev1lz",
                            "optionBannerId": "jvvswwdm",
                            "channelId": "1145",
                            "channel": {
                                "id": "1145",
                                "name": "cafef.vn - TĂ i chĂ­nh ngĂ¢n hĂ ng",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "jto3l81k",
                                    "name": "tai-chinh-ngan-hang",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/tai-chinh-ngan-hang/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "1145",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xk1",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/tai-chinh-ngan-hang.",
                                    "globalVariables": "",
                                    "channelId": "1145",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xk3",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/tai-chinh-ngan-hang/",
                                    "globalVariables": "",
                                    "channelId": "1145",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xk5",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/ngan-hang",
                                    "globalVariables": "",
                                    "channelId": "1145",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }]
                            }
                        }]
                    }],
                    "bannerType": {
                        "name": "Script",
                        "value": "script",
                        "isUpload": false,
                        "isVideo": false
                    },
                    "bannerHtmlType": {
                        "name": "PR Tracking",
                        "value": "pr-tracking",
                        "weight": 99
                    }
                }, {
                    "id": "566580",
                    "html": "<div id=\"ssppagebid_1838\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1838,w:0,h:0, group:\"1840,1841,401,1838,1839\"}); });} else {admsspPosition({sspid:1838,w:0,h:0, group:\"1840,1841,401,1838,1839\"});}</script>",
                    "width": 0,
                    "height": 0,
                    "keyword": "",
                    "isMacro": false,
                    "weight": 1,
                    "placementId": "566501",
                    "imageUrl": "",
                    "url": "",
                    "target": "_blank",
                    "isIFrame": false,
                    "isDefault": false,
                    "isRelative": false,
                    "vastTagsUrl": "",
                    "thirdPartyTracking": "",
                    "thirdPartyUrl": null,
                    "isDocumentWrite": true,
                    "optionBanners": [{
                        "id": "jw1ixncb",
                        "logical": "and",
                        "bannerId": "566580",
                        "comparison": "==",
                        "value": "",
                        "type": "channel",
                        "optionBannerChannels": [{
                            "id": "jw1ixrep",
                            "optionBannerId": "jw1ixncb",
                            "channelId": "1075",
                            "channel": {
                                "id": "1075",
                                "name": "Cafef.vn-Doanh Nghiá»‡p     ",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "jto394jm",
                                    "name": "doanh-nghiep",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/doanh-nghiep/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "1075",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xje",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/doanh-nghiep.chn",
                                    "globalVariables": "",
                                    "channelId": "1075",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xjg",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "doanh-nghiep",
                                    "globalVariables": "",
                                    "channelId": "1075",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }]
                            }
                        }]
                    }],
                    "bannerType": {
                        "name": "Script",
                        "value": "script",
                        "isUpload": false,
                        "isVideo": false
                    },
                    "bannerHtmlType": {
                        "name": "PR Tracking",
                        "value": "pr-tracking",
                        "weight": 99
                    }
                }, {
                    "id": "566592",
                    "html": "<div id=\"ssppagebid_1654\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1654,w:0,h:0, group:\"400,1654,1655,1656,1657\"}); });} else {admsspPosition({sspid:1654,w:0,h:0, group:\"400,1654,1655,1656,1657\"});}</script>",
                    "width": 0,
                    "height": 0,
                    "keyword": "",
                    "isMacro": false,
                    "weight": 1,
                    "placementId": "566501",
                    "imageUrl": "",
                    "url": "",
                    "target": "_blank",
                    "isIFrame": false,
                    "isDefault": false,
                    "isRelative": false,
                    "vastTagsUrl": "",
                    "thirdPartyTracking": "",
                    "thirdPartyUrl": null,
                    "isDocumentWrite": true,
                    "optionBanners": [{
                        "id": "jvvswwsw",
                        "logical": "or",
                        "bannerId": "566592",
                        "comparison": "==",
                        "value": "",
                        "type": "channel",
                        "optionBannerChannels": [{
                            "id": "jw1iafky",
                            "optionBannerId": "jvvswwsw",
                            "channelId": "94",
                            "channel": {
                                "id": "94",
                                "name": "Cafef.vn_ThiTruongChungKhoan",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "jto43xnu",
                                    "name": "thi-truong-chung-khoan",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/thi-truong-chung-khoan/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "94",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xfs",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/thi-truong-chung-khoan",
                                    "globalVariables": "",
                                    "channelId": "94",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xfu",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/chung-khoan",
                                    "globalVariables": "",
                                    "channelId": "94",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }]
                            }
                        }]
                    }],
                    "bannerType": {
                        "name": "Script",
                        "value": "script",
                        "isUpload": false,
                        "isVideo": false
                    },
                    "bannerHtmlType": {
                        "name": "PR Tracking",
                        "value": "pr-tracking",
                        "weight": 99
                    }
                }, {
                    "id": "566604",
                    "html": "<div id=\"ssppagebid_1642\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1642,w:0,h:0, group:\"1642,1643,1644,397\"}); });} else {admsspPosition({sspid:1642,w:0,h:0, group:\"1642,1643,1644,397\"});}</script>",
                    "width": 0,
                    "height": 0,
                    "keyword": "",
                    "isMacro": false,
                    "weight": 1,
                    "placementId": "566501",
                    "imageUrl": "",
                    "url": "",
                    "target": "_blank",
                    "isIFrame": false,
                    "isDefault": false,
                    "isRelative": false,
                    "vastTagsUrl": "",
                    "thirdPartyTracking": "",
                    "thirdPartyUrl": null,
                    "isDocumentWrite": true,
                    "optionBanners": [{
                        "id": "jvvswwxd",
                        "logical": "or",
                        "bannerId": "566604",
                        "comparison": "==",
                        "value": "",
                        "type": "channel",
                        "optionBannerChannels": [{
                            "id": "jxu5c29h",
                            "optionBannerId": "jvvswwxd",
                            "channelId": "1144",
                            "channel": {
                                "id": "1144",
                                "name": "cafef.vn - Báº¥t Ä‘á»™ng sáº£n",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "jto3k0ta",
                                    "name": "bat-dong-san",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/bat-dong-san/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "1144",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xjx",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/bat-dong-san.",
                                    "globalVariables": "",
                                    "channelId": "1144",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xjz",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/bat-dong-san/",
                                    "globalVariables": "",
                                    "channelId": "1144",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }]
                            }
                        }]
                    }],
                    "bannerType": {
                        "name": "Script",
                        "value": "script",
                        "isUpload": false,
                        "isVideo": false
                    },
                    "bannerHtmlType": {
                        "name": "PR Tracking",
                        "value": "pr-tracking",
                        "weight": 99
                    }
                }, {
                    "id": "566618",
                    "html": "<div id=\"ssppagebid_1638\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1638,w:980,h:90, group:\"1638,1639,4062,3519\"}); });} else {admsspPosition({sspid:1638,w:980,h:90, group:\"1638,1639,4062,3519\"});}</script>",
                    "width": 980,
                    "height": 90,
                    "keyword": "",
                    "isMacro": false,
                    "weight": 1,
                    "placementId": "566501",
                    "imageUrl": "",
                    "url": "",
                    "target": "",
                    "isIFrame": false,
                    "isDefault": false,
                    "isRelative": false,
                    "vastTagsUrl": "",
                    "thirdPartyTracking": "",
                    "thirdPartyUrl": "",
                    "isDocumentWrite": true,
                    "optionBanners": [{
                        "id": "jvvswxaq",
                        "logical": "or",
                        "bannerId": "566618",
                        "comparison": "==",
                        "value": "",
                        "type": "channel",
                        "optionBannerChannels": [{
                            "id": "jvvsx1ax",
                            "optionBannerId": "jvvswxaq",
                            "channelId": "93",
                            "channel": {
                                "id": "93",
                                "name": "Cafef.vn_Home",
                                "siteId": "21",
                                "optionChannels": [{
                                    "id": "jto42coj",
                                    "name": "home",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "/home/",
                                    "globalVariables": "_ADM_Channel",
                                    "channelId": "93",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                        "name": "Variable",
                                        "isInputLink": false,
                                        "isSelectOption": false,
                                        "isVariable": true,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xfj",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "=~",
                                    "value": "http://cafef.vn/home/",
                                    "globalVariables": "",
                                    "channelId": "93",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }, {
                                    "id": "jvbu4xfq",
                                    "name": "Site Page-URL",
                                    "logical": "or",
                                    "comparison": "==",
                                    "value": "http://cafef.vn/",
                                    "globalVariables": "",
                                    "channelId": "93",
                                    "valueSelect": "",
                                    "logicalSelect": "",
                                    "optionChannelType": {
                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                        "name": "Site Page-URL",
                                        "isInputLink": true,
                                        "isSelectOption": false,
                                        "isVariable": false,
                                        "status": "active",
                                        "isMultiSelect": false,
                                        "isGlobal": false,
                                        "storageType": null
                                    }
                                }]
                            }
                        }]
                    }],
                    "bannerType": {
                        "name": "Script",
                        "value": "script",
                        "isUpload": false,
                        "isVideo": false
                    },
                    "bannerHtmlType": {
                        "name": "PR Tracking",
                        "value": "pr-tracking",
                        "weight": 99
                    }
                }]
            }
        }]
    }],
    "site": {
        "id": "21",
        "domain": "http://cafef.vn",
        "pageLoad": {
            "totalPageload": 3,
            "campaigns": []
        },
        "isRunBannerDefault": false,
        "isNoBrand": false,
        "globalFilters": [],
        "channels": [{
            "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
            "name": "Location",
            "isGlobal": true,
            "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]"
        }],
        "campaignMonopoly": {
            "campaigns": []
        }
    }
};

const env = {
  "domain": "http://cafef.vn",
  "url": "http://cafef.vn/",
  "referrerUrl": "http://cafef.vn/",
  "force": [],
  "domainCore": "https://media1.admicro.vn",
  "domainLog": "https://lg1.logging.admicro.vn",
  "currentChannel": "%2fhome%2f",
  "variables": {
    "_ADM_Channel": "%2fhome%2f",
    "__RC": "4",
    "__R": "1"
  }
};

const shareEvaluate = [
  {id: 'jxcllukp', count: 111, error: 3},
  {id: 'jtgyrgq3', count: 222, error: 3},
];
const bannerEvaluate = [
  {id: 'k15uoloz', count: 111, error: 3},
  {id: 'k1ulf080', count: 111, error: 3},
  {id: '566617', count: 111, error: 3},
  {id: 'k1q7pco6', count: 111, error: 3},
];

const shareEvaluateForFirst = [
  {id: 'jxcllukp', percent: 33, error: 3},
  {id: 'jtgyrgq3', percent: 66, error: 3},
];
const bannerEvaluateForFirst = [
  {id: 'k15uoloz', percent: 33, error: 3},
  {id: 'k1ulf080', percent: 33, error: 3},
  {id: '566617', percent: 33, error: 3},
  {id: 'k1q7pco6', percent: 33, error: 3},
];
const testFirst = new ArfTesting(env, zoneData, shareEvaluateForFirst, bannerEvaluateForFirst);
testFirst.testing(666, false, 'FIRST Checking');

// const testFirst = new ArfTesting(env, zoneData, shareEvaluate, bannerEvaluate);
// testFirst.testing(333, true, 'SOV Checking');
