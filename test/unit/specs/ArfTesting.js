/* eslint-disable */
import store from '../../../src/store';
import Turn from '../../../src/vendor/ArfStorage/storage/TrackLoad/Turn';
import Zone from '../../../src/models/Zone';

var mochaAsync = (fn) => {
  return done => {
    fn.call().then(done, err => {
      done(err);
    });
  };
};

function statistical(arr, times, evaluateConfigs) {
  evaluateConfigs = evaluateConfigs || [];
  const distinct = (val, index, self) => {
    return self.indexOf(val) === index;
  };
  const uniqueitems = arr.filter(distinct);
  let result = [];
  let globalEvaluate = true;

  for (let index = 0; index < uniqueitems.length; index++) {
    const uniqueITem = uniqueitems[index];
    let count = 0;
    for (let index = 0; index < arr.length; index++) {
      const element = arr[index];
      if (element === uniqueITem) count++;
    }
    const percent = (count * 100) / times;
    let evaluateValue = true;
    const evaluateConfig = evaluateConfigs.filter(e => e.id === uniqueITem)[0];
    if (evaluateConfig) {
      const errorE = evaluateConfig.error;
      const percentE = evaluateConfig.percent;
      const countE = evaluateConfig.count;
      if (percentE) {
        evaluateValue = percentE + errorE >= percent && percentE - errorE <= percent;
      } else if (countE) {
        evaluateValue = countE === count;
      }
      globalEvaluate = globalEvaluate && evaluateValue;
    } else {
      globalEvaluate = globalEvaluate && true;
    }
    result.push({
      id: uniqueITem,
      count,
      percent: count / times,
      evaluate: evaluateValue,
    });
  }

  return {
    results: result,
    globalEvaluate,
  };
}

function pushTurn(zoneId, shareId, totalShare, activePlacements) {
  const turn = new Turn(activePlacements.map(item => item.id), shareId, [], store.state.PAGE_LOAD.activePageLoad.campaignId, store.state.FULL_CPM);
  store.commit('TRACK_TURN', { zoneId, turn, totalShare, isPageLoad: true });
  store.commit('SAVE_TRACK_LOAD_DATA');
}


export default class ArfTesting {
  constructor(env, zoneData, evaluateShare, evaluateBanner) {
    this.zoneData = zoneData;
    this.evaluateShare = evaluateShare;
    this.evaluateBanner = evaluateBanner;
    store.commit('INIT_ENV', env);
    store.state.GLOBAL_FILTER_RESULT[zoneData.site.id] = true;
  }

  test(refreshTimes, isSaveHistory) {
    const times = refreshTimes || 10000;
    let shareAppear = [];
    let bannerAppear = [];
    // const cpmCheck = [];
    let cpmFlagWrong = 0;
    let cpmFullCount = 0;

    for (let index = 0; index < times; index++) {
      const zone = new Zone(this.zoneData);

      const activeShare = zone.activeShare();
      const activePlacements = activeShare.activeSharePlacements(false, []).map(i => i.placement);
      const activeBanners = activePlacements.map(i => i.activeBanner()).map(i => i.id);
      if (isSaveHistory) pushTurn(zone.id, activeShare.id, zone.totalShare, activePlacements);
      const exitsCPD = activePlacements.reduce((res, placement) => {
        if (placement.revenueType === 'cpd') {
          return true;
        }
        return res && false;
      }, 0);
      if (store.state.FULL_CPM === true && exitsCPD === true) {
          cpmFlagWrong++;
          // this for debug cpm flag
          // const detail = {};
          // detail.turnsLeft = store.state.TURNS_LEFT;
          // detail.cpmfull = store.state.IS_FULL_CPM;
          // detail.cpdFlag = store.state.CPD_FORCE;
          // detail.shareId = {id: activeShare.id, limit: activeShare.limitTurn()};
          // detail.activePlacements = activePlacements.map(cp => ({id: cp.id, revenueType: cp.revenueType}));
          // cpmCheck.push(detail);
      }

      if (store.state.FULL_CPM) {
        cpmFullCount++;
      }

      shareAppear.push(activeShare.id);
      activeBanners.forEach(bannerId => {
        bannerAppear.push(bannerId);
      });
      store.commit('RENEW_CPM_CPD_FLAG', isSaveHistory);
    }

    const shareStatistical = statistical(shareAppear, times, this.evaluateShare);
    const bannerStatistical = statistical(bannerAppear, times, this.evaluateBanner);
    const globalEvaluate = shareStatistical.globalEvaluate && bannerStatistical.globalEvaluate;

    return {
      shareStatistical,
      bannerStatistical,
      cpmFlagWrong,
      globalEvaluate,
      cpmFullCount,
      // cpmCheck,
    };
  }

  testing(refreshTimes, isSaveHistory, msg) {
    const context = this;
    describe(msg, function () {
      this.timeout(90000);
      it(isSaveHistory ? 'SOV checking' : 'FIRST TIME checking', mochaAsync(async () => {
    
        const testResult = context.test(refreshTimes, isSaveHistory);

        describe(`cpm full count: ${testResult.cpmFullCount}`, () => {
          it('check cpm full', () => {
            expect(true).to.equal(true);
          });
        });
    
        describe(`cpm wrong: ${testResult.cpmFlagWrong}`, () => {
          it('cpm wrong count = 0', () => {
            expect(testResult.cpmFlagWrong === 0).to.equal(true);
          });
        });
    
        for (let index = 0; index < testResult.shareStatistical.results.length; index++) {
          const detail = testResult.shareStatistical.results[index];
          describe(`share ${detail.id}: ${detail.percent}(${detail.count})`, () => {
            it(isSaveHistory ? 'SOV checking' : 'FIRST TIME checking', () => {
              expect(detail.evaluate).to.equal(true);
            });
          })
        }
    
        for (let index = 0; index < testResult.bannerStatistical.results.length; index++) {
          const detail = testResult.bannerStatistical.results[index];
          describe(`banner ${detail.id}: ${detail.percent}(${detail.count})`, () => {
            it(isSaveHistory ? 'SOV checking' : 'FIRST TIME checking', () => {
              expect(detail.evaluate).to.equal(true);
            });
          })
        }
    
        expect(testResult.globalEvaluate).to.equal(true);
      }));
    });
  }
}