// /* eslint-disable */
// import store from '../../../src/store';
// import Turn from '../../../src/vendor/ArfStorage/storage/TrackLoad/Turn';
// import Zone from '../../../src/models/Zone';

// var mochaAsync = (fn) => {
//   return done => {
//     fn.call().then(done, err => {
//       done(err);
//     });
//   };
// };

// function pushTurn(zoneId, shareId, totalShare, activePlacements) {
//   const turn = new Turn(activePlacements.map(item => item.id), shareId, [], store.state.PAGE_LOAD.activePageLoad.campaignId, store.state.FULL_CPM);
//   store.commit('TRACK_TURN', {
//     zoneId,
//     turn,
//     totalShare
//   });
//   store.commit('SAVE_TRACK_LOAD_DATA');
// }

// describe('8. Testing zone - 3 cpd', function () {
//   this.timeout(90000);
//   const zoneData = {
//     "id": "k18ruv91",
//     "name": "zone_test_10/02/2019",
//     "height": 100,
//     "width": 300,
//     "css": "",
//     "outputCss": "",
//     "groupSSP": "",
//     "isResponsive": true,
//     "isMobile": false,
//     "isTemplate": false,
//     "template": "",
//     "totalShare": 3,
//     "status": "active",
//     "isPageLoad": true,
//     "isZoneVideo": false,
//     "positionOnSite": "0",
//     "timeBetweenAds": 0,
//     "siteId": "k0xoyjkc",
//     "shares": [{
//       "id": "k18ruvyg",
//       "css": "",
//       "outputCss": "",
//       "width": 300,
//       "height": 100,
//       "classes": "",
//       "isRotate": false,
//       "rotateTime": 20,
//       "type": "single",
//       "format": "",
//       "zoneId": "k18ruv91",
//       "isTemplate": false,
//       "template": "",
//       "offset": "",
//       "isAdPod": false,
//       "duration": "",
//       "sharePlacements": [{
//         "id": "k18s4iii",
//         "positionOnShare": 0,
//         "placementId": "k18s485i",
//         "shareId": "k18ruvyg",
//         "time": "[{\"startTime\":\"2019-10-01T17:00:00.000Z\",\"endTime\":null}]",
//         "placement": {
//           "id": "k18s485i",
//           "width": 300,
//           "height": 250,
//           "startTime": "2019-10-01T17:00:00.000Z",
//           "endTime": null,
//           "weight": 1,
//           "revenueType": "cpm",
//           "cpdPercent": 0,
//           "isRotate": false,
//           "rotateTime": 20,
//           "price": 0,
//           "relative": 0,
//           "campaignId": "k18s27ek",
//           "campaign": {
//             "id": "k18s27ek",
//             "startTime": "2019-10-01T17:00:00.000Z",
//             "endTime": null,
//             "views": 0,
//             "viewPerSession": 0,
//             "timeResetViewCount": 0,
//             "settingBudgetCPM": "",
//             "settingBudgetCPC": "",
//             "weight": 0,
//             "revenueType": "cpd",
//             "pageLoad": 0,
//             "totalCPM": 0,
//             "isRunMonopoly": false,
//             "optionMonopoly": "",
//             "isRunBudget": false,
//             "expense": 0,
//             "maxCPMPerDay": 0
//           },
//           "banners": [{
//             "id": "k18s48k8",
//             "html": "",
//             "width": 300,
//             "height": 250,
//             "keyword": "",
//             "isMacro": false,
//             "weight": 1,
//             "placementId": "k18s485i",
//             "imageUrl": "https://cms.surfcountor.com/cmsads/2019/10/300x-1569991010238.jpeg",
//             "bannerHtmlTypeId": "jhk08image",
//             "url": "",
//             "target": "_blank",
//             "bannerTypeId": "56c53393-914e-4182-9d7c-54bbbd4b304e",
//             "isIFrame": false,
//             "isDefault": false,
//             "isRelative": false,
//             "videoType": "",
//             "clickThroughUrl": "",
//             "vastTagsUrl": "",
//             "vastVideoUrl": "",
//             "videoUrl": "",
//             "duration": 0,
//             "thirdPartyTracking": "",
//             "thirdPartyUrl": "",
//             "isDocumentWrite": true,
//             "skipOffset": 0,
//             "isAddLinkSSP": false,
//             "optionBanners": [],
//             "bannerType": {
//               "id": "56c53393-914e-4182-9d7c-54bbbd4b304e",
//               "name": "Img",
//               "value": "img",
//               "isUpload": true,
//               "isVideo": false
//             },
//             "bannerHtmlType": null
//           }]
//         }
//       }]
//     }, {
//       "id": "k18rvgww",
//       "css": "",
//       "outputCss": "",
//       "width": 300,
//       "height": 600,
//       "classes": "",
//       "isRotate": false,
//       "rotateTime": 0,
//       "type": "multiple",
//       "format": "1,1",
//       "zoneId": "k18ruv91",
//       "isTemplate": false,
//       "template": "",
//       "offset": "",
//       "isAdPod": false,
//       "duration": null,
//       "sharePlacements": [{
//         "id": "k18s6cxf",
//         "positionOnShare": 1,
//         "placementId": "k18s61hp",
//         "shareId": "k18rvgww",
//         "time": "[{\"startTime\":\"2019-10-01T17:00:00.000Z\",\"endTime\":null}]",
//         "placement": {
//           "id": "k18s61hp",
//           "width": 300,
//           "height": 250,
//           "startTime": "2019-10-01T17:00:00.000Z",
//           "endTime": null,
//           "weight": 1,
//           "revenueType": "cpd",
//           "cpdPercent": 1,
//           "isRotate": false,
//           "rotateTime": 20,
//           "price": 0,
//           "relative": 0,
//           "campaignId": "k18s27ek",
//           "campaign": {
//             "id": "k18s27ek",
//             "startTime": "2019-10-01T17:00:00.000Z",
//             "endTime": null,
//             "views": 0,
//             "viewPerSession": 0,
//             "timeResetViewCount": 0,
//             "settingBudgetCPM": "",
//             "settingBudgetCPC": "",
//             "weight": 0,
//             "revenueType": "cpd",
//             "pageLoad": 0,
//             "totalCPM": 0,
//             "isRunMonopoly": false,
//             "optionMonopoly": "",
//             "isRunBudget": false,
//             "expense": 0,
//             "maxCPMPerDay": 0
//           },
//           "banners": [{
//             "id": "k18s620t",
//             "html": "",
//             "width": 300,
//             "height": 250,
//             "keyword": "",
//             "isMacro": false,
//             "weight": 1,
//             "placementId": "k18s61hp",
//             "imageUrl": "https://cms.surfcountor.com/cmsads/2019/10/300x-1569991057504.jpeg",
//             "bannerHtmlTypeId": "jhk08image",
//             "url": "",
//             "target": "_blank",
//             "bannerTypeId": "56c53393-914e-4182-9d7c-54bbbd4b304e",
//             "isIFrame": false,
//             "isDefault": false,
//             "isRelative": false,
//             "videoType": "",
//             "clickThroughUrl": "",
//             "vastTagsUrl": "",
//             "vastVideoUrl": "",
//             "videoUrl": "",
//             "duration": 0,
//             "thirdPartyTracking": "",
//             "thirdPartyUrl": "",
//             "isDocumentWrite": true,
//             "skipOffset": 0,
//             "isAddLinkSSP": false,
//             "optionBanners": [],
//             "bannerType": {
//               "id": "56c53393-914e-4182-9d7c-54bbbd4b304e",
//               "name": "Img",
//               "value": "img",
//               "isUpload": true,
//               "isVideo": false
//             },
//             "bannerHtmlType": null
//           }]
//         }
//       }, {
//         "id": "k18s7kaf",
//         "positionOnShare": 2,
//         "placementId": "k18s7dcb",
//         "shareId": "k18rvgww",
//         "time": "[{\"startTime\":\"2019-10-01T17:00:00.000Z\",\"endTime\":null}]",
//         "placement": {
//           "id": "k18s7dcb",
//           "width": 300,
//           "height": 250,
//           "startTime": "2019-10-01T17:00:00.000Z",
//           "endTime": null,
//           "weight": 1,
//           "revenueType": "cpd",
//           "cpdPercent": 1,
//           "isRotate": false,
//           "rotateTime": 20,
//           "price": 0,
//           "relative": 0,
//           "campaignId": "k18s27ek",
//           "campaign": {
//             "id": "k18s27ek",
//             "startTime": "2019-10-01T17:00:00.000Z",
//             "endTime": null,
//             "views": 0,
//             "viewPerSession": 0,
//             "timeResetViewCount": 0,
//             "settingBudgetCPM": "",
//             "settingBudgetCPC": "",
//             "weight": 0,
//             "revenueType": "cpd",
//             "pageLoad": 0,
//             "totalCPM": 0,
//             "isRunMonopoly": false,
//             "optionMonopoly": "",
//             "isRunBudget": false,
//             "expense": 0,
//             "maxCPMPerDay": 0
//           },
//           "banners": [{
//             "id": "k18s7dvr",
//             "html": "",
//             "width": 300,
//             "height": 250,
//             "keyword": "",
//             "isMacro": false,
//             "weight": 1,
//             "placementId": "k18s7dcb",
//             "imageUrl": "https://cms.surfcountor.com/cmsads/2019/10/bann-1569991155421.jpeg",
//             "bannerHtmlTypeId": "jhk08image",
//             "url": "",
//             "target": "_blank",
//             "bannerTypeId": "56c53393-914e-4182-9d7c-54bbbd4b304e",
//             "isIFrame": false,
//             "isDefault": false,
//             "isRelative": false,
//             "videoType": "",
//             "clickThroughUrl": "",
//             "vastTagsUrl": "",
//             "vastVideoUrl": "",
//             "videoUrl": "",
//             "duration": 0,
//             "thirdPartyTracking": "",
//             "thirdPartyUrl": "",
//             "isDocumentWrite": true,
//             "skipOffset": 0,
//             "isAddLinkSSP": false,
//             "optionBanners": [],
//             "bannerType": {
//               "id": "56c53393-914e-4182-9d7c-54bbbd4b304e",
//               "name": "Img",
//               "value": "img",
//               "isUpload": true,
//               "isVideo": false
//             },
//             "bannerHtmlType": null
//           }]
//         }
//       }, {
//         "id": "k18sb38v",
//         "positionOnShare": 2,
//         "placementId": "k18sa1yb",
//         "shareId": "k18rvgww",
//         "time": "[{\"startTime\":\"2019-10-01T17:00:00.000Z\",\"endTime\":null}]",
//         "placement": {
//           "id": "k18sa1yb",
//           "width": 300,
//           "height": 250,
//           "startTime": "2019-10-01T17:00:00.000Z",
//           "endTime": null,
//           "weight": 1,
//           "revenueType": "cpd",
//           "cpdPercent": 1,
//           "isRotate": false,
//           "rotateTime": 20,
//           "price": 0,
//           "relative": 0,
//           "campaignId": "k18s27ek",
//           "campaign": {
//             "id": "k18s27ek",
//             "startTime": "2019-10-01T17:00:00.000Z",
//             "endTime": null,
//             "views": 0,
//             "viewPerSession": 0,
//             "timeResetViewCount": 0,
//             "settingBudgetCPM": "",
//             "settingBudgetCPC": "",
//             "weight": 0,
//             "revenueType": "cpd",
//             "pageLoad": 0,
//             "totalCPM": 0,
//             "isRunMonopoly": false,
//             "optionMonopoly": "",
//             "isRunBudget": false,
//             "expense": 0,
//             "maxCPMPerDay": 0
//           },
//           "banners": [{
//             "id": "k18sa2hk",
//             "html": "",
//             "width": 300,
//             "height": 250,
//             "keyword": "",
//             "isMacro": false,
//             "weight": 1,
//             "placementId": "k18sa1yb",
//             "imageUrl": "https://cms.surfcountor.com/cmsads/2019/10/bann-1569991269395.jpeg",
//             "bannerHtmlTypeId": "jhk08image",
//             "url": "",
//             "target": "_blank",
//             "bannerTypeId": "56c53393-914e-4182-9d7c-54bbbd4b304e",
//             "isIFrame": false,
//             "isDefault": false,
//             "isRelative": false,
//             "videoType": "",
//             "clickThroughUrl": "",
//             "vastTagsUrl": "",
//             "vastVideoUrl": "",
//             "videoUrl": "",
//             "duration": 0,
//             "thirdPartyTracking": "",
//             "thirdPartyUrl": "",
//             "isDocumentWrite": true,
//             "skipOffset": 0,
//             "isAddLinkSSP": false,
//             "optionBanners": [],
//             "bannerType": {
//               "id": "56c53393-914e-4182-9d7c-54bbbd4b304e",
//               "name": "Img",
//               "value": "img",
//               "isUpload": true,
//               "isVideo": false
//             },
//             "bannerHtmlType": null
//           }]
//         }
//       }, {
//         "id": "k18sdr5n",
//         "positionOnShare": 1,
//         "placementId": "k18sdilh",
//         "shareId": "k18rvgww",
//         "time": "[{\"startTime\":\"2019-10-01T17:00:00.000Z\",\"endTime\":null}]",
//         "placement": {
//           "id": "k18sdilh",
//           "width": 300,
//           "height": 250,
//           "startTime": "2019-10-01T17:00:00.000Z",
//           "endTime": null,
//           "weight": 1,
//           "revenueType": "cpm",
//           "cpdPercent": 0,
//           "isRotate": false,
//           "rotateTime": 20,
//           "price": 0,
//           "relative": 0,
//           "campaignId": "k18s27ek",
//           "campaign": {
//             "id": "k18s27ek",
//             "startTime": "2019-10-01T17:00:00.000Z",
//             "endTime": null,
//             "views": 0,
//             "viewPerSession": 0,
//             "timeResetViewCount": 0,
//             "settingBudgetCPM": "",
//             "settingBudgetCPC": "",
//             "weight": 0,
//             "revenueType": "cpd",
//             "pageLoad": 0,
//             "totalCPM": 0,
//             "isRunMonopoly": false,
//             "optionMonopoly": "",
//             "isRunBudget": false,
//             "expense": 0,
//             "maxCPMPerDay": 0
//           },
//           "banners": [{
//             "id": "k18sdjag",
//             "html": "",
//             "width": 300,
//             "height": 250,
//             "keyword": "",
//             "isMacro": false,
//             "weight": 1,
//             "placementId": "k18sdilh",
//             "imageUrl": "https://cms.surfcountor.com/cmsads/2019/10/pass-1569991445236.jpeg",
//             "bannerHtmlTypeId": "jhk08image",
//             "url": "",
//             "target": "_blank",
//             "bannerTypeId": "56c53393-914e-4182-9d7c-54bbbd4b304e",
//             "isIFrame": false,
//             "isDefault": false,
//             "isRelative": false,
//             "videoType": "",
//             "clickThroughUrl": "",
//             "vastTagsUrl": "",
//             "vastVideoUrl": "",
//             "videoUrl": "",
//             "duration": 0,
//             "thirdPartyTracking": "",
//             "thirdPartyUrl": "",
//             "isDocumentWrite": true,
//             "skipOffset": 0,
//             "isAddLinkSSP": false,
//             "optionBanners": [],
//             "bannerType": {
//               "id": "56c53393-914e-4182-9d7c-54bbbd4b304e",
//               "name": "Img",
//               "value": "img",
//               "isUpload": true,
//               "isVideo": false
//             },
//             "bannerHtmlType": null
//           }]
//         }
//       }, {
//         "id": "k18sepg9",
//         "positionOnShare": 2,
//         "placementId": "k18segv6",
//         "shareId": "k18rvgww",
//         "time": "[{\"startTime\":\"2019-10-01T17:00:00.000Z\",\"endTime\":null}]",
//         "placement": {
//           "id": "k18segv6",
//           "width": 300,
//           "height": 250,
//           "startTime": "2019-10-01T17:00:00.000Z",
//           "endTime": null,
//           "weight": 1,
//           "revenueType": "pb",
//           "cpdPercent": 0,
//           "isRotate": false,
//           "rotateTime": 20,
//           "price": 0,
//           "relative": 0,
//           "campaignId": "k18s27ek",
//           "campaign": {
//             "id": "k18s27ek",
//             "startTime": "2019-10-01T17:00:00.000Z",
//             "endTime": null,
//             "views": 0,
//             "viewPerSession": 0,
//             "timeResetViewCount": 0,
//             "settingBudgetCPM": "",
//             "settingBudgetCPC": "",
//             "weight": 0,
//             "revenueType": "cpd",
//             "pageLoad": 0,
//             "totalCPM": 0,
//             "isRunMonopoly": false,
//             "optionMonopoly": "",
//             "isRunBudget": false,
//             "expense": 0,
//             "maxCPMPerDay": 0
//           },
//           "banners": [{
//             "id": "k18sehg9",
//             "html": "",
//             "width": 300,
//             "height": 250,
//             "keyword": "",
//             "isMacro": false,
//             "weight": 1,
//             "placementId": "k18segv6",
//             "imageUrl": "https://cms.surfcountor.com/cmsads/2019/10/pass-1569991491062.jpeg",
//             "bannerHtmlTypeId": "jhk08image",
//             "url": "",
//             "target": "_blank",
//             "bannerTypeId": "56c53393-914e-4182-9d7c-54bbbd4b304e",
//             "isIFrame": false,
//             "isDefault": false,
//             "isRelative": false,
//             "videoType": "",
//             "clickThroughUrl": "",
//             "vastTagsUrl": "",
//             "vastVideoUrl": "",
//             "videoUrl": "",
//             "duration": 0,
//             "thirdPartyTracking": "",
//             "thirdPartyUrl": "",
//             "isDocumentWrite": true,
//             "skipOffset": 0,
//             "isAddLinkSSP": false,
//             "optionBanners": [],
//             "bannerType": {
//               "id": "56c53393-914e-4182-9d7c-54bbbd4b304e",
//               "name": "Img",
//               "value": "img",
//               "isUpload": true,
//               "isVideo": false
//             },
//             "bannerHtmlType": null
//           }]
//         }
//       }]
//     }],
//     "site": {
//       "id": "k0xoyjkc",
//       "domain": "http://test.com.vn",
//       "pageLoad": 0,
//       "isRunBannerDefault": false,
//       "isNoBrand": true,
//       "globalFilters": [],
//       "channels": [],
//       "campaignMonopoly": {
//         "campaigns": []
//       }
//     }
//   };

//   const env = {
//     "domain": "http://cafef.vn",
//     "url": "http://cafef.vn/",
//     "referrerUrl": "http://cafef.vn/",
//     "force": [],
//     "domainCore": "https://media1.admicro.vn",
//     "domainLog": "https://lg1.logging.admicro.vn",
//     "currentChannel": "%2fhome%2f",
//     "variables": {
//       "_ADM_Channel": "%2Fhome%2F",
//       "__RC": "4",
//       "__R": "1"
//     }
//   };

//   store.commit('INIT_ENV', env);

//   store.state.GLOBAL_FILTER_RESULT = {
//     'k0xoyjkc': true
//   };
//   const zone = new Zone(zoneData);
//   it('should random correct ratio', mochaAsync(async () => {
//     const times = 999;
//     let k18rvgww_appear = 0;
//     let k18ruvyg_appear = 0;
//     let shareWeight_k18rvgww = [];
//     let shareWeight_k18ruvyg = [];

//     let banner_k18sehg9 = 0;
//     let banner_k18sdjag = 0;
//     let banner_k18sa2hk = 0;
//     let banner_k18s7dvr = 0;
//     let banner_k18s620t = 0;
//     let banner_k18s48k8 = 0;

//     for (let index = 0; index < times; index++) {
//       const activeShare = zone.activeShare();

//       if (!activeShare) isError = true;
//       if (activeShare.id === 'k18rvgww') {
//         k18rvgww_appear++;
//         if (shareWeight_k18rvgww.indexOf(activeShare.weight) === -1) shareWeight_k18rvgww.push(activeShare.weight);
//         const activePlacements = activeShare.activeSharePlacements(false, []).map(i => i.placement);
//         pushTurn(zone.id, activeShare.id, activeShare.totalShare, activePlacements);
//         const activeBanners = activePlacements.map(i => i.activeBanner()).map(i => i.id);
//         if (activeBanners.indexOf('k18sehg9') > -1) banner_k18sehg9++;
//         if (activeBanners.indexOf('k18sdjag') > -1) banner_k18sdjag++;
//         if (activeBanners.indexOf('k18sa2hk') > -1) banner_k18sa2hk++;
//         if (activeBanners.indexOf('k18s7dvr') > -1) banner_k18s7dvr++;
//         if (activeBanners.indexOf('k18s620t') > -1) banner_k18s620t++;
//       }
//       if (activeShare.id === 'k18ruvyg') {
//         k18ruvyg_appear++;
//         if (shareWeight_k18ruvyg.indexOf(activeShare.weight) === -1) shareWeight_k18ruvyg.push(activeShare.weight); 
//         const activePlacements = activeShare.activeSharePlacements(false, []).map(i => i.placement);
//         pushTurn(zone.id, activeShare.id, activeShare.totalShare, activePlacements);
//         const activeBanners = activePlacements.map(i => i.activeBanner()).map(i => i.id);
//         if (activeBanners.indexOf('k18s48k8') > -1) banner_k18s48k8++;
//       }

//       store.commit('RENEW_CPM_CPD_FLAG', true);
//     }

//     const ratio_share_k18rvgww = k18rvgww_appear / times;
//     const ratio_share_k18ruvyg = k18ruvyg_appear / times;

//     const ratio_banner_k18sehg9 = banner_k18sehg9 / times;
//     const ratio_banner_k18sdjag = banner_k18sdjag / times;
//     const ratio_banner_k18sa2hk = banner_k18sa2hk / times;
//     const ratio_banner_k18s7dvr = banner_k18s7dvr / times;
//     const ratio_banner_k18s620t = banner_k18s620t / times;
//     const ratio_banner_k18s48k8 = banner_k18s48k8 / times;

//     const error = 0.03;
//     const expected_share_k18rvgww = (ratio_share_k18rvgww < (0.67 + error) && ratio_share_k18rvgww > (0.67 - error));
//     const expected_share_k18ruvyg = (ratio_share_k18ruvyg < (0.33 + error) && ratio_share_k18ruvyg > (0.33 - error));

//     const expect_banner_k18sehg9 = (ratio_banner_k18sehg9 < (0 + error) && ratio_banner_k18sehg9 > (0 - error));
//     const expect_banner_k18sdjag = (ratio_banner_k18sdjag < (0.33 + error) && ratio_banner_k18sdjag > (0.33 - error));
//     const expect_banner_k18sa2hk = (ratio_banner_k18sa2hk < (0.33 + error) && ratio_banner_k18sa2hk > (0.33 - error));
//     const expect_banner_k18s7dvr = (ratio_banner_k18s7dvr < (0.33 + error) && ratio_banner_k18s7dvr > (0.33 - error));
//     const expect_banner_k18s620t = (ratio_banner_k18s620t < (0.33 + error) && ratio_banner_k18s620t > (0.33 - error));
//     const expect_banner_k18s48k8 = (ratio_banner_k18s48k8 < (0.33 + error) && ratio_banner_k18s48k8 > (0.33 - error));

//     const result = expected_share_k18rvgww &&
//       expected_share_k18ruvyg &&
//       expect_banner_k18sehg9 &&
//       expect_banner_k18sdjag &&
//       expect_banner_k18sa2hk &&
//       expect_banner_k18s7dvr &&
//       expect_banner_k18s620t &&
//       expect_banner_k18s48k8;

//     describe(`ratio_share_k18rvgww (${k18rvgww_appear}- [${shareWeight_k18rvgww.join()}]): ${Math.round(ratio_share_k18rvgww * 100)}%`, () => {
//       it('checking', () => {
//         describe(`ratio_banner_k18sehg9 (${banner_k18sehg9}): ${Math.round(ratio_banner_k18sehg9 * 100)}%`, () => {
//           it('checking', () => {
//             expect(expect_banner_k18sehg9).to.equal(true);
//           });
//         });

//         describe(`ratio_banner_k18sdjag (${banner_k18sdjag}): ${Math.round(ratio_banner_k18sdjag * 100)}%`, () => {
//           it('checking', () => {
//             expect(expect_banner_k18sdjag).to.equal(true);
//           });
//         });

//         describe(`ratio_banner_k18s620t (${banner_k18s620t}): ${Math.round(ratio_banner_k18s620t * 100)}%`, () => {
//           it('checking', () => {
//             expect(expect_banner_k18s620t).to.equal(true);
//           });
//         });

//         describe(`ratio_banner_k18sa2hk (${banner_k18sa2hk}): ${Math.round(ratio_banner_k18sa2hk * 100)}%`, () => {
//           it('checking', () => {
//             expect(expect_banner_k18sa2hk).to.equal(true);
//           });
//         });

//         describe(`ratio_banner_k18s7dvr (${banner_k18s7dvr}): ${Math.round(ratio_banner_k18s7dvr * 100)}%`, () => {
//           it('checking', () => {
//             expect(expect_banner_k18s7dvr).to.equal(true);
//           });
//         });
//         expect(expected_share_k18rvgww).to.equal(true);
//       });
//     });

//     describe(`ratio_share_k18ruvyg (${k18ruvyg_appear} - [${shareWeight_k18ruvyg.join()}]): ${Math.round(ratio_share_k18ruvyg * 100)}%`, () => {
//       it('checking', () => {
//         describe(`ratio_banner_k18s48k8 (${banner_k18s48k8}): ${Math.round(ratio_banner_k18s48k8 * 100)}%`, () => {
//           it('checking', () => {
//             expect(expect_banner_k18s48k8).to.equal(true);
//           });
//         });

//         expect(expected_share_k18ruvyg).to.equal(true);
//       });
//     });

//     expect(result).to.equal(true);
//   }));
// });
