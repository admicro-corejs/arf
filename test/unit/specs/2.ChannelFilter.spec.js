// /* eslint-disable quotes, quote-props, comma-dangle, no-unused-vars */
// import MultipleBannerTerm from '@/vendor/BannerTerm/BannerTerm';


// describe('2. Channel filter', () => {
//   beforeEach(() => {

//   });

//   it('Channel location ** __RC = 4 ** __RC <= 101', () => {
//     const env = {
//       "url": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "referrerUrl": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "force": [],
//       "domainCore": "https://media1.admicro.vn",
//       "domainLog": "https://lg1.logging.admicro.vn",
//       "variables": {
//         "__RC": "4",
//         "__R": "1",
//       },
//     };
//     const optionsBanners = {
//       "id": "1",
//       "logical": "and",
//       "bannerId": "jtzk2byv",
//       "comparison": "==",
//       "value": "",
//       "type": "channel",
//       "optionBannerChannels": [{
//         "id": "ju0mjvzd",
//         "optionBannerId": "ju0mjvqk",
//         "channelId": "ju0mjh5y",
//         "channel": {
//           "id": "ju0mjh5y",
//           "name": "test2",
//           "siteId": "jtzk0h50",
//           "optionChannels": [{
//             "id": "ju0mkgum",
//             "name": "Location",
//             "logical": "and",
//             "globalVariables": "__RC",
//             "comparison": "==",
//             "value": "100",
//             "valueSelect": "__RC<=100",
//             "logicalSelect": "<=",
//             "createdAt": "2019-04-03T03:01:34.000Z",
//             "updatedAt": "2019-04-03T03:01:34.000Z",
//             "deletedAt": null,
//             "channelId": "ju0mjh5y",
//             "optionChannelTypeId": "d045cfc5-c489-4e9c-8d74-423063db496b",
//             "optionChannelType": {
//               "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
//               "name": "Location",
//               "isInputLink": false,
//               "isSelectOption": false,
//               "isVariable": false,
//               "isMultiSelect": true,
//               "userId": null,
//               "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]",
//               "isGlobal": true,
//               "status": "active",
//               "createdAt": "2018-06-28T14:17:05.000Z",
//               "updatedAt": "2018-07-09T04:12:01.000Z",
//               "deletedAt": null
//             }
//           }]
//         }
//       }]
//     };
//     const result = new MultipleBannerTerm(optionsBanners, env).isPass;
//     expect(result).to.equal(true);
//   });

//   it('Channel location ** __RC = 45 ** __RC <= 101', () => {
//     const env = {
//       "url": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "referrerUrl": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "force": [],
//       "domainCore": "https://media1.admicro.vn",
//       "domainLog": "https://lg1.logging.admicro.vn",
//       "variables": {
//         "__RC": "45",
//         "__R": "1",
//       },
//     };
//     const optionsBanners = {
//       "id": "2",
//       "logical": "and",
//       "bannerId": "jtzk2byv",
//       "comparison": "==",
//       "value": "",
//       "type": "channel",
//       "optionBannerChannels": [{
//         "id": "ju0mjvzd",
//         "optionBannerId": "ju0mjvqk",
//         "channelId": "ju0mjh5y",
//         "channel": {
//           "id": "ju0mjh5y",
//           "name": "test2",
//           "siteId": "jtzk0h50",
//           "optionChannels": [{
//             "id": "ju0mkgum",
//             "name": "Location",
//             "logical": "and",
//             "globalVariables": "__RC",
//             "comparison": "==",
//             "value": "100",
//             "valueSelect": "__RC<=100",
//             "logicalSelect": "<=",
//             "createdAt": "2019-04-03T03:01:34.000Z",
//             "updatedAt": "2019-04-03T03:01:34.000Z",
//             "deletedAt": null,
//             "channelId": "ju0mjh5y",
//             "optionChannelTypeId": "d045cfc5-c489-4e9c-8d74-423063db496b",
//             "optionChannelType": {
//               "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
//               "name": "Location",
//               "isInputLink": false,
//               "isSelectOption": false,
//               "isVariable": false,
//               "isMultiSelect": true,
//               "userId": null,
//               "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]",
//               "isGlobal": true,
//               "status": "active",
//               "createdAt": "2018-06-28T14:17:05.000Z",
//               "updatedAt": "2018-07-09T04:12:01.000Z",
//               "deletedAt": null
//             }
//           }]
//         }
//       }]
//     };
//     const result = new MultipleBannerTerm(optionsBanners, env).isPass;
//     expect(result).to.equal(true);
//   });

//   it('Channel location ** __RC = 60 ** __RC <= 101', () => {
//     const env = {
//       "url": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "referrerUrl": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "force": [],
//       "domainCore": "https://media1.admicro.vn",
//       "domainLog": "https://lg1.logging.admicro.vn",
//       "variables": {
//         "__RC": "60",
//         "__R": "1",
//       },
//     };
//     const optionsBanners = {
//       "id": "3",
//       "logical": "and",
//       "bannerId": "jtzk2byv",
//       "comparison": "==",
//       "value": "",
//       "type": "channel",
//       "optionBannerChannels": [{
//         "id": "ju0mjvzd",
//         "optionBannerId": "ju0mjvqk",
//         "channelId": "ju0mjh5y",
//         "channel": {
//           "id": "ju0mjh5y",
//           "name": "test2",
//           "siteId": "jtzk0h50",
//           "optionChannels": [{
//             "id": "ju0mkgum",
//             "name": "Location",
//             "logical": "and",
//             "globalVariables": "__RC",
//             "comparison": "==",
//             "value": "100",
//             "valueSelect": "__RC<=100",
//             "logicalSelect": "<=",
//             "createdAt": "2019-04-03T03:01:34.000Z",
//             "updatedAt": "2019-04-03T03:01:34.000Z",
//             "deletedAt": null,
//             "channelId": "ju0mjh5y",
//             "optionChannelTypeId": "d045cfc5-c489-4e9c-8d74-423063db496b",
//             "optionChannelType": {
//               "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
//               "name": "Location",
//               "isInputLink": false,
//               "isSelectOption": false,
//               "isVariable": false,
//               "isMultiSelect": true,
//               "userId": null,
//               "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]",
//               "isGlobal": true,
//               "status": "active",
//               "createdAt": "2018-06-28T14:17:05.000Z",
//               "updatedAt": "2018-07-09T04:12:01.000Z",
//               "deletedAt": null
//             }
//           }]
//         }
//       }]
//     };
//     const result = new MultipleBannerTerm(optionsBanners, env).isPass;
//     expect(result).to.equal(true);
//   });

//   it('Channel location ** __RC = 101 ** __RC <= 101', () => {
//     const env = {
//       "url": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "referrerUrl": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "force": [],
//       "domainCore": "https://media1.admicro.vn",
//       "domainLog": "https://lg1.logging.admicro.vn",
//       "variables": {
//         "__RC": "101",
//         "__R": "1",
//       },
//     };
//     const optionsBanners = {
//       "id": "4",
//       "logical": "and",
//       "bannerId": "jtzk2byv",
//       "comparison": "==",
//       "value": "",
//       "type": "channel",
//       "optionBannerChannels": [{
//         "id": "ju0mjvzd",
//         "optionBannerId": "ju0mjvqk",
//         "channelId": "ju0mjh5y",
//         "channel": {
//           "id": "ju0mjh5y",
//           "name": "test2",
//           "siteId": "jtzk0h50",
//           "optionChannels": [{
//             "id": "ju0mkgum",
//             "name": "Location",
//             "logical": "and",
//             "globalVariables": "__RC",
//             "comparison": "==",
//             "value": "100",
//             "valueSelect": "__RC<=100",
//             "logicalSelect": "<=",
//             "createdAt": "2019-04-03T03:01:34.000Z",
//             "updatedAt": "2019-04-03T03:01:34.000Z",
//             "deletedAt": null,
//             "channelId": "ju0mjh5y",
//             "optionChannelTypeId": "d045cfc5-c489-4e9c-8d74-423063db496b",
//             "optionChannelType": {
//               "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
//               "name": "Location",
//               "isInputLink": false,
//               "isSelectOption": false,
//               "isVariable": false,
//               "isMultiSelect": true,
//               "userId": null,
//               "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]",
//               "isGlobal": true,
//               "status": "active",
//               "createdAt": "2018-06-28T14:17:05.000Z",
//               "updatedAt": "2018-07-09T04:12:01.000Z",
//               "deletedAt": null
//             }
//           }]
//         }
//       }]
//     };
//     const result = new MultipleBannerTerm(optionsBanners, env).isPass;
//     expect(result).to.not.equal(true);
//   });

//   it('Channel location ** __RC = undefined ** __RC <= 101', () => {
//     const env = {
//       "url": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "referrerUrl": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "force": [],
//       "domainCore": "https://media1.admicro.vn",
//       "domainLog": "https://lg1.logging.admicro.vn",
//       "variables": {
//         "__RC": undefined,
//         "__R": "1",
//       },
//     };
//     const optionsBanners = {
//       "id": "16",
//       "logical": "and",
//       "bannerId": "jtzk2byv",
//       "comparison": "==",
//       "value": "",
//       "type": "channel",
//       "optionBannerChannels": [{
//         "id": "ju0mjvzd",
//         "optionBannerId": "ju0mjvqk",
//         "channelId": "ju0mjh5y",
//         "channel": {
//           "id": "ju0mjh5y",
//           "name": "test2",
//           "siteId": "jtzk0h50",
//           "optionChannels": [{
//             "id": "ju0mkgum",
//             "name": "Location",
//             "logical": "and",
//             "globalVariables": "__RC",
//             "comparison": "==",
//             "value": "100",
//             "valueSelect": "__RC<=100",
//             "logicalSelect": "<=",
//             "createdAt": "2019-04-03T03:01:34.000Z",
//             "updatedAt": "2019-04-03T03:01:34.000Z",
//             "deletedAt": null,
//             "channelId": "ju0mjh5y",
//             "optionChannelTypeId": "d045cfc5-c489-4e9c-8d74-423063db496b",
//             "optionChannelType": {
//               "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
//               "name": "Location",
//               "isInputLink": false,
//               "isSelectOption": false,
//               "isVariable": false,
//               "isMultiSelect": true,
//               "userId": null,
//               "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]",
//               "isGlobal": true,
//               "status": "active",
//               "createdAt": "2018-06-28T14:17:05.000Z",
//               "updatedAt": "2018-07-09T04:12:01.000Z",
//               "deletedAt": null
//             }
//           }]
//         }
//       }]
//     };
//     const result = new MultipleBannerTerm(optionsBanners, env).isPass;
//     expect(result).to.equal(false);
//   });

//   it('Channel Variable ** _ADM_Channel = "%2Fhome%2F" ** _ADM_Channel == "/home/"', () => {
//     const env = {
//       "url": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "referrerUrl": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "force": [],
//       "domainCore": "https://media1.admicro.vn",
//       "domainLog": "https://lg1.logging.admicro.vn",
//       "variables": {
//         "_ADM_Channel": '%2Fhome%2F',
//         "__RC": "4",
//         "__R": "1",
//       },
//     };
//     const optionsBanners = {
//       "id": "5",
//       "logical": "and",
//       "bannerId": "jtzk2byv",
//       "comparison": "==",
//       "value": "",
//       "type": "channel",
//       "optionBannerChannels": [{
//         "id": "ju0mjvzd",
//         "optionBannerId": "ju0mjvqk",
//         "channelId": "ju0mjh5y",
//         "channel": {
//           "id": "ju0mjh5y",
//           "name": "test2",
//           "siteId": "jtzk0h50",
//           "optionChannels": [{
//             "id": "jtzkb1am",
//             "name": "home-test",
//             "logical": "or",
//             "globalVariables": "_ADM_Channel",
//             "comparison": "==",
//             "value": "/home/",
//             "valueSelect": "",
//             "logicalSelect": "",
//             "createdAt": "2019-04-02T09:10:28.000Z",
//             "updatedAt": "2019-04-02T09:10:28.000Z",
//             "deletedAt": null,
//             "channelId": "jtzk3803",
//             "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//             "optionChannelType": {
//               "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//               "name": "Variable",
//               "isInputLink": false,
//               "isSelectOption": false,
//               "isVariable": true,
//               "isMultiSelect": false,
//               "userId": null,
//               "storageType": null,
//               "isGlobal": false,
//               "status": "active",
//               "createdAt": "2017-01-18T17:53:31.000Z",
//               "updatedAt": "2017-01-18T17:53:31.000Z",
//               "deletedAt": null
//             }
//           }]
//         }
//       }]
//     };
//     const result = new MultipleBannerTerm(optionsBanners, env).isPass;
//     expect(result).to.equal(true);
//   });

//   it('Channel Variable ** _ADM_Channel = "%2Fhome%2F" ** _ADM_Channel != "/home/"', () => {
//     const env = {
//       "url": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "referrerUrl": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "force": [],
//       "domainCore": "https://media1.admicro.vn",
//       "domainLog": "https://lg1.logging.admicro.vn",
//       "variables": {
//         "_ADM_Channel": '%2Fhome%2F',
//         "__RC": "4",
//         "__R": "1",
//       },
//     };
//     const optionsBanners = {
//       "id": "6",
//       "logical": "and",
//       "bannerId": "jtzk2byv",
//       "comparison": "==",
//       "value": "",
//       "type": "channel",
//       "optionBannerChannels": [{
//         "id": "ju0mjvzd",
//         "optionBannerId": "ju0mjvqk",
//         "channelId": "ju0mjh5y",
//         "channel": {
//           "id": "ju0mjh5y",
//           "name": "test2",
//           "siteId": "jtzk0h50",
//           "optionChannels": [{
//             "id": "jtzkcffk",
//             "name": "thoi_su_an",
//             "logical": "or",
//             "globalVariables": "_ADM_Channel",
//             "comparison": "!=",
//             "value": "/home/",
//             "valueSelect": "",
//             "logicalSelect": "",
//             "createdAt": "2019-04-02T09:11:33.000Z",
//             "updatedAt": "2019-04-02T09:32:25.000Z",
//             "deletedAt": null,
//             "channelId": "jtzk3803",
//             "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//             "optionChannelType": {
//               "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//               "name": "Variable",
//               "isInputLink": false,
//               "isSelectOption": false,
//               "isVariable": true,
//               "isMultiSelect": false,
//               "userId": null,
//               "storageType": null,
//               "isGlobal": false,
//               "status": "active",
//               "createdAt": "2017-01-18T17:53:31.000Z",
//               "updatedAt": "2017-01-18T17:53:31.000Z",
//               "deletedAt": null
//             }
//           }]
//         }
//       }]
//     };
//     const result = new MultipleBannerTerm(optionsBanners, env).isPass;
//     expect(result).to.equal(false);
//   });

//   it('Channel Variable ** _ADM_Channel = "%2Fhome%2Fdetail" ** _ADM_Channel =~ "/home/"', () => {
//     const env = {
//       "url": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "referrerUrl": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "force": [],
//       "domainCore": "https://media1.admicro.vn",
//       "domainLog": "https://lg1.logging.admicro.vn",
//       "variables": {
//         "_ADM_Channel": '%2Fhome%2Fdetail',
//         "__RC": "4",
//         "__R": "1",
//       },
//     };
//     const optionsBanners = {
//       "id": "7",
//       "logical": "and",
//       "bannerId": "jtzk2byv",
//       "comparison": "==",
//       "value": "",
//       "type": "channel",
//       "optionBannerChannels": [{
//         "id": "ju0mjvzd",
//         "optionBannerId": "ju0mjvqk",
//         "channelId": "ju0mjh5y",
//         "channel": {
//           "id": "ju0mjh5y",
//           "name": "test2",
//           "siteId": "jtzk0h50",
//           "optionChannels": [{
//             "id": "jtzkd5qk",
//             "name": "home-1",
//             "logical": "or",
//             "globalVariables": "_ADM_Channel",
//             "comparison": "=~",
//             "value": "/home/",
//             "valueSelect": "",
//             "logicalSelect": "",
//             "createdAt": "2019-04-02T09:12:08.000Z",
//             "updatedAt": "2019-04-02T09:32:25.000Z",
//             "deletedAt": null,
//             "channelId": "jtzk3803",
//             "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//             "optionChannelType": {
//               "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//               "name": "Variable",
//               "isInputLink": false,
//               "isSelectOption": false,
//               "isVariable": true,
//               "isMultiSelect": false,
//               "userId": null,
//               "storageType": null,
//               "isGlobal": false,
//               "status": "active",
//               "createdAt": "2017-01-18T17:53:31.000Z",
//               "updatedAt": "2017-01-18T17:53:31.000Z",
//               "deletedAt": null
//             }
//           }]
//         }
//       }]
//     };
//     const result = new MultipleBannerTerm(optionsBanners, env).isPass;
//     expect(result).to.equal(true);
//   });

//   it('Channel Variable ** _ADM_Channel = "%2Fhome%2Fdetail" ** _ADM_Channel !~ "/home/"', () => {
//     const env = {
//       "url": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "referrerUrl": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "force": [],
//       "domainCore": "https://media1.admicro.vn",
//       "domainLog": "https://lg1.logging.admicro.vn",
//       "variables": {
//         "_ADM_Channel": '%2Fhome%2Fdetail',
//         "__RC": "4",
//         "__R": "1",
//       },
//     };
//     const optionsBanners = {
//       "id": "8",
//       "logical": "and",
//       "bannerId": "jtzk2byv",
//       "comparison": "==",
//       "value": "",
//       "type": "channel",
//       "optionBannerChannels": [{
//         "id": "ju0mjvzd",
//         "optionBannerId": "ju0mjvqk",
//         "channelId": "ju0mjh5y",
//         "channel": {
//           "id": "ju0mjh5y",
//           "name": "test2",
//           "siteId": "jtzk0h50",
//           "optionChannels": [{
//             "id": "jtzkfugs",
//             "name": "quoc-te",
//             "logical": "or",
//             "globalVariables": "_ADM_Channel",
//             "comparison": "!~",
//             "value": "/home/",
//             "valueSelect": "",
//             "logicalSelect": "",
//             "createdAt": "2019-04-02T09:14:13.000Z",
//             "updatedAt": "2019-04-02T09:32:25.000Z",
//             "deletedAt": null,
//             "channelId": "jtzk3803",
//             "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//             "optionChannelType": {
//               "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//               "name": "Variable",
//               "isInputLink": false,
//               "isSelectOption": false,
//               "isVariable": true,
//               "isMultiSelect": false,
//               "userId": null,
//               "storageType": null,
//               "isGlobal": false,
//               "status": "active",
//               "createdAt": "2017-01-18T17:53:31.000Z",
//               "updatedAt": "2017-01-18T17:53:31.000Z",
//               "deletedAt": null
//             }
//           }]
//         }
//       }]
//     };
//     const result = new MultipleBannerTerm(optionsBanners, env).isPass;
//     expect(result).to.equal(false);
//   });

//   it('Channel Variable ** _ADM_Channel = "%2Fhome%2Fdetail" ** _ADM_Channel =x "/^(detail)/g"', () => {
//     const env = {
//       "url": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "referrerUrl": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "force": [],
//       "domainCore": "https://media1.admicro.vn",
//       "domainLog": "https://lg1.logging.admicro.vn",
//       "variables": {
//         "_ADM_Channel": '%2Fhome%2Fdetail',
//         "__RC": "4",
//         "__R": "1",
//       },
//     };
//     const optionsBanners = {
//       "id": "9",
//       "logical": "and",
//       "bannerId": "jtzk2byv",
//       "comparison": "==",
//       "value": "",
//       "type": "channel",
//       "optionBannerChannels": [{
//         "id": "ju0mjvzd",
//         "optionBannerId": "ju0mjvqk",
//         "channelId": "ju0mjh5y",
//         "channel": {
//           "id": "ju0mjh5y",
//           "name": "test2",
//           "siteId": "jtzk0h50",
//           "optionChannels": [{
//             "id": "jtzkkhim",
//             "name": "regex-home",
//             "logical": "or",
//             "globalVariables": "_ADM_Channel",
//             "comparison": "=x",
//             "value": "/^(detail)/g",
//             "valueSelect": "",
//             "logicalSelect": "",
//             "createdAt": "2019-04-02T09:17:49.000Z",
//             "updatedAt": "2019-04-02T09:17:49.000Z",
//             "deletedAt": null,
//             "channelId": "jtzk3803",
//             "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//             "optionChannelType": {
//               "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//               "name": "Variable",
//               "isInputLink": false,
//               "isSelectOption": false,
//               "isVariable": true,
//               "isMultiSelect": false,
//               "userId": null,
//               "storageType": null,
//               "isGlobal": false,
//               "status": "active",
//             }
//           }]
//         }
//       }]
//     };
//     const result = new MultipleBannerTerm(optionsBanners, env).isPass;
//     expect(result).to.equal(false);
//   });

//   it('Channel Variable ** _ADM_Channel = "%2Fhome%2Fdetail" ** _ADM_Channel !x "/^(detail)/g"', () => {
//     const env = {
//       "url": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "referrerUrl": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "force": [],
//       "domainCore": "https://media1.admicro.vn",
//       "domainLog": "https://lg1.logging.admicro.vn",
//       "variables": {
//         "_ADM_Channel": '%2Fhome%2Fdetail',
//         "__RC": "4",
//         "__R": "1",
//       },
//     };
//     const optionsBanners = {
//       "id": "10",
//       "logical": "and",
//       "bannerId": "jtzk2byv",
//       "comparison": "==",
//       "value": "",
//       "type": "channel",
//       "optionBannerChannels": [{
//         "id": "ju0mjvzd",
//         "optionBannerId": "ju0mjvqk",
//         "channelId": "ju0mjh5y",
//         "channel": {
//           "id": "ju0mjh5y",
//           "name": "test2",
//           "siteId": "jtzk0h50",
//           "optionChannels": [{
//             "id": "jtzkkhim",
//             "name": "regex-home",
//             "logical": "or",
//             "globalVariables": "_ADM_Channel",
//             "comparison": "!x",
//             "value": "/^(detail)/g",
//             "valueSelect": "",
//             "logicalSelect": "",
//             "createdAt": "2019-04-02T09:17:49.000Z",
//             "updatedAt": "2019-04-02T09:17:49.000Z",
//             "deletedAt": null,
//             "channelId": "jtzk3803",
//             "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//             "optionChannelType": {
//               "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//               "name": "Variable",
//               "isInputLink": false,
//               "isSelectOption": false,
//               "isVariable": true,
//               "isMultiSelect": false,
//               "userId": null,
//               "storageType": null,
//               "isGlobal": false,
//               "status": "active",
//             }
//           }]
//         }
//       }]
//     };
//     const result = new MultipleBannerTerm(optionsBanners, env).isPass;
//     expect(result).to.equal(true);
//   });

//   it('Channel Url ** Url = "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992" ** Url == "http://e9.channelvn.net/"', () => {
//     const env = {
//       "url": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "referrerUrl": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "force": [],
//       "domainCore": "https://media1.admicro.vn",
//       "domainLog": "https://lg1.logging.admicro.vn",
//       "variables": {
//         "_ADM_Channel": '%2Fhome%2Fdetail',
//         "__RC": "4",
//         "__R": "1",
//       },
//     };
//     const optionsBanners = {
//       "id": "11",
//       "logical": "and",
//       "bannerId": "jtzk2byv",
//       "comparison": "==",
//       "value": "",
//       "type": "channel",
//       "optionBannerChannels": [{
//         "id": "ju0mjvzd",
//         "optionBannerId": "ju0mjvqk",
//         "channelId": "ju0mjh5y",
//         "channel": {
//           "id": "ju0mjh5y",
//           "name": "test2",
//           "siteId": "jtzk0h50",
//           "optionChannels": [{
//             "id": "jtzkm1bu",
//             "name": "Site Page-URL",
//             "logical": "or",
//             "globalVariables": "",
//             "comparison": "==",
//             "value": "http://e9.channelvn.net/",
//             "valueSelect": "",
//             "logicalSelect": "",
//             "createdAt": "2019-04-02T09:19:02.000Z",
//             "updatedAt": "2019-04-02T09:19:02.000Z",
//             "deletedAt": null,
//             "channelId": "jtzk3803",
//             "optionChannelTypeId": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//             "optionChannelType": {
//               "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//               "name": "Site Page-URL",
//               "isInputLink": true,
//               "isSelectOption": false,
//               "isVariable": false,
//               "isMultiSelect": false,
//               "userId": null,
//               "storageType": null,
//               "isGlobal": false,
//               "status": "active",
//               "createdAt": "2017-01-18T17:49:59.000Z",
//               "updatedAt": "2017-01-18T17:49:59.000Z",
//               "deletedAt": null
//             }
//           }]
//         }
//       }]
//     };
//     const result = new MultipleBannerTerm(optionsBanners, env).isPass;
//     expect(result).to.equal(false);
//   });

//   it('Channel Url ** Url = "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992" ** Url == "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992"', () => {
//     const env = {
//       "url": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "referrerUrl": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "force": [],
//       "domainCore": "https://media1.admicro.vn",
//       "domainLog": "https://lg1.logging.admicro.vn",
//       "variables": {
//         "_ADM_Channel": '%2Fhome%2Fdetail',
//         "__RC": "4",
//         "__R": "1",
//       },
//     };
//     const optionsBanners = {
//       "id": "12",
//       "logical": "and",
//       "bannerId": "jtzk2byv",
//       "comparison": "==",
//       "value": "",
//       "type": "channel",
//       "optionBannerChannels": [{
//         "id": "ju0mjvzd",
//         "optionBannerId": "ju0mjvqk",
//         "channelId": "ju0mjh5y",
//         "channel": {
//           "id": "ju0mjh5y",
//           "name": "test2",
//           "siteId": "jtzk0h50",
//           "optionChannels": [{
//             "id": "jtzkm1bu",
//             "name": "Site Page-URL",
//             "logical": "or",
//             "globalVariables": "",
//             "comparison": "==",
//             "value": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//             "valueSelect": "",
//             "logicalSelect": "",
//             "createdAt": "2019-04-02T09:19:02.000Z",
//             "updatedAt": "2019-04-02T09:19:02.000Z",
//             "deletedAt": null,
//             "channelId": "jtzk3803",
//             "optionChannelTypeId": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//             "optionChannelType": {
//               "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//               "name": "Site Page-URL",
//               "isInputLink": true,
//               "isSelectOption": false,
//               "isVariable": false,
//               "isMultiSelect": false,
//               "userId": null,
//               "storageType": null,
//               "isGlobal": false,
//               "status": "active",
//               "createdAt": "2017-01-18T17:49:59.000Z",
//               "updatedAt": "2017-01-18T17:49:59.000Z",
//               "deletedAt": null
//             }
//           }]
//         }
//       }]
//     };
//     const result = new MultipleBannerTerm(optionsBanners, env).isPass;
//     expect(result).to.equal(true);
//   });

//   it('Channel mix: true and true and true', () => {
//     const env = {
//       "url": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "referrerUrl": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "force": [],
//       "domainCore": "https://media1.admicro.vn",
//       "domainLog": "https://lg1.logging.admicro.vn",
//       "variables": {
//         "_ADM_Channel": '%2Fhome%2Fdetail',
//         "__RC": "4",
//         "__R": "1",
//       },
//     };
//     const optionsBanners = {
//       "id": "13",
//       "logical": "and",
//       "bannerId": "jtzk2byv",
//       "comparison": "==",
//       "value": "",
//       "type": "channel",
//       "optionBannerChannels": [{
//         "id": "ju0mjvzd",
//         "optionBannerId": "ju0mjvqk",
//         "channelId": "ju0mjh5y",
//         "channel": {
//           "id": "ju0mjh5y",
//           "name": "test2",
//           "siteId": "jtzk0h50",
//           "optionChannels": [{
//             "id": "jtzkm1bu",
//             "name": "Site Page-URL",
//             "logical": "and",
//             "globalVariables": "",
//             "comparison": "==",
//             "value": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//             "valueSelect": "",
//             "logicalSelect": "",
//             "createdAt": "2019-04-02T09:19:02.000Z",
//             "updatedAt": "2019-04-02T09:19:02.000Z",
//             "deletedAt": null,
//             "channelId": "jtzk3803",
//             "optionChannelTypeId": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//             "optionChannelType": {
//               "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//               "name": "Site Page-URL",
//               "isInputLink": true,
//               "isSelectOption": false,
//               "isVariable": false,
//               "isMultiSelect": false,
//               "userId": null,
//               "storageType": null,
//               "isGlobal": false,
//               "status": "active",
//               "createdAt": "2017-01-18T17:49:59.000Z",
//               "updatedAt": "2017-01-18T17:49:59.000Z",
//               "deletedAt": null
//             }
//           },
//           {
//             "id": "jtzkkhim",
//             "name": "regex-home",
//             "logical": "and",
//             "globalVariables": "_ADM_Channel",
//             "comparison": "!x",
//             "value": "/^(detail)/g",
//             "valueSelect": "",
//             "logicalSelect": "",
//             "createdAt": "2019-04-02T09:17:49.000Z",
//             "updatedAt": "2019-04-02T09:17:49.000Z",
//             "deletedAt": null,
//             "channelId": "jtzk3803",
//             "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//             "optionChannelType": {
//               "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//               "name": "Variable",
//               "isInputLink": false,
//               "isSelectOption": false,
//               "isVariable": true,
//               "isMultiSelect": false,
//               "userId": null,
//               "storageType": null,
//               "isGlobal": false,
//               "status": "active",
//             }
//           },
//           {
//             "id": "jtzkd5qk",
//             "name": "home-1",
//             "logical": "and",
//             "globalVariables": "_ADM_Channel",
//             "comparison": "=~",
//             "value": "/home/",
//             "valueSelect": "",
//             "logicalSelect": "",
//             "createdAt": "2019-04-02T09:12:08.000Z",
//             "updatedAt": "2019-04-02T09:32:25.000Z",
//             "deletedAt": null,
//             "channelId": "jtzk3803",
//             "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//             "optionChannelType": {
//               "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//               "name": "Variable",
//               "isInputLink": false,
//               "isSelectOption": false,
//               "isVariable": true,
//               "isMultiSelect": false,
//               "userId": null,
//               "storageType": null,
//               "isGlobal": false,
//               "status": "active",
//               "createdAt": "2017-01-18T17:53:31.000Z",
//               "updatedAt": "2017-01-18T17:53:31.000Z",
//               "deletedAt": null
//             }
//           }]
//         }
//       }]
//     };
//     const result = new MultipleBannerTerm(optionsBanners, env).isPass;
//     expect(result).to.equal(true);
//   });

//   it('Channel mix: true and true and true and false', () => {
//     const env = {
//       "url": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "referrerUrl": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "force": [],
//       "domainCore": "https://media1.admicro.vn",
//       "domainLog": "https://lg1.logging.admicro.vn",
//       "variables": {
//         "_ADM_Channel": '%2Fhome%2Fdetail',
//         "__RC": "4",
//         "__R": "1",
//       },
//     };
//     const optionsBanners = {
//       "id": "14",
//       "logical": "and",
//       "bannerId": "jtzk2byv",
//       "comparison": "==",
//       "value": "",
//       "type": "channel",
//       "optionBannerChannels": [{
//         "id": "ju0mjvzd",
//         "optionBannerId": "ju0mjvqk",
//         "channelId": "ju0mjh5y",
//         "channel": {
//           "id": "ju0mjh5y",
//           "name": "test2",
//           "siteId": "jtzk0h50",
//           "optionChannels": [{
//             "id": "jtzkm1bu",
//             "name": "Site Page-URL",
//             "logical": "and",
//             "globalVariables": "",
//             "comparison": "==",
//             "value": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//             "valueSelect": "",
//             "logicalSelect": "",
//             "createdAt": "2019-04-02T09:19:02.000Z",
//             "updatedAt": "2019-04-02T09:19:02.000Z",
//             "deletedAt": null,
//             "channelId": "jtzk3803",
//             "optionChannelTypeId": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//             "optionChannelType": {
//               "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//               "name": "Site Page-URL",
//               "isInputLink": true,
//               "isSelectOption": false,
//               "isVariable": false,
//               "isMultiSelect": false,
//               "userId": null,
//               "storageType": null,
//               "isGlobal": false,
//               "status": "active",
//               "createdAt": "2017-01-18T17:49:59.000Z",
//               "updatedAt": "2017-01-18T17:49:59.000Z",
//               "deletedAt": null
//             }
//           },
//           {
//             "id": "jtzkkhim",
//             "name": "regex-home",
//             "logical": "and",
//             "globalVariables": "_ADM_Channel",
//             "comparison": "!x",
//             "value": "/^(detail)/g",
//             "valueSelect": "",
//             "logicalSelect": "",
//             "createdAt": "2019-04-02T09:17:49.000Z",
//             "updatedAt": "2019-04-02T09:17:49.000Z",
//             "deletedAt": null,
//             "channelId": "jtzk3803",
//             "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//             "optionChannelType": {
//               "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//               "name": "Variable",
//               "isInputLink": false,
//               "isSelectOption": false,
//               "isVariable": true,
//               "isMultiSelect": false,
//               "userId": null,
//               "storageType": null,
//               "isGlobal": false,
//               "status": "active",
//             }
//           },
//           {
//             "id": "jtzkd5qk",
//             "name": "home-1",
//             "logical": "and",
//             "globalVariables": "_ADM_Channel",
//             "comparison": "=~",
//             "value": "/home/",
//             "valueSelect": "",
//             "logicalSelect": "",
//             "createdAt": "2019-04-02T09:12:08.000Z",
//             "updatedAt": "2019-04-02T09:32:25.000Z",
//             "deletedAt": null,
//             "channelId": "jtzk3803",
//             "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//             "optionChannelType": {
//               "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//               "name": "Variable",
//               "isInputLink": false,
//               "isSelectOption": false,
//               "isVariable": true,
//               "isMultiSelect": false,
//               "userId": null,
//               "storageType": null,
//               "isGlobal": false,
//               "status": "active",
//               "createdAt": "2017-01-18T17:53:31.000Z",
//               "updatedAt": "2017-01-18T17:53:31.000Z",
//               "deletedAt": null
//             }
//           },
//           {
//             "id": "jtzkb1am",
//             "name": "home-test",
//             "logical": "and",
//             "globalVariables": "_ADM_Channel",
//             "comparison": "==",
//             "value": "/home/",
//             "valueSelect": "",
//             "logicalSelect": "",
//             "createdAt": "2019-04-02T09:10:28.000Z",
//             "updatedAt": "2019-04-02T09:10:28.000Z",
//             "deletedAt": null,
//             "channelId": "jtzk3803",
//             "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//             "optionChannelType": {
//               "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//               "name": "Variable",
//               "isInputLink": false,
//               "isSelectOption": false,
//               "isVariable": true,
//               "isMultiSelect": false,
//               "userId": null,
//               "storageType": null,
//               "isGlobal": false,
//               "status": "active",
//               "createdAt": "2017-01-18T17:53:31.000Z",
//               "updatedAt": "2017-01-18T17:53:31.000Z",
//               "deletedAt": null
//             }
//           }]
//         }
//       }]
//     };
//     const result = new MultipleBannerTerm(optionsBanners, env).isPass;
//     expect(result).to.equal(false);
//   });

//   it('Channel mix: true and true and true or false', () => {
//     const env = {
//       "url": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "referrerUrl": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//       "force": [],
//       "domainCore": "https://media1.admicro.vn",
//       "domainLog": "https://lg1.logging.admicro.vn",
//       "variables": {
//         "_ADM_Channel": '%2Fhome%2Fdetail',
//         "__RC": "4",
//         "__R": "1",
//       },
//     };
//     const optionsBanners = {
//       "id": "17",
//       "logical": "and",
//       "bannerId": "jtzk2byv",
//       "comparison": "==",
//       "value": "",
//       "type": "channel",
//       "optionBannerChannels": [{
//         "id": "ju0mjvzd",
//         "optionBannerId": "ju0mjvqk",
//         "channelId": "ju0mjh5y",
//         "channel": {
//           "id": "ju0mjh5y",
//           "name": "test2",
//           "siteId": "jtzk0h50",
//           "optionChannels": [{
//             "id": "jtzkm1bu",
//             "name": "Site Page-URL",
//             "logical": "and",
//             "globalVariables": "",
//             "comparison": "==",
//             "value": "http://demo.admicro.vn/createdemoadx/testscript/?corejs_env_key=1992",
//             "valueSelect": "",
//             "logicalSelect": "",
//             "createdAt": "2019-04-02T09:19:02.000Z",
//             "updatedAt": "2019-04-02T09:19:02.000Z",
//             "deletedAt": null,
//             "channelId": "jtzk3803",
//             "optionChannelTypeId": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//             "optionChannelType": {
//               "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//               "name": "Site Page-URL",
//               "isInputLink": true,
//               "isSelectOption": false,
//               "isVariable": false,
//               "isMultiSelect": false,
//               "userId": null,
//               "storageType": null,
//               "isGlobal": false,
//               "status": "active",
//               "createdAt": "2017-01-18T17:49:59.000Z",
//               "updatedAt": "2017-01-18T17:49:59.000Z",
//               "deletedAt": null
//             }
//           },
//           {
//             "id": "jtzkkhim",
//             "name": "regex-home",
//             "logical": "and",
//             "globalVariables": "_ADM_Channel",
//             "comparison": "!x",
//             "value": "/^(detail)/g",
//             "valueSelect": "",
//             "logicalSelect": "",
//             "createdAt": "2019-04-02T09:17:49.000Z",
//             "updatedAt": "2019-04-02T09:17:49.000Z",
//             "deletedAt": null,
//             "channelId": "jtzk3803",
//             "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//             "optionChannelType": {
//               "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//               "name": "Variable",
//               "isInputLink": false,
//               "isSelectOption": false,
//               "isVariable": true,
//               "isMultiSelect": false,
//               "userId": null,
//               "storageType": null,
//               "isGlobal": false,
//               "status": "active",
//             }
//           },
//           {
//             "id": "jtzkd5qk",
//             "name": "home-1",
//             "logical": "and",
//             "globalVariables": "_ADM_Channel",
//             "comparison": "=~",
//             "value": "/home/",
//             "valueSelect": "",
//             "logicalSelect": "",
//             "createdAt": "2019-04-02T09:12:08.000Z",
//             "updatedAt": "2019-04-02T09:32:25.000Z",
//             "deletedAt": null,
//             "channelId": "jtzk3803",
//             "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//             "optionChannelType": {
//               "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//               "name": "Variable",
//               "isInputLink": false,
//               "isSelectOption": false,
//               "isVariable": true,
//               "isMultiSelect": false,
//               "userId": null,
//               "storageType": null,
//               "isGlobal": false,
//               "status": "active",
//               "createdAt": "2017-01-18T17:53:31.000Z",
//               "updatedAt": "2017-01-18T17:53:31.000Z",
//               "deletedAt": null
//             }
//           },
//           {
//             "id": "jtzkb1am",
//             "name": "home-test",
//             "logical": "or",
//             "globalVariables": "_ADM_Channel",
//             "comparison": "==",
//             "value": "/home/",
//             "valueSelect": "",
//             "logicalSelect": "",
//             "createdAt": "2019-04-02T09:10:28.000Z",
//             "updatedAt": "2019-04-02T09:10:28.000Z",
//             "deletedAt": null,
//             "channelId": "jtzk3803",
//             "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//             "optionChannelType": {
//               "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//               "name": "Variable",
//               "isInputLink": false,
//               "isSelectOption": false,
//               "isVariable": true,
//               "isMultiSelect": false,
//               "userId": null,
//               "storageType": null,
//               "isGlobal": false,
//               "status": "active",
//               "createdAt": "2017-01-18T17:53:31.000Z",
//               "updatedAt": "2017-01-18T17:53:31.000Z",
//               "deletedAt": null
//             }
//           }]
//         }
//       }]
//     };
//     const result = new MultipleBannerTerm(optionsBanners, env).isPass;
//     expect(result).to.equal(true);
//   });
// });
