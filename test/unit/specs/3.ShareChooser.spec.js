// /* eslint-disable */
// import store from '../../../src/store';
// import Zone from '../../../src/models/Zone';

// const zoneData = {
//   "id": "77",
//   "name": "Mixing_980x90_Autopro",
//   "height": 90,
//   "width": 980,
//   "css": "\ndiv[id*=\"adxzone_\"]{\nwidth:980px;\n  margin: 0 auto;\n}\ndiv[id*=\"adnzone_\"]{\nwidth:980px;\n  margin: 0 auto;\n}",
//   "outputCss": "#zone-77 div[id*=\"adxzone_\"] {\n  width: 980px;\n  margin: 0 auto;\n}\n#zone-77 div[id*=\"adnzone_\"] {\n  width: 980px;\n  margin: 0 auto;\n}\n",
//   "groupSSP": "",
//   "isResponsive": true,
//   "isMobile": false,
//   "isTemplate": false,
//   "template": "",
//   "totalShare": 3,
//   "status": "active",
//   "isPageLoad": true,
//   "isZoneVideo": false,
//   "positionOnSite": "0",
//   "timeBetweenAds": 0,
//   "siteId": "15",
//   "shares": [{
//     "id": "jkkj65op",
//     "css": "div[id^=\"adnzone\"] {\nmargin: 0 auto;\n    min-width: 980px;\n    max-width: 1040px;\n    display: flex;\n    justify-content: center;\n}\ndiv[id^=\"adxzone\"] {\nmargin: 0 auto;\n    min-width: 980px;\n    max-width: 1040px;\n    display: flex;\n    justify-content: center;\n}\nmax-width: 1040px;\n    margin: 0 auto;",
//     "outputCss": "#share-jkkj65op {\n  max-width: 1040px;\n  margin: 0 auto;\n}\n#share-jkkj65op div[id^=\"adnzone\"] {\n  margin: 0 auto;\n  min-width: 980px;\n  max-width: 1040px;\n  display: flex;\n  justify-content: center;\n}\n#share-jkkj65op div[id^=\"adxzone\"] {\n  margin: 0 auto;\n  min-width: 980px;\n  max-width: 1040px;\n  display: flex;\n  justify-content: center;\n}\n",
//     "width": 980,
//     "height": 90,
//     "classes": "",
//     "isRotate": false,
//     "rotateTime": 0,
//     "type": "single",
//     "format": "",
//     "zoneId": "77",
//     "isTemplate": false,
//     "template": "",
//     "offset": null,
//     "isAdPod": false,
//     "duration": null,
//     "sharePlacements": [{
//       "id": "jkkk3yw0",
//       "positionOnShare": 0,
//       "placementId": "276884",
//       "shareId": "jkkj65op",
//       "time": "[{\"startTime\":\"1989-12-31T17:00:00.000Z\",\"endTime\":null}]",
//       "placement": {
//         "id": "276884",
//         "width": 728,
//         "height": 90,
//         "startTime": "1989-12-31T17:00:00.000Z",
//         "endTime": null,
//         "weight": 1,
//         "revenueType": "cpd",
//         "cpdPercent": 1,
//         "isRotate": false,
//         "rotateTime": 0,
//         "price": 0,
//         "relative": 0,
//         "campaignId": "1042598",
//         "campaign": {
//           "id": "1042598",
//           "startTime": "1989-12-31T17:00:00.000Z",
//           "endTime": null,
//           "views": 0,
//           "viewPerSession": 0,
//           "timeResetViewCount": 0,
//           "settingBudgetCPM": null,
//           "settingBudgetCPC": null,
//           "weight": 0,
//           "revenueType": "cpd",
//           "pageLoad": 0,
//           "totalCPM": 0,
//           "isRunMonopoly": false,
//           "optionMonopoly": null,
//           "isRunBudget": false,
//           "expense": 0,
//           "maxCPMPerDay": 0
//         },
//         "banners": [{
//           "id": "276884",
//           "html": "<div style=\"clear:both; width: 720px;margin: 0 auto;\">\n<script type=\"text/javascript\">\ngoogle_ad_client = \"ca-pub-6366951472589375\";\ngoogle_ad_slot = \"1988984442\";\ngoogle_ad_width = 728;\ngoogle_ad_height = 90;\n</script>\n<script type=\"text/javascript\"\nsrc=\"//pagead2.googlesyndication.com/pagead/show_ads.js\">\n</script>\n</div>",
//           "width": 728,
//           "height": 90,
//           "keyword": "",
//           "isMacro": false,
//           "weight": 1,
//           "placementId": "276884",
//           "imageUrl": "",
//           "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//           "url": "",
//           "target": "",
//           "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//           "isIFrame": true,
//           "isDefault": false,
//           "isRelative": false,
//           "videoType": null,
//           "clickThroughUrl": null,
//           "vastTagsUrl": null,
//           "vastVideoUrl": "",
//           "videoUrl": "",
//           "duration": 0,
//           "thirdPartyTracking": "",
//           "thirdPartyUrl": null,
//           "isDocumentWrite": true,
//           "skipOffset": 0,
//           "isAddLinkSSP": false,
//           "optionBanners": [{
//             "id": "jknt4jz3",
//             "logical": "and",
//             "bannerId": "276884",
//             "comparison": "==",
//             "value": "",
//             "type": "channel",
//             "optionBannerChannels": [{
//               "id": "jknt4kp6",
//               "optionBannerId": "jknt4jz3",
//               "channelId": "jknrzdy1",
//               "channel": {
//                 "id": "jknrzdy1",
//                 "name": "Location_Nuocngoai",
//                 "siteId": "15",
//                 "optionChannels": [{
//                   "id": "jkns71fj",
//                   "name": "Location",
//                   "logical": "and",
//                   "globalVariables": "__RC",
//                   "comparison": "==",
//                   "value": "101",
//                   "valueSelect": "__RC>=101",
//                   "logicalSelect": ">=",
//                   "createdAt": "2018-08-10T09:20:44.000Z",
//                   "updatedAt": "2019-03-11T07:31:59.000Z",
//                   "deletedAt": null,
//                   "channelId": "jknrzdy1",
//                   "optionChannelTypeId": "d045cfc5-c489-4e9c-8d74-423063db496b",
//                   "optionChannelType": {
//                     "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
//                     "name": "Location",
//                     "isInputLink": false,
//                     "isSelectOption": false,
//                     "isVariable": false,
//                     "isMultiSelect": true,
//                     "userId": null,
//                     "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]",
//                     "isGlobal": true,
//                     "status": "active",
//                     "createdAt": "2018-06-28T14:17:05.000Z",
//                     "updatedAt": "2018-07-09T04:12:01.000Z",
//                     "deletedAt": null
//                   }
//                 }]
//               }
//             }]
//           }],
//           "bannerType": {
//             "id": "374da180-02c6-4558-8985-90cad792f14f",
//             "name": "Script",
//             "value": "script",
//             "isUpload": false,
//             "isVideo": false
//           },
//           "bannerHtmlType": {
//             "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//             "name": "PR Tracking",
//             "value": "pr-tracking",
//             "weight": 99
//           }
//         }]
//       }
//     }, {
//       "id": "jkkn1tsq",
//       "positionOnShare": 0,
//       "placementId": "363879",
//       "shareId": "jkkj65op",
//       "time": "[{\"startTime\":\"1989-12-31T17:00:00.000Z\",\"endTime\":null}]",
//       "placement": {
//         "id": "363879",
//         "width": 0,
//         "height": 0,
//         "startTime": "1989-12-31T17:00:00.000Z",
//         "endTime": null,
//         "weight": 1,
//         "revenueType": "pb",
//         "cpdPercent": 0,
//         "isRotate": false,
//         "rotateTime": 0,
//         "price": 0,
//         "relative": 0,
//         "campaignId": "1066410",
//         "campaign": {
//           "id": "1066410",
//           "startTime": "1989-12-31T17:00:00.000Z",
//           "endTime": null,
//           "views": 0,
//           "viewPerSession": 0,
//           "timeResetViewCount": 0,
//           "settingBudgetCPM": null,
//           "settingBudgetCPC": null,
//           "weight": 0,
//           "revenueType": "cpm",
//           "pageLoad": 0,
//           "totalCPM": 0,
//           "isRunMonopoly": false,
//           "optionMonopoly": null,
//           "isRunBudget": false,
//           "expense": 0,
//           "maxCPMPerDay": 0
//         },
//         "banners": [{
//           "id": "363879",
//           "html": "<div id=\"sspbid_512\"></div><script type=\"text/javascript\">admsspreg({sspid:512,w:980,h:90});</script>",
//           "width": 0,
//           "height": 0,
//           "keyword": "",
//           "isMacro": null,
//           "weight": 1,
//           "placementId": "363879",
//           "imageUrl": "",
//           "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//           "url": "",
//           "target": "_blank",
//           "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//           "isIFrame": false,
//           "isDefault": false,
//           "isRelative": false,
//           "videoType": null,
//           "clickThroughUrl": null,
//           "vastTagsUrl": null,
//           "vastVideoUrl": "",
//           "videoUrl": "",
//           "duration": 0,
//           "thirdPartyTracking": "",
//           "thirdPartyUrl": null,
//           "isDocumentWrite": true,
//           "skipOffset": 0,
//           "isAddLinkSSP": false,
//           "optionBanners": [{
//             "id": "jkoqubrt",
//             "logical": "and",
//             "bannerId": "363879",
//             "comparison": "==",
//             "value": "",
//             "type": "channel",
//             "optionBannerChannels": [{
//               "id": "jkoqucmf",
//               "optionBannerId": "jkoqubrt",
//               "channelId": "jknrq7p9",
//               "channel": {
//                 "id": "jknrq7p9",
//                 "name": "Location_ToĂ nquá»‘c",
//                 "siteId": "15",
//                 "optionChannels": [{
//                   "id": "jknrqicw",
//                   "name": "Location",
//                   "logical": "and",
//                   "globalVariables": "__RC,__RC",
//                   "comparison": "==",
//                   "value": "undefined,100",
//                   "valueSelect": "__RC==undefined,__RC<=100",
//                   "logicalSelect": "==,<=",
//                   "createdAt": "2018-08-10T09:07:53.000Z",
//                   "updatedAt": "2019-04-05T07:15:25.000Z",
//                   "deletedAt": null,
//                   "channelId": "jknrq7p9",
//                   "optionChannelTypeId": "d045cfc5-c489-4e9c-8d74-423063db496b",
//                   "optionChannelType": {
//                     "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
//                     "name": "Location",
//                     "isInputLink": false,
//                     "isSelectOption": false,
//                     "isVariable": false,
//                     "isMultiSelect": true,
//                     "userId": null,
//                     "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]",
//                     "isGlobal": true,
//                     "status": "active",
//                     "createdAt": "2018-06-28T14:17:05.000Z",
//                     "updatedAt": "2018-07-09T04:12:01.000Z",
//                     "deletedAt": null
//                   }
//                 }]
//               }
//             }]
//           }],
//           "bannerType": {
//             "id": "374da180-02c6-4558-8985-90cad792f14f",
//             "name": "Script",
//             "value": "script",
//             "isUpload": false,
//             "isVideo": false
//           },
//           "bannerHtmlType": {
//             "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//             "name": "PR Tracking",
//             "value": "pr-tracking",
//             "weight": 99
//           }
//         }]
//       }
//     }, {
//       "id": "jzhv9yl3",
//       "positionOnShare": 0,
//       "placementId": "jzhv81hf",
//       "shareId": "jkkj65op",
//       "time": "[{\"startTime\":\"2019-08-18T17:00:00.000Z\",\"endTime\":\"2019-09-29T16:59:59.000Z\"}]",
//       "placement": {
//         "id": "jzhv81hf",
//         "width": 1040,
//         "height": 250,
//         "startTime": "2019-08-18T17:00:00.000Z",
//         "endTime": "2019-09-29T16:59:59.000Z",
//         "weight": 1,
//         "revenueType": "cpd",
//         "cpdPercent": 1,
//         "isRotate": false,
//         "rotateTime": 0,
//         "price": 0,
//         "relative": 0,
//         "campaignId": "jzhuxxe3",
//         "campaign": {
//           "id": "jzhuxxe3",
//           "startTime": "2019-08-18T17:00:00.000Z",
//           "endTime": null,
//           "views": 0,
//           "viewPerSession": 0,
//           "timeResetViewCount": 0,
//           "settingBudgetCPM": "",
//           "settingBudgetCPC": "",
//           "weight": 0,
//           "revenueType": "cpd",
//           "pageLoad": 0,
//           "totalCPM": 0,
//           "isRunMonopoly": false,
//           "optionMonopoly": "",
//           "isRunBudget": false,
//           "expense": 0,
//           "maxCPMPerDay": 0
//         },
//         "banners": [{
//           "id": "jzhv81sg",
//           "html": "<script>\n  (function() {\n    var admid=window.frameElement.id;\n    var b = parent.document.getElementById(admid);\n    b.style.width = \"100%\";\n    var f = b.parentNode.id;\n    var a = parent.wPrototype.getElementWidth(f);\n    \n\tvar image_url = 'https://adi.admicro.vn/adt/banners/nam2015/4043/min_html5/huyphanquoc/2019_09_09/suzuki-1/screenshot/suzuki__bg_replay.png';\n\tvar banner_url = 'https://adi.admicro.vn/adt/banners/nam2015/4043/min_html5/nguyenthihuyen01/2019_08_30/swift1160x90-3/swift1160x90/swift1160x90_1160_250.html';\n    var banner_width = 1160;\n    var banner_height = 250;\n    \n\tvar b = document,\n        c = navigator.userAgent + \"\";\n    var d = '<div id=\"adstop\" style=\"position:relative;overflow:hidden\">',\n        d = -1 != c.indexOf(\"Android\") || -1 != c.indexOf(\"iPad\") || -1 != c.indexOf(\"iPhone\") ? d + ('<img src=\"' + image_url + '\" border=\"0\"/><a href=\"%%CLICK_URL_UNESC%%\" target=\"_blank\" style=\"position:absolute;top:0;left:0;width:'+banner_width+'px;height:'+banner_height+'px;display:block;z-index:9999;\"><span></span></a>') : d + ('<iframe id=\"demo_iframe\" src=\"'+banner_url+'?url=%%CLICK_URL_ESC%%\" width=\"'+banner_width+'\" frameborder=\"0\" scrolling=\"no\" height=\"'+banner_height+'\"></iframe>');\n    b.write(d + \"</div>\");\n    \n\twindow.setTimeout(function() {\n      var a = parent.wPrototype.getElementWidth(f),\n          a = 980 > a ? 980 : a,\n          b = document.getElementById(\"adstop\"),\n          a = Math.floor((banner_width - a) / 2);\n      -1 != c.indexOf(\"Android\") || -1 != c.indexOf(\"iPad\") || -1 != c.indexOf(\"iPhone\") ? b.style.marginLeft = \"-\" + (0 > a ? 0 : a) + \"px\" : b.style.marginLeft = \"-\" + (0 > a ? 0 : a) + \"px\"\n    }, 100)}\n  )();\n</script>\n\n",
//           "width": 1040,
//           "height": 250,
//           "keyword": "",
//           "isMacro": false,
//           "weight": 1,
//           "placementId": "jzhv81hf",
//           "imageUrl": "",
//           "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//           "url": "https://track.adnetwork.vn/247/adServer/act_click/cid_1565771113/bid_1565772290/wid_1286789098/zid_1565771199/pid_1446457324/cb_[timestamp]",
//           "target": "",
//           "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//           "isIFrame": true,
//           "isDefault": false,
//           "isRelative": false,
//           "videoType": "",
//           "clickThroughUrl": "",
//           "vastTagsUrl": "",
//           "vastVideoUrl": "",
//           "videoUrl": "",
//           "duration": 0,
//           "thirdPartyTracking": "",
//           "thirdPartyUrl": "https://imp.adnetwork.vn/247/adServer/act_impression/cid_1565771113/bid_1565772290/wid_1286789098/zid_1565771199/pid_1446457324/cb_[timestamp]",
//           "isDocumentWrite": true,
//           "skipOffset": 0,
//           "isAddLinkSSP": false,
//           "optionBanners": [],
//           "bannerType": {
//             "id": "374da180-02c6-4558-8985-90cad792f14f",
//             "name": "Script",
//             "value": "script",
//             "isUpload": false,
//             "isVideo": false
//           },
//           "bannerHtmlType": {
//             "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//             "name": "PR Tracking",
//             "value": "pr-tracking",
//             "weight": 99
//           }
//         }]
//       }
//     }, {
//       "id": "k011gu55",
//       "positionOnShare": 0,
//       "placementId": "k011g2hg",
//       "shareId": "jkkj65op",
//       "time": "[{\"startTime\":\"2019-09-02T17:00:00.000Z\",\"endTime\":\"2019-09-30T16:59:59.000Z\"}]",
//       "placement": {
//         "id": "k011g2hg",
//         "width": 1040,
//         "height": 250,
//         "startTime": "2019-09-02T17:00:00.000Z",
//         "endTime": "2019-09-30T16:59:59.000Z",
//         "weight": 1,
//         "revenueType": "cpd",
//         "cpdPercent": 1,
//         "isRotate": false,
//         "rotateTime": 20,
//         "price": 0,
//         "relative": 0,
//         "campaignId": "k011ddpf",
//         "campaign": {
//           "id": "k011ddpf",
//           "startTime": "2019-08-31T17:00:00.000Z",
//           "endTime": null,
//           "views": 0,
//           "viewPerSession": 0,
//           "timeResetViewCount": 0,
//           "settingBudgetCPM": "",
//           "settingBudgetCPC": "",
//           "weight": 0,
//           "revenueType": "cpd",
//           "pageLoad": 0,
//           "totalCPM": 0,
//           "isRunMonopoly": false,
//           "optionMonopoly": "",
//           "isRunBudget": false,
//           "expense": 0,
//           "maxCPMPerDay": 0
//         },
//         "banners": [{
//           "id": "k011g3ao",
//           "html": "<script>\n  (function() {\n    var admid=window.frameElement.id;\n    var b = parent.document.getElementById(admid);\n    b.style.width = \"100%\";\n    var f = b.parentNode.id;\n    var a = parent.wPrototype.getElementWidth(f);\n    \n\tvar image_url = 'https://adi.admicro.vn/adt/banners/nam2015/4043/min_html5/huyphanquoc/2019_09_01/fordvn-1/screenshot/fordvn__bg_replay.png';\n\tvar banner_url = 'https://adi.admicro.vn/adt/banners/nam2015/4043/min_html5/dieptranngoc/2019_08_30/fordeverest-1160x90/fordeverest-1160x90/fordeverest-1160x90_1160_250.html';\n    var banner_width = 1160;\n    var banner_height = 250;\n    \n\tvar b = document,\n        c = navigator.userAgent + \"\";\n    var d = '<div id=\"adstop\" style=\"position:relative;overflow:hidden\">',\n        d = -1 != c.indexOf(\"Android\") || -1 != c.indexOf(\"iPad\") || -1 != c.indexOf(\"iPhone\") ? d + ('<img src=\"' + image_url + '\" border=\"0\"/><a href=\"%%CLICK_URL_UNESC%%\" target=\"_blank\" style=\"position:absolute;top:0;left:0;width:'+banner_width+'px;height:'+banner_height+'px;display:block;z-index:9999;\"><span></span></a>') : d + ('<iframe id=\"demo_iframe\" src=\"'+banner_url+'?url=%%CLICK_URL_ESC%%\" width=\"'+banner_width+'\" frameborder=\"0\" scrolling=\"no\" height=\"'+banner_height+'\"></iframe>');\n    b.write(d + \"</div>\");\n    \n\twindow.setTimeout(function() {\n      var a = parent.wPrototype.getElementWidth(f),\n          a = 980 > a ? 980 : a,\n          b = document.getElementById(\"adstop\"),\n          a = Math.floor((banner_width - a) / 2);\n      -1 != c.indexOf(\"Android\") || -1 != c.indexOf(\"iPad\") || -1 != c.indexOf(\"iPhone\") ? b.style.marginLeft = \"-\" + (0 > a ? 0 : a) + \"px\" : b.style.marginLeft = \"-\" + (0 > a ? 0 : a) + \"px\"\n    }, 100)}\n  )();\n</script>\n\n",
//           "width": 1040,
//           "height": 250,
//           "keyword": "",
//           "isMacro": false,
//           "weight": 1,
//           "placementId": "k011g2hg",
//           "imageUrl": "",
//           "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//           "url": "https://ad.doubleclick.net/ddm/trackclk/N952192.2464500ADMICROADNETWORK/B22903359.250424312;dc_trk_aid=446830943;dc_trk_cid=118533556;dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=",
//           "target": "",
//           "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//           "isIFrame": true,
//           "isDefault": false,
//           "isRelative": false,
//           "videoType": "",
//           "clickThroughUrl": "",
//           "vastTagsUrl": "",
//           "vastVideoUrl": "",
//           "videoUrl": "",
//           "duration": 0,
//           "thirdPartyTracking": "",
//           "thirdPartyUrl": "https://ad.doubleclick.net/ddm/trackimpj/N952192.2464500ADMICROADNETWORK/B22903359.250424312;dc_trk_aid=446830943;dc_trk_cid=118533556;ord=[timestamp];dc_lat=;dc_rdid=;tag_for_child_directed_treatment=;tfua=?",
//           "isDocumentWrite": true,
//           "skipOffset": 0,
//           "isAddLinkSSP": false,
//           "optionBanners": [],
//           "bannerType": {
//             "id": "374da180-02c6-4558-8985-90cad792f14f",
//             "name": "Script",
//             "value": "script",
//             "isUpload": false,
//             "isVideo": false
//           },
//           "bannerHtmlType": {
//             "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//             "name": "PR Tracking",
//             "value": "pr-tracking",
//             "weight": 99
//           }
//         }]
//       }
//     }]
//   }, {
//     "id": "jw64yp4i",
//     "css": "",
//     "outputCss": "",
//     "width": 980,
//     "height": 90,
//     "classes": "",
//     "isRotate": false,
//     "rotateTime": 0,
//     "type": "single",
//     "format": "",
//     "zoneId": "77",
//     "isTemplate": false,
//     "template": "",
//     "offset": "",
//     "isAdPod": false,
//     "duration": null,
//     "sharePlacements": [{
//       "id": "jw650djc",
//       "positionOnShare": 1,
//       "placementId": "566269",
//       "shareId": "jw64yp4i",
//       "time": "[{\"startTime\":\"1989-12-31T17:00:00.000Z\",\"endTime\":null}]",
//       "placement": {
//         "id": "566269",
//         "width": 0,
//         "height": 0,
//         "startTime": "1989-12-31T17:00:00.000Z",
//         "endTime": null,
//         "weight": 1,
//         "revenueType": "cpm",
//         "cpdPercent": 0,
//         "isRotate": false,
//         "rotateTime": 0,
//         "price": 0,
//         "relative": 0,
//         "campaignId": "jnwuxdzu",
//         "campaign": {
//           "id": "jnwuxdzu",
//           "startTime": "1989-12-31T17:00:00.000Z",
//           "endTime": null,
//           "views": 0,
//           "viewPerSession": 0,
//           "timeResetViewCount": 0,
//           "settingBudgetCPM": null,
//           "settingBudgetCPC": null,
//           "weight": 0,
//           "revenueType": "cpm",
//           "pageLoad": 0,
//           "totalCPM": 0,
//           "isRunMonopoly": false,
//           "optionMonopoly": null,
//           "isRunBudget": false,
//           "expense": 0,
//           "maxCPMPerDay": 0
//         },
//         "banners": [{
//           "id": "566269",
//           "html": "<div id=\"ssppagebid_104\"></div><script>if(typeof(admsspPosition)==\"undefined\"){_admloadJs(\"//media1.admicro.vn/core/ssppage.js\",function(){admsspPosition({sspid:104,w:0,h:0,group:\"104,600,601\"});});}else{admsspPosition({sspid:104,w:0,h:0,group:\"104,600,601\"});}</script>",
//           "width": 0,
//           "height": 0,
//           "keyword": "",
//           "isMacro": false,
//           "weight": 1,
//           "placementId": "566269",
//           "imageUrl": "",
//           "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//           "url": "",
//           "target": "",
//           "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//           "isIFrame": false,
//           "isDefault": false,
//           "isRelative": false,
//           "videoType": "",
//           "clickThroughUrl": "",
//           "vastTagsUrl": "",
//           "vastVideoUrl": "",
//           "videoUrl": "",
//           "duration": 0,
//           "thirdPartyTracking": "",
//           "thirdPartyUrl": "",
//           "isDocumentWrite": true,
//           "skipOffset": 0,
//           "isAddLinkSSP": false,
//           "optionBanners": [{
//             "id": "jsfvnlaf",
//             "logical": "and",
//             "bannerId": "566269",
//             "comparison": "==",
//             "value": "",
//             "type": "channel",
//             "optionBannerChannels": [{
//               "id": "jsfvnnrv",
//               "optionBannerId": "jsfvnlaf",
//               "channelId": "jknrq7p9",
//               "channel": {
//                 "id": "jknrq7p9",
//                 "name": "Location_ToĂ nquá»‘c",
//                 "siteId": "15",
//                 "optionChannels": [{
//                   "id": "jknrqicw",
//                   "name": "Location",
//                   "logical": "and",
//                   "globalVariables": "__RC,__RC",
//                   "comparison": "==",
//                   "value": "undefined,100",
//                   "valueSelect": "__RC==undefined,__RC<=100",
//                   "logicalSelect": "==,<=",
//                   "createdAt": "2018-08-10T09:07:53.000Z",
//                   "updatedAt": "2019-04-05T07:15:25.000Z",
//                   "deletedAt": null,
//                   "channelId": "jknrq7p9",
//                   "optionChannelTypeId": "d045cfc5-c489-4e9c-8d74-423063db496b",
//                   "optionChannelType": {
//                     "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
//                     "name": "Location",
//                     "isInputLink": false,
//                     "isSelectOption": false,
//                     "isVariable": false,
//                     "isMultiSelect": true,
//                     "userId": null,
//                     "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]",
//                     "isGlobal": true,
//                     "status": "active",
//                     "createdAt": "2018-06-28T14:17:05.000Z",
//                     "updatedAt": "2018-07-09T04:12:01.000Z",
//                     "deletedAt": null
//                   }
//                 }]
//               }
//             }]
//           }],
//           "bannerType": {
//             "id": "374da180-02c6-4558-8985-90cad792f14f",
//             "name": "Script",
//             "value": "script",
//             "isUpload": false,
//             "isVideo": false
//           },
//           "bannerHtmlType": {
//             "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//             "name": "PR Tracking",
//             "value": "pr-tracking",
//             "weight": 99
//           }
//         }]
//       }
//     }]
//   }],
//   "site": {
//     "id": "15",
//     "domain": "http://autopro.com.vn",
//     "pageLoad": {
//       "totalPageload": 3,
//       "campaigns": []
//     },
//     "isRunBannerDefault": false,
//     "isNoBrand": false,
//     "globalFilters": [],
//     "channels": [{
//       "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
//       "name": "Location",
//       "isGlobal": true,
//       "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]"
//     }],
//     "campaignMonopoly": {
//       "campaigns": []
//     }
//   }
// }
// const env = {
//   "url": "https://autopro.com.vn/",
//   "domain": "https://autopro.com.vn",
//   "referrerUrl": "https://autopro.com.vn/",
//   "force": [],
//   "domainCore": "https://media1.admicro.vn",
//   "domainLog": "https://lg1.logging.admicro.vn",
//   "variables": {
//     "_ADM_Channel": "%2Fhome%2F",
//     "__RC": "4",
//     "__R": "1",
//   },
// };
// store.state.GLOBAL_FILTER_RESULT = {'15': true};
// store.commit('INIT_ENV', env);
// const zone = new Zone(zoneData);
// var mochaAsync = (fn) => {
//   return done => {
//     fn.call().then(done, err => {
//       done(err);
//     });
//   };
// };


// describe('3. Check Fair random in choose Share', function () {
//   this.timeout(90000);

//   it('should random correct ratio', mochaAsync(async () => {
//     const times = 10000;
//     let share_jw64yp4i = 0;
//     let share_jkkj65op = 0;
//     let share_weight_jw64yp4i = [];
//     let share_weight_jkkj65op = [];
//     let isError = false;
//     for (let index = 0; index < times; index++) {
//       const cpdFlag = store.state.CPD_FORCE;
//       const activeShare = zone.activeShare();
//       if (activeShare.id === 'jw64yp4i') {
//         share_jw64yp4i++;
//         if(share_weight_jw64yp4i.filter(i => i.cpdFlag === cpdFlag && i.weight === activeShare.weight).length === 0) share_weight_jw64yp4i.push({weight: activeShare.weight, cpdFlag});
//       } else if (activeShare.id === 'jkkj65op') {
//         share_jkkj65op++;
//         if(share_weight_jkkj65op.filter(i => i.cpdFlag === cpdFlag && i.weight === activeShare.weight).length === 0) share_weight_jkkj65op.push({weight: activeShare.weight, cpdFlag});
//       } else {
//         isError = true;
//         break;
//       }
//       store.commit('RENEW_CPM_CPD_FLAG', false);
//     }

//     const ratio_share_jw64yp4i = share_jw64yp4i / times;
//     const ratio_share_jkkj65op = share_jkkj65op / times;

//     const error = 0.03;
//     const expected1 = (ratio_share_jw64yp4i < (0.33 + error) && ratio_share_jw64yp4i > (0.33 - error));
//     const expected2 = (ratio_share_jkkj65op < (0.67 + error) && ratio_share_jkkj65op > (0.67 - error));

//     describe(`ratio_share_jw64yp4i (${share_jw64yp4i}-(${share_weight_jw64yp4i.map(i => i.cpdFlag === null ? 'null' : i.cpdFlag).join(',')})): ${ratio_share_jw64yp4i}`, () => {
//       it('checking', () => {
//         expect(expected1).to.equal(true);
//       });
//     });
//     describe(`ratio_share_jkkj65op (${share_jkkj65op}-(${share_weight_jkkj65op.map(i => i.cpdFlag === null ? 'null' : i.cpdFlag).join(',')})): ${ratio_share_jkkj65op}`, () => {
//       it('checking', () => {
//         expect(expected2).to.equal(true);
//       });
//     });
//     const result = expected1 && expected2 && !isError;

//     expect(result).to.equal(true);
//   }));
// });
