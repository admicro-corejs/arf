// /* eslint-disable */
// import store from '../../../src/store';
// import Turn from '../../../src/vendor/ArfStorage/storage/TrackLoad/Turn';
// import Zone from '../../../src/models/Zone';

// var mochaAsync = (fn) => {
//   return done => {
//     fn.call().then(done, err => {
//       done(err);
//     });
//   };
// };

// function pushTurn(zoneId, shareId, totalShare, activePlacements) {
//   const turn = new Turn(activePlacements.map(item => item.id), shareId, [], store.state.PAGE_LOAD.activePageLoad.campaignId, store.state.FULL_CPM);
//   store.commit('TRACK_TURN', { zoneId, turn, totalShare });
//   store.commit('SAVE_TRACK_LOAD_DATA');
// }

// describe('7.1 share multi full cpd - (posisition 1: 3cpd, position 2: 1 cpd, 1 cpm)', function () {
//   this.timeout(90000);

//   const zoneData = {
//     "id": "437",
//     "name": "Mixing_980x90_Afamily",
//     "height": 90,
//     "width": 980,
//     "css": " div[id*=\"adxzone_\"]{\n   width: 980px;\n   margin: 0 auto;\n}\n div[id*=\"adnzone_\"]{\n min-width:980px;\n   margin: 0 auto;\n    display: flex;\n    justify-content: center;\n    align-items: center;\n    width: auto;\n}",
//     "outputCss": "#zone-437 div[id*=\"adxzone_\"] {\n  width: 980px;\n  margin: 0 auto;\n}\n#zone-437 div[id*=\"adnzone_\"] {\n  min-width: 980px;\n  margin: 0 auto;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  width: auto;\n}\n",
//     "groupSSP": "",
//     "isResponsive": true,
//     "isMobile": false,
//     "isTemplate": true,
//     "template": "<zone id=\"jzlf9knq\"></zone>\n<script src=\"//media1.admicro.vn/cms/arf-jzlf9knq.min.js\"></script>\n%%ARF_CONTENT%%\n<script>\nvar a = new Image,\n                b = \" https://lg1.logging.admicro.vn/cpx?cov=0&tpn=1&dmn=\" + encodeURIComponent(document.domain) + \"&cmpg=1298939&items=571081&zid=1&cid=-1\";\n            a.src = b;\n</script>",
//     "totalShare": 3,
//     "status": "active",
//     "isPageLoad": true,
//     "isZoneVideo": false,
//     "positionOnSite": "0",
//     "timeBetweenAds": 0,
//     "siteId": "27",
//     "shares": [{
//         "id": "jvrkmh8q",
//         "css": "",
//         "outputCss": "",
//         "width": 980,
//         "height": 90,
//         "classes": "",
//         "isRotate": false,
//         "rotateTime": 20,
//         "type": "single",
//         "format": "",
//         "zoneId": "437",
//         "isTemplate": false,
//         "template": "",
//         "offset": "",
//         "isAdPod": false,
//         "duration": null,
//         "sharePlacements": [{
//             "id": "jvrkucst",
//             "positionOnShare": 0,
//             "placementId": "363142",
//             "shareId": "jvrkmh8q",
//             "time": "[{\"startTime\":\"1989-12-31T17:00:00.000Z\",\"endTime\":null}]",
//             "placement": {
//                 "id": "363142",
//                 "width": 0,
//                 "height": 0,
//                 "startTime": "1989-12-31T17:00:00.000Z",
//                 "endTime": null,
//                 "weight": 1,
//                 "revenueType": "pb",
//                 "cpdPercent": 0,
//                 "isRotate": false,
//                 "rotateTime": 0,
//                 "price": 0,
//                 "relative": 0,
//                 "campaignId": "1066388",
//                 "campaign": {
//                     "id": "1066388",
//                     "startTime": "1989-12-31T17:00:00.000Z",
//                     "endTime": null,
//                     "views": 0,
//                     "viewPerSession": 0,
//                     "timeResetViewCount": 0,
//                     "settingBudgetCPM": "",
//                     "settingBudgetCPC": "",
//                     "weight": 0,
//                     "revenueType": "cpm",
//                     "pageLoad": 0,
//                     "totalCPM": 0,
//                     "isRunMonopoly": false,
//                     "optionMonopoly": "",
//                     "isRunBudget": false,
//                     "expense": 0,
//                     "maxCPMPerDay": 0
//                 },
//                 "banners": [{
//                     "id": "395229",
//                     "html": "<div id=\"sspbid_1049\"></div><script type=\"text/javascript\">admsspreg({sspid:1049,w:980,h:90});</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "363142",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkw6vj",
//                         "logical": "or",
//                         "bannerId": "395229",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jvrkwn7i",
//                             "optionBannerId": "jvrkw6vj",
//                             "channelId": "290",
//                             "channel": {
//                                 "id": "290",
//                                 "name": "Afamily_home",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyr01dyb",
//                                     "name": "home",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/home/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T08:39:28.000Z",
//                                     "updatedAt": "2019-07-31T08:39:28.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "290",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "395234",
//                     "html": "<div id=\"sspbid_1055\"></div><script type=\"text/javascript\">admsspreg({sspid:1055,w:980,h:90});</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "363142",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkw6vo",
//                         "logical": "or",
//                         "bannerId": "395234",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jvrkwncr",
//                             "optionBannerId": "jvrkw6vo",
//                             "channelId": "489",
//                             "channel": {
//                                 "id": "489",
//                                 "name": "Video",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyr03akc",
//                                     "name": "video",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/video/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T08:40:57.000Z",
//                                     "updatedAt": "2019-07-31T08:40:57.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "489",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "395235",
//                     "html": "<div id=\"sspbid_1056\"></div><script type=\"text/javascript\">admsspreg({sspid:1056,w:980,h:90});</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "363142",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkw6zr",
//                         "logical": "or",
//                         "bannerId": "395235",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jytmnfx6",
//                             "optionBannerId": "jvrkw6zr",
//                             "channelId": "490",
//                             "channel": {
//                                 "id": "490",
//                                 "name": "Äáº¹p",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyr040lh",
//                                     "name": "dep",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/dep/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T08:41:31.000Z",
//                                     "updatedAt": "2019-07-31T08:41:31.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "490",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "395241",
//                     "html": "<div id=\"sspbid_1064\"></div><script type=\"text/javascript\">admsspreg({sspid:1064,w:980,h:90});</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "363142",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkw741",
//                         "logical": "or",
//                         "bannerId": "395241",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jvrkwnn8",
//                             "optionBannerId": "jvrkw741",
//                             "channelId": "1032",
//                             "channel": {
//                                 "id": "1032",
//                                 "name": "Xa hoi",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyqzudrf",
//                                     "name": "xa hoi",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/xa-hoi/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T08:34:01.000Z",
//                                     "updatedAt": "2019-07-31T08:34:01.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "1032",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "395247",
//                     "html": "<div id=\"sspbid_1072\"></div><script type=\"text/javascript\">admsspreg({sspid:1072,w:980,h:90});</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "363142",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkw78a",
//                         "logical": "or",
//                         "bannerId": "395247",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jytmwkbt",
//                             "optionBannerId": "jvrkw78a",
//                             "channelId": "491",
//                             "channel": {
//                                 "id": "491",
//                                 "name": "Äá»i Sá»‘ng",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyqpkf3w",
//                                     "name": "doi-song",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/doi-song/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T03:46:20.000Z",
//                                     "updatedAt": "2019-07-31T03:46:20.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "491",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }, {
//                                     "id": "k06jd4fw",
//                                     "name": "lifestyle-detail",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/lifestyle/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-09-05T10:16:43.000Z",
//                                     "updatedAt": "2019-09-06T04:01:19.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "491",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }, {
//                                     "id": "k092bsnt",
//                                     "name": "cong-so",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/cong-so/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-09-07T04:43:07.000Z",
//                                     "updatedAt": "2019-09-07T04:43:07.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "491",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "395253",
//                     "html": "<div id=\"sspbid_1080\"></div><script type=\"text/javascript\">admsspreg({sspid:1080,w:980,h:90});</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "363142",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkw7ck",
//                         "logical": "or",
//                         "bannerId": "395253",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jytn2myd",
//                             "optionBannerId": "jvrkw7ck",
//                             "channelId": "492",
//                             "channel": {
//                                 "id": "492",
//                                 "name": "Anngon-Kheotay",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyqpm0cy",
//                                     "name": "an-ngon",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/an-ngon/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T03:47:35.000Z",
//                                     "updatedAt": "2019-07-31T03:47:35.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "492",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "395259",
//                     "html": "<div id=\"sspbid_1088\"></div><script type=\"text/javascript\">admsspreg({sspid:1088,w:980,h:90});</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "363142",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkw7oy",
//                         "logical": "or",
//                         "bannerId": "395259",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jytrsnc6",
//                             "optionBannerId": "jvrkw7oy",
//                             "channelId": "493",
//                             "channel": {
//                                 "id": "493",
//                                 "name": "TinhYeu-HonNhan",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyqpo5wg",
//                                     "name": "tinh-yeu-hon-nhan",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/tinh-yeu-hon-nhan/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T03:49:15.000Z",
//                                     "updatedAt": "2019-07-31T03:49:15.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "493",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }, {
//                                     "id": "k06ja1pf",
//                                     "name": "yeu-detail",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/yeu/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-09-05T10:14:20.000Z",
//                                     "updatedAt": "2019-09-06T04:01:01.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "493",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "395265",
//                     "html": "<div id=\"sspbid_1096\"></div><script type=\"text/javascript\">admsspreg({sspid:1096,w:980,h:90});</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "363142",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkw7ta",
//                         "logical": "or",
//                         "bannerId": "395265",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jyts0svh",
//                             "optionBannerId": "jvrkw7ta",
//                             "channelId": "494",
//                             "channel": {
//                                 "id": "494",
//                                 "name": "SucKhoe",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyqpt534",
//                                     "name": "suc-khoe",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/suc-khoe/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T03:53:07.000Z",
//                                     "updatedAt": "2019-07-31T03:53:07.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "494",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "395271",
//                     "html": "<div id=\"sspbid_1104\"></div><script type=\"text/javascript\">admsspreg({sspid:1104,w:980,h:90});</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "363142",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkw7xk",
//                         "logical": "or",
//                         "bannerId": "395271",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jytscnth",
//                             "optionBannerId": "jvrkw7xk",
//                             "channelId": "495",
//                             "channel": {
//                                 "id": "495",
//                                 "name": "TamSu",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyqpvlxy",
//                                     "name": "tam-su",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/tam-su/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T03:55:02.000Z",
//                                     "updatedAt": "2019-07-31T03:55:02.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "495",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "395277",
//                     "html": "<div id=\"sspbid_1112\"></div><script type=\"text/javascript\">admsspreg({sspid:1112,w:980,h:90});</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "363142",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkw81t",
//                         "logical": "or",
//                         "bannerId": "395277",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jytsj6u9",
//                             "optionBannerId": "jvrkw81t",
//                             "channelId": "496",
//                             "channel": {
//                                 "id": "496",
//                                 "name": "MeVaBe",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyqxqbkt",
//                                     "name": "me-va-be",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/me-va-be/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T07:34:53.000Z",
//                                     "updatedAt": "2019-07-31T07:34:53.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "496",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }, {
//                                     "id": "k0929nzy",
//                                     "name": "day-con",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/day-con/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-09-07T04:41:27.000Z",
//                                     "updatedAt": "2019-09-07T04:41:27.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "496",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "395283",
//                     "html": "<div id=\"sspbid_1120\"></div><script type=\"text/javascript\">admsspreg({sspid:1120,w:980,h:90});</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "363142",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkw864",
//                         "logical": "or",
//                         "bannerId": "395283",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jvrkwox4",
//                             "optionBannerId": "jvrkw864",
//                             "channelId": "665",
//                             "channel": {
//                                 "id": "665",
//                                 "name": "Afamily-Nhahay",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jz4y43si",
//                                     "name": "mua-sam-nha-hay",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/mua-sam-nha-hay/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-08-10T02:54:22.000Z",
//                                     "updatedAt": "2019-08-10T02:54:22.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "665",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }, {
//                                     "id": "k06jcg63",
//                                     "name": "mua-sam-nha-hay-detail",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/mua-sam-nha-hay/detail/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-09-05T10:16:12.000Z",
//                                     "updatedAt": "2019-09-05T10:16:12.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "665",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }, {
//                                     "id": "k06jul2k",
//                                     "name": "nha-hay-mua-sam",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/nha-hay-mua-sam/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-09-05T10:30:18.000Z",
//                                     "updatedAt": "2019-09-05T10:30:18.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "665",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "395289",
//                     "html": "<div id=\"sspbid_1128\"></div><script type=\"text/javascript\">admsspreg({sspid:1128,w:980,h:90});</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "363142",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkw8ek",
//                         "logical": "or",
//                         "bannerId": "395289",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jytsxhhv",
//                             "optionBannerId": "jvrkw8ek",
//                             "channelId": "497",
//                             "channel": {
//                                 "id": "497",
//                                 "name": "HauTruong",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyqy4gd9",
//                                     "name": "hau-truong",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/hau-truong/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T07:45:52.000Z",
//                                     "updatedAt": "2019-07-31T07:45:52.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "497",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }, {
//                                     "id": "k06j6ugn",
//                                     "name": "showbiz-detail",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/showbiz/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-09-05T10:11:51.000Z",
//                                     "updatedAt": "2019-09-06T04:00:43.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "497",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "395295",
//                     "html": "<div id=\"sspbid_1136\"></div><script type=\"text/javascript\">admsspreg({sspid:1136,w:980,h:90});</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "363142",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkw8iu",
//                         "logical": "or",
//                         "bannerId": "395295",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jytt77f7",
//                             "optionBannerId": "jvrkw8iu",
//                             "channelId": "498",
//                             "channel": {
//                                 "id": "498",
//                                 "name": "GiaiTri",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyqy5tz0",
//                                     "name": "giai-tri",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/giai-tri/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T07:46:56.000Z",
//                                     "updatedAt": "2019-07-31T07:46:56.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "498",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "395301",
//                     "html": "<div id=\"sspbid_1144\"></div><script type=\"text/javascript\">admsspreg({sspid:1144,w:980,h:90});</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "363142",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkw8n5",
//                         "logical": "or",
//                         "bannerId": "395301",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jyttaufs",
//                             "optionBannerId": "jvrkw8n5",
//                             "channelId": "500",
//                             "channel": {
//                                 "id": "500",
//                                 "name": "ChuyenLa",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyqysvel",
//                                     "name": "chuyen-la",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/chuyen-la/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T08:04:51.000Z",
//                                     "updatedAt": "2019-07-31T08:04:51.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "500",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "395307",
//                     "html": "<div id=\"sspbid_1152\"></div><script type=\"text/javascript\">admsspreg({sspid:1152,w:980,h:90});</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "363142",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkw8rt",
//                         "logical": "or",
//                         "bannerId": "395307",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jvrkwpnq",
//                             "optionBannerId": "jvrkw8rt",
//                             "channelId": "501",
//                             "channel": {
//                                 "id": "501",
//                                 "name": "Ngam",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyqzgo76",
//                                     "name": "ngam",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/ngam/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T08:23:22.000Z",
//                                     "updatedAt": "2019-07-31T08:23:22.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "501",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "395308",
//                     "html": "<div id=\"sspbid_1153\"></div><script type=\"text/javascript\">admsspreg({sspid:1153,w:980,h:90});</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "363142",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkw8ru",
//                         "logical": "or",
//                         "bannerId": "395308",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jz7uyi5b",
//                             "optionBannerId": "jvrkw8ru",
//                             "channelId": "502",
//                             "channel": {
//                                 "id": "502",
//                                 "name": "Quiz",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jvrkoqae",
//                                     "name": "Site Page-URL",
//                                     "logical": "or",
//                                     "globalVariables": "",
//                                     "comparison": "=~",
//                                     "value": "/quiz",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-05-17T04:18:23.000Z",
//                                     "updatedAt": "2019-08-07T02:16:08.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "502",
//                                     "optionChannelTypeId": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//                                     "optionChannelType": {
//                                         "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//                                         "name": "Site Page-URL",
//                                         "isInputLink": true,
//                                         "isSelectOption": false,
//                                         "isVariable": false,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:49:59.000Z",
//                                         "updatedAt": "2017-01-18T17:49:59.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }, {
//                                     "id": "jvrkoqag",
//                                     "name": "Site Page-URL",
//                                     "logical": "or",
//                                     "globalVariables": "",
//                                     "comparison": "=~",
//                                     "value": "/xem-bai-tarot",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-05-17T04:18:23.000Z",
//                                     "updatedAt": "2019-07-31T03:12:36.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "502",
//                                     "optionChannelTypeId": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//                                     "optionChannelType": {
//                                         "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//                                         "name": "Site Page-URL",
//                                         "isInputLink": true,
//                                         "isSelectOption": false,
//                                         "isVariable": false,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:49:59.000Z",
//                                         "updatedAt": "2017-01-18T17:49:59.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }, {
//                                     "id": "jz0mhd18",
//                                     "name": "quiz",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/quiz/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-08-07T02:17:41.000Z",
//                                     "updatedAt": "2019-08-07T02:17:41.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "502",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "556314",
//                     "html": "<div id=\"sspbid_3556\"></div><script type=\"text/javascript\">admsspreg({sspid:3556,w:980,h:90});</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "363142",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkwbea",
//                         "logical": "or",
//                         "bannerId": "556314",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jvrkwsu5",
//                             "optionBannerId": "jvrkwbea",
//                             "channelId": "1778",
//                             "channel": {
//                                 "id": "1778",
//                                 "name": "other-tag",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jvrkoqcp",
//                                     "name": "Site Page-URL",
//                                     "logical": "or",
//                                     "globalVariables": "",
//                                     "comparison": "=~",
//                                     "value": "/tag",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-05-17T04:18:23.000Z",
//                                     "updatedAt": "2019-07-31T03:25:02.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "1778",
//                                     "optionChannelTypeId": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//                                     "optionChannelType": {
//                                         "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//                                         "name": "Site Page-URL",
//                                         "isInputLink": true,
//                                         "isSelectOption": false,
//                                         "isVariable": false,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:49:59.000Z",
//                                         "updatedAt": "2017-01-18T17:49:59.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }, {
//                                     "id": "jvrkoqcr",
//                                     "name": "Site Page-URL",
//                                     "logical": "or",
//                                     "globalVariables": "",
//                                     "comparison": "=~",
//                                     "value": "toi-nay-nha-minh-an-gi",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-05-17T04:18:23.000Z",
//                                     "updatedAt": "2019-07-31T03:25:02.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "1778",
//                                     "optionChannelTypeId": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//                                     "optionChannelType": {
//                                         "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//                                         "name": "Site Page-URL",
//                                         "isInputLink": true,
//                                         "isSelectOption": false,
//                                         "isVariable": false,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:49:59.000Z",
//                                         "updatedAt": "2017-01-18T17:49:59.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }, {
//                                     "id": "jytggder",
//                                     "name": "tag",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/tag/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-08-02T01:54:34.000Z",
//                                     "updatedAt": "2019-08-02T01:54:34.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "1778",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }, {
//                                     "id": "jz120qem",
//                                     "name": "Site Page-URL",
//                                     "logical": "or",
//                                     "globalVariables": "",
//                                     "comparison": "=~",
//                                     "value": "/bi-kip-mua-cuoi",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-08-07T09:32:39.000Z",
//                                     "updatedAt": "2019-08-07T09:32:39.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "1778",
//                                     "optionChannelTypeId": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//                                     "optionChannelType": {
//                                         "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//                                         "name": "Site Page-URL",
//                                         "isInputLink": true,
//                                         "isSelectOption": false,
//                                         "isVariable": false,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:49:59.000Z",
//                                         "updatedAt": "2017-01-18T17:49:59.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "557053",
//                     "html": "<div id=\"sspbid_2519\"></div><script type=\"text/javascript\">admsspreg({sspid:2519,w:980,h:90});</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "363142",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkwbiw",
//                         "logical": "or",
//                         "bannerId": "557053",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jvrkwszc",
//                             "optionBannerId": "jvrkwbiw",
//                             "channelId": "1776",
//                             "channel": {
//                                 "id": "1776",
//                                 "name": "tháº¿ giá»›i",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyqoh9ju",
//                                     "name": "the-gioi",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/the-gioi/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T03:15:54.000Z",
//                                     "updatedAt": "2019-07-31T03:15:54.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "1776",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }]
//             }
//         }, {
//             "id": "jvrkukkl",
//             "positionOnShare": 0,
//             "placementId": "565797",
//             "shareId": "jvrkmh8q",
//             "time": "[{\"startTime\":\"1989-12-31T17:00:00.000Z\",\"endTime\":null}]",
//             "placement": {
//                 "id": "565797",
//                 "width": 0,
//                 "height": 0,
//                 "startTime": "1989-12-31T17:00:00.000Z",
//                 "endTime": null,
//                 "weight": 1,
//                 "revenueType": "cpm",
//                 "cpdPercent": 0,
//                 "isRotate": false,
//                 "rotateTime": 0,
//                 "price": 0,
//                 "relative": 0,
//                 "campaignId": "1298661",
//                 "campaign": {
//                     "id": "1298661",
//                     "startTime": "1989-12-31T17:00:00.000Z",
//                     "endTime": null,
//                     "views": 0,
//                     "viewPerSession": 0,
//                     "timeResetViewCount": 0,
//                     "settingBudgetCPM": "",
//                     "settingBudgetCPC": "",
//                     "weight": 0,
//                     "revenueType": "cpm",
//                     "pageLoad": 0,
//                     "totalCPM": 0,
//                     "isRunMonopoly": false,
//                     "optionMonopoly": "",
//                     "isRunBudget": false,
//                     "expense": 0,
//                     "maxCPMPerDay": 0
//                 },
//                 "banners": [{
//                     "id": "565797",
//                     "html": "<div id=\"ssppagebid_1205\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1205,w:0,h:0, group:\"1205,1206,1207,1208\"}); });} else {admsspPosition({sspid:1205,w:0,h:0, group:\"1205,1206,1207,1208\"});}</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "565797",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkwgdy",
//                         "logical": "or",
//                         "bannerId": "565797",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jvrkwyu9",
//                             "optionBannerId": "jvrkwgdy",
//                             "channelId": "491",
//                             "channel": {
//                                 "id": "491",
//                                 "name": "Äá»i Sá»‘ng",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyqpkf3w",
//                                     "name": "doi-song",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/doi-song/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T03:46:20.000Z",
//                                     "updatedAt": "2019-07-31T03:46:20.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "491",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }, {
//                                     "id": "k06jd4fw",
//                                     "name": "lifestyle-detail",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/lifestyle/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-09-05T10:16:43.000Z",
//                                     "updatedAt": "2019-09-06T04:01:19.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "491",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }, {
//                                     "id": "k092bsnt",
//                                     "name": "cong-so",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/cong-so/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-09-07T04:43:07.000Z",
//                                     "updatedAt": "2019-09-07T04:43:07.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "491",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "565874",
//                     "html": "<div id=\"ssppagebid_1321\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1321,w:0,h:0, group:\"1321,1322,1323,1324\"}); });} else {admsspPosition({sspid:1321,w:0,h:0, group:\"1321,1322,1323,1324\"});}</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "565797",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkwgma",
//                         "logical": "or",
//                         "bannerId": "565874",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jvrkwz4q",
//                             "optionBannerId": "jvrkwgma",
//                             "channelId": "497",
//                             "channel": {
//                                 "id": "497",
//                                 "name": "HauTruong",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyqy4gd9",
//                                     "name": "hau-truong",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/hau-truong/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T07:45:52.000Z",
//                                     "updatedAt": "2019-07-31T07:45:52.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "497",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }, {
//                                     "id": "k06j6ugn",
//                                     "name": "showbiz-detail",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/showbiz/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-09-05T10:11:51.000Z",
//                                     "updatedAt": "2019-09-06T04:00:43.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "497",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }]
//             }
//         }, {
//             "id": "jvrkukrc",
//             "positionOnShare": 0,
//             "placementId": "566059",
//             "shareId": "jvrkmh8q",
//             "time": "[{\"startTime\":\"1989-12-31T17:00:00.000Z\",\"endTime\":null}]",
//             "placement": {
//                 "id": "566059",
//                 "width": 0,
//                 "height": 0,
//                 "startTime": "1989-12-31T17:00:00.000Z",
//                 "endTime": null,
//                 "weight": 1,
//                 "revenueType": "cpm",
//                 "cpdPercent": 0,
//                 "isRotate": false,
//                 "rotateTime": 0,
//                 "price": 0,
//                 "relative": 0,
//                 "campaignId": "1298661",
//                 "campaign": {
//                     "id": "1298661",
//                     "startTime": "1989-12-31T17:00:00.000Z",
//                     "endTime": null,
//                     "views": 0,
//                     "viewPerSession": 0,
//                     "timeResetViewCount": 0,
//                     "settingBudgetCPM": "",
//                     "settingBudgetCPC": "",
//                     "weight": 0,
//                     "revenueType": "cpm",
//                     "pageLoad": 0,
//                     "totalCPM": 0,
//                     "isRunMonopoly": false,
//                     "optionMonopoly": "",
//                     "isRunBudget": false,
//                     "expense": 0,
//                     "maxCPMPerDay": 0
//                 },
//                 "banners": [{
//                     "id": "566059",
//                     "html": "<div id=\"ssppagebid_1351\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1351,w:0,h:0, group:\"1351,1352,1353,1354\"}); });} else {admsspPosition({sspid:1351,w:0,h:0, group:\"1351,1352,1353,1354\"});}</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "566059",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkwguw",
//                         "logical": "or",
//                         "bannerId": "566059",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jvrkwzo5",
//                             "optionBannerId": "jvrkwguw",
//                             "channelId": "665",
//                             "channel": {
//                                 "id": "665",
//                                 "name": "Afamily-Nhahay",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jz4y43si",
//                                     "name": "mua-sam-nha-hay",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/mua-sam-nha-hay/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-08-10T02:54:22.000Z",
//                                     "updatedAt": "2019-08-10T02:54:22.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "665",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }, {
//                                     "id": "k06jcg63",
//                                     "name": "mua-sam-nha-hay-detail",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/mua-sam-nha-hay/detail/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-09-05T10:16:12.000Z",
//                                     "updatedAt": "2019-09-05T10:16:12.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "665",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }, {
//                                     "id": "k06jul2k",
//                                     "name": "nha-hay-mua-sam",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/nha-hay-mua-sam/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-09-05T10:30:18.000Z",
//                                     "updatedAt": "2019-09-05T10:30:18.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "665",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "566065",
//                     "html": "<div id=\"ssppagebid_1381\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1381,w:0,h:0, group:\"1381,1382,1383,1385\"}); });} else {admsspPosition({sspid:1381,w:0,h:0, group:\"1381,1382,1383,1385\"});}</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "566059",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkwgz9",
//                         "logical": "or",
//                         "bannerId": "566065",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jvrkwztj",
//                             "optionBannerId": "jvrkwgz9",
//                             "channelId": "1032",
//                             "channel": {
//                                 "id": "1032",
//                                 "name": "Xa hoi",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyqzudrf",
//                                     "name": "xa hoi",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/xa-hoi/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T08:34:01.000Z",
//                                     "updatedAt": "2019-07-31T08:34:01.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "1032",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "566073",
//                     "html": "<div id=\"ssppagebid_1366\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1366,w:0,h:0, group:\"1366,1367,1368,1375\"}); });} else {admsspPosition({sspid:1366,w:0,h:0, group:\"1366,1367,1368,1375\"});}</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "566059",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkwh3m",
//                         "logical": "or",
//                         "bannerId": "566073",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jz7uwr7o",
//                             "optionBannerId": "jvrkwh3m",
//                             "channelId": "502",
//                             "channel": {
//                                 "id": "502",
//                                 "name": "Quiz",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jvrkoqae",
//                                     "name": "Site Page-URL",
//                                     "logical": "or",
//                                     "globalVariables": "",
//                                     "comparison": "=~",
//                                     "value": "/quiz",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-05-17T04:18:23.000Z",
//                                     "updatedAt": "2019-08-07T02:16:08.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "502",
//                                     "optionChannelTypeId": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//                                     "optionChannelType": {
//                                         "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//                                         "name": "Site Page-URL",
//                                         "isInputLink": true,
//                                         "isSelectOption": false,
//                                         "isVariable": false,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:49:59.000Z",
//                                         "updatedAt": "2017-01-18T17:49:59.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }, {
//                                     "id": "jvrkoqag",
//                                     "name": "Site Page-URL",
//                                     "logical": "or",
//                                     "globalVariables": "",
//                                     "comparison": "=~",
//                                     "value": "/xem-bai-tarot",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-05-17T04:18:23.000Z",
//                                     "updatedAt": "2019-07-31T03:12:36.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "502",
//                                     "optionChannelTypeId": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//                                     "optionChannelType": {
//                                         "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//                                         "name": "Site Page-URL",
//                                         "isInputLink": true,
//                                         "isSelectOption": false,
//                                         "isVariable": false,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:49:59.000Z",
//                                         "updatedAt": "2017-01-18T17:49:59.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }, {
//                                     "id": "jz0mhd18",
//                                     "name": "quiz",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/quiz/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-08-07T02:17:41.000Z",
//                                     "updatedAt": "2019-08-07T02:17:41.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "502",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "566080",
//                     "html": "<div id=\"ssppagebid_1232\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1232,w:0,h:0, group:\"1232,1233,1234,1235\"}); });} else {admsspPosition({sspid:1232,w:0,h:0, group:\"1232,1233,1234,1235\"});}</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "566059",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkwhbv",
//                         "logical": "or",
//                         "bannerId": "566080",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jytrs5ko",
//                             "optionBannerId": "jvrkwhbv",
//                             "channelId": "492",
//                             "channel": {
//                                 "id": "492",
//                                 "name": "Anngon-Kheotay",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyqpm0cy",
//                                     "name": "an-ngon",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/an-ngon/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T03:47:35.000Z",
//                                     "updatedAt": "2019-07-31T03:47:35.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "492",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "566089",
//                     "html": "<div id=\"ssppagebid_1261\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1261,w:0,h:0, group:\"1264,1261,1262,1263\"}); });} else {admsspPosition({sspid:1261,w:0,h:0, group:\"1264,1261,1262,1263\"});}</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "566059",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkwhg8",
//                         "logical": "or",
//                         "bannerId": "566089",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jyts24i8",
//                             "optionBannerId": "jvrkwhg8",
//                             "channelId": "494",
//                             "channel": {
//                                 "id": "494",
//                                 "name": "SucKhoe",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyqpt534",
//                                     "name": "suc-khoe",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/suc-khoe/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T03:53:07.000Z",
//                                     "updatedAt": "2019-07-31T03:53:07.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "494",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "566098",
//                     "html": "<div id=\"ssppagebid_1246\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1246,w:0,h:0, group:\"1248,1249,1246,1247\"}); });} else {admsspPosition({sspid:1246,w:0,h:0, group:\"1248,1249,1246,1247\"});}</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "566059",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkwhsw",
//                         "logical": "or",
//                         "bannerId": "566098",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jytis3i7",
//                             "optionBannerId": "jvrkwhsw",
//                             "channelId": "493",
//                             "channel": {
//                                 "id": "493",
//                                 "name": "TinhYeu-HonNhan",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyqpo5wg",
//                                     "name": "tinh-yeu-hon-nhan",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/tinh-yeu-hon-nhan/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T03:49:15.000Z",
//                                     "updatedAt": "2019-07-31T03:49:15.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "493",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }, {
//                                     "id": "k06ja1pf",
//                                     "name": "yeu-detail",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/yeu/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-09-05T10:14:20.000Z",
//                                     "updatedAt": "2019-09-06T04:01:01.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "493",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "566106",
//                     "html": "<div id=\"ssppagebid_1276\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1276,w:0,h:0, group:\"1276,1277,1278,1279\"}); });} else {admsspPosition({sspid:1276,w:0,h:0, group:\"1276,1277,1278,1279\"});}</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "566059",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkwhxs",
//                         "logical": "or",
//                         "bannerId": "566106",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jytsh0gj",
//                             "optionBannerId": "jvrkwhxs",
//                             "channelId": "495",
//                             "channel": {
//                                 "id": "495",
//                                 "name": "TamSu",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyqpvlxy",
//                                     "name": "tam-su",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/tam-su/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T03:55:02.000Z",
//                                     "updatedAt": "2019-07-31T03:55:02.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "495",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "566116",
//                     "html": "<div id=\"ssppagebid_1291\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1291,w:0,h:0, group:\"1291,1292,1293,1294\"}); });} else {admsspPosition({sspid:1291,w:0,h:0, group:\"1291,1292,1293,1294\"});}</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "566059",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkwi6y",
//                         "logical": "or",
//                         "bannerId": "566116",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jytsq15v",
//                             "optionBannerId": "jvrkwi6y",
//                             "channelId": "496",
//                             "channel": {
//                                 "id": "496",
//                                 "name": "MeVaBe",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyqxqbkt",
//                                     "name": "me-va-be",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/me-va-be/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T07:34:53.000Z",
//                                     "updatedAt": "2019-07-31T07:34:53.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "496",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }, {
//                                     "id": "k0929nzy",
//                                     "name": "day-con",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/day-con/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-09-07T04:41:27.000Z",
//                                     "updatedAt": "2019-09-07T04:41:27.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "496",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "566124",
//                     "html": "<div id=\"ssppagebid_1311\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1311,w:0,h:0, group:\"1312,1313,1314,1311\"}); });} else {admsspPosition({sspid:1311,w:0,h:0, group:\"1312,1313,1314,1311\"});}</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "566059",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkwibv",
//                         "logical": "or",
//                         "bannerId": "566124",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jytsrlph",
//                             "optionBannerId": "jvrkwibv",
//                             "channelId": "490",
//                             "channel": {
//                                 "id": "490",
//                                 "name": "Äáº¹p",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyr040lh",
//                                     "name": "dep",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/dep/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T08:41:31.000Z",
//                                     "updatedAt": "2019-07-31T08:41:31.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "490",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "566135",
//                     "html": "<div id=\"ssppagebid_1336\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1336,w:0,h:0, group:\"1336,1337,1338,1339\"}); });} else {admsspPosition({sspid:1336,w:0,h:0, group:\"1336,1337,1338,1339\"});}</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "566059",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkwiko",
//                         "logical": "or",
//                         "bannerId": "566135",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jythsvfq",
//                             "optionBannerId": "jvrkwiko",
//                             "channelId": "498",
//                             "channel": {
//                                 "id": "498",
//                                 "name": "GiaiTri",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyqy5tz0",
//                                     "name": "giai-tri",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/giai-tri/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T07:46:56.000Z",
//                                     "updatedAt": "2019-07-31T07:46:56.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "498",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "566143",
//                     "html": "<div id=\"ssppagebid_1396\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1396,w:0,h:0, group:\"1396,1397,1398,1399\"}); });} else {admsspPosition({sspid:1396,w:0,h:0, group:\"1396,1397,1398,1399\"});}</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "566059",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkwit7",
//                         "logical": "or",
//                         "bannerId": "566143",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jvrkx1q4",
//                             "optionBannerId": "jvrkwit7",
//                             "channelId": "1776",
//                             "channel": {
//                                 "id": "1776",
//                                 "name": "tháº¿ giá»›i",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyqoh9ju",
//                                     "name": "the-gioi",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/the-gioi/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T03:15:54.000Z",
//                                     "updatedAt": "2019-07-31T03:15:54.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "1776",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "566419",
//                     "html": "<div id=\"ssppagebid_1225\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1225,w:0,h:0, group:\"1225,1226,1227,1228\"}); });} else {admsspPosition({sspid:1225,w:0,h:0, group:\"1225,1226,1227,1228\"});}</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "566059",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "https://lg1.logging.admicro.vn/cpx?cov=0&tpn=1&dmn=cpd.admicro.vn&cmpg=1298939&items=570813&zid=1&cid=-1",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkwixl",
//                         "logical": "or",
//                         "bannerId": "566419",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jvrkx212",
//                             "optionBannerId": "jvrkwixl",
//                             "channelId": "290",
//                             "channel": {
//                                 "id": "290",
//                                 "name": "Afamily_home",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyr01dyb",
//                                     "name": "home",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/home/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T08:39:28.000Z",
//                                     "updatedAt": "2019-07-31T08:39:28.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "290",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }, {
//                     "id": "566432",
//                     "html": "<div id=\"ssppagebid_1200\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1200,w:0,h:0, group:\"1200,1201,1202,1203\"}); });} else {admsspPosition({sspid:1200,w:0,h:0, group:\"1200,1201,1202,1203\"});}</script>",
//                     "width": 0,
//                     "height": 0,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "566059",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": false,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jvrkwj2p",
//                         "logical": "or",
//                         "bannerId": "566432",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jvrkx216",
//                             "optionBannerId": "jvrkwj2p",
//                             "channelId": "1778",
//                             "channel": {
//                                 "id": "1778",
//                                 "name": "other-tag",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jvrkoqcp",
//                                     "name": "Site Page-URL",
//                                     "logical": "or",
//                                     "globalVariables": "",
//                                     "comparison": "=~",
//                                     "value": "/tag",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-05-17T04:18:23.000Z",
//                                     "updatedAt": "2019-07-31T03:25:02.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "1778",
//                                     "optionChannelTypeId": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//                                     "optionChannelType": {
//                                         "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//                                         "name": "Site Page-URL",
//                                         "isInputLink": true,
//                                         "isSelectOption": false,
//                                         "isVariable": false,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:49:59.000Z",
//                                         "updatedAt": "2017-01-18T17:49:59.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }, {
//                                     "id": "jvrkoqcr",
//                                     "name": "Site Page-URL",
//                                     "logical": "or",
//                                     "globalVariables": "",
//                                     "comparison": "=~",
//                                     "value": "toi-nay-nha-minh-an-gi",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-05-17T04:18:23.000Z",
//                                     "updatedAt": "2019-07-31T03:25:02.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "1778",
//                                     "optionChannelTypeId": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//                                     "optionChannelType": {
//                                         "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//                                         "name": "Site Page-URL",
//                                         "isInputLink": true,
//                                         "isSelectOption": false,
//                                         "isVariable": false,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:49:59.000Z",
//                                         "updatedAt": "2017-01-18T17:49:59.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }, {
//                                     "id": "jytggder",
//                                     "name": "tag",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/tag/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-08-02T01:54:34.000Z",
//                                     "updatedAt": "2019-08-02T01:54:34.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "1778",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }, {
//                                     "id": "jz120qem",
//                                     "name": "Site Page-URL",
//                                     "logical": "or",
//                                     "globalVariables": "",
//                                     "comparison": "=~",
//                                     "value": "/bi-kip-mua-cuoi",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-08-07T09:32:39.000Z",
//                                     "updatedAt": "2019-08-07T09:32:39.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "1778",
//                                     "optionChannelTypeId": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//                                     "optionChannelType": {
//                                         "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
//                                         "name": "Site Page-URL",
//                                         "isInputLink": true,
//                                         "isSelectOption": false,
//                                         "isVariable": false,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:49:59.000Z",
//                                         "updatedAt": "2017-01-18T17:49:59.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }]
//             }
//         }, {
//             "id": "jvrkvezy",
//             "positionOnShare": 0,
//             "placementId": "276873",
//             "shareId": "jvrkmh8q",
//             "time": "[{\"startTime\":\"1989-12-31T17:00:00.000Z\",\"endTime\":null}]",
//             "placement": {
//                 "id": "276873",
//                 "width": 728,
//                 "height": 90,
//                 "startTime": "1989-12-31T17:00:00.000Z",
//                 "endTime": null,
//                 "weight": 1,
//                 "revenueType": "pr",
//                 "cpdPercent": 1,
//                 "isRotate": false,
//                 "rotateTime": 0,
//                 "price": 0,
//                 "relative": 0,
//                 "campaignId": "1042597",
//                 "campaign": {
//                     "id": "1042597",
//                     "startTime": "1989-12-31T17:00:00.000Z",
//                     "endTime": null,
//                     "views": 0,
//                     "viewPerSession": 0,
//                     "timeResetViewCount": 0,
//                     "settingBudgetCPM": "",
//                     "settingBudgetCPC": "",
//                     "weight": 0,
//                     "revenueType": "cpd",
//                     "pageLoad": 0,
//                     "totalCPM": 0,
//                     "isRunMonopoly": false,
//                     "optionMonopoly": "",
//                     "isRunBudget": false,
//                     "expense": 0,
//                     "maxCPMPerDay": 0
//                 },
//                 "banners": [{
//                     "id": "276873",
//                     "html": "<div style=\"clear:both;width:728px; margin:0 auto;\"><script type=\"text/javascript\">\r\ngoogle_ad_client = \"ca-pub-6366951472589375\";\r\n\r\ngoogle_ad_slot = \"5426976522\";\r\ngoogle_ad_width = 728;\r\ngoogle_ad_height = 90;\r\n\r\n</script>\r\n<script type=\"text/javascript\"\r\nsrc=\"//pagead2.googlesyndication.com/pagead/show_ads.js\">\r\n</script>\r\n</div>",
//                     "width": 728,
//                     "height": 90,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "276873",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "",
//                     "target": "_blank",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": true,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "jys30cry",
//                         "logical": "and",
//                         "bannerId": "276873",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "jys30dcv",
//                             "optionBannerId": "jys30cry",
//                             "channelId": "jys2nivx",
//                             "channel": {
//                                 "id": "jys2nivx",
//                                 "name": "location_nuocngoai",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jys2vc6b",
//                                     "name": "Location",
//                                     "logical": "and",
//                                     "globalVariables": "__RC",
//                                     "comparison": "==",
//                                     "value": "101",
//                                     "valueSelect": "__RC>=101",
//                                     "logicalSelect": ">=",
//                                     "createdAt": "2019-08-01T02:46:31.000Z",
//                                     "updatedAt": "2019-08-01T02:46:31.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "jys2nivx",
//                                     "optionChannelTypeId": "d045cfc5-c489-4e9c-8d74-423063db496b",
//                                     "optionChannelType": {
//                                         "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
//                                         "name": "Location",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": false,
//                                         "isMultiSelect": true,
//                                         "userId": null,
//                                         "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]",
//                                         "isGlobal": true,
//                                         "status": "active",
//                                         "createdAt": "2018-06-28T14:17:05.000Z",
//                                         "updatedAt": "2018-07-09T04:12:01.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }, {
//                                     "id": "jys2vc7t",
//                                     "name": "Location",
//                                     "logical": "and",
//                                     "globalVariables": "__RC",
//                                     "comparison": "!=",
//                                     "value": "undefined",
//                                     "valueSelect": "__RC==undefined",
//                                     "logicalSelect": "==",
//                                     "createdAt": "2019-08-01T02:46:31.000Z",
//                                     "updatedAt": "2019-08-01T02:46:31.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "jys2nivx",
//                                     "optionChannelTypeId": "d045cfc5-c489-4e9c-8d74-423063db496b",
//                                     "optionChannelType": {
//                                         "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
//                                         "name": "Location",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": false,
//                                         "isMultiSelect": true,
//                                         "userId": null,
//                                         "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]",
//                                         "isGlobal": true,
//                                         "status": "active",
//                                         "createdAt": "2018-06-28T14:17:05.000Z",
//                                         "updatedAt": "2018-07-09T04:12:01.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }]
//             }
//         }, {
//             "id": "k1msrutd",
//             "positionOnShare": 0,
//             "placementId": "k1msrgn2",
//             "shareId": "jvrkmh8q",
//             "time": "[{\"startTime\":\"2019-10-11T17:00:00.000Z\",\"endTime\":\"2019-10-18T16:59:59.000Z\"}]",
//             "placement": {
//                 "id": "k1msrgn2",
//                 "width": 1040,
//                 "height": 90,
//                 "startTime": "2019-10-11T17:00:00.000Z",
//                 "endTime": "2019-10-18T16:59:59.000Z",
//                 "weight": 1,
//                 "revenueType": "cpd",
//                 "cpdPercent": 1,
//                 "isRotate": false,
//                 "rotateTime": 20,
//                 "price": 0,
//                 "relative": 0,
//                 "campaignId": "k1mspa5o",
//                 "campaign": {
//                     "id": "k1mspa5o",
//                     "startTime": "2019-10-11T17:00:00.000Z",
//                     "endTime": null,
//                     "views": 0,
//                     "viewPerSession": 0,
//                     "timeResetViewCount": 0,
//                     "settingBudgetCPM": "",
//                     "settingBudgetCPC": "",
//                     "weight": 0,
//                     "revenueType": "cpd",
//                     "pageLoad": 0,
//                     "totalCPM": 0,
//                     "isRunMonopoly": false,
//                     "optionMonopoly": "",
//                     "isRunBudget": false,
//                     "expense": 0,
//                     "maxCPMPerDay": 0
//                 },
//                 "banners": [{
//                     "id": "k1msrh9n",
//                     "html": "<script>\n  (function() {\n    var admid=window.frameElement.id;\n    var b = parent.document.getElementById(admid);\n    b.style.width = \"100%\";\n    var f = b.parentNode.id;\n    var a = parent.wPrototype.getElementWidth(f);\n    \n\tvar image_url = 'https://adi.admicro.vn/adt/banners/nam2015/4043/min_html5/huyphanquoc/2019_10_12/thegioinuochoa-1/screenshot/thegioinuochoa__bg_replay.png';\n\tvar banner_url = 'https://adi.admicro.vn/adt/banners/nam2015/4043/min_html5/huyennguyenthithu/2019_10_10/1040x90/1040x90/1040x90.html';\n    var banner_width = 1040;\n    var banner_height = 90;\n\t\n    var b = document,\n        c = navigator.userAgent + \"\";\n    var d = '<div id=\"adstop\" style=\"position:relative;overflow:hidden\">',\n        d = -1 != c.indexOf(\"Android\") || -1 != c.indexOf(\"iPad\") || -1 != c.indexOf(\"iPhone\") ? d + ('<img src=\"' + image_url + '\" border=\"0\"/><a href=\"%%CLICK_URL_UNESC%%\" target=\"_blank\" style=\"position:absolute;top:0;left:0;width:' + banner_width + 'px;height:' + banner_height + 'px;display:block;z-index:9999;\"><span></span></a>') : d + ('<iframe id=\"demo_iframe\" src=\"'+banner_url+'?url=%%CLICK_URL_ESC%%\" width=\"'+banner_width+'\" frameborder=\"0\" scrolling=\"no\" height=\"'+banner_height+'\"></iframe>');\n    b.write(d + \"</div>\");\n  }\n  )();\n</script>",
//                     "width": 1040,
//                     "height": 90,
//                     "keyword": "",
//                     "isMacro": false,
//                     "weight": 1,
//                     "placementId": "k1msrgn2",
//                     "imageUrl": "",
//                     "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                     "url": "https://www.thegioinuochoa.com.vn/promotion/?utm_source=AdMicro&utm_medium=CPD&utm_campaign=thegioinuochoa",
//                     "target": "",
//                     "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
//                     "isIFrame": true,
//                     "isDefault": false,
//                     "isRelative": false,
//                     "videoType": "",
//                     "clickThroughUrl": "",
//                     "vastTagsUrl": "",
//                     "vastVideoUrl": "",
//                     "videoUrl": "",
//                     "duration": 0,
//                     "thirdPartyTracking": "",
//                     "thirdPartyUrl": "",
//                     "isDocumentWrite": true,
//                     "skipOffset": 0,
//                     "isAddLinkSSP": false,
//                     "optionBanners": [{
//                         "id": "k1mssbfv",
//                         "logical": "and",
//                         "bannerId": "k1msrh9n",
//                         "comparison": "==",
//                         "value": "",
//                         "type": "channel",
//                         "optionBannerChannels": [{
//                             "id": "k1msscfh",
//                             "optionBannerId": "k1mssbfv",
//                             "channelId": "290",
//                             "channel": {
//                                 "id": "290",
//                                 "name": "Afamily_home",
//                                 "siteId": "27",
//                                 "optionChannels": [{
//                                     "id": "jyr01dyb",
//                                     "name": "home",
//                                     "logical": "or",
//                                     "globalVariables": "_ADM_Channel",
//                                     "comparison": "=~",
//                                     "value": "/home/",
//                                     "valueSelect": "",
//                                     "logicalSelect": "",
//                                     "createdAt": "2019-07-31T08:39:28.000Z",
//                                     "updatedAt": "2019-07-31T08:39:28.000Z",
//                                     "deletedAt": null,
//                                     "channelId": "290",
//                                     "optionChannelTypeId": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                     "optionChannelType": {
//                                         "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
//                                         "name": "Variable",
//                                         "isInputLink": false,
//                                         "isSelectOption": false,
//                                         "isVariable": true,
//                                         "isMultiSelect": false,
//                                         "userId": null,
//                                         "storageType": null,
//                                         "isGlobal": false,
//                                         "status": "active",
//                                         "createdAt": "2017-01-18T17:53:31.000Z",
//                                         "updatedAt": "2017-01-18T17:53:31.000Z",
//                                         "deletedAt": null
//                                     }
//                                 }]
//                             }
//                         }]
//                     }],
//                     "bannerType": {
//                         "id": "374da180-02c6-4558-8985-90cad792f14f",
//                         "name": "Script",
//                         "value": "script",
//                         "isUpload": false,
//                         "isVideo": false
//                     },
//                     "bannerHtmlType": {
//                         "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
//                         "name": "PR Tracking",
//                         "value": "pr-tracking",
//                         "weight": 99
//                     }
//                 }]
//             }
//         }]
//     }],
//     "site": {
//         "id": "27",
//         "domain": "http://afamily.vn",
//         "pageLoad": {
//             "totalPageload": 3,
//             "campaigns": []
//         },
//         "isRunBannerDefault": false,
//         "isNoBrand": false,
//         "globalFilters": [],
//         "channels": [{
//             "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
//             "name": "Location",
//             "isGlobal": true,
//             "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]"
//         }],
//         "campaignMonopoly": {
//             "campaigns": []
//         }
//     }
// };
  
//   const env = {
//       "domain": "http://afamily.vn",
//       "url": "http://afamily.vn/",
//       "referrerUrl": "http://afamily.vn/",
//       "force": [],
//       "domainCore": "https://media1.admicro.vn",
//       "domainLog": "https://lg1.logging.admicro.vn",
//       "currentChannel": "%2fhome%2f",
//       "variables": {
//         "_ADM_Channel": "%2Fhome%2F",
//         "__RC": "4",
//         "__R": "1"
//       }
//   };
  
//   store.commit('INIT_ENV', env);
  
//   store.state.GLOBAL_FILTER_RESULT[zoneData.site.id] = true;

//   function statistical(arr, times, evaluateConfigs) {
//     evaluateConfigs = evaluateConfigs || [];
//     const distinct = (val, index, self) => {
//       return self.indexOf(val) === index;
//     };
//     const uniqueitems = arr.filter(distinct);
//     let result = [];

//     for (let index = 0; index < uniqueitems.length; index++) {
//       const uniqueITem = uniqueitems[index];
//       let count = 0;
//       for (let index = 0; index < arr.length; index++) {
//         const element = arr[index];
//         if (element === uniqueITem) count++;
//       }
//       const percent = (count * 100) / times;
//       let evaluateValue;
//       const evaluateConfig = evaluateConfigs.filter(e => e.id === uniqueITem)[0];
//       if (evaluateConfig) {
//         const errorE = evaluateConfig.error;
//         const percentE = evaluateConfig.percent;
//         const countE = evaluateConfig.count;
//         if (percentE) {
//           evaluateValue = percentE + errorE >= percent && percentE - errorE <= percent;
//         } else if (countE) {
//           evaluateValue = countE === count;
//         }
//       }
//       result.push({
//         id: uniqueITem,
//         count,
//         percent: count / times,
//         evaluate: evaluateValue,
//       });
//     }

//     return result;
//   }
  
//   function test(refreshTimes, isSaveHistory, evaluateShare, evaluateBanner) {
//     const times = refreshTimes || 10000;
//     let shareAppear = [];
//     let bannerAppear = [];
//     const cpmCheck = [];
//     let cpmFlagWrong = 0;
//     let cpmFlagCount = 0;

//     for (let index = 0; index < times; index++) {
//       const zone = new Zone(zoneData);

//       const activeShare = zone.activeShare();
//       const activePlacements = activeShare.activeSharePlacements(false, []).map(i => i.placement);
//       const activeBanners = activePlacements.map(i => i.activeBanner()).map(i => i.id);
//       if (isSaveHistory) pushTurn(zone.id, activeShare.id, zone.totalShare, activePlacements);
//       const exitsCPD = activePlacements.reduce((res, placement) => {
//         if (placement.revenueType === 'cpd') {
//           return true;
//         }
//         return res && false;
//       }, 0);
//       if (store.state.FULL_CPM === true && exitsCPD === true) {
//           cpmFlagWrong++;
//           const detail = {};
//           detail.turnsLeft = store.state.TURNS_LEFT;
//           detail.cpmfull = store.state.IS_FULL_CPM;
//           detail.cpdFlag = store.state.CPD_FORCE;
//           detail.shareId = {id: activeShare.id, limit: activeShare.limitTurn()};
//           detail.activePlacements = activePlacements.map(cp => ({id: cp.id, revenueType: cp.revenueType}));
//           cpmCheck.push(detail);
//       }

//       if (store.state.FULL_CPM === true) cpmFlagCount++;

//       shareAppear.push(activeShare.id);
//       activeBanners.forEach(bannerId => {
//         bannerAppear.push(bannerId);
//       });
//       store.commit('RENEW_CPM_CPD_FLAG', isSaveHistory);
//     }

//     const shareStatistical = statistical(shareAppear, times, evaluateShare);
//     const bannerStatistical = statistical(bannerAppear, times, evaluateBanner);

//     return {
//       shareStatistical,
//       bannerStatistical,
//       cpmFlagWrong,
//       cpmCheck,
//       cpmFlagCount
//     };
//   }

//   it('FIRST TIME should random correct ratio', mochaAsync(async () => {
    
//     const testResult = test(999, false);

//     describe(`cpm wrong: ${testResult.cpmFlagWrong}`, () => {
//       it('FIRST checking', () => {
//         expect(testResult.cpmFlagWrong === 0).to.equal(true);
//       });
//     });
//     describe(`cpm happen: ${testResult.cpmFlagCount}`, () => {
//       it('FIRST checking', () => {
//         expect(true).to.equal(true);
//       });
//     })

//     for (let index = 0; index < testResult.shareStatistical.length; index++) {
//       const detail = testResult.shareStatistical[index];
//       describe(`share ${detail.id}: ${detail.percent}(${detail.count})`, () => {
//         it('FIRST TIME checking', () => {
//           expect(true).to.equal(true);
//         });
//       })
//     }

//     for (let index = 0; index < testResult.bannerStatistical.length; index++) {
//       const detail = testResult.bannerStatistical[index];
//       describe(`banner ${detail.id}: ${detail.percent}(${detail.count})`, () => {
//         it('FIRST TIME checking', () => {
//           expect(true).to.equal(true);
//         });
//       })
//     }

//     expect(true).to.equal(true);
//   }));

//   // it('SOV should random correct ratio', mochaAsync(async () => {
    
//   //   const testResult = test(666, true);

//   //   describe(`cpm wrong: ${testResult.cpmFlagWrong}`, () => {
//   //     it('SOV checking', () => {
//   //       expect(testResult.cpmFlagWrong === 0).to.equal(true);
//   //     });
//   //   })

//   //   for (let index = 0; index < testResult.shareStatistical.length; index++) {
//   //     const detail = testResult.shareStatistical[index];
//   //     describe(`share ${detail.id}: ${detail.percent}(${detail.count})`, () => {
//   //       it('SOV checking', () => {
//   //         expect(true).to.equal(true);
//   //       });
//   //     })
//   //   }

//   //   for (let index = 0; index < testResult.bannerStatistical.length; index++) {
//   //     const detail = testResult.bannerStatistical[index];
//   //     describe(`banner ${detail.id}: ${detail.percent}(${detail.count})`, () => {
//   //       it('SOV checking', () => {
//   //         expect(true).to.equal(true);
//   //       });
//   //     })
//   //   }

//   //   expect(true).to.equal(true);
//   // }));

// });
