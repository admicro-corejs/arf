import * as casperjs from 'casper';

const casper = new casperjs.Casper();

casper.test.begin('Test demo', 5, function suite(test) {
    casper.start("http://localhost:8080/", function() {
        this.echo('Access test');
    });

    casper.wait(2000, function() {
        this.echo("I've waited for a second.");
        this.capture('demo.jpg', undefined, {
            format: 'jpg',
            quality: 100,
        });
    });

    casper.waitForResource('Arf.js', function() {
        this.echo('# core has been loaded.');
    });



    casper.waitForResource('arf-jmg15j90.min.js', function() {
        this.echo('# code zone has been loaded.');
    });

    casper.then(function() {
        test.assertExists('#zone-jmg15j90', 'zone rendered');
    });

    casper.run(function() {
        test.done();
    });
});