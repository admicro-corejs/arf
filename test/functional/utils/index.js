import fs from 'fs';
import { exec } from 'child_process';
import { StringDecoder } from 'string_decoder';
import chalk from 'chalk';
import logSymbols from 'log-symbols';

const decoder = new StringDecoder('utf8');
const specsPath = 'test/functional/specs';
const tempPath = 'test/functional/temp';

export function getSpecsPath() {
    return specsPath;
};

export function getTempPath() {
    return tempPath;
}

export function printLogsTest(process) {
    process.stdout.on('data', (data) => {
    //   const log = JSON.parse(decoder.end(Buffer.from(data)));
    //   if (log.result) {
    //     console.log(logSymbols.success, chalk.green(log.msg));
    //   } else {
    //     console.log(logSymbols.error, chalk.redBright(log.msg));
    //   }
        const msg =  decoder.end(Buffer.from(data));
        const isPass = msg.indexOf('PASS') === 0;
        const isFail = msg.indexOf('FAIL') === 0;
        if (isPass) {
            console.log(logSymbols.success, chalk.green(msg));
        } else if (isFail) {
            console.log(logSymbols.error, chalk.redBright(msg));
        } else {
            console.log(logSymbols.info, chalk.yellowBright(msg));
        }
    });
  
    process.stderr.on('data', (data) => {
      console.log(chalk.red(decoder.end(Buffer.from(data))));
    });
  }
  
  export async function readLogs(process) {
    return await new Promise((resolve) => {
      process.stdout.on('data', (data) => {
        resolve(decoder.end(Buffer.from(data)));
      });
  
      process.stderr.on('data', (data) => {
        resolve('');
        console.log(decoder.end(Buffer.from(data)));
      });
    });
  }
  
  export function readDir(path) {
    return fs.readdirSync(path);
  }
  
  export function getName(fullFileName) {
    return fullFileName.split('.')[0];
  }
  
  export async function compiler(filePath) {
    const compilerContent = exec(`npx babel ${filePath}`);
    const content = await readLogs(compilerContent);
    return content;
  }
  
  export async function testing(filePath, name) {
    const tempName = `${name}.temp.spec.js`;
    const tempFilePath = `${getTempPath()}/${tempName}`;
    const content = await compiler(filePath);
  
    fs.writeFile(tempFilePath, content, (err) => {
      if (err) throw err;
      exec('yarn dev');
      const pc = exec(`node ./node_modules/casperjs/bin/casperjs.js test ${tempFilePath} --xunit=test/functional/logs/${name}.${Date.now()}.xml`);
      printLogsTest(pc);
    });
  }