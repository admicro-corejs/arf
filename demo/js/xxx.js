!function(e, t) {
    "object" == typeof exports && "object" == typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define("Template", [], t) : "object" == typeof exports ? exports.Template = t() : e.Template = t()
}("undefined" != typeof self ? self : this, function() {
    return function(e) {
        function t(o) {
            if (n[o])
                return n[o].exports;
            var r = n[o] = {
                i: o,
                l: !1,
                exports: {}
            };
            return e[o].call(r.exports, r, r.exports, t), r.l = !0, r.exports
        }
        var n = {};
        return t.m = e, t.c = n, t.d = function(e, n, o) {
            t.o(e, n) || Object.defineProperty(e, n, {
                configurable: !1,
                enumerable: !0,
                get: o
            })
        }, t.n = function(e) {
            var n = e && e.__esModule ? function() {
                return e.default
            } : function() {
                return e
            };
            return t.d(n, "a", n), n
        }, t.o = function(e, t) {
            return Object.prototype.hasOwnProperty.call(e, t)
        }, t.p = "/", t(t.s = 0)
    }([function(e, t, n) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var o = n(1),
            r = {};
        location.search.replace(new RegExp("([^?=&]+)(=([^&]*))?", "g"), function(e, t, n, o) {
            r[t] = o
        });
        var a = void 0;
        try {
            a = "dev" === o.a("4bCG4bCH4bCU", "377fvt+6377fut++", r.corejs_env_key) ? "" : ".min"
        } catch (e) {
            a = ".min"
        }
        var c = document.createElement("script");
        c.id = "arf-core-js", c.type = "application/javascript", c.src = "//media1.admicro.vn/cms/Arf" + a + ".js", document.getElementById(c.id) || document.getElementsByTagName("body")[0].appendChild(c), window.arfZonesQueue = window.arfZonesQueue || [], window.arfZonesQueue.push({
            el: document.getElementById("7249"),
            propsData: {
                model: {
                    "id": "7249",
                    "name": "m.cafef.vn - Top",
                    "height": 0,
                    "width": 0,
                    "css": "",
                    "outputCss": "",
                    "groupSSP": "",
                    "isResponsive": true,
                    "isMobile": true,
                    "isTemplate": true,
                    "template": "<zone id=\"k6it4c9o\"></zone>\n<script src=\"//media1.admicro.vn/cms/arf-k6it4c9o.min.js\"></script>\n%%ARF_CONTENT%%",
                    "totalShare": 5,
                    "status": "active",
                    "isPageLoad": false,
                    "isZoneVideo": false,
                    "siteId": "222",
                    "shares": [{
                        "id": "jo83hf40",
                        "css": "",
                        "outputCss": "",
                        "width": 0,
                        "height": 0,
                        "classes": "",
                        "isRotate": false,
                        "rotateTime": 0,
                        "type": "multiple",
                        "format": "1,1",
                        "zoneId": "7249",
                        "isTemplate": false,
                        "template": "",
                        "offset": null,
                        "isAdPod": false,
                        "duration": null,
                        "sharePlacements": [{
                            "id": "jods5we4",
                            "positionOnShare": 2,
                            "placementId": "512293",
                            "shareId": "jo83hf40",
                            "time": "[{\"startTime\":\"1989-12-31T17:00:00.000Z\",\"endTime\":null}]",
                            "placement": {
                                "id": "512293",
                                "width": 0,
                                "height": 0,
                                "startTime": "1989-12-31T17:00:00.000Z",
                                "endTime": null,
                                "weight": 1,
                                "revenueType": "cpm",
                                "cpdPercent": 0,
                                "isRotate": false,
                                "rotateTime": 0,
                                "relative": 0,
                                "campaignId": "1069796",
                                "campaign": {
                                    "id": "1069796",
                                    "startTime": "1989-12-31T17:00:00.000Z",
                                    "endTime": null,
                                    "weight": 0,
                                    "revenueType": "cpm",
                                    "pageLoad": 0,
                                    "totalCPM": 0,
                                    "isRunMonopoly": false,
                                    "optionMonopoly": null,
                                    "isRunBudget": false
                                },
                                "banners": [{
                                    "id": "512293",
                                    "html": "<div id=\"sspbid_110191\"></div> <script type=\"text/javascript\">admsspreg({sspid:110191,w:640,h:320,zcp:7249})</script>",
                                    "width": 0,
                                    "height": 0,
                                    "keyword": "",
                                    "isMacro": false,
                                    "weight": 1,
                                    "placementId": "512293",
                                    "imageUrl": "",
                                    "url": "",
                                    "target": "_blank",
                                    "isIFrame": false,
                                    "isDefault": false,
                                    "isRelative": false,
                                    "vastTagsUrl": null,
                                    "thirdPartyTracking": null,
                                    "thirdPartyUrl": null,
                                    "isDocumentWrite": true,
                                    "activationDate": "1989-12-31T17:00:00.000Z",
                                    "expirationDate": null,
                                    "optionBanners": [{
                                        "id": "jods6i1p",
                                        "logical": "and",
                                        "bannerId": "512293",
                                        "comparison": "==",
                                        "value": "",
                                        "type": "channel",
                                        "optionBannerChannels": [{
                                            "id": "jods6kay",
                                            "optionBannerId": "jods6i1p",
                                            "channelId": "420",
                                            "channel": {
                                                "id": "420",
                                                "name": "m.cafef.vn - Home",
                                                "siteId": "222",
                                                "optionChannels": [{
                                                    "id": "jo83hrr8",
                                                    "name": "Site Page-URL",
                                                    "logical": "or",
                                                    "comparison": "==",
                                                    "value": "http://m.cafef.vn/",
                                                    "globalVariables": "",
                                                    "channelId": "420",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                        "name": "Site Page-URL",
                                                        "isInputLink": true,
                                                        "isSelectOption": false,
                                                        "isVariable": false,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jo83hrra",
                                                    "name": "Site Page-URL",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "m.cafef.vn/home.chn",
                                                    "globalVariables": "",
                                                    "channelId": "420",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                        "name": "Site Page-URL",
                                                        "isInputLink": true,
                                                        "isSelectOption": false,
                                                        "isVariable": false,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jptlzssz",
                                                    "name": "home",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/home/",
                                                    "globalVariables": "_ADM_Channel",
                                                    "channelId": "420",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                        "name": "Variable",
                                                        "isInputLink": false,
                                                        "isSelectOption": false,
                                                        "isVariable": true,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }]
                                            }
                                        }]
                                    }],
                                    "bannerType": {
                                        "name": "Script",
                                        "value": "script",
                                        "isUpload": false,
                                        "isVideo": false
                                    },
                                    "bannerHtmlType": {
                                        "name": "PR Tracking",
                                        "value": "pr-tracking",
                                        "weight": 99
                                    }
                                }, {
                                    "id": "512295",
                                    "html": "<div id=\"sspbid_110193\"></div><script type=\"text/javascript\">admsspreg({sspid:110193,w:640,h:320,zcp:\"7249\"});</script>",
                                    "width": 640,
                                    "height": 320,
                                    "keyword": "",
                                    "isMacro": false,
                                    "weight": 1,
                                    "placementId": "512293",
                                    "imageUrl": "",
                                    "url": "",
                                    "target": "_blank",
                                    "isIFrame": false,
                                    "isDefault": false,
                                    "isRelative": false,
                                    "vastTagsUrl": null,
                                    "thirdPartyTracking": null,
                                    "thirdPartyUrl": null,
                                    "isDocumentWrite": true,
                                    "activationDate": "1989-12-31T17:00:00.000Z",
                                    "expirationDate": null,
                                    "optionBanners": [{
                                        "id": "jods84tv",
                                        "logical": "and",
                                        "bannerId": "512295",
                                        "comparison": "==",
                                        "value": "",
                                        "type": "channel",
                                        "optionBannerChannels": [{
                                            "id": "jods86ys",
                                            "optionBannerId": "jods84tv",
                                            "channelId": "824",
                                            "channel": {
                                                "id": "824",
                                                "name": "m.cafef.vn - Doanh nghiep",
                                                "siteId": "222",
                                                "optionChannels": [{
                                                    "id": "jo83hrtl",
                                                    "name": "Site Page-URL",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/doanh-nghiep",
                                                    "globalVariables": "",
                                                    "channelId": "824",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                        "name": "Site Page-URL",
                                                        "isInputLink": true,
                                                        "isSelectOption": false,
                                                        "isVariable": false,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jpum0z47",
                                                    "name": "doanh-nghiep",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/doanh-nghiep/",
                                                    "globalVariables": "_ADM_Channel",
                                                    "channelId": "824",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                        "name": "Variable",
                                                        "isInputLink": false,
                                                        "isSelectOption": false,
                                                        "isVariable": true,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }]
                                            }
                                        }]
                                    }],
                                    "bannerType": {
                                        "name": "Script",
                                        "value": "script",
                                        "isUpload": false,
                                        "isVideo": false
                                    },
                                    "bannerHtmlType": {
                                        "name": "PR Tracking",
                                        "value": "pr-tracking",
                                        "weight": 99
                                    }
                                }, {
                                    "id": "512298",
                                    "html": "<div id=\"sspbid_110196\"></div><script type=\"text/javascript\">admsspreg({sspid:110196,w:640,h:320,zcp:\"7249\"});</script>",
                                    "width": 640,
                                    "height": 320,
                                    "keyword": "",
                                    "isMacro": false,
                                    "weight": 1,
                                    "placementId": "512293",
                                    "imageUrl": "",
                                    "url": "",
                                    "target": "_blank",
                                    "isIFrame": false,
                                    "isDefault": false,
                                    "isRelative": false,
                                    "vastTagsUrl": null,
                                    "thirdPartyTracking": null,
                                    "thirdPartyUrl": null,
                                    "isDocumentWrite": true,
                                    "activationDate": "1989-12-31T17:00:00.000Z",
                                    "expirationDate": null,
                                    "optionBanners": [{
                                        "id": "joe06q36",
                                        "logical": "and",
                                        "bannerId": "512298",
                                        "comparison": "==",
                                        "value": "",
                                        "type": "channel",
                                        "optionBannerChannels": [{
                                            "id": "joe06shz",
                                            "optionBannerId": "joe06q36",
                                            "channelId": "823",
                                            "channel": {
                                                "id": "823",
                                                "name": "m.cafef.vn - Bat dong san",
                                                "siteId": "222",
                                                "optionChannels": [{
                                                    "id": "jo83hrtb",
                                                    "name": "Site Page-URL",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/bat-dong-san",
                                                    "globalVariables": "",
                                                    "channelId": "823",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                        "name": "Site Page-URL",
                                                        "isInputLink": true,
                                                        "isSelectOption": false,
                                                        "isVariable": false,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jo83hrtd",
                                                    "name": "Site Page-URL",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/thi-truong-dau-tu/",
                                                    "globalVariables": "",
                                                    "channelId": "823",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                        "name": "Site Page-URL",
                                                        "isInputLink": true,
                                                        "isSelectOption": false,
                                                        "isVariable": false,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jo83hrtf",
                                                    "name": "Site Page-URL",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/chinh-sach-quy-hoach/",
                                                    "globalVariables": "",
                                                    "channelId": "823",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                        "name": "Site Page-URL",
                                                        "isInputLink": true,
                                                        "isSelectOption": false,
                                                        "isVariable": false,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jo83hrth",
                                                    "name": "Site Page-URL",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/phong-cach/",
                                                    "globalVariables": "",
                                                    "channelId": "823",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                        "name": "Site Page-URL",
                                                        "isInputLink": true,
                                                        "isSelectOption": false,
                                                        "isVariable": false,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jo83hrtj",
                                                    "name": "Site Page-URL",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/tin-tuc-du-an/",
                                                    "globalVariables": "",
                                                    "channelId": "823",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                        "name": "Site Page-URL",
                                                        "isInputLink": true,
                                                        "isSelectOption": false,
                                                        "isVariable": false,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jpum2fcw",
                                                    "name": "/bat-dong-san",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/bat-dong-san/",
                                                    "globalVariables": "_ADM_Channel",
                                                    "channelId": "823",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                        "name": "Variable",
                                                        "isInputLink": false,
                                                        "isSelectOption": false,
                                                        "isVariable": true,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jpumyuo7",
                                                    "name": "thi-truong-dau-tu",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/thi-truong-dau-tu/",
                                                    "globalVariables": "_ADM_Channel",
                                                    "channelId": "823",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                        "name": "Variable",
                                                        "isInputLink": false,
                                                        "isSelectOption": false,
                                                        "isVariable": true,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jpumzb9z",
                                                    "name": "chinh-sach-quy-hoach",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/chinh-sach-quy-hoach/",
                                                    "globalVariables": "_ADM_Channel",
                                                    "channelId": "823",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                        "name": "Variable",
                                                        "isInputLink": false,
                                                        "isSelectOption": false,
                                                        "isVariable": true,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jpumzsrb",
                                                    "name": "/phong-cach/",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/phong-cach/",
                                                    "globalVariables": "_ADM_Channel",
                                                    "channelId": "823",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                        "name": "Variable",
                                                        "isInputLink": false,
                                                        "isSelectOption": false,
                                                        "isVariable": true,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jpun0b10",
                                                    "name": "tin-tuc-du-an",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/tin-tuc-du-an/",
                                                    "globalVariables": "_ADM_Channel",
                                                    "channelId": "823",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                        "name": "Variable",
                                                        "isInputLink": false,
                                                        "isSelectOption": false,
                                                        "isVariable": true,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }]
                                            }
                                        }]
                                    }],
                                    "bannerType": {
                                        "name": "Script",
                                        "value": "script",
                                        "isUpload": false,
                                        "isVideo": false
                                    },
                                    "bannerHtmlType": {
                                        "name": "PR Tracking",
                                        "value": "pr-tracking",
                                        "weight": 99
                                    }
                                }, {
                                    "id": "512301",
                                    "html": "<div id=\"sspbid_110199\"></div><script type=\"text/javascript\">admsspreg({sspid:110199,w:640,h:320,zcp:\"7249\"});</script>",
                                    "width": 640,
                                    "height": 320,
                                    "keyword": "",
                                    "isMacro": false,
                                    "weight": 1,
                                    "placementId": "512293",
                                    "imageUrl": "",
                                    "url": "",
                                    "target": "_blank",
                                    "isIFrame": false,
                                    "isDefault": false,
                                    "isRelative": false,
                                    "vastTagsUrl": null,
                                    "thirdPartyTracking": null,
                                    "thirdPartyUrl": null,
                                    "isDocumentWrite": true,
                                    "activationDate": "1989-12-31T17:00:00.000Z",
                                    "expirationDate": null,
                                    "optionBanners": [{
                                        "id": "joe08w0v",
                                        "logical": "and",
                                        "bannerId": "512301",
                                        "comparison": "==",
                                        "value": "",
                                        "type": "channel",
                                        "optionBannerChannels": [{
                                            "id": "joe08y45",
                                            "optionBannerId": "joe08w0v",
                                            "channelId": "822",
                                            "channel": {
                                                "id": "822",
                                                "name": "m.cafef.vn - Thi truong chung khoan",
                                                "siteId": "222",
                                                "optionChannels": [{
                                                    "id": "jo83hrt9",
                                                    "name": "Site Page-URL",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/thi-truong-chung-khoan",
                                                    "globalVariables": "",
                                                    "channelId": "822",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                        "name": "Site Page-URL",
                                                        "isInputLink": true,
                                                        "isSelectOption": false,
                                                        "isVariable": false,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jpum06ej",
                                                    "name": "thi-truong-chung-khoan",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/thi-truong-chung-khoan/",
                                                    "globalVariables": "_ADM_Channel",
                                                    "channelId": "822",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                        "name": "Variable",
                                                        "isInputLink": false,
                                                        "isSelectOption": false,
                                                        "isVariable": true,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }]
                                            }
                                        }]
                                    }],
                                    "bannerType": {
                                        "name": "Script",
                                        "value": "script",
                                        "isUpload": false,
                                        "isVideo": false
                                    },
                                    "bannerHtmlType": {
                                        "name": "PR Tracking",
                                        "value": "pr-tracking",
                                        "weight": 99
                                    }
                                }, {
                                    "id": "512304",
                                    "html": "<div id=\"sspbid_110202\"></div><script type=\"text/javascript\">admsspreg({sspid:110202,w:640,h:320,zcp:\"7249\"});</script>",
                                    "width": 640,
                                    "height": 320,
                                    "keyword": "",
                                    "isMacro": false,
                                    "weight": 1,
                                    "placementId": "512293",
                                    "imageUrl": "",
                                    "url": "",
                                    "target": "_blank",
                                    "isIFrame": false,
                                    "isDefault": false,
                                    "isRelative": false,
                                    "vastTagsUrl": null,
                                    "thirdPartyTracking": null,
                                    "thirdPartyUrl": null,
                                    "isDocumentWrite": true,
                                    "activationDate": "1989-12-31T17:00:00.000Z",
                                    "expirationDate": null,
                                    "optionBanners": [{
                                        "id": "joe0aemh",
                                        "logical": "and",
                                        "bannerId": "512304",
                                        "comparison": "==",
                                        "value": "",
                                        "type": "channel",
                                        "optionBannerChannels": [{
                                            "id": "joe0agba",
                                            "optionBannerId": "joe0aemh",
                                            "channelId": "1200",
                                            "channel": {
                                                "id": "1200",
                                                "name": "m.cafef.vn - Thời sự",
                                                "siteId": "222",
                                                "optionChannels": [{
                                                    "id": "jo83hrtz",
                                                    "name": "Site Page-URL",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/thoi-su",
                                                    "globalVariables": "",
                                                    "channelId": "1200",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                        "name": "Site Page-URL",
                                                        "isInputLink": true,
                                                        "isSelectOption": false,
                                                        "isVariable": false,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jpth2et3",
                                                    "name": "thoi-su",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/thoi-su/",
                                                    "globalVariables": "_ADM_Channel",
                                                    "channelId": "1200",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                        "name": "Variable",
                                                        "isInputLink": false,
                                                        "isSelectOption": false,
                                                        "isVariable": true,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }]
                                            }
                                        }]
                                    }],
                                    "bannerType": {
                                        "name": "Script",
                                        "value": "script",
                                        "isUpload": false,
                                        "isVideo": false
                                    },
                                    "bannerHtmlType": {
                                        "name": "PR Tracking",
                                        "value": "pr-tracking",
                                        "weight": 99
                                    }
                                }, {
                                    "id": "512307",
                                    "html": "<div id=\"sspbid_110205\"></div><script type=\"text/javascript\">admsspreg({sspid:110205,w:640,h:320,zcp:\"7249\"});</script>",
                                    "width": 640,
                                    "height": 320,
                                    "keyword": "",
                                    "isMacro": false,
                                    "weight": 1,
                                    "placementId": "512293",
                                    "imageUrl": "",
                                    "url": "",
                                    "target": "_blank",
                                    "isIFrame": false,
                                    "isDefault": false,
                                    "isRelative": false,
                                    "vastTagsUrl": null,
                                    "thirdPartyTracking": null,
                                    "thirdPartyUrl": null,
                                    "isDocumentWrite": true,
                                    "activationDate": "1989-12-31T17:00:00.000Z",
                                    "expirationDate": null,
                                    "optionBanners": [{
                                        "id": "joe0c1pr",
                                        "logical": "and",
                                        "bannerId": "512307",
                                        "comparison": "==",
                                        "value": "",
                                        "type": "channel",
                                        "optionBannerChannels": [{
                                            "id": "joe0c3nf",
                                            "optionBannerId": "joe0c1pr",
                                            "channelId": "825",
                                            "channel": {
                                                "id": "825",
                                                "name": "m.cafef.vn - Tai chinh ngan hang",
                                                "siteId": "222",
                                                "optionChannels": [{
                                                    "id": "jo83hrtn",
                                                    "name": "Site Page-URL",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/tai-chinh-ngan-hang",
                                                    "globalVariables": "",
                                                    "channelId": "825",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                        "name": "Site Page-URL",
                                                        "isInputLink": true,
                                                        "isSelectOption": false,
                                                        "isVariable": false,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jpun0zws",
                                                    "name": "/tai-chinh-ngan-hang",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/tai-chinh-ngan-hang/",
                                                    "globalVariables": "_ADM_Channel",
                                                    "channelId": "825",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                        "name": "Variable",
                                                        "isInputLink": false,
                                                        "isSelectOption": false,
                                                        "isVariable": true,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }]
                                            }
                                        }]
                                    }],
                                    "bannerType": {
                                        "name": "Script",
                                        "value": "script",
                                        "isUpload": false,
                                        "isVideo": false
                                    },
                                    "bannerHtmlType": {
                                        "name": "PR Tracking",
                                        "value": "pr-tracking",
                                        "weight": 99
                                    }
                                }, {
                                    "id": "512310",
                                    "html": "<div id=\"sspbid_110208\"></div><script type=\"text/javascript\">admsspreg({sspid:110208,w:640,h:320,zcp:\"7249\"});</script>",
                                    "width": 640,
                                    "height": 320,
                                    "keyword": "",
                                    "isMacro": false,
                                    "weight": 1,
                                    "placementId": "512293",
                                    "imageUrl": "",
                                    "url": "",
                                    "target": "_blank",
                                    "isIFrame": false,
                                    "isDefault": false,
                                    "isRelative": false,
                                    "vastTagsUrl": null,
                                    "thirdPartyTracking": null,
                                    "thirdPartyUrl": null,
                                    "isDocumentWrite": true,
                                    "activationDate": "1989-12-31T17:00:00.000Z",
                                    "expirationDate": null,
                                    "optionBanners": [{
                                        "id": "joe0dhqj",
                                        "logical": "and",
                                        "bannerId": "512310",
                                        "comparison": "==",
                                        "value": "",
                                        "type": "channel",
                                        "optionBannerChannels": [{
                                            "id": "joe0djx5",
                                            "optionBannerId": "joe0dhqj",
                                            "channelId": "826",
                                            "channel": {
                                                "id": "826",
                                                "name": "m.cafef.vn - Tai chinh quoc te",
                                                "siteId": "222",
                                                "optionChannels": [{
                                                    "id": "jo83hrtp",
                                                    "name": "Site Page-URL",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/tai-chinh-quoc-te",
                                                    "globalVariables": "",
                                                    "channelId": "826",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                        "name": "Site Page-URL",
                                                        "isInputLink": true,
                                                        "isSelectOption": false,
                                                        "isVariable": false,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jpth7f0a",
                                                    "name": "taichinh-quocte",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/tai-chinh-quoc-te/",
                                                    "globalVariables": "_ADM_Channel",
                                                    "channelId": "826",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                        "name": "Variable",
                                                        "isInputLink": false,
                                                        "isSelectOption": false,
                                                        "isVariable": true,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }]
                                            }
                                        }]
                                    }],
                                    "bannerType": {
                                        "name": "Script",
                                        "value": "script",
                                        "isUpload": false,
                                        "isVideo": false
                                    },
                                    "bannerHtmlType": {
                                        "name": "PR Tracking",
                                        "value": "pr-tracking",
                                        "weight": 99
                                    }
                                }, {
                                    "id": "512313",
                                    "html": "<div id=\"sspbid_110211\"></div><script type=\"text/javascript\">admsspreg({sspid:110211,w:640,h:320,zcp:\"7249\"});</script>",
                                    "width": 640,
                                    "height": 320,
                                    "keyword": "",
                                    "isMacro": false,
                                    "weight": 1,
                                    "placementId": "512293",
                                    "imageUrl": "",
                                    "url": "",
                                    "target": "_blank",
                                    "isIFrame": false,
                                    "isDefault": false,
                                    "isRelative": false,
                                    "vastTagsUrl": null,
                                    "thirdPartyTracking": null,
                                    "thirdPartyUrl": null,
                                    "isDocumentWrite": true,
                                    "activationDate": "1989-12-31T17:00:00.000Z",
                                    "expirationDate": null,
                                    "optionBanners": [{
                                        "id": "joe0evx8",
                                        "logical": "and",
                                        "bannerId": "512313",
                                        "comparison": "==",
                                        "value": "",
                                        "type": "channel",
                                        "optionBannerChannels": [{
                                            "id": "joe0exna",
                                            "optionBannerId": "joe0evx8",
                                            "channelId": "827",
                                            "channel": {
                                                "id": "827",
                                                "name": "m.cafef.vn - Kinh te vi mo",
                                                "siteId": "222",
                                                "optionChannels": [{
                                                    "id": "jo83hrtr",
                                                    "name": "Site Page-URL",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/vi-mo-dau-tu",
                                                    "globalVariables": "",
                                                    "channelId": "827",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                        "name": "Site Page-URL",
                                                        "isInputLink": true,
                                                        "isSelectOption": false,
                                                        "isVariable": false,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jo83hrtt",
                                                    "name": "Site Page-URL",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/kinh-te-vi-mo-dau-tu/",
                                                    "globalVariables": "",
                                                    "channelId": "827",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                        "name": "Site Page-URL",
                                                        "isInputLink": true,
                                                        "isSelectOption": false,
                                                        "isVariable": false,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jpun1wdv",
                                                    "name": "/vi-mo-dau-tu",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/vi-mo-dau-tu/",
                                                    "globalVariables": "_ADM_Channel",
                                                    "channelId": "827",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                        "name": "Variable",
                                                        "isInputLink": false,
                                                        "isSelectOption": false,
                                                        "isVariable": true,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jpun28o1",
                                                    "name": "/kinh-te-vi-mo-dau-tu",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/kinh-te-vi-mo-dau-tu/",
                                                    "globalVariables": "_ADM_Channel",
                                                    "channelId": "827",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                        "name": "Variable",
                                                        "isInputLink": false,
                                                        "isSelectOption": false,
                                                        "isVariable": true,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }]
                                            }
                                        }]
                                    }],
                                    "bannerType": {
                                        "name": "Script",
                                        "value": "script",
                                        "isUpload": false,
                                        "isVideo": false
                                    },
                                    "bannerHtmlType": {
                                        "name": "PR Tracking",
                                        "value": "pr-tracking",
                                        "weight": 99
                                    }
                                }, {
                                    "id": "512316",
                                    "html": "<div id=\"sspbid_110214\"></div><script type=\"text/javascript\">admsspreg({sspid:110214,w:640,h:320,zcp:\"7249\"});</script>",
                                    "width": 640,
                                    "height": 320,
                                    "keyword": "",
                                    "isMacro": false,
                                    "weight": 1,
                                    "placementId": "512293",
                                    "imageUrl": "",
                                    "url": "",
                                    "target": "_blank",
                                    "isIFrame": false,
                                    "isDefault": false,
                                    "isRelative": false,
                                    "vastTagsUrl": null,
                                    "thirdPartyTracking": null,
                                    "thirdPartyUrl": null,
                                    "isDocumentWrite": true,
                                    "activationDate": "1989-12-31T17:00:00.000Z",
                                    "expirationDate": null,
                                    "optionBanners": [{
                                        "id": "joe0ga25",
                                        "logical": "and",
                                        "bannerId": "512316",
                                        "comparison": "==",
                                        "value": "",
                                        "type": "channel",
                                        "optionBannerChannels": [{
                                            "id": "joe0gcbm",
                                            "optionBannerId": "joe0ga25",
                                            "channelId": "1414",
                                            "channel": {
                                                "id": "1414",
                                                "name": "m.cafef.vn-Sống",
                                                "siteId": "222",
                                                "optionChannels": [{
                                                    "id": "jo83hrud",
                                                    "name": "Site Page-URL",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/song",
                                                    "globalVariables": "",
                                                    "channelId": "1414",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                        "name": "Site Page-URL",
                                                        "isInputLink": true,
                                                        "isSelectOption": false,
                                                        "isVariable": false,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jo83hruf",
                                                    "name": "Site Page-URL",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/du-lieu",
                                                    "globalVariables": "",
                                                    "channelId": "1414",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                        "name": "Site Page-URL",
                                                        "isInputLink": true,
                                                        "isSelectOption": false,
                                                        "isVariable": false,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jptks80d",
                                                    "name": "song",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/song/",
                                                    "globalVariables": "_ADM_Channel",
                                                    "channelId": "1414",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                        "name": "Variable",
                                                        "isInputLink": false,
                                                        "isSelectOption": false,
                                                        "isVariable": true,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jptkuuwx",
                                                    "name": "du-lieu",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/du-lieu/",
                                                    "globalVariables": "_ADM_Channel",
                                                    "channelId": "1414",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                        "name": "Variable",
                                                        "isInputLink": false,
                                                        "isSelectOption": false,
                                                        "isVariable": true,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }]
                                            }
                                        }]
                                    }],
                                    "bannerType": {
                                        "name": "Script",
                                        "value": "script",
                                        "isUpload": false,
                                        "isVideo": false
                                    },
                                    "bannerHtmlType": {
                                        "name": "PR Tracking",
                                        "value": "pr-tracking",
                                        "weight": 99
                                    }
                                }, {
                                    "id": "512319",
                                    "html": "<div id=\"sspbid_110217\"></div><script type=\"text/javascript\">admsspreg({sspid:110217,w:640,h:320,zcp:\"7249\"});</script>",
                                    "width": 640,
                                    "height": 320,
                                    "keyword": "",
                                    "isMacro": false,
                                    "weight": 1,
                                    "placementId": "512293",
                                    "imageUrl": "",
                                    "url": "",
                                    "target": "_blank",
                                    "isIFrame": false,
                                    "isDefault": false,
                                    "isRelative": false,
                                    "vastTagsUrl": null,
                                    "thirdPartyTracking": null,
                                    "thirdPartyUrl": null,
                                    "isDocumentWrite": true,
                                    "activationDate": "1989-12-31T17:00:00.000Z",
                                    "expirationDate": null,
                                    "optionBanners": [{
                                        "id": "joe0hnk4",
                                        "logical": "and",
                                        "bannerId": "512319",
                                        "comparison": "==",
                                        "value": "",
                                        "type": "channel",
                                        "optionBannerChannels": [{
                                            "id": "joe0hprv",
                                            "optionBannerId": "joe0hnk4",
                                            "channelId": "828",
                                            "channel": {
                                                "id": "828",
                                                "name": "m.cafef.vn - Hang hoa nguyen lieu",
                                                "siteId": "222",
                                                "optionChannels": [{
                                                    "id": "jo83hrtv",
                                                    "name": "Site Page-URL",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/hang-hoa-nguyen-lieu",
                                                    "globalVariables": "",
                                                    "channelId": "828",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                        "name": "Site Page-URL",
                                                        "isInputLink": true,
                                                        "isSelectOption": false,
                                                        "isVariable": false,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jpun2yoi",
                                                    "name": "/hang-hoa-nguyen-lieu",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/hang-hoa-nguyen-lieu/",
                                                    "globalVariables": "_ADM_Channel",
                                                    "channelId": "828",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                        "name": "Variable",
                                                        "isInputLink": false,
                                                        "isSelectOption": false,
                                                        "isVariable": true,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jzur3h5s",
                                                    "name": "thi-truong",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/thi-truong/",
                                                    "globalVariables": "_ADM_Channel",
                                                    "channelId": "828",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                        "name": "Variable",
                                                        "isInputLink": false,
                                                        "isSelectOption": false,
                                                        "isVariable": true,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }]
                                            }
                                        }, {
                                            "id": "joe0i644",
                                            "optionBannerId": "joe0hnk4",
                                            "channelId": "1199",
                                            "channel": {
                                                "id": "1199",
                                                "name": "m.cafef.vn - Tin mới",
                                                "siteId": "222",
                                                "optionChannels": [{
                                                    "id": "jo83hrtx",
                                                    "name": "Site Page-URL",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/tin-moi",
                                                    "globalVariables": "",
                                                    "channelId": "1199",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                        "name": "Site Page-URL",
                                                        "isInputLink": true,
                                                        "isSelectOption": false,
                                                        "isVariable": false,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jpunuwle",
                                                    "name": "/tin-moi",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/tin-moi/",
                                                    "globalVariables": "_ADM_Channel",
                                                    "channelId": "1199",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                        "name": "Variable",
                                                        "isInputLink": false,
                                                        "isSelectOption": false,
                                                        "isVariable": true,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }]
                                            }
                                        }]
                                    }],
                                    "bannerType": {
                                        "name": "Script",
                                        "value": "script",
                                        "isUpload": false,
                                        "isVideo": false
                                    },
                                    "bannerHtmlType": {
                                        "name": "PR Tracking",
                                        "value": "pr-tracking",
                                        "weight": 99
                                    }
                                }, {
                                    "id": "512322",
                                    "html": "<div id=\"sspbid_110220\"></div><script type=\"text/javascript\">admsspreg({sspid:110220,w:640,h:320,zcp:\"7249\"});</script>",
                                    "width": 640,
                                    "height": 320,
                                    "keyword": "",
                                    "isMacro": false,
                                    "weight": 1,
                                    "placementId": "512293",
                                    "imageUrl": "",
                                    "url": "",
                                    "target": "_blank",
                                    "isIFrame": false,
                                    "isDefault": false,
                                    "isRelative": false,
                                    "vastTagsUrl": null,
                                    "thirdPartyTracking": null,
                                    "thirdPartyUrl": null,
                                    "isDocumentWrite": true,
                                    "activationDate": "1989-12-31T17:00:00.000Z",
                                    "expirationDate": null,
                                    "optionBanners": [{
                                        "id": "joe0jme9",
                                        "logical": "and",
                                        "bannerId": "512322",
                                        "comparison": "==",
                                        "value": "",
                                        "type": "channel",
                                        "optionBannerChannels": [{
                                            "id": "joe0job1",
                                            "optionBannerId": "joe0jme9",
                                            "channelId": "1415",
                                            "channel": {
                                                "id": "1415",
                                                "name": " m.cafef.vn-Video",
                                                "siteId": "222",
                                                "optionChannels": [{
                                                    "id": "jo83hruh",
                                                    "name": "Site Page-URL",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/video",
                                                    "globalVariables": "",
                                                    "channelId": "1415",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                        "name": "Site Page-URL",
                                                        "isInputLink": true,
                                                        "isSelectOption": false,
                                                        "isVariable": false,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jo83hruj",
                                                    "name": "Site Page-URL",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/watch-list",
                                                    "globalVariables": "",
                                                    "channelId": "1415",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                        "name": "Site Page-URL",
                                                        "isInputLink": true,
                                                        "isSelectOption": false,
                                                        "isVariable": false,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jptl9ry3",
                                                    "name": "watch-list",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/watch-list/",
                                                    "globalVariables": "_ADM_Channel",
                                                    "channelId": "1415",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                        "name": "Variable",
                                                        "isInputLink": false,
                                                        "isSelectOption": false,
                                                        "isVariable": true,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jptl9rye",
                                                    "name": "video",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/video/",
                                                    "globalVariables": "_ADM_Channel",
                                                    "channelId": "1415",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                        "name": "Variable",
                                                        "isInputLink": false,
                                                        "isSelectOption": false,
                                                        "isVariable": true,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }]
                                            }
                                        }]
                                    }],
                                    "bannerType": {
                                        "name": "Script",
                                        "value": "script",
                                        "isUpload": false,
                                        "isVideo": false
                                    },
                                    "bannerHtmlType": {
                                        "name": "PR Tracking",
                                        "value": "pr-tracking",
                                        "weight": 99
                                    }
                                }]
                            }
                        }, {
                            "id": "jtawu0nm",
                            "positionOnShare": 2,
                            "placementId": "jtawtc0o",
                            "shareId": "jo83hf40",
                            "time": "[{\"startTime\":\"2019-03-15T17:00:00.000Z\",\"endTime\":null}]",
                            "placement": {
                                "id": "jtawtc0o",
                                "width": 320,
                                "height": 100,
                                "startTime": "2019-03-15T17:00:00.000Z",
                                "endTime": null,
                                "weight": 1,
                                "revenueType": "pr",
                                "cpdPercent": 0,
                                "isRotate": false,
                                "rotateTime": 20,
                                "relative": 0,
                                "campaignId": "1292082",
                                "campaign": {
                                    "id": "1292082",
                                    "startTime": "1989-12-31T17:00:00.000Z",
                                    "endTime": null,
                                    "weight": 0,
                                    "revenueType": "cpd",
                                    "pageLoad": 0,
                                    "totalCPM": 0,
                                    "isRunMonopoly": false,
                                    "optionMonopoly": null,
                                    "isRunBudget": false
                                },
                                "banners": [{
                                    "id": "jtawtd85",
                                    "html": "<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script>\n\n<ins class=\"adsbygoogle\"\n    style=\"display:block;margin:0 auto;width:320px;height:100px\"\n    data-ad-client=\"ca-pub-6366951472589375\"\n    data-ad-slot=\"6558860802\"></ins>\n<script>\n(adsbygoogle = window.adsbygoogle || []).push({});\n</script>",
                                    "width": 320,
                                    "height": 100,
                                    "keyword": "",
                                    "isMacro": false,
                                    "weight": 1,
                                    "placementId": "jtawtc0o",
                                    "imageUrl": "",
                                    "url": "",
                                    "target": "",
                                    "isIFrame": false,
                                    "isDefault": false,
                                    "isRelative": false,
                                    "vastTagsUrl": "",
                                    "thirdPartyTracking": "",
                                    "thirdPartyUrl": "",
                                    "isDocumentWrite": true,
                                    "activationDate": "2019-03-15T17:00:00.000Z",
                                    "expirationDate": null,
                                    "optionBanners": [{
                                        "id": "jtawubqc",
                                        "logical": "and",
                                        "bannerId": "jtawtd85",
                                        "comparison": "==",
                                        "value": "",
                                        "type": "channel",
                                        "optionBannerChannels": [{
                                            "id": "jtawufao",
                                            "optionBannerId": "jtawubqc",
                                            "channelId": "jofg795l",
                                            "channel": {
                                                "id": "jofg795l",
                                                "name": "Nuoc_Ngoai",
                                                "siteId": "222",
                                                "optionChannels": [{
                                                    "id": "jofg7e12",
                                                    "name": "Location",
                                                    "logical": "and",
                                                    "comparison": "==",
                                                    "value": "101",
                                                    "globalVariables": "__RC",
                                                    "channelId": "jofg795l",
                                                    "valueSelect": "__RC>=101",
                                                    "logicalSelect": ">=",
                                                    "optionChannelType": {
                                                        "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
                                                        "name": "Location",
                                                        "isInputLink": false,
                                                        "isSelectOption": false,
                                                        "isVariable": false,
                                                        "status": "active",
                                                        "isMultiSelect": true,
                                                        "isGlobal": true,
                                                        "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]"
                                                    }
                                                }]
                                            }
                                        }]
                                    }, {
                                        "id": "jtawuc76",
                                        "logical": "and",
                                        "bannerId": "jtawtd85",
                                        "comparison": "==",
                                        "value": "",
                                        "type": "channel",
                                        "optionBannerChannels": [{
                                            "id": "jtawuh05",
                                            "optionBannerId": "jtawuc76",
                                            "channelId": "jt8lo39t",
                                            "channel": {
                                                "id": "jt8lo39t",
                                                "name": "location_undefined",
                                                "siteId": "222",
                                                "optionChannels": [{
                                                    "id": "jt8lobwj",
                                                    "name": "Location",
                                                    "logical": "and",
                                                    "comparison": "!=",
                                                    "value": "undefined",
                                                    "globalVariables": "__RC",
                                                    "channelId": "jt8lo39t",
                                                    "valueSelect": "__RC==undefined",
                                                    "logicalSelect": "==",
                                                    "optionChannelType": {
                                                        "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
                                                        "name": "Location",
                                                        "isInputLink": false,
                                                        "isSelectOption": false,
                                                        "isVariable": false,
                                                        "status": "active",
                                                        "isMultiSelect": true,
                                                        "isGlobal": true,
                                                        "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]"
                                                    }
                                                }]
                                            }
                                        }]
                                    }],
                                    "bannerType": {
                                        "name": "Script",
                                        "value": "script",
                                        "isUpload": false,
                                        "isVideo": false
                                    },
                                    "bannerHtmlType": {
                                        "name": "PR Tracking",
                                        "value": "pr-tracking",
                                        "weight": 99
                                    }
                                }]
                            }
                        }, {
                            "id": "k6zuoduk",
                            "positionOnShare": 2,
                            "placementId": "k6zumnxw",
                            "shareId": "jo83hf40",
                            "time": "[{\"startTime\":\"2020-02-23T17:00:00.000Z\",\"endTime\":\"2020-02-24T03:08:55.300Z\"},{\"startTime\":\"2020-02-24T03:08:55.300Z\",\"endTime\":\"2020-03-08T16:59:59.000Z\"}]",
                            "placement": {
                                "id": "k6zumnxw",
                                "width": 640,
                                "height": 320,
                                "startTime": "2020-02-23T17:00:00.000Z",
                                "endTime": "2020-03-08T16:59:59.000Z",
                                "weight": 1,
                                "revenueType": "cpd",
                                "cpdPercent": 1,
                                "isRotate": false,
                                "rotateTime": 20,
                                "relative": 0,
                                "campaignId": "k6prjdv9",
                                "campaign": {
                                    "id": "k6prjdv9",
                                    "startTime": "2020-02-16T17:00:00.000Z",
                                    "endTime": null,
                                    "weight": 0,
                                    "revenueType": "cpd",
                                    "pageLoad": 0,
                                    "totalCPM": 0,
                                    "isRunMonopoly": false,
                                    "optionMonopoly": "",
                                    "isRunBudget": false
                                },
                                "banners": [{
                                    "id": "k6zumozr",
                                    "html": "<div id=\"admprtop\" style=\"padding: 15px 0;background: #000;\">\n</div>\n<script>\n(function  (){\nwindow.setTimeout(function(){\nvar _wdTop = windowPrototype.wdWidth();\n var html= '<div id=\"TopCms\" style=\"position: relative;width:' + _wdTop + \"px;height:\" + 320 * _wdTop / 640 + 'px;overflow: hidden;background:#000;padding-bottom: 8px;margin: 0 auto;\"><iframe id=\"ultra-top-iframe' + '\"scrolling=\"no\" style=\"width: 100%;height: ' + 320 * _wdTop / 640 + 'px !important;display: block;overflow: hidden;background-position: center;border: none;\" src=\"https://adi.admicro.vn/adt/banners/nam2015/4043/min_html5/lamlethi/2020_02_17/640x320-1/640x320/640x320.html?wd=' + _wdTop + '\"></iframe><a target=\"_blank\" href=\"%%CLICK_URL_UNESC%%\" style=\"position: absolute;top:0;left:0;width: 100%;height:100%;z-index:1;\"></a></div>';\ndocument.getElementById(\"admprtop\").innerHTML = html;\ndocument.getElementById(\"admprtop\").style.width = window.innerWidth+\"px\";\ndocument.getElementById(\"adm-slot-7254\").style.overflow = \"visible\";\nif(window.location.hostname==\"m.dantri.com.vn\"){\ndocument.getElementById(\"admprtop\").style.marginLeft = \"-10px\";\ndocument.getElementById(\"admprtop\").style.marginTop = \"-10px\";\ndocument.getElementById(\"TopCms\").style.paddingBottom = 0;\n}\n},500);\n})();\n</script>",
                                    "width": 640,
                                    "height": 320,
                                    "keyword": "",
                                    "isMacro": false,
                                    "weight": 1,
                                    "placementId": "k6zumnxw",
                                    "imageUrl": "",
                                    "url": "https://www.kbsec.com.vn/vi/san-pham.htm KB",
                                    "target": "",
                                    "isIFrame": false,
                                    "isDefault": false,
                                    "isRelative": false,
                                    "vastTagsUrl": "",
                                    "thirdPartyTracking": "",
                                    "thirdPartyUrl": "",
                                    "isDocumentWrite": true,
                                    "activationDate": "2020-02-23T17:00:00.000Z",
                                    "expirationDate": "2020-03-08T16:59:59.000Z",
                                    "optionBanners": [{
                                        "id": "k6zup3d1",
                                        "logical": "and",
                                        "bannerId": "k6zumozr",
                                        "comparison": "==",
                                        "value": "",
                                        "type": "channel",
                                        "optionBannerChannels": [{
                                            "id": "k6zup4p8",
                                            "optionBannerId": "k6zup3d1",
                                            "channelId": "420",
                                            "channel": {
                                                "id": "420",
                                                "name": "m.cafef.vn - Home",
                                                "siteId": "222",
                                                "optionChannels": [{
                                                    "id": "jo83hrr8",
                                                    "name": "Site Page-URL",
                                                    "logical": "or",
                                                    "comparison": "==",
                                                    "value": "http://m.cafef.vn/",
                                                    "globalVariables": "",
                                                    "channelId": "420",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                        "name": "Site Page-URL",
                                                        "isInputLink": true,
                                                        "isSelectOption": false,
                                                        "isVariable": false,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jo83hrra",
                                                    "name": "Site Page-URL",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "m.cafef.vn/home.chn",
                                                    "globalVariables": "",
                                                    "channelId": "420",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                        "name": "Site Page-URL",
                                                        "isInputLink": true,
                                                        "isSelectOption": false,
                                                        "isVariable": false,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jptlzssz",
                                                    "name": "home",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/home/",
                                                    "globalVariables": "_ADM_Channel",
                                                    "channelId": "420",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                        "name": "Variable",
                                                        "isInputLink": false,
                                                        "isSelectOption": false,
                                                        "isVariable": true,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }]
                                            }
                                        }]
                                    }],
                                    "bannerType": {
                                        "name": "Script",
                                        "value": "script",
                                        "isUpload": false,
                                        "isVideo": false
                                    },
                                    "bannerHtmlType": {
                                        "name": "PR Tracking",
                                        "value": "pr-tracking",
                                        "weight": 99
                                    }
                                }]
                            }
                        }, {
                            "id": "k6zvpmar",
                            "positionOnShare": 1,
                            "placementId": "k6zvnner",
                            "shareId": "jo83hf40",
                            "time": "[{\"startTime\":\"2020-02-23T17:00:00.000Z\",\"endTime\":null}]",
                            "placement": {
                                "id": "k6zvnner",
                                "width": 0,
                                "height": 0,
                                "startTime": "2020-02-23T17:00:00.000Z",
                                "endTime": null,
                                "weight": 1,
                                "revenueType": "cpm",
                                "cpdPercent": 0,
                                "isRotate": false,
                                "rotateTime": 20,
                                "relative": 0,
                                "campaignId": "k6zvnd4p",
                                "campaign": {
                                    "id": "k6zvnd4p",
                                    "startTime": "2020-02-23T17:00:00.000Z",
                                    "endTime": null,
                                    "weight": 0,
                                    "revenueType": "cpm",
                                    "pageLoad": 0,
                                    "totalCPM": 0,
                                    "isRunMonopoly": false,
                                    "optionMonopoly": "",
                                    "isRunBudget": false
                                },
                                "banners": [{
                                    "id": "k6zvnq43",
                                    "html": "<script></script>",
                                    "width": 0,
                                    "height": 0,
                                    "keyword": "",
                                    "isMacro": false,
                                    "weight": 1,
                                    "placementId": "k6zvnner",
                                    "imageUrl": "",
                                    "url": "",
                                    "target": "",
                                    "isIFrame": false,
                                    "isDefault": false,
                                    "isRelative": false,
                                    "vastTagsUrl": "",
                                    "thirdPartyTracking": "",
                                    "thirdPartyUrl": "",
                                    "isDocumentWrite": true,
                                    "activationDate": "2020-02-23T17:00:00.000Z",
                                    "expirationDate": null,
                                    "optionBanners": [{
                                        "id": "k6zvpy9b",
                                        "logical": "and",
                                        "bannerId": "k6zvnq43",
                                        "comparison": "==",
                                        "value": "",
                                        "type": "channel",
                                        "optionBannerChannels": [{
                                            "id": "k6zvpz7g",
                                            "optionBannerId": "k6zvpy9b",
                                            "channelId": "420",
                                            "channel": {
                                                "id": "420",
                                                "name": "m.cafef.vn - Home",
                                                "siteId": "222",
                                                "optionChannels": [{
                                                    "id": "jo83hrr8",
                                                    "name": "Site Page-URL",
                                                    "logical": "or",
                                                    "comparison": "==",
                                                    "value": "http://m.cafef.vn/",
                                                    "globalVariables": "",
                                                    "channelId": "420",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                        "name": "Site Page-URL",
                                                        "isInputLink": true,
                                                        "isSelectOption": false,
                                                        "isVariable": false,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jo83hrra",
                                                    "name": "Site Page-URL",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "m.cafef.vn/home.chn",
                                                    "globalVariables": "",
                                                    "channelId": "420",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                        "name": "Site Page-URL",
                                                        "isInputLink": true,
                                                        "isSelectOption": false,
                                                        "isVariable": false,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }, {
                                                    "id": "jptlzssz",
                                                    "name": "home",
                                                    "logical": "or",
                                                    "comparison": "=~",
                                                    "value": "/home/",
                                                    "globalVariables": "_ADM_Channel",
                                                    "channelId": "420",
                                                    "valueSelect": "",
                                                    "logicalSelect": "",
                                                    "optionChannelType": {
                                                        "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                        "name": "Variable",
                                                        "isInputLink": false,
                                                        "isSelectOption": false,
                                                        "isVariable": true,
                                                        "status": "active",
                                                        "isMultiSelect": false,
                                                        "isGlobal": false,
                                                        "storageType": null
                                                    }
                                                }]
                                            }
                                        }]
                                    }],
                                    "bannerType": {
                                        "name": "Script",
                                        "value": "script",
                                        "isUpload": false,
                                        "isVideo": false
                                    },
                                    "bannerHtmlType": {
                                        "name": "PR Tracking",
                                        "value": "pr-tracking",
                                        "weight": 99
                                    }
                                }]
                            }
                        }]
                    }],
                    "site": {
                        "id": "222",
                        "domain": "http://m.cafef.vn",
                        "pageLoad": {
                            "totalPageload": 3,
                            "campaigns": []
                        },
                        "isRunBannerDefault": false,
                        "isNoBrand": false,
                        "globalFilters": [],
                        "channels": [{
                            "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
                            "name": "Location",
                            "isGlobal": true,
                            "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]"
                        }],
                        "campaignMonopoly": {
                            "campaigns": []
                        }
                    }
                }
            }
        })
    }, function(e, t, n) {
        "use strict";
        function o(e) {
            return decodeURIComponent(atob(e).split("").map(function(e) {
                return "%" + ("00" + e.charCodeAt(0).toString(16)).slice(-2)
            }).join(""))
        }
        function r(e, t) {
            var n = "",
                o = "";
            o = e.toString();
            for (var r = 0; r < o.length; r += 1) {
                var a = o.charCodeAt(r),
                    c = a ^ t;
                n += String.fromCharCode(c)
            }
            return n
        }
        function a(e, t, n) {
            return r(o(e), t && n ? r(o(t), n) : r(o(c), 90))
        }
        t.a = a;
        var c = "a25qbmtjY2g="
    }])
});

