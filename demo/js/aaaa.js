! function (e) {
    var n = {};

    function t(o) {
        if (n[o]) return n[o].exports;
        var r = n[o] = {
            i: o,
            l: !1,
            exports: {}
        };
        return e[o].call(r.exports, r, r.exports, t), r.l = !0, r.exports
    }
    t.m = e, t.c = n, t.d = function (e, n, o) {
        t.o(e, n) || Object.defineProperty(e, n, {
            configurable: !1,
            enumerable: !0,
            get: o
        })
    }, t.r = function (e) {
        Object.defineProperty(e, "__esModule", {
            value: !0
        })
    }, t.n = function (e) {
        var n = e && e.__esModule ? function () {
            return e.default
        } : function () {
            return e
        };
        return t.d(n, "a", n), n
    }, t.o = function (e, n) {
        return Object.prototype.hasOwnProperty.call(e, n)
    }, t.p = "", t(t.s = 1)
}([function (e, n, t) {
    "use strict";
    Object.defineProperty(n, "__esModule", {
        value: !0
    }), n.encode = function (e, n, t) {
        return function (e) {
            return btoa(encodeURIComponent(e).replace(/%([0-9A-F]{2})/g, function (e, n) {
                return String.fromCharCode("0x" + n)
            }))
        }(c(e, n && t ? c(r(n), t) : c(r(o), 90)))
    }, n.decode = function (e, n, t) {
        return c(r(e), n && t ? c(r(n), t) : c(r(o), 90))
    };
    var o = "a25qbmtjY2g=";

    function r(e) {
        return decodeURIComponent(atob(e).split("").map(function (e) {
            return "%" + ("00" + e.charCodeAt(0).toString(16)).slice(-2)
        }).join(""))
    }

    function c(e, n) {
        var t = "",
            o = "";
        o = e.toString();
        for (var r = 0; r < o.length; r += 1) {
            var c = o.charCodeAt(r) ^ n;
            t += String.fromCharCode(c)
        }
        return t
    }
}, function (e, n, t) {
    "use strict";
    var o = function (e) {
        if (e && e.__esModule) return e;
        var n = {};
        if (null != e)
            for (var t in e) Object.prototype.hasOwnProperty.call(e, t) && (n[t] = e[t]);
        return n.default = e, n
    }(t(0));
    var r = {};
    location.search.replace(new RegExp("([^?=&]+)(=([^&]*))?", "g"), function (e, n, t, o) {
        r[n] = o
    });
    var c = void 0;
    try {
        c = "dev" === o.decode("4bCG4bCH4bCU", "377fvt+6377fut++", r.corejs_env_key) ? "" : ".min", document.getElementById("78") || document.getElementById("admzone78") || document.getElementById("adm-slot-78") || document.write('<zone id="78"></zone>')
    } catch (e) {
        c = ".min"
    }
    var d = {
        el: document.getElementById("78") || document.getElementById("admzone78") || document.getElementById("adm-slot-78"),
        propsData: {
            model: {
                "id": "78",
                "name": "Mixing_300x385_Autopro",
                "height": 600,
                "width": 300,
                "css": "",
                "outputCss": "",
                "groupSSP": "",
                "isResponsive": true,
                "isMobile": false,
                "isTemplate": false,
                "template": "",
                "totalShare": 3,
                "isPageLoad": true,
                "isZoneVideo": false,
                "positionOnSite": "0",
                "timeBetweenAds": 0,
                "siteId": "15",
                "shares": [{
                    "id": "jkkne2ae",
                    "css": "> :first-child {\n    padding-bottom: 10px;\n}",
                    "outputCss": "#share-jkkne2ae > :first-child {\n  padding-bottom: 10px;\n}\n",
                    "rotateTime": 0,
                    "width": 300,
                    "height": 600,
                    "classes": "",
                    "isRotate": false,
                    "type": "multiple",
                    "format": "1,1",
                    "zoneId": "78",
                    "isTemplate": false,
                    "template": "",
                    "offset": null,
                    "isAdPod": false,
                    "duration": null,
                    "sharePlacements": [{
                        "id": "jkkwlq9b",
                        "positionOnShare": 1,
                        "placementId": "jkkw6uww",
                        "shareId": "jkkne2ae",
                        "time": "[{\"startTime\":\"1989-12-31T17:00:00.000Z\",\"endTime\":null}]",
                        "placement": {
                            "id": "jkkw6uww",
                            "width": 300,
                            "height": 250,
                            "startTime": "1989-12-31T17:00:00.000Z",
                            "endTime": null,
                            "weight": 1,
                            "revenueType": "cpd",
                            "cpdPercent": 1,
                            "isRotate": false,
                            "rotateTime": 20,
                            "price": 0,
                            "relative": 0,
                            "campaignId": "1042598",
                            "campaign": {
                                "id": "1042598",
                                "startTime": "1989-12-31T17:00:00.000Z",
                                "endTime": null,
                                "views": 0,
                                "viewPerSession": 0,
                                "timeResetViewCount": 0,
                                "weight": 0,
                                "revenueType": "cpd",
                                "pageLoad": 0,
                                "expense": 0,
                                "totalCPM": 0,
                                "isRunBudget": false,
                                "isRunMonopoly": false,
                                "optionMonopoly": null,
                                "settingBudgetCPM": null,
                                "settingBudgetCPC": null,
                                "maxCPMPerDay": 0
                            },
                            "banners": [{
                                "id": "276906",
                                "html": "<script type=\"text/javascript\">\ngoogle_ad_client = \"ca-pub-6366951472589375\";\n\ngoogle_ad_slot = \"1988984442\";\ngoogle_ad_width = 300;\ngoogle_ad_height = 250;\n\n</script>\n<script type=\"text/javascript\"\nsrc=\"//pagead2.googlesyndication.com/pagead/show_ads.js\">\n</script>",
                                "width": 300,
                                "height": 250,
                                "keyword": "",
                                "isMacro": null,
                                "weight": 1,
                                "placementId": "jkkw6uww",
                                "imageUrl": "",
                                "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
                                "url": "",
                                "target": "",
                                "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
                                "isIFrame": true,
                                "isDefault": false,
                                "isRelative": false,
                                "videoType": null,
                                "clickThroughUrl": null,
                                "vastTagsUrl": null,
                                "vastVideoUrl": "",
                                "videoUrl": "",
                                "duration": 0,
                                "thirdPartyTracking": "",
                                "thirdPartyUrl": null,
                                "isDocumentWrite": true,
                                "skipOffset": 0,
                                "isAddLinkSSP": false,
                                "optionBanners": [{
                                    "id": "jknt6v8t",
                                    "logical": "and",
                                    "bannerId": "276906",
                                    "comparison": "==",
                                    "value": "",
                                    "type": "channel",
                                    "optionBannerChannels": [{
                                        "id": "jknt6w57",
                                        "optionBannerId": "jknt6v8t",
                                        "channelId": "jknrzdy1",
                                        "channel": {
                                            "id": "jknrzdy1",
                                            "name": "Location_Nuocngoai",
                                            "siteId": "15",
                                            "optionChannels": [{
                                                "id": "jkns71fj",
                                                "name": "Location",
                                                "logical": "and",
                                                "globalVariables": "__RC",
                                                "comparison": "==",
                                                "value": "101",
                                                "valueSelect": "__RC>=101",
                                                "logicalSelect": ">=",
                                                "createdAt": "2018-08-10T09:20:44.000Z",
                                                "updatedAt": "2019-03-11T07:31:59.000Z",
                                                "deletedAt": null,
                                                "channelId": "jknrzdy1",
                                                "optionChannelTypeId": "d045cfc5-c489-4e9c-8d74-423063db496b",
                                                "optionChannelType": {
                                                    "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
                                                    "name": "Location",
                                                    "isInputLink": false,
                                                    "isSelectOption": false,
                                                    "isVariable": false,
                                                    "isMultiSelect": true,
                                                    "userId": null,
                                                    "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]",
                                                    "isGlobal": true,
                                                    "status": "active",
                                                    "createdAt": "2018-06-28T14:17:05.000Z",
                                                    "updatedAt": "2018-07-09T04:12:01.000Z",
                                                    "deletedAt": null
                                                }
                                            }]
                                        }
                                    }]
                                }],
                                "bannerType": {
                                    "id": "374da180-02c6-4558-8985-90cad792f14f",
                                    "name": "Script",
                                    "value": "script",
                                    "isUpload": false,
                                    "isVideo": false
                                },
                                "bannerHtmlType": {
                                    "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
                                    "name": "PR Tracking",
                                    "value": "pr-tracking",
                                    "weight": 99
                                }
                            }]
                        }
                    }, {
                        "id": "jseemf32",
                        "positionOnShare": 2,
                        "placementId": "jseekyba",
                        "shareId": "jkkne2ae",
                        "time": "[{\"startTime\":\"2019-02-20T17:00:00.000Z\",\"endTime\":null}]",
                        "placement": {
                            "id": "jseekyba",
                            "width": 300,
                            "height": 250,
                            "startTime": "2019-02-20T17:00:00.000Z",
                            "endTime": null,
                            "weight": 1,
                            "revenueType": "cpm",
                            "cpdPercent": 0,
                            "isRotate": false,
                            "rotateTime": 20,
                            "price": 0,
                            "relative": 0,
                            "campaignId": "jsedz1jb",
                            "campaign": {
                                "id": "jsedz1jb",
                                "startTime": "2019-02-20T17:00:00.000Z",
                                "endTime": null,
                                "views": 0,
                                "viewPerSession": 0,
                                "timeResetViewCount": 0,
                                "weight": 0,
                                "revenueType": "cpm",
                                "pageLoad": 0,
                                "expense": 0,
                                "totalCPM": 0,
                                "isRunBudget": false,
                                "isRunMonopoly": false,
                                "optionMonopoly": null,
                                "settingBudgetCPM": null,
                                "settingBudgetCPC": null,
                                "maxCPMPerDay": 0
                            },
                            "banners": [{
                                "id": "276907",
                                "html": "<script type=\"text/javascript\">\ngoogle_ad_client = \"ca-pub-6366951472589375\";\n\ngoogle_ad_slot = \"1988984442\";\ngoogle_ad_width = 300;\ngoogle_ad_height = 250;\n\n</script>\n<script type=\"text/javascript\"\nsrc=\"//pagead2.googlesyndication.com/pagead/show_ads.js\">\n</script>",
                                "width": 300,
                                "height": 250,
                                "keyword": "",
                                "isMacro": false,
                                "weight": 1,
                                "placementId": "jseekyba",
                                "imageUrl": "",
                                "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
                                "url": "",
                                "target": "",
                                "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
                                "isIFrame": true,
                                "isDefault": false,
                                "isRelative": false,
                                "videoType": null,
                                "clickThroughUrl": null,
                                "vastTagsUrl": null,
                                "vastVideoUrl": "",
                                "videoUrl": "",
                                "duration": 0,
                                "thirdPartyTracking": "",
                                "thirdPartyUrl": null,
                                "isDocumentWrite": true,
                                "skipOffset": 0,
                                "isAddLinkSSP": false,
                                "optionBanners": [{
                                    "id": "jknt7q64",
                                    "logical": "and",
                                    "bannerId": "276907",
                                    "comparison": "==",
                                    "value": "",
                                    "type": "channel",
                                    "optionBannerChannels": [{
                                        "id": "jknt7qtk",
                                        "optionBannerId": "jknt7q64",
                                        "channelId": "jknrzdy1",
                                        "channel": {
                                            "id": "jknrzdy1",
                                            "name": "Location_Nuocngoai",
                                            "siteId": "15",
                                            "optionChannels": [{
                                                "id": "jkns71fj",
                                                "name": "Location",
                                                "logical": "and",
                                                "globalVariables": "__RC",
                                                "comparison": "==",
                                                "value": "101",
                                                "valueSelect": "__RC>=101",
                                                "logicalSelect": ">=",
                                                "createdAt": "2018-08-10T09:20:44.000Z",
                                                "updatedAt": "2019-03-11T07:31:59.000Z",
                                                "deletedAt": null,
                                                "channelId": "jknrzdy1",
                                                "optionChannelTypeId": "d045cfc5-c489-4e9c-8d74-423063db496b",
                                                "optionChannelType": {
                                                    "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
                                                    "name": "Location",
                                                    "isInputLink": false,
                                                    "isSelectOption": false,
                                                    "isVariable": false,
                                                    "isMultiSelect": true,
                                                    "userId": null,
                                                    "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]",
                                                    "isGlobal": true,
                                                    "status": "active",
                                                    "createdAt": "2018-06-28T14:17:05.000Z",
                                                    "updatedAt": "2018-07-09T04:12:01.000Z",
                                                    "deletedAt": null
                                                }
                                            }]
                                        }
                                    }]
                                }],
                                "bannerType": {
                                    "id": "374da180-02c6-4558-8985-90cad792f14f",
                                    "name": "Script",
                                    "value": "script",
                                    "isUpload": false,
                                    "isVideo": false
                                },
                                "bannerHtmlType": {
                                    "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
                                    "name": "PR Tracking",
                                    "value": "pr-tracking",
                                    "weight": 99
                                }
                            }]
                        }
                    }, {
                        "id": "jsefscov",
                        "positionOnShare": 2,
                        "placementId": "jsefr7tg",
                        "shareId": "jkkne2ae",
                        "time": "[{\"startTime\":\"2019-02-20T17:00:00.000Z\",\"endTime\":null}]",
                        "placement": {
                            "id": "jsefr7tg",
                            "width": 300,
                            "height": 250,
                            "startTime": "2019-02-20T17:00:00.000Z",
                            "endTime": null,
                            "weight": 1,
                            "revenueType": "pb",
                            "cpdPercent": 0,
                            "isRotate": false,
                            "rotateTime": 20,
                            "price": 0,
                            "relative": 0,
                            "campaignId": "jsedz1jb",
                            "campaign": {
                                "id": "jsedz1jb",
                                "startTime": "2019-02-20T17:00:00.000Z",
                                "endTime": null,
                                "views": 0,
                                "viewPerSession": 0,
                                "timeResetViewCount": 0,
                                "weight": 0,
                                "revenueType": "cpm",
                                "pageLoad": 0,
                                "expense": 0,
                                "totalCPM": 0,
                                "isRunBudget": false,
                                "isRunMonopoly": false,
                                "optionMonopoly": null,
                                "settingBudgetCPM": null,
                                "settingBudgetCPC": null,
                                "maxCPMPerDay": 0
                            },
                            "banners": [{
                                "id": "363881",
                                "html": "<div id=\"sspbid_514\"></div><script type=\"text/javascript\">admsspreg({sspid:514,w:300,h:250});</script>",
                                "width": 300,
                                "height": 250,
                                "keyword": "",
                                "isMacro": false,
                                "weight": 1,
                                "placementId": "jsefr7tg",
                                "imageUrl": "",
                                "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
                                "url": "",
                                "target": "",
                                "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
                                "isIFrame": false,
                                "isDefault": false,
                                "isRelative": false,
                                "videoType": null,
                                "clickThroughUrl": null,
                                "vastTagsUrl": null,
                                "vastVideoUrl": "",
                                "videoUrl": "",
                                "duration": 0,
                                "thirdPartyTracking": "",
                                "thirdPartyUrl": "",
                                "isDocumentWrite": true,
                                "skipOffset": 0,
                                "isAddLinkSSP": false,
                                "optionBanners": [{
                                    "id": "jkoquwdx",
                                    "logical": "and",
                                    "bannerId": "363881",
                                    "comparison": "==",
                                    "value": "",
                                    "type": "channel",
                                    "optionBannerChannels": [{
                                        "id": "jkoquwz2",
                                        "optionBannerId": "jkoquwdx",
                                        "channelId": "jknrq7p9",
                                        "channel": {
                                            "id": "jknrq7p9",
                                            "name": "Location_Toànquốc",
                                            "siteId": "15",
                                            "optionChannels": [{
                                                "id": "jknrqicw",
                                                "name": "Location",
                                                "logical": "and",
                                                "globalVariables": "__RC,__RC",
                                                "comparison": "==",
                                                "value": "undefined,100",
                                                "valueSelect": "__RC==undefined,__RC<=100",
                                                "logicalSelect": "==,<=",
                                                "createdAt": "2018-08-10T09:07:53.000Z",
                                                "updatedAt": "2019-04-05T07:15:25.000Z",
                                                "deletedAt": null,
                                                "channelId": "jknrq7p9",
                                                "optionChannelTypeId": "d045cfc5-c489-4e9c-8d74-423063db496b",
                                                "optionChannelType": {
                                                    "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
                                                    "name": "Location",
                                                    "isInputLink": false,
                                                    "isSelectOption": false,
                                                    "isVariable": false,
                                                    "isMultiSelect": true,
                                                    "userId": null,
                                                    "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]",
                                                    "isGlobal": true,
                                                    "status": "active",
                                                    "createdAt": "2018-06-28T14:17:05.000Z",
                                                    "updatedAt": "2018-07-09T04:12:01.000Z",
                                                    "deletedAt": null
                                                }
                                            }]
                                        }
                                    }]
                                }],
                                "bannerType": {
                                    "id": "374da180-02c6-4558-8985-90cad792f14f",
                                    "name": "Script",
                                    "value": "script",
                                    "isUpload": false,
                                    "isVideo": false
                                },
                                "bannerHtmlType": {
                                    "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
                                    "name": "PR Tracking",
                                    "value": "pr-tracking",
                                    "weight": 99
                                }
                            }]
                        }
                    }, {
                        "id": "juydbaq8",
                        "positionOnShare": 1,
                        "placementId": "juyd7xo3",
                        "shareId": "jkkne2ae",
                        "time": "[{\"startTime\":\"2019-04-26T17:00:00.000Z\",\"endTime\":\"2019-05-17T16:59:59.000Z\"}]",
                        "placement": {
                            "id": "juyd7xo3",
                            "width": 300,
                            "height": 250,
                            "startTime": "2019-04-26T17:00:00.000Z",
                            "endTime": "2019-05-17T16:59:59.000Z",
                            "weight": 1,
                            "revenueType": "cpd",
                            "cpdPercent": 1,
                            "isRotate": false,
                            "rotateTime": 20,
                            "price": 0,
                            "relative": 0,
                            "campaignId": "juyd3qjp",
                            "campaign": {
                                "id": "juyd3qjp",
                                "startTime": "2019-04-26T17:00:00.000Z",
                                "endTime": null,
                                "views": 0,
                                "viewPerSession": 0,
                                "timeResetViewCount": 0,
                                "weight": 0,
                                "revenueType": "cpd",
                                "pageLoad": 0,
                                "expense": 0,
                                "totalCPM": 0,
                                "isRunBudget": false,
                                "isRunMonopoly": false,
                                "optionMonopoly": "",
                                "settingBudgetCPM": "",
                                "settingBudgetCPC": "",
                                "maxCPMPerDay": 0
                            },
                            "banners": [{
                                "id": "juyd7yv7",
                                "html": "<script>\n    (function() {\n        var doc = document,\n            ua = navigator.userAgent + '';\n        var videourl = 'https://adi.admicro.vn/adt/banners/nam2015/4043/min_html5/lanphamthi/2019_04_25/300x250_Mec/300x250_Mec/300x250_Mec.html';\n        var imageurl =  'https://adi.admicro.vn/adt/banners/nam2015/4043/min_html5/huyphanquoc/2019_04_27/mercedesbenz/screenshot/mercedesbenz_0.png';\n        var imgwidth = '%%WIDTH%%';\n        var imgheight = '%%HEIGHT%%';\n        var html = '<div style=\"position:relative;\">';\n        if (ua.indexOf('Android') != -1 || ua.indexOf('iPad') != -1 || ua.indexOf('iPhone') != -1) {\n            html += ('<img src=\"' + imageurl + '\" border=\"0\" /><a href=\"%%CLICK_URL_UNESC%%\" target=\"_blank\" style=\"position:absolute; top:0; left:0; width:' + imgwidth + 'px; height:' + imgheight + 'px; display:block;z-index:9999;\"><span></span></a>')\n        } else {\n            html += ('<iframe style=\" width: '+imgwidth+' !important;border: none;display: block;\"src=\"'+videourl+'?url=%%CLICK_URL_ESC%%\"width='+imgwidth+' height='+imgheight+' scrolling=\"no\"></iframe>')\n        }\n        html += '</div>';\n        doc.write(html);\n    })();\n</script>",
                                "width": 300,
                                "height": 250,
                                "keyword": "",
                                "isMacro": false,
                                "weight": 1,
                                "placementId": "juyd7xo3",
                                "imageUrl": "",
                                "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
                                "url": "http://bs.serving-sys.com/Serving/adServer.bs?cn=trd&pli=1074696543&adid=1077921844&ord=[timestamp]",
                                "target": "",
                                "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
                                "isIFrame": false,
                                "isDefault": false,
                                "isRelative": false,
                                "videoType": "",
                                "clickThroughUrl": "",
                                "vastTagsUrl": "",
                                "vastVideoUrl": "",
                                "videoUrl": "",
                                "duration": 0,
                                "thirdPartyTracking": "",
                                "thirdPartyUrl": "http://bs.serving-sys.com/Serving/adServer.bs?cn=display&c=19&pli=1074696543&adid=1077921844&ord=[timestamp]",
                                "isDocumentWrite": true,
                                "skipOffset": 0,
                                "isAddLinkSSP": false,
                                "optionBanners": [],
                                "bannerType": {
                                    "id": "374da180-02c6-4558-8985-90cad792f14f",
                                    "name": "Script",
                                    "value": "script",
                                    "isUpload": false,
                                    "isVideo": false
                                },
                                "bannerHtmlType": {
                                    "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
                                    "name": "PR Tracking",
                                    "value": "pr-tracking",
                                    "weight": 99
                                }
                            }]
                        }
                    }]
                }, {
                    "id": "jvey4cpt",
                    "css": "",
                    "outputCss": "",
                    "rotateTime": 0,
                    "width": 300,
                    "height": 600,
                    "classes": "",
                    "isRotate": false,
                    "type": "single",
                    "format": "",
                    "zoneId": "78",
                    "isTemplate": false,
                    "template": "",
                    "offset": "",
                    "isAdPod": false,
                    "duration": null,
                    "sharePlacements": [{
                        "id": "jvey4we7",
                        "positionOnShare": 0,
                        "placementId": "jvc57598",
                        "shareId": "jvey4cpt",
                        "time": "[{\"startTime\":\"2019-05-05T17:00:00.000Z\",\"endTime\":null}]",
                        "placement": {
                            "id": "jvc57598",
                            "width": 0,
                            "height": 0,
                            "startTime": "2019-05-05T17:00:00.000Z",
                            "endTime": null,
                            "weight": 1,
                            "revenueType": "cpd",
                            "cpdPercent": 2,
                            "isRotate": false,
                            "rotateTime": 20,
                            "price": 0,
                            "relative": 0,
                            "campaignId": "jsedz1jb",
                            "campaign": {
                                "id": "jsedz1jb",
                                "startTime": "2019-02-20T17:00:00.000Z",
                                "endTime": null,
                                "views": 0,
                                "viewPerSession": 0,
                                "timeResetViewCount": 0,
                                "weight": 0,
                                "revenueType": "cpm",
                                "pageLoad": 0,
                                "expense": 0,
                                "totalCPM": 0,
                                "isRunBudget": false,
                                "isRunMonopoly": false,
                                "optionMonopoly": null,
                                "settingBudgetCPM": null,
                                "settingBudgetCPC": null,
                                "maxCPMPerDay": 0
                            },
                            "banners": [{
                                "id": "jvc5777w",
                                "html": "<div id=\"ssppagebid_600\"></div><script>if(typeof(admsspPosition)==\"undefined\"){_admloadJs(\"//media1.admicro.vn/core/ssppage.js\",function(){admsspPosition({sspid:600,w:0,h:0,group:\"104,600,601\"});});}else{admsspPosition({sspid:600,w:0,h:0,group:\"104,600,601\"});}</script>\n\n",
                                "width": 0,
                                "height": 0,
                                "keyword": "",
                                "isMacro": false,
                                "weight": 1,
                                "placementId": "jvc57598",
                                "imageUrl": "",
                                "bannerHtmlTypeId": "727fa59f-e673-4638-ad56-698b340b1f1c",
                                "url": "",
                                "target": "",
                                "bannerTypeId": "374da180-02c6-4558-8985-90cad792f14f",
                                "isIFrame": false,
                                "isDefault": false,
                                "isRelative": false,
                                "videoType": "",
                                "clickThroughUrl": "",
                                "vastTagsUrl": "",
                                "vastVideoUrl": "",
                                "videoUrl": "",
                                "duration": 0,
                                "thirdPartyTracking": "",
                                "thirdPartyUrl": "",
                                "isDocumentWrite": true,
                                "skipOffset": 0,
                                "isAddLinkSSP": false,
                                "optionBanners": [],
                                "bannerType": {
                                    "id": "374da180-02c6-4558-8985-90cad792f14f",
                                    "name": "Script",
                                    "value": "script",
                                    "isUpload": false,
                                    "isVideo": false
                                },
                                "bannerHtmlType": {
                                    "id": "727fa59f-e673-4638-ad56-698b340b1f1c",
                                    "name": "PR Tracking",
                                    "value": "pr-tracking",
                                    "weight": 99
                                }
                            }]
                        }
                    }]
                }],
                "site": {
                    "id": "15",
                    "domain": "http://autopro.com.vn",
                    "pageLoad": {
                        "totalPageload": 3,
                        "campaigns": []
                    },
                    "isRunBannerDefault": false,
                    "globalFilters": [],
                    "channels": [{
                        "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
                        "name": "Location",
                        "isGlobal": true,
                        "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]"
                    }],
                    "campaignMonopoly": {
                        "campaigns": []
                    }
                }
            }
        }
    };
    if (!document.getElementById("arf-core-js")) {
        var u = document.createElement("script");
        u.id = "arf-core-js", u.type = "application/javascript", u.src = "//media1.admicro.vn/cms/Arf" + c + ".js", document.getElementsByTagName("body")[0].appendChild(u)
    }
    window.arfZonesQueue = window.arfZonesQueue || [], d.propsData.model && window.arfZonesQueue.push(d)
}]);
//# sourceMappingURL=Template.min.js.map