/*
 * Opentag, a tag deployment platform
 * Copyright 2011-2016, Qubit Group
 * http://opentag.qubitproducts.com
 */
(function() {
  var M = function() {
      return this
  }();
  M.qubit = M.qubit || {};
  M.qubit.SYSTEM_DEFAULTS = {
      pingServerUrl: "opentag-stats.qubit.com/ping",
      tellLoadTimesProbability: 1
  }
})();
(function() {
  var M = {
      "id": "176515"
  };
  try {
      var J = function() {
          return this
      }();
      if (!J.qubit) J.qubit = {};
      var L = J.qubit;
      if (!L.cs) L.cs = {};
      L.CLIENT_CONFIG = M;
      var K = M.id || "efault";
      K = "d" + K;
      if (!L.cs[K]) L.cs[K] = {};
      L.cs[K].ClientConfig = M;
      if (L.SYSTEM_DEFAULTS) L.cs[K].SystemDefaults = L.SYSTEM_DEFAULTS
  } catch (ca) {}
})();
(function() {
  (function() {
      function M() {
          return ia.uv
      }
      function J() {
          var h = M();
          if (!h || !h.emit) return false;
          if (!da) {
              da = true;
              for (var a = 0; a < X.length; a++) {
                  var w = X[a];
                  if (w.on) h.on.apply(h, w.on);
                  else if (w.emit) h.emit.apply(h, w.emit)
              }
              h.on(/.*/, function(p) {
                  var v = p.meta.type;
                  var e = Y[v];
                  if (!e) Y[v] = {
                      current: p,
                      history: [p]
                  };
                  else {
                      e.current = p;
                      e.history.push(p)
                  }
              }).replay();
              Z = true
          }
          return true
      }(function() {
          if (!Function.prototype.bind) Function.prototype.bind = function(h) {
              if (typeof this !== "function") throw new TypeError("Function.prototype.bind - what is trying " +
                  "to be bound is not callable");
              var a = Array.prototype.slice.call(arguments, 1);
              var w = this;
              var p = function() {};
              var v = function() {
                  return w.apply(this instanceof p ? this : h, a.concat(Array.prototype.slice.call(arguments)))
              };
              p.prototype = this.prototype;
              v.prototype = new p;
              return v
          }
      })();
      var L = {
          __anonymous__: true
      };
      var K = null;
      try {
          K = (false || eval)("this")
      } catch (h) {}
      if (!K) try {
          var ca = function() {
              return this
          };
          K = ca()
      } catch (h) {}
      L = K;
      var A = L.qubit || {};
      if (!L.qubit) L.qubit = A;
      var ea = "3.3.0";
      if (A.VERSION && A.VERSION !== ea) try {
          console.warn("There is already 'qubit.VERSION' set to: " +
              A.VERSION)
      } catch (h) {}
      A.VERSION = ea;
      try {
          if (typeof module === "object") module.exports = L
      } catch (h) {}
      var N = function() {};
      var fa;
      (function() {
          function h() {}
          function a(v, e, n, c, r) {
              var t = v.split(".");
              var u = h.NAMESPACE_BASE || L;
              var C = null;
              var f = null;
              var x;
              if (r) u = K;
              var z = u;
              u = n || u;
              for (x = 0; x < t.length - 1; x += 1) {
                  C = u;
                  f = t[x];
                  u[f] = u[f] || {};
                  u = u[f]
              }
              C = u;
              f = t[t.length - 1];
              if (K.TAGSDK_NS_OVERRIDE) c = false;
              if (K.TAGSDK_NS_FORCED_OVERRIDE_OPTION !== undefined) c = !K.TAGSDK_NS_FORCED_OVERRIDE_OPTION;
              if (e !== undefined) {
                  if (C[f] === undefined ||
                      !c) C[f] = e
              } else C[f] = C[f] || {};
              if (e) {
                  e.CLASSPATH = t.join(".");
                  t.splice(t.length - 1, 1);
                  e.PACKAGE_NAME = t.join(".")
              }
              return {
                  root: z,
                  object: C,
                  instance: C[f]
              }
          }
          h.global = function() {
              return K
          };
          h.STANDARD_CS_NS = "qubit.cs";
          h.clientSpaceClasspath = function() {
              if (window.qubit.CLIENT_CONFIG) return "qubit.cs.d" + window.qubit.CLIENT_CONFIG.id;
              return h.STANDARD_CS_NS
          };
          h.globalNamespace = function(v, e, n, c) {
              return a(v, e, n, c, true)
          };
          h.namespace = function(v, e, n, c) {
              return a(v, e, n, c, false)
          };
          h.clientNamespace = function(v, e, n, c) {
              return h.namespace(h.clientSpaceClasspath() +
                  "." + v, e, n, c)
          };
          h.clazz = function(v, e, n, c, r) {
              h.namespace(v, e, c, true);
              if (typeof n === "function") {
                  e.SUPER = n;
                  e.superclass = n;
                  e.prototype = new n(r);
                  e.prototype.SUPER = n;
                  e.prototype.CLASS = e
              }
              var t = v.split(".");
              if (e.prototype) {
                  e.prototype.CLASSPATH = t.join(".");
                  e.prototype.CLASS_NAME = t[t.length - 1];
                  t.splice(t.length - 1, 1);
                  e.prototype.PACKAGE_NAME = t.join(".")
              } else {
                  e.CLASSPATH = t.join(".");
                  e.STATIC_NAME = t[t.length - 1];
                  t.splice(t.length - 1, 1);
                  e.PACKAGE_NAME = t.join(".")
              }
              return e
          };
          h.clazz("qubit.Define", h);
          h.clientClazz =
              function(v, e, n, c, r) {
                  return h.clazz(h.clientSpaceClasspath() + "." + v, e, n, c, r)
              };
          h.STANDARD_VS_NS = "qubit.vs";
          h.vendorsSpaceClasspath = function(v) {
              var e = A.VENDOR_SPACE_CP;
              var n = e === undefined || e === null ? h.STANDARD_VS_NS : e;
              if (v)
                  if (n) return n + "." + v;
                  else return v;
              return n
          };
          var w = h.vendorsSpaceClasspath();
          var p;
          if (w) p = h.namespace(w, {}, null, true).instance;
          else p = h.global();
          h.getVendorSpace = function() {
              return p
          };
          h.vendorNamespace = function(v, e, n, c) {
              v = h.vendorsSpaceClasspath(v);
              return h.namespace(v, e, n, c)
          };
          h.vendorClazz =
              function(v, e, n, c, r) {
                  v = h.vendorsSpaceClasspath(v);
                  return h.clazz(v, e, n, c, r)
              }
      })();
      (function() {
          function h(v) {}
          var a = "abcdefghijklmnopqrstuvwxyz" + "0123456789" + "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "*!-#$\x26+()@" + "'%./:\x3c\x3e?[" + '"]^_`{|}~' + "\\" + ";\x3d";
          var w = {};
          for (var p = 0; p < a.length; p++) w[a.charAt(p)] = p;
          A.Define.clazz("qubit.Cookie", h);
          h.cookieAlphabet = a;
          h.cookieAlphabetMap = w;
          h.decode = function(v) {
              return unescape(v)
          };
          h.encode = function(v) {
              return escape(v)
          };
          h.set = function(v, e, n, c, r) {
              var t;
              if (n) {
                  var u = new Date;
                  u.setTime(u.getTime() + n * 24 * 60 * 60 * 1E3);
                  t = "; expires\x3d" + u.toGMTString()
              } else t = "";
              if (!r) {
                  v = h.encode(v);
                  e = h.encode(e)
              }
              var C = v + "\x3d" + e + t + "; path\x3d/;";
              if (c) C += " domain\x3d" + c;
              document.cookie = C
          };
          h.get = function(v, e) {
              var n = v + "\x3d";
              var c = document.cookie.split(";");
              for (var r = 0; r < c.length; r++) {
                  for (var t = c[r]; t.charAt(0) === " ";) t = t.substring(1, t.length);
                  if (t.indexOf(n) === 0) {
                      var u = t.substring(n.length, t.length);
                      if (!e) u = h.decode(u);
                      return u
                  }
              }
              return null
          };
          h.getAllForName = function(v, e) {
              if (!v) return [];
              var n =
                  v + "\x3d";
              var c = document.cookie.split(";");
              var r = [];
              for (var t = 0; t < c.length; t++) {
                  for (var u = c[t]; u.charAt(0) === " ";) u = u.substring(1, u.length);
                  if (u.indexOf(n) === 0) {
                      var C = u.substring(n.length, u.length);
                      if (e) C = h.decode(C);
                      r.push(C)
                  }
              }
              return r
          };
          h.getAll = function(v) {
              var e = document.cookie.split(";");
              var n = [];
              for (var c = 0; c < e.length; c++) {
                  for (var r = e[c]; r.charAt(0) === " ";) r = r.substring(1, r.length);
                  var t = r.substring(0, r.indexOf("\x3d"));
                  var u = r.substring(t.length + 1, r.length);
                  if (v) u = h.decode(u);
                  n.push([t, u])
              }
              return n
          };
          h.rm = function(v, e) {
              h.set(v, "", -1, e)
          }
      })();
      (function() {
          function h(f, x, z) {
              this.collectLogs = !!h.isCollecting();
              this.collectLocally = z;
              this.collection = [];
              this.getPrefix = function() {
                  var B = "";
                  if (x) {
                      if (typeof x === "function") B = x(this.ref);
                      else if (x.CLASS_NAME) B = x.CLASS_NAME;
                      else if (x.constructor && x.constructor.name) B = x.constructor.name;
                      if (B) B += " -\x3e "
                  }
                  return (f || "") + B
              }
          }
          function a(f, x, z, B, l, k, q, d) {
              var b;
              var g = v >= d;
              if (h.getCollectLevel() >= 0 || g) {
                  if (l) b = [B, q, z];
                  else b = [x + f.getPrefix() + B, k, z];
                  b[3] = d;
                  f.collect(b,
                      d);
                  if (g) f.printMessage.apply(f, b)
              }
          }
          var w = A.Define;
          var p = null;
          w.clazz("qubit.opentag.Log", h);
          h.LEVEL_FINEST = 4;
          h.LEVEL_FINE = 3;
          h.LEVEL_INFO = 2;
          h.LEVEL_WARN = 1;
          h.LEVEL_ERROR = 0;
          h.LEVEL_NONE = -1;
          h.MAX_LOG_LEN = 1E4;
          h.prototype.MAX_LOG_LEN = -1;
          var v = h.LEVEL_NONE;
          var e = h.LEVEL_NONE;
          var n = true;
          h.setLevelFromCookie = function() {
              var f = A.Cookie.get("qubit.opentag.Log.LEVEL");
              if (f) {
                  f = +f;
                  if (!isNaN(f))
                      if (f > 0) {
                          var x = h.LEVEL_FINE;
                          h.setCollectLevel(f > x ? f : x);
                          h.setLevel(+f)
                      }
              }
          };
          h.setCollecting = function(f) {
              n = !!f
          };
          h.isCollecting =
              function() {
                  return n || h.COLLECT
              };
          h.setLevel = function(f) {
              v = +f
          };
          h.setCollectAndLevel = function(f) {
              h.setLevel(f);
              h.setCollectLevel(f)
          };
          h.getLevel = function() {
              return v
          };
          h.setCollectLevel = function(f) {
              e = +f
          };
          h.getCollectLevel = function() {
              return e
          };
          var c = [];
          h.logsCollection = c;
          h.rePrintAll = function(f, x, z, B) {
              var l = v;
              if (f !== undefined) v = f;
              try {
                  if (h.isCollecting()) {
                      try {
                          if (!z) p.clear()
                      } catch (b) {}
                      var k = B || h.logsCollection;
                      var q = 0;
                      for (var d = 0; d < k.length; d++)(function(b) {
                          var g = k[b];
                          var m = g[3];
                          if (m !== undefined && v >= m) {
                              q++;
                              if (!x) h.print.apply(h, g);
                              else A.opentag.Timed.setTimeout(function() {
                                  if (f !== undefined) v = f;
                                  try {
                                      h.print.apply(h, g)
                                  } finally {
                                      v = l
                                  }
                              }, q * x)
                          }
                      })(d)
                  }
              } catch (b) {} finally {
                  v = l
              }
          };
          var r = w.global();
          var t = !!(r.webkitMediaStream || r.webkitURL || r.mozContact);
          h.isStyleSupported = function() {
              return t
          };
          var u = {};
          h.setConsole = function(f) {
              p = f || u;
              return p
          };
          h.delayPrint = -1;
          var C = (new Date).valueOf();
          h.prototype.printMessage = function(f, x, z, B) {
              if (h.delayPrint > 0) {
                  var l = h.delayPrint;
                  var k = C - (new Date).valueOf();
                  if (k > 0) l += k;
                  try {
                      A.opentag.Timed.setTimeout(function() {
                          this.print(f,
                              x, z, B)
                      }.bind(this), l)
                  } catch (q) {
                      setTimeout(function() {
                          this.print(f, x, z, B)
                      }.bind(this), l)
                  }
                  C = (new Date).valueOf() + l
              } else this.print(f, x, z, B)
          };
          h.prototype.print = function(f, x, z, B) {
              h.print(f, x, z, B)
          };
          h.print = function(f, x, z, B) {
              if (B !== undefined && v < B) return;
              try {
                  if (p && p.log)
                      if (x && h.isStyleSupported())
                          if (z && p[z]) p[z]("%c" + f, x);
                          else p.log("%c" + f, x);
                  else if (z && p[z]) p[z](f);
                  else p.log(f)
              } catch (l) {}
          };
          h.prototype.collect = function(f, x) {
              if (x === undefined) x = h.getCollectLevel();
              var z = false;
              var B = this.collectLogs && h.isCollecting() &&
                  h.getCollectLevel() >= +x;
              if (B) {
                  c.push(f);
                  z = true
              }
              if (this.collectLocally && B) {
                  this.collection.push(f);
                  z = true
              }
              if (h.MAX_LOG_LEN > 0)
                  if (c.length > h.MAX_LOG_LEN) c.splice(0, c.length - h.MAX_LOG_LEN);
              if (h.MAX_LOG_LEN > 0 || this.MAX_LOG_LEN > 0) {
                  var l = h.MAX_LOG_LEN;
                  if (this.MAX_LOG_LEN > 0) l = this.MAX_LOG_LEN;
                  if (this.collection.length > l) this.collection.splice(0, this.collection.length - l)
              }
              return z ? f : null
          };
          h.clearAllLogs = function() {
              try {
                  p.clear()
              } catch (f) {} finally {
                  c.splice(0, c.length)
              }
          };
          h.getCollectedByLevel = function(f, x) {
              x =
                  x || c;
              var z = [];
              for (var B = 0; B < x.length; B++) {
                  var l = x[B];
                  var k = l[0];
                  var q = k[4];
                  if (q === f) z.push(l)
              }
              return z
          };
          h.prototype.rePrint = function(f, x, z) {
              h.rePrintAll(f, x, !z, this.collection)
          };
          h.prototype.FINEST = function(f, x) {
              a(this, "FINEST: ", false, f, x, "color:#CCCCCC;", false, h.LEVEL_FINEST)
          };
          h.prototype.FINE = function(f, x) {
              a(this, "FINE: ", false, f, x, "color:#999999;", false, h.LEVEL_FINE)
          };
          h.prototype.INFO = function(f, x, z) {
              a(this, "INFO: ", "info", f, x, z, z, h.LEVEL_INFO)
          };
          h.prototype.WARN = function(f, x) {
              a(this, "WARN: ", "warn",
                  f, x, "color:#26A110;", false, h.LEVEL_WARN)
          };
          h.prototype.ERROR = function(f, x) {
              a(this, "ERROR: ", "error", f, x, "color:red;", false, h.LEVEL_ERROR)
          };
          h.setConsole(w.global().console);
          h.setLevelFromCookie()
      })();
      (function() {
          function h(b) {
              return r.addToArrayIfNotExist(r.ANON_VARS, b)
          }
          function a(b) {
              return b.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1")
          }
          function w(b, g) {
              var m = u.length;
              for (var y = 0; y < m; y++)
                  if (b === u[y][0]) return u[y][1];
              u[u.length] = [b, g];
              return false
          }
          function p(b, g, m, y, D) {
              var E = false;
              var F = false;
              var G =
                  false;
              var I = false;
              var O = false;
              var U = false;
              var Q = false;
              if (g) {
                  O = !!g.all;
                  E = O || g.nodes;
                  I = O || g.win;
                  F = O;
                  Q = !!g.emptyForMaxDeep;
                  G = g.noFunctions && !O;
                  if (g.noOwn !== undefined) F = !!g.noOwn;
                  if (g.noFunctions !== undefined) G = !!g.noFunctions;
                  if (g.win !== undefined) I = !!g.win;
                  if (g.nodes !== undefined) E = !!g.nodes;
                  U = !!g.copyReference
              }
              if (m !== undefined && !m) {
                  if (Q) return;
                  return b
              } else if (m !== undefined) m--;
              if (!b || !(b instanceof Object)) return b;
              if (!E) {
                  try {
                      if (b instanceof Node) return b
                  } catch (ga) {
                      if (b instanceof ActiveXObject && b.nodeType !==
                          undefined) return b
                  }
                  if (b === document) return b
              }
              if (!I)
                  if (b === t) return b;
              var P = b instanceof Array ? [] : {};
              if (b instanceof Date) P = new Date(b);
              if (!G && b instanceof Function) {
                  var V = String(b).replace(/\s+/g, "");
                  if (V.indexOf("{[nativecode]}") + 14 === V.length) P = function() {
                      return b.apply(D || this, arguments)
                  };
                  else P = function() {
                      return b.apply(this, arguments)
                  }
              }
              if (y === undefined) {
                  u = [];
                  y = 0
              }
              var W = w(b, P);
              if (W) return W;
              var S;
              if (P instanceof Array)
                  for (S = 0; S < b.length; S++)
                      if (b[S] === b[S]) P[P.length] = p(b[S], g, m, y + 1, b);
                      else P[P.length] =
                          b[S];
              else {
                  S = 0;
                  for (var T in b) {
                      if (F || b.hasOwnProperty(T))
                          if (b[T] === b[T]) P[T] = p(b[T], g, m, y + 1, b);
                          else P[T] = b[T];
                      S++
                  }
              }
              if (g.proto) P.prototype = p(b.prototype, g, m, y + 1, b);
              if (U) P.___copy_ref = b;
              return P
          }
          function v(b, g) {
              for (var m = 0; m < g && m < C.length; m++)
                  if (b === C[m]) return true;
              return false
          }
          function e(b, g, m, y, D, E, F, G) {
              m = m || {};
              if (m.hasOwn === undefined) m.hasOwn = true;
              if (m.objectsOnly && !(b instanceof Object)) return;
              if (G !== undefined && !G) return;
              else if (G !== undefined) G--;
              if (!m || !m.nodes) try {
                  if (b instanceof Node) return
              } catch (V) {
                  if (b instanceof ActiveXObject && b.nodeType !== undefined) return
              }
              if (b === t) return;
              if (y === undefined) {
                  C = [];
                  y = 0
              }
              if (v(b, y)) return;
              C[y] = b;
              D = D || b;
              if (D && E && D[E] !== D[E]) return;
              var I = g(b, D, E, F);
              if (I) return;
              var O = 0;
              var U = "";
              for (var Q in b) {
                  if (!m.hasOwn || b.hasOwnProperty(Q)) try {
                      var P = b[Q];
                      if (m.track) U = F ? F + "." + Q : Q;
                      e(P, g, m, y + 1, D, Q, U, G)
                  } catch (V) {}
                  O++
              }
          }
          var n = A.Define;
          var c = A.Cookie;
          var r = function() {};
          var t = n.global();
          n.clazz("qubit.opentag.Utils", r);
          r.global = n.global;
          r.namespace = n.namespace;
          r.clazz = n.clazz;
          r.getObjectUsingPath = function(b,
              g) {
              g = g || t;
              var m = b.split(".");
              for (var y = 0; y < m.length; y++)
                  if (g && m[y]) g = g[m[y]];
              return g
          };
          r.getParentObject = function(b) {
              var g = b.lastIndexOf(".");
              var m = b.substring(0, g);
              return r.getObjectUsingPath(m)
          };
          r.variableExists = function(b) {
              return b !== undefined && b !== null
          };
          r.ANON_VARS = [];
          r.getAnonymousAcessor = function(b) {
              var g = r.indexInArray(b, r.ANON_VARS);
              if (g === -1) g = h(b);
              return "qubit.opentag.Utils.ANON_VARS[" + g + "]"
          };
          r.replaceAll = function(b, g, m) {
              return b.replace(new RegExp(a(g), "g"), m)
          };
          r.secureText = function(b) {
              if (typeof b !==
                  "string") b += "";
              b = b.replace(/</g, "\x26lt;");
              b = b.replace(/>/g, "\x26gt;");
              return b
          };
          r.getUrl = function() {
              return document.location.href
          };
          r.getQueryParam = function(b) {
              var g;
              var m;
              var y;
              var D;
              var E;
              var F;
              var G;
              D = r.getUrl();
              if (D.indexOf("?") > 0) {
                  F = D.substring(D.indexOf("?") + 1).split("\x26");
                  for (g = 0, m = F.length; g < m; g += 1) {
                      E = F[g];
                      if (E.indexOf("\x3d") > 0) {
                          G = E.split("\x3d");
                          if (G.length === 2 && G[0] === b) return G[1]
                      }
                  }
              }
              return null
          };
          r.getElementValue = function(b) {
              var g = document.getElementById(b);
              if (g) return g.textContent ||
                  g.innerText;
              return null
          };
          var u = [];
          r.objectCopy = function(b, g) {
              g = g || {};
              var m = p(b, g, g.maxDeep);
              u = [];
              return m
          };
          var C = [];
          r.traverse = function(b, g, m) {
              var y;
              e(b, g, m, y, y, y, y, m.maxDeep)
          };
          r.prepareQuotedString = function(b) {
              if (typeof b === "string") return '"' + b.replace(/\"/g, '\\"') + '"';
              else return b
          };
          r.expressionToFunction = function(b, g) {
              g = g || "";
              var m = "function (" + g + ") {" + b + "}";
              return r.gevalAndReturn(m).result
          };
          r.defineWrappedClass = function(b, g, m, y, D) {
              var E = function() {
                  if (D) return D.apply(this, arguments);
                  else return g.apply(this,
                      arguments)
              };
              n.clazz(b, E, g, y);
              for (var F in m)
                  if (m.hasOwnProperty(F) && F !== "CONSTRUCTOR") E.prototype[F] = m[F];
              return E
          };
          r.copyAandAddFromB = function(b, g, m) {
              m = m || 8;
              var y = this.objectCopy(b, {
                  maxDeep: m
              });
              for (var D in g)
                  if (g.hasOwnProperty(D)) y[D] = g[D];
              return y
          };
          r.keys = function(b) {
              if (b instanceof Object) {
                  if (Object.keys) return Object.keys(b);
                  var g = [];
                  for (var m in b)
                      if (b.hasOwnProperty(m)) g[g.length] = m;
                  return g
              } else throw "keys() called on non-object!";
          };
          r.getSrcElement = function(b) {
              var g;
              b = b || window.event;
              if (b.srcElement) g =
                  b.srcElement;
              else if (b.target) g = b.target;
              return g
          };
          r.addToArrayIfNotExist = function(b, g, m) {
              var y = 0;
              for (var D = false; y < b.length; y += 1) {
                  var E = m && m(b[y], g);
                  if (E || b[y] === g) {
                      D = true;
                      break
                  }
              }
              if (!D) {
                  b[b.length] = g;
                  y = -1
              }
              return y
          };
          r.indexInArray = function(b, g) {
              for (var m = 0; m < b.length; m++)
                  if (b[m] === g) return m;
              return -1
          };
          r.existsInArray = function(b, g) {
              for (var m = 0; m < b.length; m++)
                  if (b[m] === g) return true;
              return false
          };
          r.removeFromArray = function(b, g) {
              var m = 0;
              for (var y = 0; m < b.length;)
                  if (b[m] === g) {
                      b.splice(m, 1);
                      y++
                  } else m++;
              return y
          };
          r.addClass = function(b, g) {
              var m;
              try {
                  b.classList.add(g)
              } catch (y) {
                  if (b.className === null) {
                      b.className = g;
                      return
                  }
                  m = b.className.split(" ");
                  r.addToArrayIfNotExist(m, g);
                  b.className = m.join(" ")
              }
          };
          r.removeClass = function(b, g) {
              var m;
              try {
                  b.classList.remove(g)
              } catch (y) {
                  if (b.className === null) {
                      b.className = "";
                      return
                  }
                  m = b.className.split(" ");
                  r.removeFromArray(m, g);
                  b.className = m.join(" ")
              }
          };
          var f = "try{this.qubitopentagutilsgevalandreturn__var_test__\x3d(";
          var x = ");}catch(ex){" + "this.qubitopentagutilsgevalandreturn__var_test__error \x3d ex;}";
          r.gevalAndReturn = function(b) {
              var g = K;
              g.qubitopentagutilsgevalandreturn__var_test__ = undefined;
              g.qubitopentagutilsgevalandreturn__var_test__error = undefined;
              b = f + b + x;
              r.geval(b);
              var m = g.qubitopentagutilsgevalandreturn__var_test__;
              var y = g.qubitopentagutilsgevalandreturn__var_test__error;
              try {
                  g.qubitopentagutilsgevalandreturn__var_test__ = fa;
                  g.qubitopentagutilsgevalandreturn__var_test__error = fa;
                  delete g.qubitopentagutilsgevalandreturn__var_test__;
                  delete g.qubitopentagutilsgevalandreturn__var_test__error
              } catch (D) {}
              return {
                  result: m,
                  error: y
              }
          };
          r.trim = function(b) {
              try {
                  return String(b).trim()
              } catch (g) {
                  return String(b).replace(/^\s+|\s+$/g, "")
              }
          };
          r.setIfUnset = function(b, g) {
              if (b && g)
                  for (var m in g)
                      if (g.hasOwnProperty(m) && !b.hasOwnProperty(m)) b[m] = g[m]
          };
          var z = Math.floor(Math.random() * 1E12);
          r.UUID = function() {
              if (!t.__qubit_uuid_cnt_43567bdfhgtb4vt5yeh978__) t.__qubit_uuid_cnt_43567bdfhgtb4vt5yeh978__ = (new Date).valueOf();
              return z + "." + ++t.__qubit_uuid_cnt_43567bdfhgtb4vt5yeh978__
          };
          r.overrideFromLeftToRight = function(b, g) {
              if (b && g)
                  for (var m in g)
                      if (g.hasOwnProperty(m)) b[m] =
                          g[m];
              return b
          };
          r.geval = function(b) {
              if (window && window.execScript) return window.execScript(b === "" ? " " : b);
              else return function() {
                  return t["eval"].call(t, b)
              }()
          };
          var B = [];
          r.bodyAvailable = function(b) {
              var g = !!document.body;
              if (g) {
                  if (B) {
                      for (var m = 0; m < B.length; m++) try {
                          B[m]()
                      } catch (y) {}
                      B = false
                  }
                  b()
              } else B.push(b)
          };
          r.rmCookiesMatching = function(b) {
              var g = c.getAll();
              for (var m = 0; m < g.length; m++) {
                  var y = g[m][0];
                  if (y.match(b) === 0) c.rm(y)
              }
          };
          var l = [];
          var k = false;
          var q = false;
          r.bodyReady = function(b) {
              if (q) {
                  if (b) b();
                  return true
              }
              k =
                  k || !!(document.body && document.readyState === "complete");
              if (k) {
                  q = true;
                  for (var g = 0; g < l.length; g++) try {
                      l[g]()
                  } catch (m) {}
                  if (b) b()
              } else if (b) l.push(b);
              return k
          };
          var d = t.onload;
          t.onload = function(b) {
              try {
                  r.bodyReady()
              } catch (m) {
                  try {
                      window.qubit.opentag.Utils.bodyReady()
                  } catch (y) {}
              }
              var g = false;
              try {
                  g = d
              } catch (m) {}
              if (g) g(b)
          };
          (function() {
              var b;
              var g = false;
              var m = 1;
              var y;
              var D;
              var E;
              var F;
              D = function(I) {
                  var O;
                  if (I === true) m -= 1;
                  if (!m || I !== true && !g) {
                      if (!document.body) return setTimeout(D, 1);
                      g = true;
                      if (I !== true) {
                          m -= 1;
                          if (m >
                              0) return
                      }
                      for (; y.length > 0;) {
                          O = y.shift();
                          O()
                      }
                  }
              };
              F = function() {
                  if (g) return;
                  try {
                      document.documentElement.doScroll("left")
                  } catch (I) {
                      setTimeout(F, 1);
                      return
                  }
                  D()
              };
              E = function() {
                  if (y) return;
                  y = [];
                  if (document.readyState === "complete") return setTimeout(D, 1);
                  if (document.addEventListener) {
                      document.addEventListener("DOMContentLoaded", b, false);
                      window.addEventListener("load", D, false)
                  } else if (document.attachEvent) {
                      document.attachEvent("onreadystatechange", b);
                      window.attachEvent("onload", D);
                      var I = false;
                      try {
                          I = window.frameElement ===
                              null || window.frameElement === undefined
                      } catch (O) {}
                      if (document.documentElement.doScroll && I) F()
                  }
              };
              var G = function(I) {
                  E();
                  if (g) setTimeout(I, 1);
                  else y.push(I)
              };
              if (document.addEventListener) b = function() {
                  document.removeEventListener("DOMContentLoaded", b, false);
                  D()
              };
              else if (document.attachEvent) b = function() {
                  if (document.readyState === "complete") {
                      document.detachEvent("onreadystatechange", b);
                      D()
                  }
              };
              G(function() {
                  k = true;
                  r.bodyReady()
              })
          })()
      })();
      var R = {};
      (function() {
          function h(u) {
              return u < 10 ? "0" + u : u
          }
          function a(u) {
              e.lastIndex =
                  0;
              return e.test(u) ? '"' + u.replace(e, function(C) {
                  var f = r[C];
                  return typeof f === "string" ? f : "\\u" + ("0000" + C.charCodeAt(0).toString(16)).slice(-4)
              }) + '"' : '"' + u + '"'
          }
          function w(u) {
              return isFinite(u.valueOf()) ? u.getUTCFullYear() + "-" + h(u.getUTCMonth() + 1) + "-" + h(u.getUTCDate()) + "T" + h(u.getUTCHours()) + ":" + h(u.getUTCMinutes()) + ":" + h(u.getUTCSeconds()) + "Z" : null
          }
          function p(u, C) {
              var f;
              var x;
              var z;
              var B;
              var l = n;
              var k;
              var q = C[u];
              if (q instanceof Date) q = w(q);
              else if (q instanceof String || q instanceof Number || q instanceof Boolean) q = q.valueOf();
              if (typeof t === "function") q = t.call(C, u, q);
              switch (typeof q) {
                  case "string":
                      return a(q);
                  case "number":
                      return isFinite(q) ? String(q) : "null";
                  case "boolean":
                  case "null":
                      return String(q);
                  case "object":
                      if (!q) return "null";
                      n += c;
                      k = [];
                      if (Object.prototype.toString.apply(q) === "[object Array]") {
                          B = q.length;
                          for (f = 0; f < B; f += 1) try {
                              k[f] = p(f, q) || "null"
                          } catch (d) {
                              k[f] = '{"stack_exceeded": null}'
                          }
                          z = k.length === 0 ? "[]" : n ? "[\n" + n + k.join(",\n" + n) + "\n" + l + "]" : "[" + k.join(",") + "]";
                          n = l;
                          return z
                      }
                      if (t && typeof t ===
                          "object") {
                          B = t.length;
                          for (f = 0; f < B; f += 1)
                              if (typeof t[f] === "string") {
                                  x = t[f];
                                  try {
                                      z = p(x, q)
                                  } catch (d) {
                                      z = '{"stack_exceeded": null}'
                                  }
                                  if (z) k.push(a(x) + (n ? ": " : ":") + z)
                              }
                      } else
                          for (x in q)
                              if (Object.prototype.hasOwnProperty.call(q, x)) {
                                  try {
                                      z = p(x, q)
                                  } catch (d) {
                                      z = '{"stack_exceeded": null}'
                                  }
                                  if (z) k.push(a(x) + (n ? ": " : ":") + z)
                              }
                      z = k.length === 0 ? "{}" : n ? "{\n" + n + k.join(",\n" + n) + "\n" + l + "}" : "{" + k.join(",") + "}";
                      n = l;
                      return z
              }
          }
          var v = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
          var e = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
          var n;
          var c;
          var r = {
              "\b": "\\b",
              "\t": "\\t",
              "\n": "\\n",
              "\f": "\\f",
              "\r": "\\r",
              '"': '\\"',
              "\\": "\\\\"
          };
          var t;
          if (typeof R.stringify !== "function") R.stringify = function(u, C, f) {
              var x;
              n = "";
              c = "";
              if (typeof f === "number")
                  for (x = 0; x < f; x += 1) c += " ";
              else if (typeof f === "string") c = f;
              t = C;
              if (C && typeof C !== "function" && (typeof C !== "object" || typeof C.length !== "number")) throw new Error("JSON.stringify");
              return p("", {
                  "": u
              })
          };
          if (typeof R.parse !== "function") R.parse = function(u, C) {
              function f(z, B) {
                  var l;
                  var k;
                  var q = z[B];
                  if (q && typeof q === "object")
                      for (l in q)
                          if (Object.prototype.hasOwnProperty.call(q, l)) {
                              k = f(q, l);
                              if (k !== undefined) q[l] = k;
                              else delete q[l]
                          }
                  return C.call(z, B, q)
              }
              var x;
              u = String(u);
              v.lastIndex = 0;
              if (v.test(u)) u = u.replace(v, function(z) {
                  return "\\u" + ("0000" + z.charCodeAt(0).toString(16)).slice(-4)
              });
              if (/^[\],:{}\s]*$/.test(u.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, "@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,
                      "]").replace(/(?:^|:|,)(?:\s*\[)+/g, ""))) {
                  x = eval("(" + u + ")");
                  return typeof C === "function" ? f({
                      "": x
                  }, "") : x
              }
              throw new SyntaxError("JSON.parse");
          }
      })();
      var H = {};
      H.html = H.html || {};
      (function() {
          function h(c) {
              return c.replace(/^\s+/, "").replace(/\s+$/, "")
          }
          var a = {
              callbacks: {},
              unfired_events: [],
              early_callbacks: null,
              currentUV: null
          };
          var w;
          var p = "universal_variable";
          var v = 0;
          var e = 500;
          var n;
          a._isArray = function(c) {
              return Object.prototype.toString.call(c) === "[object Array]"
          };
          a._targetChanged = function(c, r, t) {
              var u;
              var C;
              var f;
              if (t === null)
                  if (r === "null") return false;
                  else return true;
              else if (t === undefined)
                  if (r === t) return false;
                  else return true;
              else {
                  u = R.parse(r);
                  C = a._getNested(u, c);
                  f = a._getNested(t, c);
                  return !a._jsonIsEqual(C, f)
              }
          };
          a._processCallbacks = function() {
              var c;
              if (a.early_callbacks && a.early_callbacks.length > 0) {
                  for (c = 0; c < a.early_callbacks.length; c += 1) a.push(a.early_callbacks[c]);
                  a.early_callbacks = null
              }
          };
          a._keyStringToArr = function(c) {
              c = h(c);
              if (c === "") return [];
              else {
                  c = c.replace(/\[(\w+)\]/g, ".$1");
                  c = c.replace(/^\./,
                      "");
                  return c.split(".")
              }
          };
          a._getNested = function(c, r) {
              var t = a._keyStringToArr(r);
              for (var u; t.length > 0;) {
                  u = t.shift();
                  if (c.hasOwnProperty(u)) c = c[u];
                  else return
              }
              return c
          };
          a._jsonIsEqual = function(c, r) {
              if (typeof c !== "string") c = R.stringify(c, a._stripEvents);
              if (typeof r !== "string") r = R.stringify(r, a._stripEvents);
              return c === r
          };
          a._stripEvents = function(c, r) {
              if (c !== "events") return r;
              else return undefined
          };
          a._on = function(c, r) {
              var t;
              var u;
              t = c.split(":");
              c = t[0];
              u = t[1];
              a.callbacks[c] = a.callbacks[c] || [];
              if (u) a.callbacks[c].push({
                  keyString: u,
                  func: r
              });
              else a.callbacks[c].push({
                  func: r
              })
          };
          a._trigger = function(c, r) {
              var t;
              var u;
              if (a.callbacks[c])
                  for (t = 0; t < a.callbacks[c].length; t += 1)
                      if (typeof a.callbacks[c][t].func === "function") {
                          u = a.callbacks[c][t].keyString;
                          if (u) {
                              if (c === "change" && a._targetChanged(u, a.currentUV, w)) a.callbacks[c][t].func(r)
                          } else a.callbacks[c][t].func(r)
                      }
          };
          a._eventsPush = function(c) {
              var r;
              var t;
              w.events[w.events.length] = c;
              c.time = c.time || (new Date).getTime();
              if (a.callbacks.event) {
                  r = 0;
                  t = a.callbacks.event.length;
                  for (r; r < t; r += 1) a.callbacks.event[r].func(c)
              }
              c.has_fired =
                  true
          };
          a._getUnfiredEvents = function() {
              var c = 0;
              for (c = 0; c < w.events.length; c += 1)
                  if (!w.events[c].has_fired) {
                      a.unfired_events.push(w.events.splice(c, 1)[0]);
                      c -= 1
                  }
          };
          a._fireEvents = function() {
              for (; a.unfired_events.length > 0;) w.events.push(a.unfired_events.shift())
          };
          a._resetEventsPush = function() {
              w.events = w.events || [];
              if (w.events.push.toString().indexOf("[native code]") !== -1) {
                  w.events.push = a._eventsPush;
                  a._getUnfiredEvents();
                  a._fireEvents()
              }
          };
          a._checkForChanges = function() {
              if (a.callbacks.change && a.callbacks.change.length >
                  0)
                  if (!a._jsonIsEqual(a.currentUV, w)) {
                      a._trigger("change", w);
                      a.currentUV = R.stringify(w, a._stripEvents)
                  }
          };
          a._setUVLocation = function(c) {
              p = c;
              a._initUV()
          };
          a._initUV = function() {
              window[p] = window[p] || {
                  events: []
              };
              w = window[p];
              if (!w.events) w.events = []
          };
          a.push = function(c) {
              if (!a._isArray(c)) return;
              if (c[0] === "on") a._on(c[1], c[2]);
              else if (c[0] === "trigger" && c[1]) a._trigger(c[1])
          };
          a.init = function(c, r) {
              if (r) p = r;
              a._initUV();
              if (!window.uv_listener || a._isArray(window.uv_listener)) {
                  a.early_callbacks = window.uv_listener ||
                      null;
                  window.uv_listener = a;
                  if (!c) a.start()
              } else if (r) window.uv_listener._setUVLocation(p)
          };
          a.start = function() {
              a.currentUV = R.stringify(w, a._stripEvents);
              n = setInterval(function() {
                  a._initUV();
                  a._resetEventsPush();
                  a._checkForChanges()
              }, e);
              a._processCallbacks()
          };
          H.html.UVListener = a
      })();
      (function() {
          function h(w) {
              if (w) {
                  this._rate = w.rate || 10;
                  this._smallestRate = -1;
                  if (w.start) this.startPooling();
                  this.config = w
              }
              this.inervals = [];
              this._lck_obj = {};
              this._binded_pool = this._pool.bind(this)
          }
          A.Define.clazz("qubit.opentag.Timer",
              h);
          h.prototype.timers = [];
          h.prototype.startPooling = function(w) {
              if (w && this.config.dynamic)
                  if (this._smallestRate < 0 || this._smallestRate > w) this._smallestRate = Math.min(Math.floor(w / 2), 1500);
              if (!this.started) {
                  this.started = true;
                  setTimeout(this._binded_pool, 0)
              }
          };
          h.prototype._pool = function() {
              this.maxFrequent(function() {
                  var p = "";
                  if (this.config && this.config.name) p = "[" + this.config.name + "]"
              }, 5E3, this._lck_obj);
              this.callTimers();
              if (this.timers.length !== 0) {
                  var w = this._smallestRate > this._rate ? this._smallestRate : this._rate;
                  setTimeout(this._binded_pool, w)
              } else {
                  this.started = false;
                  this._smallestRate = -1
              }
          };
          h.prototype.callTimers = function() {
              this.lastCalled = (new Date).valueOf();
              for (var w = 0; w < this.timers.length; w++) {
                  var p = this.timers[w];
                  var v = (new Date).valueOf();
                  if (v >= p.time) {
                      try {
                          p.execute()
                      } catch (e) {}
                      this.timers.splice(w, 1);
                      --w
                  }
              }
          };
          h.prototype.cancellAll = function() {
              this.timers = []
          };
          h.prototype.setRate = function(w) {
              this._rate = w
          };
          h.prototype.maxFrequent = function(w, p, v) {
              if (!v) {
                  w.__maxFrequent__timer_opentag_qubit_ = w.__maxFrequent__timer_opentag_qubit_ || {};
                  v = w.__maxFrequent__timer_opentag_qubit_
              }
              var e = v.____last__timed__max__frequent____;
              if (!e || (new Date).valueOf() - e > p) {
                  e = (new Date).valueOf();
                  v.____last__timed__max__frequent____ = e;
                  w()
              }
          };
          h.prototype.runIfNotScheduled = function(w, p, v) {
              v = v || w;
              if (v.__lastRun__ && (new Date).valueOf() < p + v.__lastRun__) return this.schedule(w, p, v);
              else {
                  v.__lastRun__ = (new Date).valueOf();
                  w();
                  return true
              }
          };
          h.prototype.schedule = function(w, p, v) {
              if (v.___scheduled___) return false;
              else {
                  v.___scheduled___ = (new Date).valueOf();
                  this.setTimeout(function() {
                      v.___scheduled___ =
                          false;
                      v.__lastRun__ = (new Date).valueOf();
                      w()
                  }, p);
                  return true
              }
          };
          var a = 1;
          h.prototype.setTimeout = function(w, p) {
              var v = {
                  id: a++,
                  time: (new Date).valueOf() + +p,
                  execute: w
              };
              this.timers.push(v);
              this.startPooling(p);
              return v
          };
          h.prototype.setInterval = function(w, p) {
              var v = setInterval(w, p);
              this.inervals.push(v);
              return v
          }
      })();
      (function() {
          A.Define.namespace("qubit.opentag.Timed", new A.opentag.Timer({
              rate: 37,
              dynamic: true
          }));
          var h = A.opentag.Timed;
          h.tillTrue = function(a, w, p) {
              var v = function() {
                  if (!a()) h.setTimeout(v, p ||
                      33);
                  else w()
              };
              v()
          }
      })();
      H.html.fileLoader = {};
      H.html.fileLoader.load = function(h, a, w, p, v, e) {
          var n;
          var c;
          var r;
          var t;
          var u;
          u = false;
          t = function(C) {
              return function() {
                  if (!u) {
                      u = true;
                      if (C && !r) r = {
                          url: document.location.href
                      };
                      w(h, r, C)
                  }
              }
          };
          try {
              if (a) c = a(h)
          } catch (C) {
              c = false;
              w(h, "Exception loading pre", true)
          } finally {
              if (c !== false) {
                  n = H.html.fileLoader.createScriptEl(h, v, false, e);
                  if (w) {
                      n.onload = t(false);
                      n.onerror = t(true);
                      n.onreadystatechange = function() {
                          if (this.readyState === "complete" || this.readyState === "loaded") setTimeout(function() {
                                  t(false)()
                              },
                              1)
                      }
                  }
                  if (!p) p = window.document.getElementsByTagName("head")[0];
                  p.appendChild(n)
              }
          }
      };
      H.html.fileLoader.createScriptEl = function(h, a, w, p) {
          var v;
          var e = document.createElement("script");
          e.type = "text/javascript";
          e.src = H.html.fileLoader.tidyUrl(h) + (w ? "?" + (new Date).getTime() : "");
          if (a !== false) {
              e.async = "true";
              e.defer = "true"
          } else {
              e.async = "false";
              if (e.async !== false) e.async = false;
              e.defer = "false";
              if (e.defer !== false) e.defer = false
          }
          for (v in p)
              if (p.hasOwnProperty(v)) e.setAttribute(v, p[v]);
          return e
      };
      H.html.fileLoader.tidyUrl =
          function(h) {
              if (h.substring(0, 5) === "http:") return h;
              if (h.substring(0, 6) === "https:") return h;
              return "//" + h
          };
      (function() {
          function h(p) {
              this.calls = {}
          }
          var a = A.Define;
          var w = A.opentag.Utils;
          h.prototype.on = function(p, v) {
              if (!this.calls[p]) this.calls[p] = [];
              return w.addToArrayIfNotExist(this.calls[p], v)
          };
          h.prototype.call = function(p, v) {
              var e = this.calls[p];
              if (e)
                  for (var n = 0; n < e.length; n++) try {
                      e[n](v)
                  } catch (c) {}
          };
          h.prototype.remove = function(p, v) {
              if (this.calls[p]) return w.removeFromArray(this.calls[p], v);
              return null
          };
          h.prototype.removeAll = function(p) {
              var v = 0;
              for (var e in this.calls)
                  if (this.calls.hasOwnProperty(e)) v += w.removeFromArray(this.calls[e], p);
              return v
          };
          h.prototype.clear = function() {
              this.calls = {}
          };
          a.clazz("qubit.Events", h)
      })();
      (function() {
          function h(e) {
              this.log = new A.opentag.Log("", function() {
                  return this.CLASS_NAME + "[" + this.config.name + "]"
              }.bind(this), "collectLogs");
              this.config = {
                  order: 0,
                  include: true,
                  name: "Filter-" + w++,
                  script: undefined,
                  session: undefined,
                  restartable: true
              };
              this.runtimeId = a.UUID();
              this.session =
                  null;
              if (e) {
                  for (var n in e)
                      if (e.hasOwnProperty(n)) this.config[n] = e[n];
                  if (e.session) this.setSession(e.session);
                  this.register(this)
              }
              this.events = new A.Events({})
          }
          var a = A.opentag.Utils;
          var w = 0;
          var p = [];
          A.Define.clazz("qubit.opentag.filter.BaseFilter", h);
          h.state = {
              DISABLED: -3,
              SESSION: -2,
              PASS: -1,
              FAIL: 0
          };
          var v = "restart";
          h.prototype.EVENT_TYPES = {
              RESTART_EVENT: v
          };
          h.prototype.prepareForRestart = function() {
              if (this.config.restartable) {
                  this.events.call(v, this);
                  this.reset()
              }
          };
          h.prototype.onRestart = function(e) {
              this.events.on(v,
                  e)
          };
          h.prototype.reset = function() {
              this.runtimeId = a.UUID();
              this.enable()
          };
          h.prototype.disable = function() {
              this.config.disabled = true
          };
          h.prototype.enable = function() {
              this.config.disabled = false
          };
          h.prototype.match = function() {
              return true
          };
          h.prototype.setSession = function(e) {
              this.session = e
          };
          h.prototype.getSession = function() {
              return this.session
          };
          h.prototype.getState = function() {
              var e = h.state.PASS;
              if (this.config.disabled) return h.state.DISABLED;
              if (this.config.script) e = this.config.script.call(this, e);
              if (isNaN(+e)) e =
                  h.state.FAIL;
              this.lastState = +e;
              return e
          };
          h.getFilters = function() {
              return p
          };
          h.prototype.register = function() {
              h.register(this)
          };
          h.register = function(e) {
              a.addToArrayIfNotExist(p, e)
          }
      })();
      (function() {
          var h = A.Define;
          var a = {
              CONTAINS: "CONTAINS",
              MATCHES_EXACTLY: "MATCHES_EXACTLY",
              STARTS_WITH: "STARTS_WITH",
              ENDS_WITH: "ENDS_WITH",
              REGULAR_EXPRESSION: "REGULAR_EXPRESSION",
              ALL_URLS: "ALL_URLS"
          };
          h.namespace("qubit.opentag.filter.pattern.PatternType", a)
      })();
      (function() {
          function h(e) {
              this._lockObject = {};
              var n = {
                  patternType: v.CONTAINS,
                  pattern: ""
              };
              if (e)
                  for (var c in e)
                      if (e.hasOwnProperty(c)) n[c] = e[c];
              h.SUPER.call(this, n)
          }
          var a = A.opentag.Utils;
          var w = A.opentag.filter.BaseFilter;
          var p = A.opentag.Timed;
          var v = A.opentag.filter.pattern.PatternType;
          A.Define.clazz("qubit.opentag.filter.URLFilter", h, w);
          h.prototype.PATTERNS = v;
          h.prototype.getURL = function(e) {
              return e || a.getUrl()
          };
          h.prototype.match = function(e) {
              e = this.getURL(e);
              var n = true;
              var c = this.config.pattern;
              switch (this.config.patternType) {
                  case v.CONTAINS:
                      n = e.toLowerCase().indexOf(c.toLowerCase()) >=
                          0;
                      break;
                  case v.MATCHES_EXACTLY:
                      n = e.toLowerCase() === this.config.pattern.toLowerCase();
                      break;
                  case v.STARTS_WITH:
                      n = e.toLowerCase().indexOf(c.toLowerCase()) === 0;
                      break;
                  case v.ENDS_WITH:
                      n = e.lastIndexOf(c.toLowerCase()) + c.length === e.length;
                      break;
                  case v.REGULAR_EXPRESSION:
                      n = (new RegExp(c)).test(e);
                      break;
                  case v.ALL_URLS:
                      n = true;
                      break
              }
              return n
          }
      })();
      (function() {
          function h(e) {
              var n = {};
              if (e) {
                  for (var c in e)
                      if (e.hasOwnProperty(c)) {
                          if (c === "customStarter" && e[c]) this.customStarter = e[c];
                          else if (c === "customScript" &&
                              e[c]) this.customScript = e[c];
                          n[c] = e[c]
                      }
                  this.uid = "f" + v++
              }
              this.tagsToRun = [];
              h.SUPER.call(this, n)
          }
          var a = A.opentag.filter.BaseFilter;
          var w = A.opentag.filter.URLFilter;
          var p = A.opentag.Utils;
          var v = 0;
          A.Define.clazz("qubit.opentag.filter.Filter", h, w);
          h.prototype.customStarter = function(e, n, c) {
              n(false)
          };
          h.prototype.isSession = function() {
              if (this.config.sessionDisabled) return false;
              if (this.customStarter === null && this.customScript === null) return false;
              return true
          };
          h.prototype.customScript = function(e) {
              return true
          };
          h.prototype.match = function(e) {
              var n = true;
              try {
                  if (this.customScript) {
                      if (this._matchState === undefined) this._matchState = !!this.customScript(this.getSession());
                      n = this._matchState
                  }
              } catch (c) {
                  n = false
              }
              return n && h.SUPER.prototype.match.call(this, e)
          };
          h.prototype.runTag = function(e) {
              p.addToArrayIfNotExist(this.tagsToRun, e);
              if (!this.starterExecuted) {
                  if (!this._starterWasRun) {
                      this._starterWasRun = true;
                      var n = this.runtimeId;
                      var c = function(r) {
                          if (n !== this.runtimeId) return;
                          this.reRun = r;
                          this.starterExecuted = (new Date).valueOf();
                          this._processQueuedTagsToRun()
                      }.bind(this);
                      if (this.customStarter) this.customStarter(this.getSession(), c, e);
                      else h.prototype.customStarter.call(this, this.getSession(), c, e)
                  }
              } else this._triggerTag(e)
          };
          h.prototype._processQueuedTagsToRun = function() {
              for (var e = 0; e < this.tagsToRun.length; e++) {
                  var n = this.tagsToRun[e];
                  this._triggerTag(n)
              }
          };
          h.prototype._triggerTag = function(e) {
              if (this.reRun === true) e.run();
              else e.runOnce()
          };
          h.prototype.getState = function(e) {
              if (e) this.setSession(e);
              var n = h.SUPER.prototype.getState.call(this);
              if (n === a.state.DISABLED) return a.state.DISABLED;
              if (n === a.state.PASS)
                  if (this.isSession()) n = a.state.SESSION;
              if (this.config.script) n = this.config.script.call(this, n, this.getSession());
              this.lastState = n;
              return n
          };
          h.prototype.reset = function() {
              h.SUPER.prototype.reset.call(this);
              this._matchState = undefined;
              this._starterWasRun = undefined;
              this.starterExecuted = undefined;
              this.tagsToRun = [];
              this.reRun = undefined
          }
      })();
      H.html.GlobalEval = {};
      H.html.GlobalEval.globalEval = function(h) {
          if (window.execScript) window.execScript(h ===
              "" ? " " : h);
          else {
              var a = function() {
                  window["eval"].call(window, h)
              };
              a()
          }
      };
      H.html.HtmlInjector = {};
      H.html.HtmlInjector.inject = function(h, a, w, p, v) {
          var e;
          var n;
          var c;
          var r;
          var t;
          var u;
          var C;
          if (w.toLowerCase().indexOf("\x3cscript") >= 0) {
              c = document.createElement("div");
              c.innerHTML = "a" + w;
              r = c.getElementsByTagName("script");
              t = [];
              for (e = 0, n = r.length; e < n; e += 1) t.push(r[e]);
              C = [];
              for (e = 0, n = t.length; e < n; e += 1) {
                  u = t[e];
                  var f = {
                      attributes: H.html.HtmlInjector.getAttributes(u)
                  };
                  if (u.src) f.src = u.src;
                  else f.script = u.innerHTML;
                  C.push(f);
                  u.parentNode.removeChild(u)
              }
              if (c.innerHTML)
                  if (c.innerHTML.length > 0) c.innerHTML = c.innerHTML.substring(1);
              H.html.HtmlInjector.doInject(h, a, c);
              H.html.HtmlInjector.loadScripts(C, 0, p, h)
          } else {
              c = document.createElement("div");
              c.innerHTML = w;
              H.html.HtmlInjector.doInject(h, a, c);
              if (p) p()
          }
      };
      H.html.HtmlInjector.doInject = function(h, a, w) {
          if (w.childNodes.length > 0) {
              for (var p = document.createDocumentFragment(); w.childNodes.length > 0;) p.appendChild(w.removeChild(w.childNodes[0]));
              if (a) H.html.HtmlInjector.injectAtStart(h,
                  p);
              else H.html.HtmlInjector.injectAtEnd(h, p)
          }
      };
      H.html.HtmlInjector.injectAtStart = function(h, a) {
          if (h.childNodes.length === 0) h.appendChild(a);
          else h.insertBefore(a, h.childNodes[0])
      };
      H.html.HtmlInjector.injectAtEnd = function(h, a, w) {
          if (!w) w = 1;
          if (h === document.body && document.readyState !== "complete" && w < 50) setTimeout(function() {
              H.html.HtmlInjector.injectAtEnd(h, a, w + 1)
          }, 100);
          else h.appendChild(a)
      };
      H.html.HtmlInjector.loadScripts = function(h, a, w, p) {
          var v;
          var e;
          var n = false;
          for (v = h.length; a < v; a += 1) {
              e = h[a];
              if (e.src) {
                  n =
                      true;
                  break
              } else H.html.GlobalEval.globalEval(e.script)
          }
          if (n) H.html.fileLoader.load(e.src, null, function() {
              H.html.HtmlInjector.loadScripts(h, a + 1, w, p)
          }, p, false, e.attributes);
          if (w && a === v) w()
      };
      H.html.HtmlInjector.getAttributes = function(h) {
          var a;
          var w;
          var p;
          var v;
          var e;
          var n = {};
          if (h) {
              p = h.attributes;
              w = p.length;
              for (a = 0; a < w; a++) {
                  v = p[a].value;
                  e = p[a].name.toLowerCase();
                  if (v !== "" && (e === "id" || e === "class" || e === "charset" || e.substr(0, 5) === "data-")) n[e] = v
              }
              return n
          }
      };
      (function() {
          function h() {
              C = C || {
                  write: document.write,
                  writeln: document.writeln
              }
          }
          function a() {
              document.write = C.write;
              document.writeln = C.writeln;
              C = null
          }
          var w = A.opentag.filter.BaseFilter;
          var p = A.opentag.Utils;
          var v = H.html.HtmlInjector;
          var e = H.html.fileLoader;
          var n = A.opentag.filter.Filter;
          var c = function() {};
          A.Define.clazz("qubit.opentag.TagsUtils", c);
          var r = false;
          c.bodyLoaded = function() {
              if (r) return true;
              r = !!(document.body && document.readyState !== "loading");
              return r
          };
          c.bodyAvailable = function(d) {
              return !!document.body
          };
          var t = {};
          var u = {
              SUCCESS: "success",
              FAIL: "failure",
              INIT: "not started"
          };
          c.loadScript = function(d) {
              var b = d.url;
              var g = function(D, E, F) {
                  t[b].error = E;
                  if (F) {
                      t[b].state = u.FAIL;
                      d.onerror()
                  } else {
                      t[b].state = u.SUCCESS;
                      d.onsuccess()
                  }
              };
              if (t[b]) {
                  if (d.noMultipleLoad) return g(b, t[b].error, t[b].state === u.FAIL);
                  t[b].count += 1
              } else t[b] = {
                  count: 1,
                  state: null
              };
              var m = !d.async;
              var y = c.bodyLoaded();
              if (m && y);
              m = m && !y;
              if (m) c.writeScriptURL(b, function(D, E) {
                  g(b, E, !D)
              });
              else e.load(b, false, g, d.node, d.async)
          };
          var C = null;
          c.redirectDocumentWritesToArray = function(d, b) {
              var g = d;
              h();
              document.write =
                  function(m) {
                      g.push(m)
                  };
              document.writeln = function(m) {
                  g.push(m)
              }
          };
          c.flushDocWritesArray = function(d, b, g, m, y) {
              var D = b;
              if (D && d) {
                  var E = d.splice(0, d.length);
                  try {
                      c.injectHTML(D, g, E.join("\n"), y || N);
                      return true
                  } catch (G) {}
              } else {
                  var F = "Flushing location not found!";
                  return false
              }
              if (y) y();
              return true
          };
          c.unlockDocumentWrites = function() {
              if (C) a()
          };
          var f = c.prototype.PACKAGE_NAME + ".TagsUtils._writeScriptURL_callbacks";
          var x = {};
          A.Define.namespace(f, x, K, true);
          var z = 0;
          var B = (new Date).valueOf();
          c.writeScriptURL = function(d,
              b) {
              var g = "_" + B + "_" + z++;
              var m = f + "." + g;
              var y = false;
              x[g] = function(O) {
                  if (y) return;
                  y = true;
                  if (O) b(false, "error while loading script " + d);
                  else b(true);
                  x[g] = undefined;
                  delete c.writeScriptURL.callbacks[g]
              };
              var D = 'if(this.readyState \x3d\x3d\x3d "loaded" || ' + 'this.readyState \x3d\x3d\x3d "complete"){ try {' + m + "(true)" + "} catch (ex) {}}";
              var E = "try{" + m + "(false)}catch(ex){}";
              var F = "try{" + m + "(true)}catch(ex){}";
              var G = "scr";
              var I;
              d = e.tidyUrl(d);
              I = "\x3c" + G + "ipt onload\x3d'" + E + "'  onerror\x3d'" + F + "' onreadystatechange\x3d'" +
                  D + "' type\x3d'text/javascript' " + " src\x3d'" + d + "'\x3e" + "\x3c/" + G + "ipt\x3e";
              if (C) {
                  a();
                  document.write(I);
                  h()
              } else document.write(I);
              p.bodyReady(function() {
                  if (!y) {
                      y = true;
                      b(true)
                  }
              })
          };
          c.writeScriptURL.callbacks = {};
          var l = w.state.SESSION;
          var k = w.state.PASS;
          var q = w.state.FAIL;
          c.filtersState = function(d, b, g, m) {
              d = d.sort(function(T, ga) {
                  try {
                      var aa = ga.config.order;
                      var ba = T.config.order;
                      if (isNaN(-ba)) ba = 0;
                      if (isNaN(-aa)) aa = 0;
                      return aa - ba
                  } catch (ja) {
                      return 0
                  }
              });
              var y = k;
              if (!d || d.length === 0) return y;
              var D = null;
              var E =
                  false;
              var F = false;
              var G = 0;
              var I;
              var O;
              var U = [];
              var Q;
              var P;
              for (var V = 0; V < d.length; V++) {
                  Q = d[V];
                  Q.setSession(b);
                  if (Q.match()) {
                      I = Q.getState();
                      if (I > 0) {
                          if (G === 0 || G > I) G = I
                      } else if (I === w.state.DISABLED) E = true;
                      else if (I === l) {
                          F = true;
                          D = Q;
                          O = Q;
                          U.push(Q)
                      } else D = Q
                  } else P = Q
              }
              var W = false;
              if (D === null) {
                  W = true;
                  if (!E) y = q;
                  else y = k
              } else if (D.config.include) y = I;
              else y = I === k ? q : k;
              if (G > 0 && (y === k || W)) y = G;
              if (y === l || y === k && F) {
                  if (!O.config.include) return q;
                  y = l;
                  if (O instanceof n && O.isSession())
                      if (m)
                          for (var S = 0; S < U.length; S++) try {
                              U[S].runTag(g)
                          } catch (T) {}
              }
              if (g.config.dedupe &&
                  y === k)
                  if (P && P instanceof n && P.isSession()) {
                      g.sendDedupePing = true;
                      y = q
                  }
              return y
          };
          c.injectHTML = function(d, b, g, m) {
              return v.inject(d, !b ? 1 : 0, g, m || N)
          };
          c.getHTMLLocationForTag = function(d) {
              var b;
              var g = d.prepareLocationObject(d.config.locationObject);
              switch (g) {
                  case "HEAD":
                      b = document.getElementsByTagName("head")[0];
                      break;
                  case "BODY":
                      b = document.body;
                      break;
                  default:
                      if (g) b = document.getElementById(g);
                      else b = document.body
              }
              return b
          }
      })();
      (function() {
          function h(c) {
              this.config = {};
              this.handlers = [];
              this.parameters = null;
              this.callHandlersOnRead = false;
              if (c) {
                  this.uniqueId = "BV" + v++;
                  h.ALL_VARIABLES[this.uniqueId] = this;
                  for (var r in c)
                      if (c.hasOwnProperty(r)) {
                          this.config[r] = c[r];
                          if (r === "value") this.value = c.value
                      }
                  var t = h.register(this);
                  if (t && t !== this);
                  return t
              }
          }
          function a() {
              if (n) return false;
              for (var c = 0; c < e.length; c++) try {
                  var r = e[c];
                  r.getValue()
              } catch (t) {}
              p.setTimeout(a, h.CHECK_POLL_RATE);
              return true
          }
          var w = A.opentag.Utils;
          var p = A.opentag.Timed;
          var v = 0;
          A.Define.clazz("qubit.opentag.pagevariable.BaseVariable", h);
          h.ALL_VARIABLES = {};
          var e = [];
          h.OBSERVED_VARIABLES = e;
          h.pageVariables = [];
          h.register = function(c) {
              if (c instanceof h) {
                  for (var r = 0; r < h.pageVariables.length; r++) {
                      var t = h.pageVariables[r];
                      if (c === t) return t
                  }
                  h.pageVariables.push(c);
                  return c
              }
              return null
          };
          h.prototype.getValue = function() {
              this._updateCurrentValue(this.value);
              return this.currentValue
          };
          h.prototype._updateCurrentValue = function(c) {
              if (!this.valuesAreEqual(c, this.currentValue)) {
                  this.oldValue = this.currentValue;
                  this.currentValue = c;
                  if (!n || this.callHandlersOnRead) this._handleValueChanged();
                  return true
              }
              return false
          };
          h.prototype.getDefaultValue = function() {
              return this.defaultValue
          };
          h.prototype.setDefaultValue = function(c) {
              this.defaultValue = c
          };
          h.prototype.exists = function(c) {
              var r = w.variableExists(this.getValue());
              if (c) r = r || w.variableExists(this.getDefaultValue());
              return r
          };
          h.prototype.getRelativeValue = function(c, r) {
              var t = this.getValue();
              if (!w.variableExists(t)) t = r;
              var u;
              if (c && !w.variableExists(t)) {
                  u = this.getDefaultValue();
                  if (w.variableExists(u)) t = u
              }
              return t
          };
          h.prototype.replaceToken = function(c,
              r, t, u) {
              var C = this.exists();
              var f = C ? this.getValue() : t;
              c = "\\$\\{" + c + "\\}";
              if (u || f instanceof Array) {
                  var x;
                  if (C) x = this.getValueAccessorString();
                  else x = w.getAnonymousAcessor(f);
                  return r.replace(new RegExp(c, "g"), x)
              } else return r.replace(new RegExp(c, "g"), f)
          };
          h.prototype.getAccessorString = function() {
              return "qubit.opentag.pagevariable.BaseVariable.ALL_VARIABLES." + this.uniqueId
          };
          h.prototype.getValueAccessorString = function() {
              return this.getAccessorString() + ".getValue()"
          };
          h.prototype.onValueChanged = function(c,
              r) {
              if (w.addToArrayIfNotExist(this.handlers, c) === -1);
              if (r !== false) this.startObservingForChanges()
          };
          h.prototype.deatchOnValueChanged = function(c) {
              if (w.removeFromArray(this.handlers, c) > 0);
          };
          h.prototype._handleValueChanged = function() {
              var c = {
                  oldValue: this.oldValue,
                  newValue: this.currentValue,
                  variable: this
              };
              for (var r = 0; r < this.handlers.length; r++) try {
                  this.handlers[r](c)
              } catch (t) {}
          };
          var n = true;
          h.CHECK_POLL_RATE = 333;
          h.prototype.startObservingForChanges = function() {
              this.addToObservedVariables();
              if (n) {
                  n = false;
                  a()
              }
          };
          h.prototype.stopObservingForChanges = function() {
              this.removeFromObservedVariables();
              if (e.length === 0) n = true
          };
          h.prototype.addToObservedVariables = function() {
              w.addToArrayIfNotExist(e, this)
          };
          h.prototype.removeFromObservedVariables = function() {
              w.removeFromArray(e, this)
          };
          h.prototype.getObservedVariables = function() {
              return e
          };
          h.prototype.valuesAreEqual = function(c, r) {
              return c === r
          }
      })();
      (function() {
          function h(p) {
              this._lockExprObject = {};
              h.SUPER.apply(this, arguments)
          }
          var a = A.opentag.Utils;
          var w = A.opentag.Timed;
          A.Define.clazz("qubit.opentag.pagevariable.Expression", h, A.opentag.pagevariable.BaseVariable);
          h.prototype.getValue = function() {
              var p;
              var v;
              var e = this.value;
              try {
                  if (e && e.indexOf("[#]") === -1) {
                      var n = a.gevalAndReturn(e);
                      p = n.result;
                      this.failMessage = null;
                      v = n.error
                  } else p = h.parseUVArray(e)
              } catch (t) {
                  v = t
              }
              if (v) {
                  var c = "could not read value of expression: \n" + e + "\nexact cause: " + v;
                  if (this.failMessage !== c) this.failMessage = c;
                  p = null
              }
              var r = this._updateCurrentValue(p);
              return p
          };
          h.parseUVArray = function(p) {
              var v = p.split("[#]");
              var e = a.gevalAndReturn(v[0]).result;
              var n = [];
              var c = v[1];
              if (c.indexOf(".") === 0) c = c.replace(".", "");
              for (var r = 0; r < e.length; r++) {
                  var t = a.getObjectUsingPath(c, e[r]);
                  n.push(t)
              }
              return n
          };
          h.prototype.replaceToken = function(p, v, e, n) {
              if (this.getValue() instanceof Array) n = true;
              return h.SUPER.prototype.replaceToken.call(this, p, v, e, n)
          }
      })();
      (function() {
          function h(w) {
              h.SUPER.apply(this, arguments)
          }
          var a = A.opentag.Utils;
          A.Define.clazz("qubit.opentag.pagevariable.DOMText", h, A.opentag.pagevariable.BaseVariable);
          h.prototype.getValue =
              function() {
                  var w = a.getElementValue(this.value);
                  this._updateCurrentValue(w);
                  return w
              }
      })();
      (function() {
          function h(a) {
              h.SUPER.apply(this, arguments);
              this._lockObject = {}
          }
          A.Define.clazz("qubit.opentag.pagevariable.Cookie", h, A.opentag.pagevariable.BaseVariable);
          h.prototype.getValue = function() {
              var a = A.Cookie.get(this.value);
              this._updateCurrentValue(a);
              return a
          }
      })();
      (function() {
          function h(w) {
              h.SUPER.apply(this, arguments)
          }
          var a = A.opentag.Utils;
          A.Define.clazz("qubit.opentag.pagevariable.URLQuery", h, A.opentag.pagevariable.BaseVariable);
          h.prototype.getValue = function() {
              var w = a.getQueryParam(this.value);
              this._updateCurrentValue(w);
              return w
          }
      })();
      (function() {
          function h(a) {
              h.SUPER.apply(this, arguments)
          }
          A.Define.clazz("qubit.opentag.pagevariable.UniversalVariable", h, A.opentag.pagevariable.Expression)
      })();
      (function a() {
          function w() {
              function p(k) {
                  B.level = k
              }
              function v(k, q) {
                  B.info(k, "event emitted");
                  q = u(q || {});
                  q.meta = q.meta || {};
                  q.meta.type = k;
                  x.push(q);
                  c();
                  l.listeners = f(l.listeners, function(d) {
                      return !d.disposed
                  })
              }
              function e(k, q, d) {
                  function b() {
                      B.info("Replaying events");
                      n(function() {
                          t(l.events, function(D) {
                              if (m.disposed) return;
                              if (!C(k, D.meta.type)) return;
                              q.call(d, D)
                          })
                      });
                      return y
                  }
                  function g() {
                      B.info("Disposing event handler");
                      m.disposed = true;
                      return y
                  }
                  B.info("Attaching event handler for", k);
                  var m = {
                      type: k,
                      callback: q,
                      disposed: false,
                      context: d || window
                  };
                  l.listeners.push(m);
                  var y = {
                      replay: b,
                      dispose: g
                  };
                  return y
              }
              function n(k) {
                  B.info("Calling event handlers");
                  z++;
                  try {
                      k()
                  } catch (q) {
                      B.error("UV API Error", q.stack)
                  }
                  z--;
                  c()
              }
              function c() {
                  if (x.length === 0) B.info("No more events to process");
                  if (x.length > 0 && z > 0) B.info("Event will be processed later");
                  if (x.length > 0 && z === 0) {
                      B.info("Processing event");
                      var k = x.shift();
                      l.events.push(k);
                      n(function() {
                          t(l.listeners, function(q) {
                              if (q.disposed) return;
                              if (!C(q.type, k.meta.type)) return;
                              try {
                                  q.callback.call(q.context, k)
                              } catch (d) {
                                  B.error("Error emitting UV event", d.stack)
                              }
                          })
                      })
                  }
              }
              function r(k, q, d) {
                  var b = l.on(k, function() {
                      q.apply(d || window, arguments);
                      b.dispose()
                  });
                  return b
              }
              function t(k, q) {
                  var d = k.length;
                  for (var b = 0; b < d; b++) q(k[b], b)
              }
              function u(k) {
                  var q = {};
                  for (var d in k)
                      if (k.hasOwnProperty(d)) q[d] = k[d];
                  return q
              }
              function C(k, q) {
                  return typeof k === "string" ? k === q : k.test(q)
              }
              function f(k, q) {
                  var d = k.length;
                  var b = [];
                  for (var g = 0; g < d; g++)
                      if (q(k[g])) b.push(k[g]);
                  return b
              }
              var x = [];
              var z = 0;
              var B = {
                  info: function q() {
                      if (B.level > p.INFO) return;
                      if (console && console.info) console.info.apply(console, arguments)
                  },
                  error: function d() {
                      if (B.level > p.ERROR) return;
                      if (console && console.error) console.error.apply(console, arguments)
                  }
              };
              p.ALL = 0;
              p.INFO = 1;
              p.ERROR = 2;
              p.OFF = 3;
              p(p.ERROR);
              var l = {
                  on: e,
                  emit: v,
                  once: r,
                  events: [],
                  listeners: [],
                  logLevel: p
              };
              return l
          }
          if (typeof module === "object" && module.exports) module.exports = w;
          else if (window && window.uv === undefined) window.uv = w()
      })();
      var Y = {};
      var X = [];
      var da = false;
      var Z = false;
      var ia = A.Define.global();
      var ha = {
          allEventsByType: Y,
          subscribe: function(a, w) {
              if (Z) {
                  var p = M();
                  p.on(a, w)
              } else X.push({
                  on: [a, w]
              })
          },
          getEventHistory: function(a) {
              return this.allEventsByType[a]
          },
          publish: function(a, w) {
              if (Z) {
                  var p = M();
                  p.emit(a, w)
              } else X.push({
                  emit: [a, w]
              })
          },
          connect: function() {
              J()
          }
      };
      ha.connect();
      A.Define.namespace("qubit.qprotocol.PubSub", ha);
      (function() {
          function a(e) {
              a.SUPER.apply(this, arguments)
          }
          var w = A.qprotocol.PubSub;
          var p = A.opentag.Utils;
          var v = ":";
          A.Define.clazz("qubit.opentag.pagevariable.QProtocolVariable", a, A.opentag.pagevariable.BaseVariable);
          a.prototype.init = function() {
              if (this.initialized) return false;
              else this.initialized = (new Date).valueOf();
              this.callHandlersOnRead = true;
              if (!this.handlerAttached) {
                  var e = this;
                  var n = this.getValueAndPath(this.value);
                  this.eventName = n[0];
                  this.objectPath = n[1];
                  this.updateValue();
                  w.subscribe(this.eventName, function(c) {
                      e.updateValue(c)
                  });
                  this.handlerAttached = (new Date).valueOf()
              }
          };
          a.prototype.getValueAndPath = function(e) {
              var n = e;
              var c;
              if (n) {
                  var r = e.indexOf(v);
                  if (r !== -1) {
                      n = e.substring(0, r);
                      c = e.substring(r + 1)
                  }
              }
              return [n, c]
          };
          a.prototype.getValue = function() {
              this.init();
              return this.currentValue
          };
          a.prototype.getEventsHistory = function() {
              return w.getEventHistory(this.eventName)
          };
          a.prototype.updateValue = function(e) {
              if (!e) {
                  var n = this.getEventsHistory();
                  if (n) e = n.current
              }
              var c;
              var r = this.objectPath;
              if (e && r) c = p.getObjectUsingPath(r, e);
              else c = e;
              this._updateCurrentValue(c);
              return c
          };
          a.prototype.startObservingForChanges = function() {};
          a.prototype.stopObservingForChanges = function() {}
      })();
      (function() {
          function a() {}
          function w(k, q) {
              var d = [];
              try {
                  var b = k.parameters;
                  if (b)
                      for (var g = 0; g < b.length; g++)
                          if (k.getVariableForParameter(b[g]) === q) d.push(b[g])
              } catch (m) {}
              return d
          }
          var p = A.opentag.Utils;
          var v = A.opentag.TagsUtils;
          var e = A.opentag.Timed;
          var n = A.opentag.pagevariable.BaseVariable;
          var c = A.opentag.pagevariable.Expression;
          var r = A.opentag.pagevariable.UniversalVariable;
          var t = A.opentag.pagevariable.DOMText;
          var u = A.opentag.pagevariable.Cookie;
          var C = A.opentag.pagevariable.URLQuery;
          A.Define.clazz("qubit.opentag.TagHelper", a);
          a.injectHTMLForLoader = function(k, q, d, b) {
              var g = b !== undefined ? b : k.getHtml();
              if (g) {
                  var m = k.config.locationPlaceHolder === "END";
                  var y = v.getHTMLLocationForTag(k);
                  k.injectHTMLNotFinished = true;
                  try {
                      if (y) v.injectHTML(y, m, g, function() {
                          k.injectHTMLNotFinished = false;
                          if (q) try {
                              q()
                          } catch (D) {}
                      }.bind(k));
                      else if (d && document.readyState === "loading") {
                          document.write(g);
                          k.injectHTMLNotFinished = false
                      } else k.injectHTMLFailed = (new Date).valueOf()
                  } catch (D) {
                      k.injectHTMLNotFinished = false;
                      k.injectHTMLFailed = (new Date).valueOf()
                  }
              }
          };
          a.getAllVariablesWithParameters = function(k) {
              var q = k.getPageVariables();
              var d = [];
              for (var b = 0; b < q.length; b++) {
                  var g = q[b];
                  var m = w(k, g);
                  for (var y = 0; y < m.length; y++) d.push({
                      parameter: m[y],
                      variable: g
                  })
              }
              return d
          };
          var f = {};
          a.allParameterVariablesReadyForTag = function(k, q) {
              var d = q;
              var b = true;
              var g = k.getPageVariables();
              for (var m = 0; m < g.length; m++) {
                  var y = g[m];
                  try {
                      var D = w(k, y);
                      var E = y.exists();
                      if (!E && d) {
                          if (D.length > 0) E = !!D[0].defaultValue;
                          E = E || y.exists(true)
                      }
                      if (!E) {
                          b = false;
                          break
                      }
                  } catch (F) {
                      b = false;
                      break
                  }
              }
              return b
          };
          var x = "2";
          var z = "3";
          var B = "4";
          var l = "5";
          a.validateAndGetVariableForParameter = function(k) {
              if (k.hasOwnProperty("variable") && k.variable) k.variable = a.initPageVariable(k.variable);
              return k.variable
          };
          a.initPageVariable = function(k) {
              if (!k || k instanceof n) return k;
              if (typeof k === "string") {
                  var q =
                      p.getObjectUsingPath(k);
                  if (q && q instanceof n) return q
              }
              switch (k.type) {
                  case x:
                      return new c(k);
                  case z:
                      return new C(k);
                  case B:
                      return new u(k);
                  case l:
                      return new t(k);
                  case "EPRESSION":
                      return new c(k);
                  case "URL_PARAMETER":
                      return new C(k);
                  case "COOKIE_VALUE":
                      return new u(k);
                  case "DOM_VALUE":
                      return new t(k);
                  default:
                      return new n(k)
              }
          }
      })();
      (function() {
          function a(f) {
              this.log = new r("", function() {
                  return this.CLASS_NAME + "[" + this.config.name + "]"
              }.bind(this), "collectLogs");
              this.urlsLoaded = 0;
              this.urlsFailed = 0;
              this.runCounter =
                  0;
              this.events = new A.Events({});
              this._depLoadedHandler = function() {
                  if (this.dependenciesLoaded() && this.awaitingDependencies) this._triggerLoadingAndExecution()
              }.bind(this);
              this.config = {
                  name: "Tag-" + c++,
                  async: true,
                  usesDocumentWrite: false,
                  timeout: this.LOADING_TIMEOUT,
                  dependencies: [],
                  url: null,
                  urlLocation: null,
                  locationPlaceHolder: "END",
                  locationObject: null,
                  dontWaitForInjectionLocation: false,
                  noMultipleLoad: false,
                  loadDependenciesOnLoad: false,
                  restartable: true
              };
              this.restartCounter = 0;
              this.cancelled = false;
              this.runtimeId =
                  p.UUID();
              this.delayDocWrite = false;
              this.dependencies = [];
              this._lockObject = {
                  count: 0
              };
              this._lockObjectDepsLoaded = {};
              this.genericDependencies = [];
              if (f) {
                  if (!f.name) {
                      var x = "Tag-" + c++;
                      this.config.name = x
                  }
                  this.addState("INITIAL");
                  for (var z in f) this.config[z] = f[z];
                  if (f.genericDependencies) this.genericDependencies = this.genericDependencies.concat(f.genericDependencies);
                  if (f.dependencies) this.dependencies = f.dependencies.concat(this.dependencies);
                  if (f.PACKAGE) this._package = f.PACKAGE;
                  this.onInit()
              }
          }
          function w() {
              if (this.cancelled) {
                  this._handleCancel();
                  return
              }
              this.waitForDependenciesFinished = (new Date).valueOf();
              var f = this._fullBodyNeededAndUnLoaded();
              var x = this._bodyNeededButNotAvailable();
              if (f || x) this.waitForDependenciesFinished = false;
              else {
                  if (!this.timeoutCountdownStart) this.timeoutCountdownStart = (new Date).valueOf();
                  if (this.allDependenciesLoaded()) this._markLoadedSuccesfuly();
                  else if (this._loadingOutOfTimeFrames()) {
                      this.loadingTimedOut = (new Date).valueOf();
                      if (this.allDependenciesLoaded(true)) this._markLoadedSuccesfuly();
                      else {
                          this.addState("TIMED_OUT");
                          this._markLoadingDependenciesFailed();
                          this._triggerOnLoadTimeout()
                      }
                  } else this.waitForDependenciesFinished = false
              }
              if (!this.waitForDependenciesFinished) this._setTimeout(w.bind(this), 65);
              else this.addState("LOADED_DEPENDENCIES")
          }
          var p = A.opentag.Utils;
          var v = A.opentag.TagsUtils;
          var e = A.opentag.Timed;
          var n = A.opentag.TagHelper;
          var c = 0;
          var r = A.opentag.Log;
          A.Define.clazz("qubit.opentag.GenericLoader", a);
          var t = "after";
          var u = "before";
          var C = "restart";
          a.prototype.EVENT_TYPES = {
              AFTER_EVENT: t,
              BEFORE_EVENT: u,
              RESTART_EVENT: C
          };
          a.prototype.onInit = N;
          a.prototype.LOADING_TIMEOUT = 5 * 1E3;
          a.prototype.getHtml = function() {
              if (this.config.html) return this.config.html;
              if (this.htmlContent) return p.trim(this.htmlContent);
              return null
          };
          a.prototype._executeScript = function() {
              var f = false;
              try {
                  this.script();
                  f = true
              } catch (x) {
                  this.addState("EXECUTED_WITH_ERRORS");
                  this.executedWithErrors = (new Date).valueOf();
                  this._onError(x)
              } finally {
                  this._onExecute(f)
              }
          };
          a.prototype.getTimeout = function() {
              return this._getTimeout()
          };
          a.prototype._getTimeout = function(f) {
              var x = +this.config.timeout;
              var z = this.dependencies;
              if (x !== -1 && z.length > 0) {
                  var B = 0;
                  f = f || [];
                  var l = p.indexInArray(f, this) !== -1;
                  if (!l) {
                      f[f.length] = this;
                      for (var k = 0; k < z.length; k++) {
                          var q = z[k]._getTimeout(f);
                          if (q > B) B = q
                      }
                      if (B > 0) x += B
                  } else return 0
              }
              return x
          };
          a.prototype._onExecute = function(f) {
              this.onExecute(f)
          };
          a.prototype.onExecute = N;
          a.prototype._flushDocWrites = function(f) {
              var x = true;
              this._docWriteNotFlushed = false;
              try {
                  var z = v.getHTMLLocationForTag(this);
                  if (z && this._securedWrites && this._securedWrites.length > 0) {
                      var B =
                          this.config.locationPlaceHolder === "END";
                      x = v.flushDocWritesArray(this._securedWrites, z, B, this.log, f);
                      if (x) this._docWriteFlushed = (new Date).valueOf();
                      else this._docWriteNotFlushed = (new Date).valueOf()
                  }
              } catch (l) {
                  this._onError(l)
              }
              if (f) f();
              if (this._securedWrites && this._securedWrites.length > 0) {
                  x = false;
                  this._docWriteNotFlushed = (new Date).valueOf()
              }
              return x
          };
          a.prototype.log = N;
          a.prototype.finished = function() {
              return !!this.runIsFinished
          };
          a.prototype.script = function() {};
          a.prototype.before = function() {
              this.beforeRun =
                  (new Date).valueOf();
              try {
                  this.events.call(u, this)
              } catch (f) {
                  this._onError(f)
              }
          };
          a.prototype.onBefore = function(f) {
              this.events.on(u, f)
          };
          a.prototype.after = function(f) {
              this.afterRun = (new Date).valueOf();
              try {
                  this.events.call(t, {
                      success: f,
                      tag: this
                  })
              } catch (x) {
                  this._onError(x)
              }
          };
          a.prototype.onAfter = function(f) {
              this.events.on(t, f)
          };
          a.prototype.runOnce = function() {
              if (!this._runOnceTriggered && !this.scriptExecuted) {
                  this._runOnceTriggered = (new Date).valueOf();
                  this.run()
              }
          };
          a.CANCEL_ALL = false;
          a.prototype.run = function() {
              if (!isNaN(this.config.runningLimit))
                  if (this.config.runningLimit <=
                      this.runCounter) return false;
              if (this.cancelled || a.CANCEL_ALL) {
                  this._handleCancel();
                  return false
              }
              if (this.isRunning) return false;
              if (this.lastRun) this.reset();
              this.lastRun = this.isRunning = (new Date).valueOf();
              this.runCounter++;
              this._ignoreDeps = !!this.ignoreDependencies;
              if (!this._ignoreDeps && !this.dependenciesLoaded()) {
                  this._attachDepsEventsToContinue();
                  return false
              }
              return this._triggerLoadingAndExecution()
          };
          a.prototype._triggerLoadingAndExecution = function() {
              this.awaitingDependencies = -(new Date).valueOf();
              this.load();
              if (this._ignoreDeps) this.execute();
              else this.waitForDependenciesAndExecute();
              return true
          };
          a.prototype._attachDepsEventsToContinue = function() {
              this.awaitingDependencies = (new Date).valueOf();
              var f = this.dependencies;
              for (var x = 0; x < f.length; x++) try {
                  f[x].events.on("success", this._depLoadedHandler)
              } catch (z) {}
          };
          a.prototype.dependenciesLoaded = function() {
              var f = this.dependencies;
              for (var x = 0; x < f.length; x++)
                  if (f[x] !== this) {
                      var z = +f[x].scriptExecuted > 0;
                      if (!z) return false
                  }
              return true
          };
          a.prototype._setTimeout =
              function(f, x) {
                  this._wasTimed = (new Date).valueOf();
                  var z = this.runtimeId;
                  var B = function() {
                      if (this.runtimeId === z) return f()
                  }.bind(this);
                  return e.setTimeout(B, x)
              };
          a.prototype._handleCancel = function() {
              this.addState("CANCELLED");
              try {
                  this.onCancel()
              } catch (f) {
                  this._onError(f)
              }
          };
          a.prototype.waitForDependenciesAndExecute = function() {
              if (this.loadedDependencies) this.execute();
              else if (this.loadingDependenciesFailed) {
                  this._markFailure();
                  this._markFinished()
              } else this._setTimeout(this.waitForDependenciesAndExecute.bind(this),
                  30)
          };
          a.prototype.execute = function() {
              this._triggerExecution()
          };
          a.prototype._triggerExecution = function() {
              if (this.cancelled) {
                  this._handleCancel();
                  return
              }
              if (this.scriptExecuted) return;
              var f = true;
              if (this.shouldWaitForDocWriteProtection()) f = false;
              else {
                  if (!this._beforeEntered) {
                      this._beforeEntered = (new Date).valueOf();
                      var x = false;
                      try {
                          x = this.before()
                      } catch (B) {
                          this._onError(B)
                      }
                      if (x) {
                          this._markFailure();
                          this._markFinished();
                          return
                      }
                  }
                  f = this.loadExecutionURLsAndHTML(this._triggerExecution.bind(this))
              }
              if (this.scriptExecuted) return;
              if (this.unexpectedFail) f = true;
              if (!f) this._setTimeout(this._triggerExecution.bind(this), 30);
              else {
                  this._flushDocWrites();
                  if (this.scriptLoadingFailed || this.injectHTMLFailed || this.unexpectedFail) this._markFailure();
                  else {
                      this.scriptExecuted = (new Date).valueOf();
                      this.addState("EXECUTED");
                      this._executeScript()
                  }
                  if (this.cancelled) {
                      this._handleCancel();
                      return false
                  } else {
                      var z = this.scriptExecuted > 0;
                      try {
                          if (!this.afterRun) {
                              this.afterRun = (new Date).valueOf();
                              this.after(z)
                          }
                      } catch (B) {
                          this.executedWithErrors = (new Date).valueOf()
                      }
                      if (!this.executedWithErrors)
                          if (z) this.events.call("success")
                  }
                  this._flushDocWrites();
                  this._markFinished()
              }
          };
          a.prototype._markFailure = function() {
              this.scriptExecuted = -(new Date).valueOf();
              this.addState("FAILED_TO_EXECUTE")
          };
          a.prototype._markFinished = function() {
              this.runIsFinished = (new Date).valueOf();
              this.isRunning = false;
              if (a.LOCK_DOC_WRITE === this) {
                  this._flushDocWrites();
                  v.unlockDocumentWrites();
                  a.LOCK_DOC_WRITE = false
              }
              this.onFinished(true)
          };
          a.prototype.onFinished = N;
          a.prototype.onCancel = N;
          a.prototype.onFinished = N;
          a.prototype.shouldWaitForDocWriteProtection = function() {
              if (this.willSecureDocumentWrite())
                  if (!a.LOCK_DOC_WRITE) {
                      a.LOCK_DOC_WRITE =
                          this;
                      this._secureWriteAndCollectForExecution()
                  } else if (a.LOCK_DOC_WRITE !== this) {
                  if (!this._lockedDocWriteInformed) this._lockedDocWriteInformed = (new Date).valueOf();
                  return true
              }
              return false
          };
          a.prototype.runWithoutDependencies = function() {
              this.ignoreDependencies = true;
              this.run()
          };
          a.prototype.loadExecutionURLsAndHTML = function(f) {
              if (this.cancelled) {
                  this._handleCancel();
                  return true
              }
              if (!this._loadExecutionURLsAndHTMLInformed) this._loadExecutionURLsAndHTMLInformed = true;
              this._triggerURLsLoading(f);
              if (!this.loadURLsNotFinished) {
                  this._flushDocWrites();
                  this._triggerHTMLInjection();
                  this._flushDocWrites();
                  if (!this.injectHTMLNotFinished) {
                      this._flushDocWrites();
                      if (!this._docWriteNotFlushed) {
                          if (this._docWriteFlushed);
                          return true
                      }
                  }
              }
              return false
          };
          a.prototype._triggerURLsLoading = function(f) {
              if (!this._urlLoadTriggered && this.config.url) {
                  this._urlLoadTriggered = true;
                  this.loadURLs(false, f)
              }
          };
          a.prototype._triggerHTMLInjection = function() {
              if (!this._injectHTMLTriggered && this.getHtml()) {
                  this._injectHTMLTriggered = true;
                  this.injectHTML()
              }
          };
          a.prototype.STATE = {
              INITIAL: 0,
              STARTED: 1,
              LOADING_DEPENDENCIES: 2,
              LOADED_DEPENDENCIES: 4,
              LOADING_URL: 8,
              LOADED_URL: 16,
              EXECUTED: 32,
              EXECUTED_WITH_ERRORS: 64,
              FAILED_TO_LOAD_DEPENDENCIES: 128,
              FAILED_TO_LOAD_URL: 256,
              FAILED_TO_EXECUTE: 512,
              TIMED_OUT: 1024,
              UNEXPECTED_FAIL: 2048,
              CANCELLED: 2048 * 2
          };
          a.prototype.addState = function(f) {
              if (this.STATE.hasOwnProperty(f)) {
                  this.state = this.state | this.STATE[f];
                  try {
                      this.onStateChange(f)
                  } catch (x) {
                      this._onError(x)
                  }
              }
          };
          a.prototype.onStateChange = N;
          a.prototype.cancel = function() {
              this.cancelled = (new Date).valueOf()
          };
          a.prototype.unCancel = function() {
              this.cancelled = false
          };
          a.prototype.state = a.prototype.STATE.INITIAL;
          a.prototype._markLoadedSuccesfuly = function() {
              this.loadedDependencies = (new Date).valueOf();
              this.onAllDependenciesLoaded()
          };
          a.prototype._secureWriteAndCollectForExecution = function() {
              if (!this._securedWrites) {
                  this._securedWrites = [];
                  v.redirectDocumentWritesToArray(this._securedWrites, this.log)
              }
          };
          a.prototype._markLoadingDependenciesFailed = function() {
              this.addState("FAILED_TO_LOAD_DEPENDENCIES");
              this.loadingDependenciesFailed =
                  (new Date).valueOf()
          };
          a.prototype.allDependenciesLoaded = function(f, x) {
              return this.getDependenciesToBeLoaded(f, x).length === 0
          };
          a.prototype.getDependenciesToBeLoaded = function(f, x) {
              var z = x || [];
              if (!this.injectionLocationReady()) z.push("injection location");
              var B;
              var l = this.dependencies;
              for (B = 0; B < l.length; B++)
                  if (l[B] !== this) {
                      var k = +l[B].scriptExecuted > 0;
                      if (!k) {
                          var q = l[B].config ? l[B].config.name : "anonymous";
                          z.push("dependant Tag with name -\x3e " + q)
                      }
                  }
              for (B = 0; B < this.genericDependencies.length; B++) {
                  var d =
                      this.genericDependencies[B](this);
                  if (!d) z.push("this.genericDependencies[" + B + "] (index: " + B + ")")
              }
              if (z !== "");
              return z
          };
          a.prototype.docWriteAsksToWaitForBody = function() {
              return !!(this.delayDocWrite && this.config.usesDocumentWrite)
          };
          a.prototype._bodyNeededButNotAvailable = function() {
              if (this._dontWaitForInjections()) return false;
              return this._isBodyLocationNeeded() && !v.bodyAvailable()
          };
          a.prototype._isBodyLocationNeeded = function() {
              if (!this.isLoadingAsynchronously()) return false;
              if (this._isBodyLocationSet()) return true;
              else {
                  var f = this.config.locationObject === "HEAD";
                  return f && this.config.locationPlaceHolder === "END"
              }
          };
          a.prototype._isBodyLocationSet = function() {
              var f = this.config.locationObject;
              return !f || f === "BODY"
          };
          a.prototype._fullBodyNeededAndUnLoaded = function() {
              if (this._dontWaitForInjections()) return false;
              var f = false;
              if (this._isBodyLocationNeeded()) f = this.config.locationPlaceHolder === "END";
              f = f || (this.fullbodyNeeded || this.docWriteAsksToWaitForBody());
              return f && !p.bodyReady()
          };
          a.prototype._dontWaitForInjections = function() {
              return this.config.dontWaitForInjectionLocation ||
                  this.dontWaitForInjectionLocation || a.dontWaitForInjectionLocation
          };
          a.prototype.injectionLocationReady = function() {
              if (this._dontWaitForInjections()) return true;
              if (this._fullBodyNeededAndUnLoaded()) return false;
              if (!this.isLoadingAsynchronously()) return true;
              return !!v.getHTMLLocationForTag(this)
          };
          a.prototype._loadingOutOfTimeFrames = function() {
              if (this.getTimeout() < 0) return false;
              return (new Date).valueOf() - this.timeoutCountdownStart > this.getTimeout()
          };
          a.prototype.loadDependencies = function() {
              this._loadDependencies()
          };
          a.prototype.addClientDependenciesList = function(f, x) {
              return this.addDependenciesList(f, A.Define.clientSpaceClasspath())
          };
          a.prototype.addDependenciesList = function(f, x) {
              if (!f || f.length === 0) return;
              if (!this.failedDependenciesToParse) this.failedDependenciesToParse = [];
              var z = this.dependencies;
              for (var B = 0; B < f.length; B++) {
                  var l = f[B];
                  var k = false;
                  if (l instanceof a) z.push(l);
                  else if (typeof l === "string") {
                      var q = l;
                      if (x) l = x + "." + l;
                      var d = p.getObjectUsingPath(l);
                      if (d)
                          if (d instanceof a) z.push(d);
                          else k = true;
                      else this.failedDependenciesToParse.push(q)
                  } else k =
                      true;
                  if (k) {
                      this.badDepsObjects = this.badDepsObjects || [];
                      p.addToArrayIfNotExist(this.badDepsObjects, l)
                  }
              }
          };
          a.prototype._loadDependencies = function(f) {
              f = f || [];
              var x = this.dependencies;
              var z = p.indexInArray(f, this) !== -1;
              if (!z) {
                  f[f.length] = this;
                  for (var B = 0; B < x.length; B++) x[B].load(f)
              }
          };
          a.prototype.onError = N;
          a.prototype._onError = function(f) {
              try {
                  this.onError(f)
              } catch (x) {}
          };
          a.prototype._triggerOnLoadTimeout = function() {
              this.onLoadTimeout()
          };
          a.prototype.onLoadTimeout = N;
          a.prototype.onScriptsLoadSuccess = N;
          a.prototype.onScriptLoadError =
              N;
          a.prototype.onAllDependenciesLoaded = N;
          a.prototype.onBeforeLoad = null;
          a.prototype.load = function() {
              if (this.loadStarted) return;
              else {
                  this.loadStarted = (new Date).valueOf();
                  try {
                      if (this.onBeforeLoad) this.onBeforeLoad()
                  } catch (f) {
                      this._onError(f)
                  }
              }
              this.addState("LOADING_DEPENDENCIES");
              try {
                  if (!this._ignoreDeps && this.config.loadDependenciesOnLoad) this.loadDependencies()
              } catch (f) {
                  throw f;
              }
              w.call(this)
          };
          a.prototype._singleUrlLoadHandler = function(f, x, z) {
              ++this.urlsLoaded;
              if (!f) ++this.urlsFailed;
              if (this.urlsLoaded ===
                  x.length) {
                  this.loadURLsNotFinished = false;
                  if (f && this.urlsFailed === 0) {
                      this.addState("LOADED_URL");
                      this.urlsLoaded = (new Date).valueOf();
                      try {
                          if (z) z(true)
                      } catch (l) {
                          this._onError(l)
                      } finally {
                          this.onScriptsLoadSuccess()
                      }
                  } else {
                      var B = "error loading urls. Failed " + this.urlsFailed;
                      this._onError(B);
                      this.addState("FAILED_TO_LOAD_URL");
                      this.urlsLoaded = -(new Date).valueOf();
                      try {
                          this.scriptLoadingFailed = true;
                          if (z) z(false)
                      } catch (l) {
                          this._onError(l)
                      } finally {
                          this.onScriptLoadError(B)
                      }
                  }
              }
          };
          a.prototype.loadURLs = function(f,
              x) {
              var z = f || this.config.url;
              this.addState("LOADING_URL");
              try {
                  if (z && !(z instanceof Array)) z = [z];
                  for (var B = 0; B < z.length; B++) {
                      this.loadURLsNotFinished = true;
                      var l = z[B];
                      l = this.prepareURL(l);
                      this.loadURL(l, function(k) {
                          this._singleUrlLoadHandler(k, z, x)
                      }.bind(this))
                  }
              } catch (k) {
                  this.loadURLsNotFinished = false;
                  this.addState("UNEXPECTED_FAIL");
                  this.unexpectedFail = (new Date).valueOf();
                  this._onError(k)
              }
          };
          a.prototype.prepareLocationObject = function(f) {
              return f
          };
          a.prototype.prepareURL = function(f) {
              return f
          };
          a.prototype.prepareHTML =
              function(f) {
                  return f
              };
          a.prototype.loadURL = function(f, x, z) {
              var B = f;
              this.addState("LOADING_URL");
              v.loadScript({
                  onsuccess: function() {
                      try {
                          if (x) x(true)
                      } catch (l) {}
                  }.bind(this),
                  onerror: function() {
                      try {
                          if (x) x(false)
                      } catch (l) {}
                  }.bind(this),
                  url: B,
                  node: z || this.config.urlLocation,
                  async: this.isLoadingAsynchronously(),
                  noMultipleLoad: this.config.noMultipleLoad
              })
          };
          a.prototype.onRestart = function(f) {
              this.events.on(C, f)
          };
          a.prototype.prepareForRestart = function() {
              if (this.config.restartable) {
                  var f = this.restartCounter + 1;
                  this.events.call(C, this);
                  this.reset();
                  this.restartCounter = f
              }
          };
          a.prototype.restart = function() {
              if (this.config.restartable) {
                  this.prepareForRestart();
                  this.run()
              }
          };
          a.prototype.reset = function() {
              var f;
              this._injectHTMLTriggered = f;
              this._loadExecutionURLsAndHTMLInformed = f;
              this._lockedDocWriteInformed = f;
              this._runOnceTriggered = f;
              this._urlLoadTriggered = f;
              this.afterRun = f;
              this.beforeRun = f;
              this.filtersRunTriggered = f;
              this.injectHTMLFailed = f;
              this.loadStarted = f;
              this.loadURLsNotFinished = f;
              this.loadedDependencies = f;
              this.loadingDependenciesFailed =
                  f;
              this.loadingTimedOut = f;
              this.runIsFinished = f;
              this.scriptExecuted = f;
              this.scriptLoadingFailed = f;
              this.delayDocWrite = f;
              this._securedWrites = f;
              this.state = 0;
              this.unexpectedFail = f;
              this.urlsFailed = 0;
              this.urlsLoaded = 0;
              this.waitForDependenciesFinished = f;
              this.isRunning = f;
              this.lastRun = f;
              this.cancelled = f;
              this._beforeEntered = f;
              this.awaitingDependencies = f;
              this.timeoutCountdownStart = f;
              this.restartCounter = 0;
              this.runtimeId = p.UUID();
              this.addState("INITIAL")
          };
          a.prototype.isLoadingAsynchronously = function() {
              if (this._wasTimed) return true;
              return !!(this.config.async || this.forceAsynchronous)
          };
          a.prototype.willSecureDocumentWrite = function() {
              return this.config.usesDocumentWrite && this.isLoadingAsynchronously()
          };
          a.prototype.injectHTML = function(f) {
              var x = !this.docWriteAsksToWaitForBody();
              var z = this.prepareHTML(this.getHtml());
              if (z) n.injectHTMLForLoader(this, f, x, z)
          };
          a.prototype.clone = function() {
              var f = new a(this.config);
              return f
          }
      })();
      (function() {
          function a(d) {
              var b = {
                  filterTimeout: d && d.filterTimeout || this.FILTER_WAIT_TIMEOUT,
                  PACKAGE: d && d.PACKAGE,
                  dedupe: false,
                  needsConsent: false,
                  inactive: false,
                  variables: null,
                  runner: null,
                  disabled: false,
                  locked: false,
                  reRunOnVariableChange: false
              };
              p.setIfUnset(d, b);
              a.SUPER.apply(this, arguments);
              this.namedVariables = {};
              this._runtimeMarkers = {};
              this.parameters = [];
              this.filters = [];
              this.session = null;
              this.owningContainer = null;
              this.pingSent = false;
              this.oldPingSent = false;
              this.dedupePingSent = false;
              this.oldDedupePingSent = false;
              this.reRunCounter = 0;
              if (d) {
                  this.addState("INITIAL");
                  try {
                      a.register(this)
                  } catch (g) {}
                  this.setupConfig(d);
                  this.uniqueRefString = null;
                  if (d.init) try {
                      d.init.call(this, d)
                  } catch (g) {}
                  this.onTagInit();
                  this.handleVariableChange = this._handleVariableChange.bind(this)
              }
          }
          function w(d, b) {
              var g = d.namedVariables;
              var m = r.initPageVariable(g[b]);
              g[b] = m;
              return m
          }
          var p = A.opentag.Utils;
          var v = A.opentag.TagsUtils;
          var e = A.opentag.Timed;
          var n = A.opentag.filter.BaseFilter;
          var c = A.opentag.GenericLoader;
          var r = A.opentag.TagHelper;
          var t = A.opentag.pagevariable.BaseVariable;
          var u = A.Cookie;
          var C = A.Define;
          C.clazz("qubit.opentag.BaseTag",
              a, c);
          a.prototype.setupConfig = function(d) {
              if (!d) return;
              if (d.filters) this.addFilters(d.filters);
              if (d.parameters) this.addParameters(d.parameters);
              if (d.variables)
                  for (var b in d.variables)
                      if (d.variables.hasOwnProperty(b)) {
                          var g = this.getParameterByTokenName(b);
                          if (g) {
                              var m = d.variables[b];
                              g.variable = m;
                              if (m.defaultValue !== undefined) g.defaultValue = m.defaultValue;
                              if (m.uv !== undefined) g.uv = m.uv
                          }
                      }
              if (d.locked) this.lock()
          };
          a.prototype.valueForToken = function(d, b) {
              var g = this.getParameterByTokenName(d);
              if (g) {
                  if (b ===
                      undefined)
                      if (this.loadingTimedOut) b = true;
                  return this.getParameterValue(g, b)
              }
              var m = this.namedVariables;
              if (m && m[d]) {
                  var y = w(this, d);
                  if (y) return y.getRelativeValue(b)
              }
              return undefined
          };
          a.prototype.addParameters = function(d) {
              this.parameters = this.parameters.concat(d)
          };
          a.prototype.LOADING_TIMEOUT = 5 * 1E3;
          a.prototype.FILTER_WAIT_TIMEOUT = -1;
          a.prototype._isVariable = function(d) {
              return d !== null && d !== undefined
          };
          a.prototype.run = function() {
              if (this.destroyed) throw "Tag is destroyed.";
              this.resolveAllDynamicData();
              this.runPostInitialisationSection();
              var d = this.parameters;
              if (d)
                  for (var b = 0; b < d.length; b++) {
                      var g = this.getVariableForParameter(d[b]);
                      if (!this._isVariable(g)) {
                          this._markLoadingDependenciesFailed();
                          this._markFinished();
                          return
                      }
                  }
              if (this.config.runner) {
                  var m = false;
                  try {
                      this.addState("AWAITING_CALLBACK");
                      m = this._runner = (new Date).valueOf();
                      this.config.runner.call(this)
                  } catch (y) {}
                  return m
              } else {
                  this._runner = false;
                  return this.start()
              }
          };
          a.prototype.lock = function() {
              this.locked = true;
              this._unlock = null
          };
          a.prototype.unlock = function() {
              this.locked = false;
              if (this._unlock) {
                  this._unlock();
                  this._unlock = false
              }
          };
          a.prototype.start = function() {
              if (!this.locked) return a.SUPER.prototype.run.call(this);
              else {
                  this._unlock = function() {
                      return a.SUPER.prototype.run.call(this)
                  }.bind(this);
                  return false
              }
          };
          a.prototype.startOnce = function() {
              if (!this.locked) return a.SUPER.prototype.runOnce.call(this);
              else {
                  this._unlock = function() {
                      return a.SUPER.prototype.runOnce.call(this)
                  }.bind(this);
                  return false
              }
          };
          a.prototype.getFilters = function() {
              return this.filters
          };
          a.prototype.runOnceIfFiltersPass =
              function() {
                  if (!this._runOnceIfFiltersPassTriggered && !this.scriptExecuted) {
                      this._runOnceIfFiltersPassTriggered = (new Date).valueOf();
                      this.runIfFiltersPass()
                  }
              };
          a.prototype.runIfFiltersPass = function() {
              if (this.destroyed) throw "Tag is destroyed.";
              this.resolveAllDynamicData();
              this.runPostInitialisationSection();
              var d = this.filtersState(true);
              this.addState("FILTER_ACTIVE");
              if (!this.filtersRunTriggered) this.filtersRunTriggered = (new Date).valueOf();
              if (d === n.state.SESSION) {
                  this.addState("AWAITING_CALLBACK");
                  this.awaitingCallback =
                      (new Date).valueOf()
              } else if (d === n.state.PASS) {
                  this.filtersPassed = (new Date).valueOf();
                  try {
                      this.onFiltersPassed()
                  } catch (g) {}
                  this.run()
              } else if (d === n.state.FAIL) {
                  this._markFiltersFailed();
                  this._markFinished()
              } else if (d > 0) {
                  var b = this.config.filterTimeout;
                  if (b < 0 || (new Date).valueOf() - this.filtersRunTriggered > b) {
                      if (!this._awaitingForFilterInformed) {
                          this._awaitingForFilterInformed = (new Date).valueOf();
                          try {
                              this.onFiltersDelayed()
                          } catch (g) {}
                      }
                      this._setTimeout(this.runIfFiltersPass.bind(this), d)
                  } else {
                      this._markFiltersFailed();
                      this._markFinished();
                      this.filtersRunTimedOut = (new Date).valueOf()
                  }
              }
              try {
                  this.onFiltersCheck(d)
              } catch (g) {}
              return d
          };
          a.prototype._markFiltersFailed = function() {
              this.addState("FILTERS_FAILED");
              this.filtersPassed = -(new Date).valueOf()
          };
          a.prototype.STATE = {
              INITIAL: 0,
              FILTER_ACTIVE: 1,
              AWAITING_CALLBACK: 2,
              FILTERS_FAILED: 4,
              STARTED: 8,
              LOADING_DEPENDENCIES: 16,
              LOADED_DEPENDENCIES: 32,
              LOADING_URL: 64,
              LOADED_URL: 128,
              EXECUTED: 256,
              EXECUTED_WITH_ERRORS: 512,
              FAILED_TO_LOAD_DEPENDENCIES: 1024,
              FAILED_TO_LOAD_URL: 2048,
              FAILED_TO_EXECUTE: 4096,
              TIMED_OUT: 4096 * 2,
              UNEXPECTED_FAIL: 4096 * 2 * 2,
              CANCELLED: 4096 * 2 * 2 * 2
          };
          a.prototype.addState = function(d) {
              a.SUPER.prototype.addState.call(this, d);
              try {
                  a.onStateChange(this)
              } catch (y) {}
              this.stateStack = [];
              var b = this.STATE;
              var g = this.state;
              var m = this.stateStack;
              if (g & b.INITIAL) m.push("Initial state.");
              if (g & b.FILTER_ACTIVE) m.push("Tag running with filters pass triggered.");
              if (g & b.FILTERS_FAILED) m.push("Filters failed to pass.");
              if (g & b.AWAITING_CALLBACK) m.push("Awaiting callback to run this tag. Not pooling.");
              if (g &
                  b.STARTED) m.push("Tag is initialised and loading has been started.");
              if (g & b.LOADING_DEPENDENCIES) m.push("Dependencies are being loaded.");
              if (g & b.LOADED_DEPENDENCIES) m.push("Dependencies loading process has been finished.");
              if (g & b.LOADING_URL) m.push("External URL is being loaded.");
              if (g & b.LOADED_URL) m.push("External URL has been loaded.");
              if (g & b.EXECUTED) m.push("Main script has been executed.");
              if (g & b.EXECUTED_WITH_ERRORS) m.push("Main script has been executed but errors occured.");
              if (g & b.FAILED_TO_LOAD_DEPENDENCIES) m.push("Dependencies has failed to load.");
              if (g & b.FAILED_TO_LOAD_URL) m.push("URL location failed to load.");
              if (g & b.FAILED_TO_EXECUTE) m.push("Script failed to execute.");
              if (g & b.TIMED_OUT) m.push("Script timed out awaiting for dependencies.");
              if (g & b.UNEXPECTED_FAIL) m.push("Script occured UNEXPECTED exception and is failed.");
              if (g & b.CANCELLED) m.push("Tag has been cancelled.")
          };
          a.prototype.onTagInit = N;
          a.prototype.postInitialisationSection = N;
          a.onStateChange = N;
          a.prototype.onFiltersDelayed = N;
          a.prototype.onFiltersPassed = N;
          a.prototype.onFiltersCheck =
              N;
          a.prototype.state = a.prototype.STATE.INITIAL;
          a.prototype.arePageVariablesLoaded = function(d) {
              return r.allParameterVariablesReadyForTag(this, d)
          };
          a.prototype.getDependenciesToBeLoaded = function(d, b) {
              var g = b || [];
              if (!this.arePageVariablesLoaded(d)) g.push("page variables");
              return a.SUPER.prototype.getDependenciesToBeLoaded.call(this, d, g)
          };
          a.prototype.resolveAllDynamicData = function() {
              this.resolvePageVariables();
              this.resolveDependencies();
              this.resolveFilters()
          };
          a.prototype.resolveDependencies = function() {
              var d =
                  this.failedDependenciesToParse;
              if (d) {
                  this.failedDependenciesToParse = null;
                  this.addDependenciesList(d, C.clientSpaceClasspath())
              }
              return this.dependencies
          };
          a.prototype.addClientVariablesMap = function(d) {
              this.unresolvedClientVariablesMap = this._addVariablesMap(d, C.clientSpaceClasspath());
              return this.unresolvedClientVariablesMap
          };
          a.prototype.resolvePageVariables = function() {
              var d = this.unresolvedClientVariablesMap;
              if (d) {
                  this.unresolvedClientVariablesMap = null;
                  this.addClientVariablesMap(d)
              }
              d = this.unresolvedVariablesMap;
              if (d) {
                  this.unresolvedVariablesMap = null;
                  this.addVariablesMap(d)
              }
              return this.getPageVariables()
          };
          a.prototype.addVariablesMap = function(d) {
              this.unresolvedVariablesMap = this._addVariablesMap(d);
              return this.unresolvedVariablesMap
          };
          a.prototype._addVariablesMap = function(d, b) {
              if (!d) return;
              var g = {};
              var m = this.namedVariables;
              for (var y in d)
                  if (d.hasOwnProperty(y)) {
                      var D = d[y];
                      if (D instanceof t) m[y] = D;
                      else if (typeof D === "string") {
                          var E = D;
                          if (b) D = b + "." + D;
                          var F = p.getObjectUsingPath(D);
                          if (F)
                              if (F instanceof t) m[y] = F;
                              else m[y] = D;
                          else g[y] = E
                      } else;
                  }
              return g
          };
          a.prototype.prepareURL = function(d) {
              return this.replaceTokensWithValues(d)
          };
          a.prototype.prepareLocationObject = function(d) {
              return this.replaceTokensWithValues(d)
          };
          a.prototype.prepareHTML = function(d) {
              if (d) d = this.replaceTokensWithValues(d);
              return d
          };
          a.prototype._executeScript = function() {
              if (this.config && this.config.script)
                  if (typeof this.config.script === "function") this.script = this.config.script;
                  else {
                      var d = this.replaceTokensWithValues(String(this.config.script));
                      this.script =
                          p.expressionToFunction(d).bind(this)
                  }
              a.SUPER.prototype._executeScript.call(this)
          };
          a.prototype.replaceTokensWithValues = function(d) {
              if (!d || d.indexOf("${") === -1) return d;
              var b = this.parameters;
              if (b)
                  for (var g = 0; g < b.length; g++) {
                      var m = b[g];
                      var y = this.getVariableForParameter(m);
                      if (y) {
                          var D = b[g].token;
                          var E = this.valueForToken(D);
                          d = y.replaceToken(D, d, E)
                      }
                  }
              return d
          };
          a.prototype.getParameter = function(d) {
              var b = this.parameters;
              var g = null;
              if (b)
                  for (var m = 0; m < b.length; m++)
                      if (b[m].name === d) g = b[m];
              return g
          };
          a.prototype.setParameterValueByTokenName =
              function(d, b) {
                  var g = this.getParameterByTokenName(d);
                  if (g !== null) {
                      g.variable = {
                          value: b
                      };
                      return true
                  }
                  return false
              };
          a.prototype.getParameterValue = function(d, b) {
              var g = typeof d === "string" ? this.getParameter(d) : d;
              if (g) {
                  var m = this.getVariableForParameter(g);
                  if (m) try {
                      var y;
                      if (b && g.defaultValue !== "") y = p.gevalAndReturn(g.defaultValue).result;
                      y = m.getRelativeValue(b, y);
                      return y
                  } catch (D) {
                      return undefined
                  }
              }
              return undefined
          };
          a.prototype.filtersState = function(d) {
              var b = d;
              return v.filtersState(this.filters, this.session,
                  this, b)
          };
          a.prototype.addFilter = function(d) {
              p.addToArrayIfNotExist(this.filters, d)
          };
          a.prototype.addFilters = function(d) {
              for (var b = 0; b < d.length; b++) this.addFilter(d[b])
          };
          a.prototype.addClientFiltersList = function(d) {
              this.unresolvedClientFilterClasspaths = this.addFiltersList(d, C.clientSpaceClasspath());
              return this.unresolvedClientFilterClasspaths
          };
          a.prototype.resolveFilters = function() {
              var d = this.unresolvedClientFilterClasspaths;
              if (d) {
                  this.unresolvedClientFilterClasspaths = null;
                  this.addClientFiltersList(d)
              }
              return this.filters
          };
          a.prototype.addFiltersList = function(d, b) {
              var g = [];
              for (var m = 0; m < d.length; m++) try {
                  var y = d[m];
                  var D = y;
                  var E = null;
                  if (typeof y === "string") {
                      E = y;
                      if (b) y = b + "." + y;
                      y = p.getObjectUsingPath(y)
                  }
                  if (typeof y === "function") {
                      var F = y;
                      y = new F
                  }
                  if (!y);
                  if (!y instanceof n) y = null;
                  if (y) this.addFilter(y);
                  else if (E) p.addToArrayIfNotExist(g, E)
              } catch (G) {
                  this.failedFilters = this.failedFilters || [];
                  this.failedFilters.push(d[m])
              }
              return g
          };
          a.prototype.reset = function() {
              a.SUPER.prototype.reset.call(this);
              var d;
              this.filtersPassed = d;
              this.dedupePingSent =
                  false;
              this.oldDedupePingSent = false;
              this.pingSent = false;
              this.oldPingSent = false;
              this._runOnceIfFiltersPassTriggered = d;
              this.filtersRunTriggered = d;
              this._runner = d;
              this.reRunCounter = 0;
              this.detachVariablesChangedListeners()
          };
          a.prototype.resetFilters = function() {
              for (var d = 0; d < this.filters.length; d++) this.filters[d].reset()
          };
          a.prototype.getParameterByTokenName = function(d) {
              var b = null;
              if (this.parameters) {
                  var g = this.parameters;
                  for (var m = 0; m < g.length; m++)
                      if (g[m].token === d) b = g[m]
              }
              return b
          };
          a.prototype.removeFilter =
              function(d) {
                  p.removeFromArray(this.filters, d)
              };
          var f = 0;
          var x = [];
          var z = {};
          var B = {};
          a.register = function(d) {
              var b = p.addToArrayIfNotExist(x, d);
              if (b === -1) d._tagIndex = x.length - 1;
              else d._tagIndex = b;
              if (d.config.id) {
                  var g = "Q" + d.config.id;
                  B[g] = d;
                  d.uniqueId = g
              } else B[d.CLASSPATH] = d
          };
          a.getById = function(d) {
              return B[String(d)]
          };
          a.prototype.getById = a.getById;
          a.prototype.unregister = function(d) {
              a.unregister(d || this)
          };
          a.prototype.destroy = function() {
              this.destroyed = true;
              this.cancel();
              a.unregister(this)
          };
          a.unregister =
              function(d) {
                  var b = p.removeFromArray(x, d);
                  d._tagIndex = -1
              };
          a.getTags = function() {
              return x
          };
          a.getAccessorsMap = function() {
              return z
          };
          a.prototype.getTags = function() {
              return x
          };
          a.prototype.getPageVariables = function() {
              var d = this.parameters;
              var b = [];
              if (d)
                  for (var g = 0; g < d.length; g++) {
                      var m = this.getVariableForParameter(d[g]);
                      if (this._isVariable(m)) p.addToArrayIfNotExist(b, m)
                  }
              if (this.namedVariables)
                  for (var y in this.namedVariables) p.addToArrayIfNotExist(b, w(this, y));
              return b
          };
          a.prototype.getAccessorString = function() {
              if (!this._accessorsMapKey) {
                  this._accessorsMapKey =
                      "_" + f++;
                  z[this._accessorsMapKey] = this
              }
              return "qubit.opentag.BaseTag.getAccessorsMap()." + this._accessorsMapKey
          };
          a.prototype.getVariableForParameter = function(d) {
              var b;
              var g = this.namedVariables;
              if (g && g[d.token]) b = w(this, d.token);
              if (!b) b = r.validateAndGetVariableForParameter(d);
              return b
          };
          a.prototype.printVariablesState = function() {
              var d = [];
              var b = r.getAllVariablesWithParameters(this);
              for (var g = 0; g < b.length; g++) {
                  var m = b[g].parameter;
                  var y = b[g].variable;
                  var D;
                  if (m && m.token) D = this.valueForToken(m.token);
                  else D =
                      y.getRelativeValue(true);
                  var E = {
                      name: y.config.name,
                      exists: y.exists(),
                      token: m ? m.token : null,
                      value: D,
                      variable: y
                  };
                  d.push(E)
              }
              return d
          };
          a.prototype._triggerOnLoadTimeout = function() {
              this.onLoadTimeout()
          };
          a.prototype.getId = function() {
              return this._getUniqueId()
          };
          a.prototype._getUniqueId = function() {
              if (this.config.id) return this.config.id;
              if (String(this.CLASSPATH).indexOf(C.STANDARD_CS_NS) === 0) return this.CLASSPATH.substring(C.STANDARD_CS_NS.length + 1);
              else return this.CLASSPATH + "#" + this.config.name
          };
          var l = "qubit.tag.forceRunning_";
          var k = "qubit.tag.disableRunning_";
          var q = "qubit.tag.forceAllToRun";
          a.prototype.cookieSaysToRunEvenIfDisabled = function() {
              var d = this._getUniqueId();
              var b = !!u.get(q);
              if (!b) b = !!u.get(l + d);
              return b
          };
          a.prototype.setCookieForcingTagToRun = function() {
              var d = this._getUniqueId();
              u.set(l + d, "true")
          };
          a.setCookieForcingTagsToRun = function() {
              u.set(q, "true")
          };
          a.prototype.setCookieToDisable = function() {
              var d = this._getUniqueId();
              u.set(k + d, "true")
          };
          a.prototype.rmCookieToDisable = function() {
              var d = this._getUniqueId();
              u.rm(k +
                  d)
          };
          a.prototype.disabledByCookie = function() {
              var d = this._getUniqueId();
              return !!u.get(k + d)
          };
          a.rmCookieForcingTagsToRun = function() {
              u.rm(q)
          };
          a.prototype.rmCookieForcingTagToRun = function() {
              var d = this._getUniqueId();
              u.rm(l + d)
          };
          a.rmAllDisablingCookies = function() {
              p.rmCookiesMatching(k)
          };
          a.rmAllCookiesForcingTagToRun = function() {
              p.rmCookiesMatching(l);
              a.rmCookieForcingTagsToRun()
          };
          a.prototype.onVariableChanged = function(d) {
              this.attachVariablesChangedListeners(true);
              this.events.on("variableChanged", d)
          };
          a.prototype._handleVariableChange =
              function(d) {
                  this.events.call("variableChanged", d);
                  if (!this.isRunning && this.config.reRunOnVariableChange) {
                      if (!isNaN(this.config.reRunLimit))
                          if (this.config.reRunLimit <= this.reRunCounter) return;
                      var b = this.reRunCounter + 1;
                      this.reset();
                      this.reRunCounter = b;
                      this.resetFilters();
                      this.runIfFiltersPass()
                  }
              };
          a.prototype.attachVariablesChangedListeners = function(d) {
              var b = this.getPageVariables();
              var g = !!(this.config.reRunOnVariableChange || this.config.observeVariables || d);
              if (g)
                  for (var m = 0; m < b.length; m++) {
                      var y = b[m];
                      y.onValueChanged(this.handleVariableChange, g)
                  }
          };
          a.prototype.detachVariablesChangedListeners = function() {
              var d = this.getPageVariables();
              for (var b = 0; b < d.length; b++) {
                  var g = d[b];
                  g.deatchOnValueChanged(this.handleVariableChange)
              }
          };
          a.prototype._markFinished = function() {
              a.SUPER.prototype._markFinished.call(this);
              this.attachVariablesChangedListeners()
          };
          a.prototype.prepareForRestart = function(d) {
              if (this.config.restartable) {
                  a.SUPER.prototype.prepareForRestart.call(this, false);
                  if (!d) {
                      var b = this.getFilters();
                      for (var g =
                              0; g < b.length; g++) b[g].prepareForRestart()
                  }
              }
          };
          a.prototype.restart = function(d) {
              if (this.config.restartable) {
                  this.prepareForRestart(d);
                  this.runIfFiltersPass()
              }
          };
          a.prototype.runPostInitialisationSection = function() {
              if (!this._postInitialisationRun) {
                  this._postInitialisationRun = true;
                  if (this.postInitialisationSection) try {
                      this.postInitialisationSection()
                  } catch (d) {}
              }
          }
      })();
      (function() {
          function a(e) {
              p.setIfUnset(e, a.defaultConfig);
              if (this.singleton) {
                  var n = this.PACKAGE_NAME + "." + this.CLASS_NAME;
                  var c = A.opentag.Utils.getObjectUsingPath(n,
                      L);
                  if (c.__instance) return c.__instance;
                  c.__instance = this
              }
              a.SUPER.call(this, e)
          }
          function w(e, n) {
              var c = e.toString();
              c = c.replace(/\s*function\s*\([\w\s,_\d\$]*\)\s*\{/, "");
              c = c.substring(0, c.lastIndexOf("}"));
              c = c.replace(/(["']\s*\+\s*)\s*_*\w+\s*\.\s*valueForToken\s*\(\s*'([^']*)'\s*\)/g, '$1"${$2}"');
              c = c.replace(/\s*_*\w+\s*\.\s*valueForToken\s*\(\s*'([^']*)'\s*\)(\s*\+\s*["'])/g, '"${$1}"$2');
              c = c.replace(/(["']\s*\+\s*)\s*_*\w+\s*\.\s*valueForToken\s*\(\s*"([^"]*)"\s*\)/g, '$1"${$2}"');
              c = c.replace(/\s*_*\w+\s*\.\s*valueForToken\s*\(\s*"([^"]*)"\s*\)(\s*\+\s*["'])/g,
                  '"${$1}"$2');
              c = c.replace(/(\s*)_*\w+\s*\.\s*valueForToken\s*\(\s*'([^']*)'(\s*)\)/g, "$1${$2}$3");
              c = c.replace(/(\s*)_*\w+\s*\.\s*valueForToken\s*\(\s*"([^"]*)"(\s*)\)/g, "$1${$2}$3");
              c = n.replaceTokensWithValues(c);
              p.geval(c)
          }
          var p = A.opentag.Utils;
          var v = A.Define;
          A.Define.clazz("qubit.opentag.LibraryTag", a, A.opentag.BaseTag);
          a.defaultConfig = {
              vendor: null,
              imageUrl: null,
              description: "",
              async: false,
              isPrivate: false,
              upgradeable: true,
              html: "",
              parameters: [],
              prePostWindowScope: false
          };
          a.prototype.pre = function() {};
          a.prototype.post = function() {};
          a.prototype.before = function() {
              a.SUPER.prototype.before.call(this);
              try {
                  var e = this.config;
                  if (e && e.pre)
                      if (typeof e.pre === "function") this.pre = e.pre;
                      else {
                          var n = this.replaceTokensWithValues(String(e.pre));
                          this.pre = p.expressionToFunction(n).bind(this)
                      }
                  if (this.config.prePostWindowScope && this.pre !== a.prototype.pre && this.pre.toString) w(this.pre, this);
                  else this.pre()
              } catch (c) {
                  return true
              }
              return false
          };
          a.prototype.after = function(e) {
              a.SUPER.prototype.after.call(this, e);
              try {
                  var n =
                      this.config;
                  if (n && n.post)
                      if (typeof n.post === "function") this.post = n.post;
                      else {
                          var c = this.replaceTokensWithValues(String(n.post));
                          this.post = p.expressionToFunction(c).bind(this)
                      }
                  if (this.config.prePostWindowScope && this.post !== a.prototype.post && this.post.toString) w(this.post, this);
                  else this.post(e)
              } catch (r) {}
          };
          a.define = function(e, n) {
              e = e.replace(/^[\.]+/g, "").replace(/[\.]+$/g, "").replace(/\.+/g, ".");
              e = A.Define.vendorsSpaceClasspath(e);
              var c = {};
              if (n.getDefaultConfig) c = n.getDefaultConfig();
              var r = n.CONSTRUCTOR;
              var t = {};
              for (var u in n)
                  if (u !== "config") t[u] = n[u];
              var C = function(z) {
                  z = z || {};
                  z = p.overrideFromLeftToRight(c, z);
                  var B = A.opentag.LibraryTag.call(this, z);
                  if (r) r.call(this, z);
                  if (B) return B
              };
              var f = A.opentag.Utils.defineWrappedClass(e, a, t, K, C);
              var x = v.STANDARD_VS_NS + ".";
              if (e.indexOf(x) !== 0) p.namespace(x + e, f);
              return f
          };
          a.getLibraryByClasspath = function(e) {
              return p.getObjectUsingPath(e, A.Define.getVendorSpace())
          }
      })();
      (function() {
          function a(p) {
              var v = {
                  url: null,
                  html: "",
                  locationPlaceHolder: "NOT_END",
                  locationObject: "BODY",
                  async: true
              };
              w.setIfUnset(p, v);
              a.SUPER.call(this, p)
          }
          var w = A.opentag.Utils;
          A.Define.clazz("qubit.opentag.CustomTag", a, A.opentag.LibraryTag)
      })();
      (function() {
          var a = A.opentag.Utils;
          var w = A.opentag.filter.BaseFilter;
          var p = function() {};
          A.Define.clazz("qubit.opentag.Tags", p);
          p.getById = function(e) {
              return A.opentag.BaseTag.getById(String(e))
          };
          p.getAllTagsByState = function() {
              return A.opentag.Container.getAllTagsByState(p.getTags())
          };
          p.findTagByName = function(e) {
              var n = this.getTags();
              var c = [];
              for (var r = 0; r < n.length; r++)
                  if (n[r].config.name ===
                      e) c.push(n[r]);
              return c
          };
          p.findTagByMatch = function(e) {
              var n = this.getTags();
              var c = [];
              for (var r = 0; r < n.length; r++)
                  if (n[r].config.name.match(e)) c.push(n[r]);
              return c
          };
          p.findTagContainers = function(e) {
              var n = p.getContainers();
              var c = [];
              for (var r = 0; r < n.length; r++) {
                  var t = n[r].tags;
                  for (var u in t)
                      if (t[u] === e) {
                          c.push(n[r]);
                          break
                      }
              }
              return c
          };
          p.getTags = function() {
              return A.opentag.BaseTag.getTags()
          };
          p.resetAllTags = function(e) {
              var n = p.getTags();
              for (var c = 0; c < n.length; c++) {
                  n[c].reset();
                  if (!e) n[c].resetFilters()
              }
          };
          p.getContainersPageVariables = function() {
              var e = p.getContainers();
              var n = [];
              for (var c = 0; c < e.length; c++) n = n.concat(e.getPageVariables());
              return n
          };
          p.getAllPageVariables = function() {
              var e = p.getTags();
              var n = [];
              for (var c = 0; c < e.length; c++) n = n.concat(e[c].getPageVariables());
              return n
          };
          p.cancelAll = function() {
              var e = p.getTags();
              for (var n = 0; n < e.length; n++) e[n].cancel()
          };
          p.resetAll = function(e) {
              var n = p.getTags();
              for (var c = 0; c < n.length; c++) {
                  n[c].reset();
                  if (!e) n[c].resetFilters()
              }
          };
          p.getPageVariableByName = function(e) {
              var n =
                  p.getAllPageVariables();
              var c = [];
              for (var r = 0; r < n.length; r++)
                  if (n[r].config.name === e) c.push(n[r]);
              return c
          };
          p.getLoadTime = function(e) {
              var n = e.beforeRun;
              var c = e.runIsFinished;
              if (isNaN(c)) return {
                  tag: e,
                  loadTime: null
              };
              else return {
                  tag: e,
                  loadTime: c - n
              }
          };
          p.getLoadTimes = function(e) {
              var n = [];
              if (e instanceof A.opentag.BaseTag) {
                  n.push([p.getLoadTime(e[t])]);
                  return n
              }
              e = e || p.getTags();
              var c = e instanceof Array;
              if (c)
                  for (var r = 0; r < e.length; r++) {
                      if (e[r] instanceof A.opentag.BaseTag) n.push(p.getLoadTime(e[r]))
                  } else
                      for (var t in e)
                          if (e[t] instanceof A.opentag.BaseTag) n.push(p.getLoadTime(e[t]));
              return n
          };
          p.forceAllContainersAndTagsToRunIfDisabled = function() {
              A.opentag.Container.setCookieForDisabledContainersToRun();
              A.opentag.BaseTag.setCookieForcingTagsToRun()
          };
          p.rmAllContainersAndTagsForcingFlags = function() {
              A.opentag.Container.rmCookieForDisabledContainersToRun();
              A.opentag.BaseTag.rmAllCookiesForcingTagToRun()
          };
          p.getContainers = function() {
              return A.opentag.Container.getContainers()
          };
          p.findAllTagsByClassPath = function(e) {
              var n = (new Date).valueOf();
              var c = [];
              var r = [];
              try {
                  r.push(A.opentag.CustomTag);
                  r.push(A.opentag.LibraryTag)
              } catch (f) {}
              var t = p.getTags();
              for (var u = 0; u < t.length; u++) {
                  var C = t[u];
                  if (a.indexInArray(r, C) < 0 && C.PACKAGE_NAME.indexOf(e) === 0) c.push(C)
              }
              return c
          };
          p.findAllTags = function(e, n) {
              var c = A.opentag.BaseTag;
              var r;
              var t = [];
              var u = (new Date).valueOf();
              try {
                  t.push(A.opentag.CustomTag);
                  t.push(A.opentag.LibraryTag)
              } catch (C) {}
              r = p.findAllInstances(e, c, t, n);
              return r
          };
          var v = function(e, n, c, r) {
              var t = [];
              if (typeof e === "string") e = a.getObjectUsingPath(e);
              if (e) {
                  var u = {
                      objectsOnly: true
                  };
                  if (r) u.maxDeep = true;
                  var C = function(f, x, z, B) {
                      if (n(f)) {
                          for (var l = 0; l < c.length; l++)
                              if (c[l] === f) return true;
                          a.addToArrayIfNotExist(t, f);
                          return true
                      }
                      return false
                  }.bind(this);
                  a.traverse(e, C, u)
              }
              return t
          };
          p.findAllInstances = function(e, n, c, r) {
              var t = function(u) {
                  return u instanceof n
              };
              return v(e, t, c, r)
          };
          p.findAllInheriting = function(e, n, c, r) {
              var t = function(u) {
                  return u.prototype instanceof n
              };
              return v(e, t, c, r)
          };
          p.findAllFilters = function(e, n) {
              var c = [];
              try {
                  c.push(A.opentag.filter.Filter);
                  c.push(A.opentag.filter.URLFilter)
              } catch (r) {}
              return p.findAllInheriting(e, w, c, n)
          }
      })();
      H.cookie = {};
      H.cookie.PageView = {};
      H.cookie.PageView.update = function() {
          var a;
          var w;
          w = function v() {
              return Math.floor(1 + Math.random() * 65536).toString(36).substring(1)
          };
          if (!window.__pageViewId__) {
              a = (new Date).getTime().toString(36);
              window.__pageViewId__ = a + w() + w() + w()
          }
          return window.__pageViewId__
      };
      H.html.PostData = function(a, w, p, v) {
          var e;
          var n;
          var c;
          var r;
          var t;
          var u;
          var C;
          var f;
          var x;
          var z;
          z = 2;
          x = 5E3;
          C = false;
          f = function() {
              if (z >
                  0) setTimeout(function() {
                  if (!C) {
                      z -= 1;
                      e()
                  }
              }, x)
          };
          n = navigator.userAgent.toLowerCase();
          c = n.indexOf("msie") !== -1;
          r = n.indexOf("msie 9") !== -1;
          t = n.indexOf("msie 7") !== -1 || n.indexOf("msie 6") !== -1;
          u = ("https:" === document.location.protocol ? "https:" : "http:") + a;
          p = p || "POST";
          e = function() {
              var B;
              try {
                  B = null;
                  try {
                      B = new XMLHttpRequest
                  } catch (l) {}
                  if (B && !c) B.open(p, u, true);
                  else if (typeof XDomainRequest !== "undefined") {
                      B = new XDomainRequest;
                      B.open(p, u)
                  } else B = null;
                  try {
                      B.withCredentials = false
                  } catch (l) {}
                  if (B.setRequestHeader)
                      if (v) B.setRequestHeader("Content-Type",
                          v);
                      else B.setRequestHeader("Content-Type", "text/plain;charset\x3dUTF-8");
                  B.onload = function() {
                      C = true
                  };
                  B.onreadystatechange = function() {};
                  B.ontimeout = function() {};
                  B.onerror = function() {};
                  B.onprogress = function() {};
                  B.send(w)
              } catch (l) {
                  try {
                      try {
                          H.html.fileLoader.load(u)
                      } catch (k) {
                          if (window.console && window.console.log) window.console.log(l)
                      }
                  } catch (k) {}
              }
          };
          if (t) {
              H.html.fileLoader.load(u);
              return
          } else e()
      };
      (function() {
          function a() {}
          function w(v, e) {
              var n = {};
              n.clientId = "" + e.clientId;
              n.containerId = "" + v.getContainerId();
              n.classpath = "" + v.PACKAGE_NAME;
              n.opentagStats = true;
              if (v.sentPing) n.containerLoad = false;
              else {
                  v.sentPing = (new Date).valueOf();
                  n.containerLoad = true
              }
              n.isS3 = false;
              n.pageViewId = H.cookie.PageView.update();
              n.tags = [];
              return n
          }
          var p = "text/plain;charset\x3dUTF-8";
          A.Define.clazz("qubit.opentag.Ping", a);
          a.prototype.sendErrors = function(v, e) {};
          a.getPingID = function(v) {
              if (v.config.id) return v.config.id;
              var e = v.PACKAGE_NAME.lastIndexOf(".");
              if (e !== -1) return v.PACKAGE_NAME.substring(e + 1);
              else return v.PACKAGE_NAME
          };
          a.prototype.send = function(v, e) {
              var n = v.config;
              var c = w(v, n);
              var r = c.tags;
              var t = n.pingServerUrl;
              for (var u = 0; u < e.length; u++) {
                  var C = e[u].tag;
                  var f = e[u].loadTime;
                  if (f === null || isNaN(f)) continue;
                  var x = {};
                  var z = a.getPingID(C);
                  if (!C.pingSent && z && f !== null)
                      if (z !== undefined) {
                          x.tagId = z;
                          x.loadTime = f;
                          x.fired = true;
                          r.push(x);
                          C.pingSent = true
                      } else;
                  else if (C.pingSent);
                  else if (f === null);
              }
              if (t && (r.length > 0 || c.containerLoad)) {
                  var B = R.stringify(c);
                  var l = "//" + t;
                  H.html.PostData(l, B, "POST", p)
              } else {
                  if (!r.length);
                  if (!t);
              }
          };
          a.prototype.sendDedupe =
              function(v, e) {
                  var n = v.config;
                  var c = w(v, n);
                  var r = n.pingServerUrl;
                  var t = c.tags;
                  for (var u = 0; u < e.length; u++) {
                      var C = e[u];
                      var f = a.getPingID(C);
                      var x = {};
                      if (f === undefined);
                      else if (!C.dedupePingSent) {
                          x.tagId = "" + f;
                          x.fired = false;
                          x.loadTime = 0;
                          t.push(x);
                          C.dedupePingSent = true
                      }
                  }
                  if (r && (t.length > 0 || c.containerLoad)) {
                      var z = R.stringify(c);
                      var B = "//" + r;
                      H.html.PostData(B, z, "POST", p)
                  } else {
                      if (!t.length);
                      if (!r);
                  }
              }
      })();
      H.cookie.SimpleSessionCounter = {};
      H.cookie.SimpleSessionCounter._cookieName = "_qst_s";
      H.cookie.SimpleSessionCounter._sessionCookie =
          "_qsst_s";
      H.cookie.SimpleSessionCounter.update = function(a) {
          var w;
          var p;
          var v;
          var e = 30;
          w = A.Cookie.get(H.cookie.SimpleSessionCounter._cookieName);
          p = A.Cookie.get(H.cookie.SimpleSessionCounter._sessionCookie);
          if (!w) w = 1;
          else {
              w = parseInt(w, 10);
              if (!p || parseInt(p, 10) < (new Date).getTime() - e * 60 * 1E3) w += 1
          }
          A.Cookie.set(H.cookie.SimpleSessionCounter._cookieName, w, 365, a);
          A.Cookie.set(H.cookie.SimpleSessionCounter._sessionCookie, (new Date).getTime().toString(), null, a);
          return w
      };
      (function() {
          function a(t) {
              if (t)
                  if (t.alphabet) {
                      this.alphabet =
                          t.alphabet;
                      this.dict = {};
                      for (var u = 0; u < this.alphabet.length; u++) this.dict[this.alphabet[u]] = u
                  } else {
                      this.alphabet = p;
                      this.dict = n
                  }
          }
          function w(t, u) {
              for (var C in u)
                  if (t === u[C]) return C;
              return null
          }
          var p = [];
          var v = Math.pow(2, 8);
          for (var e = 0; e < v; e++) p.push(String.fromCharCode(e));
          var n = {};
          for (var c = 0; c < p.length; c++) n[p[c]] = c;
          var r = A.Define;
          r.clazz("qubit.compression.LZW", a);
          a.prototype.encode = function(t) {
              var u = this.alphabet.length;
              var C = {};
              var f = [];
              var x = 0;
              var z = t.charAt(x++);
              var B;
              for (var l = this.dict; !!(B =
                      t.charAt(x++));) {
                  var k = z + B;
                  if (l.hasOwnProperty(k) || C.hasOwnProperty(k)) z = k;
                  else {
                      var q = l.hasOwnProperty(z) ? l[z] : C[z];
                      if (q === undefined) throw "Dictionary base is to small for those contents: " + z;
                      f.push(q);
                      C[k] = u++;
                      z = B
                  }
              }
              if (z !== "") f.push(C.hasOwnProperty(z) ? C[z] : l[z]);
              return f
          };
          a.prototype.decode = function(t) {
              var u = this.dict;
              var C = this.alphabet.length;
              var f;
              var x = {};
              var z = w(t[0], u);
              var B = z;
              var l = [z];
              for (var k = 1; k < t.length; k++) {
                  var q = t[k];
                  f = w(q, u);
                  if (f === null) {
                      if (x.hasOwnProperty(q)) f = x[q];
                      if (f === null) f =
                          B + z
                  }
                  l.push(f);
                  z = f.charAt(0);
                  x[C++] = B + z;
                  B = f
              }
              return l.join("")
          }
      })();
      (function() {
          function a(l) {
              var k = 0;
              var q = l < 0;
              if (q) l = -l;
              var d = "";
              var b = true;
              do {
                  k = l % x;
                  if (b) {
                      d = u[f[k]];
                      b = false
                  } else d = f[k] + d;
                  l = (l - k) / x
              } while (l > 0);
              if (q) return "-" + d;
              return d
          }
          function w(l) {
              var k = 0;
              var q = 0;
              var d = true;
              for (var b = 0; b < l.length; b++) {
                  var g = l.charAt(l.length - 1 - b);
                  if (d) {
                      d = false;
                      g = v.charAt(r[g])
                  }
                  k += n[g] * Math.pow(x, q++)
              }
              return k
          }
          function p(l) {}
          var v = "abcdefghijklmnopqrstuvwxyz" + "0123456789" + "'%./:\x3c\x3e?[";
          var e = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
              "*!-+()@{|}" + '"]^_`~$\x26#';
          var n = {};
          for (var c = 0; c < v.length; c++) n[v.charAt(c)] = c;
          var r = {};
          for (var t = 0; t < v.length; t++) r[e.charAt(t)] = t;
          var u = {};
          for (var C = 0; C < v.length; C++) u[v.charAt(C)] = e.charAt(C);
          var f = v.split("");
          var x = f.length;
          var z = new A.compression.LZW({});
          var B = A.Define;
          B.clazz("qubit.compression.Compressor", p);
          p.prototype.compress = function(l, k) {
              var q = (k || z).encode(l);
              var d = [];
              for (var b = 0; b < q.length; b++) d.push(String.fromCharCode(q[b]));
              return d.join("")
          };
          p.prototype.compressAnsi = function(l,
              k) {
              var q = (k || z).encode(l);
              var d = [];
              for (var b = 0; b < q.length; b++) {
                  var g = a(q[b]);
                  d.push(g)
              }
              return d.join("")
          };
          p.prototype.decompressAnsi = function(l, k) {
              var q = [];
              var d = "";
              for (var b = 0; b < l.length; b++) {
                  var g = l.charAt(b);
                  if (r.hasOwnProperty(g)) {
                      var m = d + g;
                      d = "";
                      m = w(m);
                      q.push(m)
                  } else d += g
              }
              return (k || z).decode(q)
          };
          p.prototype.decompress = function(l, k) {
              var q = [];
              for (var d = 0; d < l.length; d++) q.push(l.charCodeAt(d));
              return (k || z).decode(q)
          }
      })();
      (function() {
          function a(q) {
              var d = [];
              for (var b = 0; b < q.length; b++) {
                  var g = n(q[b][0]);
                  d.push([new RegExp(g, "g"), "*" + q[b][1]])
              }
              return d
          }
          function w(q, d) {
              for (var b = 0; b < d.length; b++)
                  if (d[b][1] === q) return d[b][0];
              return null
          }
          function p(q) {
              this._regexDefs = f;
              this._defs = C;
              if (q)
                  if (q.definitions) {
                      this._regexDefs = a(q.definitions);
                      this._defs = q.definitions
                  }
          }
          function v(q, d) {
              var b = [];
              for (var g = 0; g < q.length; g++) {
                  var m = true;
                  if (d) m = q.charCodeAt(g) <= d;
                  var y = u.cookieAlphabetMap.hasOwnProperty(q.charAt(g));
                  if (m && !y) b.push("*" + q.charCodeAt(g) + ".");
                  else b.push(q.charAt(g))
              }
              return b.join("")
          }
          function e(q,
              d) {
              d = d.replace(/@/g, "@@");
              var b = [];
              var g = 0;
              for (var m = 0; g < q.length; g++) {
                  var y = q[g][0];
                  var D = new RegExp(n(y), "g");
                  var E = d.replace(D, "@" + m + "-");
                  if (E !== d) {
                      b.push(q[g][0]);
                      m++;
                      d = E
                  }
              }
              return [d, b]
          }
          function n(q) {
              return q.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1")
          }
          function c(q) {
              var d = {};
              var b = "";
              for (var g = 0; g < q.length; g++) {
                  var m = q.charAt(g);
                  if (!z[m]) {
                      if (isNaN(d[b])) d[b] = q.split(b).length - 1;
                      b = ""
                  } else b += m
              }
              var y = [];
              for (var D in d)
                  if (d.hasOwnProperty(D)) {
                      var E = d[D];
                      if (E >= k && D.length >= l) y.push([D, E])
                  }
              y = y.sort(function(F,
                  G) {
                  if (F[0].length === G[0].length) return 0;
                  if (G[0].length > F[0].length) return 1;
                  else return -1
              });
              return y
          }
          function r(q, d) {
              if (!d || d.length === 0 || !q) return q;
              var b = "";
              var g = false;
              var m = false;
              var y = "";
              for (var D = 0; D < q.length; D++) {
                  var E = q.charAt(D);
                  if (E === "@" || g || m)
                      if (g || m) {
                          g = false;
                          if (E === "@") b += "@";
                          else if (!isNaN(+("-" + E))) {
                              m = true;
                              y = y + E
                          } else if (m) {
                              if (d && E === "-" && d[+y]) b += d[+y];
                              else b += "@" + y + E;
                              y = "";
                              m = false
                          } else b += "@" + E
                      } else g = true;
                  else b += E
              }
              if (y) b += "@" + y;
              if (g) b += "@";
              return b
          }
          var t = A.Define;
          var u = A.Cookie;
          var C = [
              ['","referrer":[{"url":"http://', "1-"],
              ['","referrer":[{"url":"https://', "2-"],
              [',"referrer":[{"url":"http://', "3-"],
              [',"referrer":[{"url":"https://', "4-"],
              [',"sessionStartTime":', "5-"],
              ["www.google.co.uk", "6-"],
              ["www.google.", "7-"],
              ['"sessionStartTime":', "8-"],
              ['"landing":"', "9-"],
              ["http%3A%2F%2Fwww", "10-"],
              ['"landing":', "L"],
              ['"time":', "A"],
              ['"pageViews":', "P"],
              ['"sessionCount":', "B"],
              ['"referrer":', "R"],
              ['"url":"http://www.', "J"],
              ['"url":"https://www.', "M"],
              ['"url":"', "I"],
              ['"url":', "U"],
              ["http://www.", "W"],
              ["https://www.", "V"],
              ["%2Fen%2Ftsuk%2F", "K"],
              ['"sessionLandingPage":', "F"],
              ["http%3A%2F%2F", "D"],
              ["http://", "H"],
              ["https://", "X"],
              ['""', "O"],
              ['",', "Y"],
              ['":{}}', "z"],
              ["\x3c", "S"],
              ["\x3e", "G"],
              ["[", "Z"],
              ["]", "E"],
              ["{", "a"],
              ["}", "b"],
              ["(", "c"],
              [")", "d"],
              ["!", "e"],
              ["#", "f"],
              ["$", "g"],
              ["!", "q"],
              ["'", "i"],
              [":", "j"],
              ["?", "k"],
              ["^", "x"],
              ["`", "m"],
              ["|", "n"],
              ["~", "o"],
              ["%", "v"],
              [",", "C"]
          ];
          var f = a(C);
          t.clazz("qubit.opentag.compression.Encoder", p);
          p.prototype.encode = function d(b, g) {
              var m =
                  b.replace(/\*/g, "**");
              var y = c(m);
              for (var D = 0; D < this._regexDefs.length; D++) {
                  var E = this._regexDefs[D];
                  m = m.replace(E[0], E[1])
              }
              m = m.replace(/;/g, "*-");
              m = m.replace(/&/g, "*.");
              m = m.replace(/\\/g, "*/");
              m = m.replace(/=/g, "*+");
              m = m.replace(/\n/g, "*N");
              m = m.replace(/ /g, "*_");
              m = m.replace(/\t/g, "*T");
              m = m.replace(/"/g, "*Q");
              var F = c(m);
              F.concat(y);
              var G = e(F, m);
              var I = G[1];
              var O = I.length > 0;
              if (O) m = G[0];
              if (!g) m = v(m);
              else m = v(m, g);
              if (O) return "Y" + I.join("*") + "@" + m;
              else return "N" + m
          };
          var x = "abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ+_.";
          var z = {};
          for (var B = 0; B < x.length; B++) z[x.charAt(B)] = true;
          var l = 4;
          var k = 2;
          p.prototype.decode = function(d) {
              var b = null;
              if (d.charAt(0) === "N") d = d.substring(1);
              else if (d.charAt(0) === "Y") {
                  var g = d.indexOf("@");
                  if (g >= 0) {
                      b = d.substring(1, g);
                      b = b.split("*");
                      d = d.substring(g + 1);
                      d = r(d, b)
                  }
              }
              var m = "";
              var y = false;
              var D = false;
              var E = "";
              for (var F = 0; F < d.length; F++) {
                  var G = d.charAt(F);
                  if (G === "*" || y || D)
                      if (y || D) {
                          y = false;
                          if (!isNaN(+("-" + G))) {
                              E = E + G;
                              D = true
                          } else if (D) {
                              if (G === ".") m += String.fromCharCode(+E);
                              else if (G === "-" && w(E + "-",
                                      this._defs)) m += w(E + "-", this._defs);
                              else m += "*" + E + G;
                              E = "";
                              D = false
                          } else if (G === "*") m += "*";
                          else if (G === "-") m += ";";
                          else if (G === "/") m += "\\";
                          else if (G === ".") m += "\x26";
                          else if (G === "+") m += "\x3d";
                          else if (G === "N") m += "\n";
                          else if (G === "_") m += " ";
                          else if (G === "T") m += "\t";
                          else if (G === "Q") m += '"';
                          else if (w(G, this._defs) !== null) {
                              var I = w(G, this._defs);
                              m += I
                          } else m += "*" + G
                      } else y = true;
                  else m += G
              }
              if (E) m += "*" + E;
              if (y) m += "*";
              return m
          }
      })();
      (function() {
          function a(e) {
              this.testBinary = false;
              this.binSupported = v;
              if (e) {
                  this.compressor =
                      new A.compression.Compressor;
                  this.encoder = new A.opentag.compression.Encoder({})
              }
          }
          var w = A.Define;
          var p = A.Cookie;
          var v = false;
          w.clazz("qubit.opentag.compression.CookieCompressor", a);
          a.prototype.compress = function(e, n) {
              if (typeof e !== "string" || e === "") return e;
              var c = this.encoder.encode(e);
              var r;
              if (this.binSupported || this.testBinary) {
                  var t = this.compressor.compress(c);
                  r = '"B' + this.encoder.encode(t, 128) + '"';
                  p.set("__qtag_test_bin__", r, undefined, undefined, true);
                  var u = p.get("__qtag_test_bin__", true);
                  p.rm("__qtag_test_bin__");
                  if (u && u !== r) r = null
              }
              var C;
              var f = this.encoder.encode(this.compressor.compressAnsi(c));
              if (!n && c.length <= f.length) C = "E" + c;
              else C = "C" + f;
              if (r && r.length < C.length) return r;
              else return C
          };
          a.prototype.decompress = function(e) {
              if (typeof e !== "string" || e === "") return e;
              if (e.charAt(0) === '"') e = e.substring(1, e.length - 1);
              var n = e.charAt(0);
              e = e.substring(1);
              switch (n) {
                  case "E":
                      return this.encoder.decode(e);
                  case "C":
                      var c = this.compressor.decompressAnsi(this.encoder.decode(e));
                      return this.encoder.decode(c);
                  case "B":
                      var r =
                          this.compressor.decompress(this.encoder.decode(e));
                      return this.encoder.decode(r);
                  default:
                      throw "This code is not supported! Code: " + n;
              }
          }
      })();
      (function() {
          var a = A.Cookie;
          var w = A.opentag.Utils;
          var p = function() {};
          A.Define.clazz("qubit.opentag.Session", p);
          var v = new A.opentag.compression.CookieCompressor({});
          p.readCompressedCookie = function(e) {
              var n = a.get(e, true);
              return v.decompress(n)
          };
          p.setupSession = function(e) {
              var n = e.config;
              var c;
              var r;
              var t;
              var u;
              var C;
              var f;
              c = {};
              c.sessionCount = H.cookie.SimpleSessionCounter.update(n.cookieDomain);
              C = "qtag_" + e.getContainerId();
              var x = "x_qtag_" + e.getContainerId();
              t = a.get(C);
              var z = !!t;
              if (t === null) {
                  t = a.get(x, true);
                  t = v.decompress(t)
              }
              if (t) try {
                  t = R.parse(t)
              } catch (l) {
                  t = {
                      sc: 0,
                      sessionCount: 0,
                      pageViews: 0,
                      sessionStartTime: 0,
                      referrer: [],
                      sessionLandingPage: "",
                      __v: {}
                  }
              } else t = {
                  sc: 0,
                  sessionCount: 0,
                  pageViews: 0,
                  sessionStartTime: 0,
                  referrer: [],
                  sessionLandingPage: "",
                  __v: {}
              };
              f = (new Date).getTime();
              if (c.sessionCount !== parseInt(t.sc, 10)) {
                  t.sessionStartTime = f;
                  t.sc = c.sessionCount;
                  t.sessionCount += 1;
                  t.referrer.push({
                      url: p.getReferrer(),
                      landing: w.getUrl().substring(0, 300),
                      time: f
                  });
                  t.sessionLandingPage = w.getUrl().substring(0, 300)
              } else if (p.isReferrerDifferent())
                  if (!p.referrerIsSameAsPrevious(t.referrer, f, 30 * 60 * 1E3)) {
                      t.referrer.push({
                          url: p.getReferrer(),
                          landing: w.getUrl().substring(0, 300),
                          time: f
                      });
                      t.sessionLandingPage = w.getUrl().substring(0, 300);
                      t.sessionStartTime = f;
                      t.sessionCount += 1
                  }
              c.sessionCount = t.sessionCount;
              c.sessionStartTime = t.sessionStartTime;
              c.pageStartTime = f;
              t.pageViews += 1;
              c.pageViews = t.pageViews;
              c.sessionLandingPage = t.sessionLandingPage;
              c.referrer = t.referrer;
              if (c.referrer.length > 5) c.referrer.splice(2, c.referrer.length - 5);
              u = R.stringify(t);
              for (r = 0; v.compress(u).length > n.maxCookieLength && r < 5;) {
                  if (t.referrer.length >= 3) t.referrer.splice(2, 1);
                  else if (t.referrer.length === 2) t.referrer = [t.referrer[0]];
                  else if (t.referrer.length === 1) t.referrer = [];
                  u = R.stringify(t);
                  r += 1
              }
              c.referrer = t.referrer;
              if (z) a.rm(C);
              var B = v.compress(u);
              a.rm(x);
              if (n.maxCookieLength > 0) a.set(x, B, 365, n.cookieDomain, true);
              c.setVariable = function(l, k, q) {
                  var d = !!q ? q : 0;
                  t.__v[l] = [k, d];
                  var b = v.compress(R.stringify(t));
                  if (n.maxCookieLength > 0) a.set(x, b, 365, n.cookieDomain, true);
                  else a.rm(x)
              };
              c.getCookie = function(l, k) {
                  var q = a.get(l, true);
                  if (q && (k || l.indexOf("x_") === 0)) try {
                      q = v.decompress(q)
                  } catch (d) {} else if (q !== null) q = a.decode(q);
                  return q
              };
              c.getVariable = function(l) {
                  var k;
                  var q;
                  var d;
                  k = t.__v[l];
                  if (k) {
                      q = k[1];
                      if (q === 0 || q > (new Date).getTime()) return k[0]
                  }
                  return null
              };
              c.on = function(l, k, q) {
                  if (k.attachEvent) k.attachEvent("on" + l, q);
                  else if (k.addEventListener) k.addEventListener(l, q, false)
              };
              c.getTagCookie = function() {
                  return p.readCompressedCookie(x)
              };
              p.lastSession = c;
              return c
          };
          p.referrerIsSameAsPrevious = function(e, n, c) {
              var r;
              var t;
              var u;
              if (e.length > 0) {
                  r = p.getReferrer();
                  t = w.getUrl().substring(0, 300);
                  u = e[e.length - 1];
                  return u.url === r && u.landing === t && u.time + c > n
              }
              return false
          };
          p.isReferrerDifferent = function() {
              var e;
              var n;
              var c;
              c = p.getReferrer();
              e = c.indexOf("://");
              if (e === -1) return true;
              e += 3;
              try {
                  if (c.substring(e).indexOf(p.getDomain()) !== 0) return true;
                  return false
              } catch (r) {
                  return true
              }
          };
          p.getReferrer =
              function() {
                  if (document.referrer) return document.referrer.substring(0, 300);
                  return "direct"
              };
          p.getDomain = function() {
              return document.location.host
          }
      })();
      (function() {
          function a() {}
          var w = A.opentag.LibraryTag;
          var p = A.Define;
          a.library = function() {
              return w.define.apply(w, arguments)
          };
          a.libraryRef = function() {
              return w.getLibraryByClasspath.apply(w.getLibraryByClasspath, arguments)
          };
          p.namespace("qubit.Quick", a)
      })();
      (function() {
          function a(l) {
              this.runQueue = [];
              this.tags = [];
              this.config = {
                  cookieDomain: "",
                  maxCookieLength: 1E3,
                  gzip: true,
                  delayDocWrite: false,
                  clientId: "",
                  name: "",
                  tellLoadTimesProbability: null,
                  pingServerUrl: null,
                  trackSession: null,
                  disabled: false,
                  containerId: "",
                  scanTags: false,
                  noPings: false,
                  restartable: true
              };
              this.ignoreTagsState = false;
              if (l) {
                  this.setConfig(l);
                  if (!l.name) this.config.name = "Cont-" + C++;
                  a.register(this);
                  if (a.NO_PINGS) this.config.noPings = true;
                  this.ping = new A.opentag.Ping(this.config);
                  this._sendPingsTrigger = this.sendPings.bind(this);
                  if (l.init) try {
                      l.init.call(this, l)
                  } catch (q) {}
                  var k = this;
                  this._tagLoadedHandler =
                      function(q) {
                          if (k._containerAlreadySentPings) {
                              k.sendPingsNotTooOften();
                              if (q.success);
                          }
                      }
              }
              this.events = new A.Events({});
              return this
          }
          function w(l, k, q, d) {
              if (l[q]) {
                  d[q] = d[q] + 1 || 1;
                  q += "(" + d[q] + ")"
              }
              l[q] = k
          }
          var p = A.opentag.Utils;
          var v = A.opentag.filter.BaseFilter;
          var e = A.opentag.filter.Filter;
          var n = A.opentag.BaseTag;
          var c = A.opentag.Timed;
          var r = A.opentag.Tags;
          var t = A.opentag.Session;
          var u = A.Cookie;
          var C = 1;
          try {
              window.opentag_consentGiven = function() {
                  a.consentIsGiven = true;
                  var l = a.getContainers();
                  for (var k = 0; k < l.length; k++) try {
                      l[k].run()
                  } catch (q) {}
              }.bind(this)
          } catch (l) {}
          A.Define.clazz("qubit.opentag.Container",
              a);
          var f = [];
          var x = "restart";
          a.prototype.EVENT_TYPES = {
              RESTART_EVENT: x
          };
          a.register = function(l) {
              p.addToArrayIfNotExist(f, l)
          };
          a.prototype.isTellingLoadTimes = function() {
              var l = this.config.tellLoadTimesProbability;
              if (l === null) l = 0;
              return l > Math.random()
          };
          a.prototype.destroy = function(l) {
              this.destroyed = true;
              this.unregister();
              for (var k = 0; k < this.tags.length; k++) {
                  var q = this.tags[k];
                  if (q.owningContainer === this) {
                      q.owningContainer = null;
                      if (l && q instanceof n) q.destroy()
                  }
              }
              this.tags = []
          };
          a.destroyAll = function(l) {
              var k =
                  a.getContainers();
              for (var q = 0; q < k.length; q++) k[q].destroy(l)
          };
          a.findContainersByName = function(l) {
              var k = a.getContainers();
              var q = [];
              for (var d = 0; d < k.length; d++)
                  if (k[d].config.name === l) q.push(k[d]);
              return q
          };
          a.getById = function(l) {
              var k = a.getContainers();
              for (var q = 0; q < k.length; q++)
                  if (k[q].getContainerId() === l) return k[q];
              return null
          };
          a.prototype.unregister = function(l) {
              a.unregister(this, l)
          };
          a.unregister = function(l, k) {
              p.addToArrayIfNotExist(f, l);
              var q = p.removeFromArray(f, l);
              if (k) {
                  for (var d = 0; d < l.tags.length; d++) {
                      var b =
                          l.tags[d];
                      if (b.owningContainer === l) {
                          b.owningContainer = null;
                          if (b instanceof n) b.unregister()
                      }
                  }
                  l.tags = []
              }
              if (!q || q.length === 0);
          };
          a.prototype.hasConsent = function() {
              return u.get("qubitconsent") === "Accepted"
          };
          a.prototype.register = function(l) {
              a.register(l || this)
          };
          a.getContainers = function() {
              return f
          };
          a.prototype.getContainers = function() {
              return a.getContainers()
          };
          a.prototype.onTagRegistered = function(l) {};
          a.prototype.registerTag = function(l) {
              if (l.owningContainer !== null) return false;
              else {
                  this.tags.push(l);
                  l.owningContainer =
                      this;
                  l.onAfter(this._tagLoadedHandler);
                  try {
                      this.onTagRegistered(l)
                  } catch (k) {}
              }
              return true
          };
          a.prototype.unregisterTag = function(l) {
              if (l.owningContainer === this) l.owningContainer = null;
              for (var k = 0; k < this.tags.length;)
                  if (this.tags[k] === l) this.tags.splice(k, 1);
                  else k++
          };
          a.prototype.registerTags = function(l) {
              for (var k = 0; k < l.length; k++) this.registerTag(l[k])
          };
          a.prototype.setConfig = function(l) {
              for (var k in l) this.config[k] = l[k]
          };
          a.prototype.run = function() {
              this.runTags({
                  command: "runOnceIfFiltersPass"
              })
          };
          a.prototype.runWithoutFilters =
              function() {
                  this.runTags({
                      command: "run"
                  })
              };
          a.prototype.containerScriptLoadedSynchronously = function() {
              var l;
              var k;
              var q;
              var d;
              var b;
              d = document.getElementsByTagName("script");
              for (l = 0, k = d.length; l < k; l += 1) {
                  q = d[l];
                  b = q.getAttribute("src");
                  if (!!b && b.indexOf("" + this.config.clientId + "-" + this.getContainerId() + ".js") > 0) return q.getAttribute("async") === null && (q.getAttribute("defer") === false || q.getAttribute("defer") === "" || q.getAttribute("defer") === null)
              }
              return true
          };
          a.prototype.prepareSessionIfNeeded = function() {
              var l =
                  this.config.trackSession;
              if (l !== true && l !== false)
                  for (var k = 0; k < this.tags.length; k++) {
                      var q = this.tags[k];
                      q.resolveAllDynamicData();
                      var d = q.getFilters();
                      for (var b = 0; b < d.length; b++) {
                          var g = d[b];
                          if (g instanceof e && g.isSession()) {
                              this.trackSession = true;
                              break
                          }
                      }
                      if (this.trackSession) break
                  } else this.trackSession = l;
              if (a.TRACK_SESSION) this.trackSession = true;
              if (this.trackSession) this.session = t.setupSession(this);
              if (this.session);
          };
          a.runtimeCounter = a.runtimeCounter || 1;
          a.prototype.runTags = function(l, k) {
              if (this.destroyed) throw "Container has been destroyed.";
              if (!k)
                  if (a.LOCKED || p.global().QUBIT_CONTAINERS_LOCKED) return;
              if (this.onBeforeRun) try {
                  this.onBeforeRun()
              } catch (E) {}
              var q = !this.containerScriptLoadedSynchronously();
              var d = "runIfFiltersPass";
              if (l && l.command) d = l.command;
              this.runningStarted = (new Date).valueOf();
              if (this.config.scanTags)
                  if (!this._scanned) {
                      this.scanForTags();
                      this._scanned = (new Date).valueOf()
                  }
              this.prepareSessionIfNeeded();
              var b = this.getTagsInOrder();
              var g = "___otctnmk" + a.runtimeCounter++;
              for (var m = 0; m < b.length; m++) try {
                  var y = b[m];
                  if (this.includedToRun(y)) this._checkAndRun(y,
                      d, q, g)
              } catch (E) {}
              c.setTimeout(function() {
                  this.sendPingsNotTooOften()
              }.bind(this), 3100);
              this.waitForAllTagsToFinish();
              for (var D = 0; D < b.length; D++) {
                  b[D]._runtimeMarkers[g] = undefined;
                  delete b[D]._runtimeMarkers[g]
              }
          };
          a.prototype.getTagsInOrder = function() {
              var l = [];
              for (var k = 0; k < this.tags.length; k++) {
                  var q = this.tags[k];
                  var d = q.config.priority;
                  if (+d > 0) {
                      var b = 0;
                      for (var g = 0; g < l.length; g++) {
                          var m = l[g];
                          if (m) {
                              var y = +m.config.priority;
                              if (y > 0 && y > d) b++;
                              else break
                          }
                      }
                      l.splice(b, 0, q)
                  } else l.push(q)
              }
              return l
          };
          a.prototype._checkAndRun =
              function(l, k, q, d) {
                  if (!l._runtimeMarkers[d]) l._runtimeMarkers[d] = true;
                  else return;
                  var b = l.resolveDependencies();
                  if (b.length > 0)
                      for (var g = 0; g < b.length; g++) this._checkAndRun(b[g], k, q, d);
                  this._tagRunner(l, k, q)
              };
          a.prototype._tagRunner = function(l, k, q) {
              try {
                  if (this.includedToRun(l)) {
                      if (q) l.forceAsynchronous = true;
                      if (this.config.delayDocWrite) l.delayDocWrite = true;
                      l.session = l.session || this.session;
                      l[k]()
                  }
              } catch (d) {}
          };
          a.prototype.getContainerId = function() {
              if (this.config.containerId) return this.config.containerId;
              else {
                  if (this._pkgName) return this._pkgName;
                  var l = this.PACKAGE_NAME.split(".");
                  l = l[l.length - 1];
                  this._pkgName = l;
                  return l
              }
          };
          a.prototype.includedToRun = function(l) {
              if (!l) return false;
              var k = l.config;
              if (k.inactive) return false;
              if (k.disabled)
                  if (!l.cookieSaysToRunEvenIfDisabled()) return false;
              if (l.disabledByCookie()) return false;
              var q = a.consentIsGiven || !k.needsConsent || this.hasConsent();
              var d = l.state === n.prototype.STATE.INITIAL;
              return this.ignoreTagsState || q && d
          };
          a.prototype.scanForTags = function(l, k) {
              if (l) this.tags = [];
              var q;
              if (k && typeof k === "object") q = this.findAllTags(k);
              else q = this.findAllTagsByClassPath();
              this.registerTags(q);
              return q
          };
          a.prototype.findAllTags = function(l, k) {
              l = l || this.tagsPackageName || this.PACKAGE_NAME;
              return r.findAllTags(l, k)
          };
          a.prototype.findAllTagsByClassPath = function(l) {
              l = l || this.PACKAGE_NAME;
              return r.findAllTagsByClassPath(l)
          };
          a.prototype.findAllFilters = function(l, k) {
              l = l || this.tagsPackageName || this.PACKAGE_NAME;
              return r.findAllFilters(l, k)
          };
          a.prototype.waitForAllTagsToFinish = function() {
              if (this._waitForAllTagsToFinishWaiting) return;
              if (!this._lastWaited) this._lastWaited = (new Date).valueOf();
              var l = (new Date).valueOf() - this._lastWaited > 15 * 1E3;
              var k = this.allTagsFinished() || l;
              if (!this._showFinishedOnce && k) {
                  this._lastWaited = null;
                  if (l);
                  this._showFinishedOnce = true;
                  this.runningFinished = (new Date).valueOf();
                  this._containerAlreadySentPings = (new Date).valueOf();
                  this.sendPingsNotTooOften();
                  if (this.onTagsInitiallyRun) this.onTagsInitiallyRun()
              } else if (!k) {
                  this._waitForAllTagsToFinishWaiting = true;
                  this._showFinishedOnce = false;
                  c.setTimeout(function() {
                      this._waitForAllTagsToFinishWaiting =
                          false;
                      this.waitForAllTagsToFinish()
                  }.bind(this), 100)
              } else;
          };
          a.prototype.onTagsInitiallyRun = N;
          a.prototype.resetAllTags = function(l) {
              for (var k = 0; k < this.tags.length; k++) {
                  this.tags[k].reset();
                  if (!l) this.tags[k].resetFilters()
              }
          };
          a.prototype._prepareTagsToRestart = function(l) {
              for (var k = 0; k < this.tags.length; k++) this.tags[k].prepareForRestart(l)
          };
          a.prototype.reset = function(l, k) {
              this.runningFinished = undefined;
              this._waitForAllTagsToFinishWaiting = undefined;
              this.runningStarted = undefined;
              this._showFinishedOnce =
                  undefined;
              if (!l) this.resetAllTags(k)
          };
          a.prototype.onRestart = function(l) {
              this.events.on(x, l)
          };
          a.prototype.restart = function(l, k) {
              if (this.config.restartable) {
                  this.events.call(x, this);
                  this.reset(true, true);
                  if (!l) this._prepareTagsToRestart(k);
                  this.run()
              }
          };
          a.restartAll = function(l, k) {
              var q = a.getContainers();
              for (var d = 0; d < q.length; d++) try {
                  q[d].restart(l, k)
              } catch (b) {
                  log.ERROR("Restart failed for container " + q[d].config.name, b)
              }
          };
          a.resetAll = function(l, k) {
              var q = a.getContainers();
              for (var d = 0; d < q.length; d++) try {
                  q[d].reset(l,
                      k)
              } catch (b) {
                  log.ERROR("Reset failed for container " + q[d].config.name, b)
              }
          };
          a.prototype.sendPingsNotTooOften = function() {
              this._sndLck = this._sndLck || {};
              c.runIfNotScheduled(this._sendPingsTrigger, 2E3, this._sndLck)
          };
          a.prototype.sendPings = function() {
              if (this.config.noPings) return;
              var l;
              if (this.isTellingLoadTimes()) {
                  var k = this.getAllTagsByState();
                  var q = [];
                  if (k.run) q = r.getLoadTimes(k.run);
                  this.lastPingsSentTime = (new Date).valueOf();
                  this.ping.send(this, q);
                  q = r.getLoadTimes();
                  var d = [];
                  for (l = 0; l < q.length; l++) {
                      var b =
                          q[l].tag;
                      if (b.config.dedupe && b.sendDedupePing) d.push(b)
                  }
                  if (d.length > 0) {
                      this.lastDedupePingsSentTime = (new Date).valueOf();
                      this.ping.sendDedupe(this, d)
                  }
                  if (k.other) {
                      q = r.getLoadTimes(k.other);
                      var g = [];
                      for (l = 0; l < q.length; l++) g.push(q[l]);
                      if (g.length > 0) this.ping.send(this, g)
                  }
                  if (k.awaiting) {
                      q = r.getLoadTimes(k.awaiting);
                      var m = [];
                      for (l = 0; l < q.length; l++) m.push(q[l]);
                      if (m.length > 0) this.ping.send(this, m)
                  }
              } else this.ping.send(this, [])
          };
          a.prototype.getAllTagsByState = function() {
              return a.getAllTagsByState(this.tags)
          };
          a.getAllTagsByState = function(l) {
              var k = null;
              var q = null;
              var d = null;
              var b = null;
              var g = null;
              var m = null;
              var y = {};
              var D = n.prototype.STATE.EXECUTED_WITH_ERRORS;
              for (var E = 0; E < l.length; E++) {
                  var F = l[E];
                  if (F instanceof n) {
                      var G = F.config.name;
                      if (F.scriptExecuted > 0) {
                          k = k || {};
                          w(k, F, G, y)
                      } else if (F.locked) {
                          m = m || {};
                          w(m, F, G, y)
                      } else if (F.scriptExecuted < 0 || F.state >= D) {
                          b = b || {};
                          w(b, F, G)
                      } else if (F.filtersState() === v.state.SESSION || F.filtersState() > 0) {
                          d = d || {};
                          w(d, F, G, y)
                      } else if (F.config.needsConsent) {
                          g = g || {};
                          w(g, F, G, y)
                      } else {
                          q =
                              q || {};
                          w(q, F, G, y)
                      }
                  }
              }
              return {
                  run: k,
                  failed: b,
                  awaiting: d,
                  consent: g,
                  locked: m,
                  other: q
              }
          };
          a.prototype.allTagsFinished = function() {
              for (var l = 0; l < this.tags.length; l++) {
                  var k = this.tags[l];
                  if (k instanceof A.opentag.BaseTag) {
                      var q = k.filtersState();
                      if (!k.config.disabled) {
                          var d = k.filtersState() < 0 && !k.locked;
                          var b = !(k.finished() || k.config.runner && !k.isRunning);
                          if (d && b) {
                              var g = q !== v.state.SESSION;
                              var m = +k.awaitingDependencies > 0;
                              if (g && !m) return false
                          }
                      }
                  }
              }
              return true
          };
          a.prototype.getPageVariables = function() {
              var l = [];
              for (var k =
                      0; k < this.tags.length; k++) {
                  var q = this.tags[k].getPageVariables();
                  for (var d = 0; d < q.length; d++) p.addToArrayIfNotExist(l, q[d])
              }
              return l
          };
          a.getPageVariableByName = function(l) {
              var k = this.getAllPageVariables();
              var q = [];
              for (var d = 0; d < k.length; d++)
                  if (k[d].config.name === l) q.push(k[d]);
              return q
          };
          var z = "qubit.opentag.disableContainerRunning_";
          var B = "qubit.opentag.forceContainerRunning";
          a.prototype._getCookieNameForDisabling = function() {
              return z + this.getContainerId() + this.config.name
          };
          a.prototype.disabledByCookie =
              function() {
                  return !!u.get(this._getCookieNameForDisabling())
              };
          a.prototype.setCookieToDisable = function() {
              u.set(this._getCookieNameForDisabling(), "true")
          };
          a.prototype.rmCookieToDisable = function() {
              u.rm(this._getCookieNameForDisabling())
          };
          a.rmAllDisablingCookies = function() {
              p.rmCookiesMatching(z)
          };
          a.setCookieForDisabledContainersToRun = function() {
              u.set(B, "true")
          };
          a.rmCookieForDisabledContainersToRun = function() {
              u.rm(B)
          }
      })();
      (function() {
          function a() {}
          function w() {
              var u = false;
              if (r.get("opentag_debug") || document.location.href.indexOf("opentag_debug") >=
                  0) u = true;
              return u
          }
          function p() {
              var u = false;
              if (r.get("opentag_debug_tool") || document.location.href.indexOf("opentag_debug_tool") >= 0) u = true;
              return u
          }
          function v() {
              try {
                  var u = K.TAGSDK_MAIN_DISABLED_FUNCTION;
                  if (u && u()) return true
              } catch (C) {}
              if (document.location.href.indexOf("opentag_disabled\x3dtrue") >= 0) return true;
              return false
          }
          function e(u, C, f) {
              if (f && (u[C] === undefined || u[C] === "" || u[C] === null)) u[C] = f
          }
          function n(u) {
              var C = u.PACKAGE_NAME.split(".");
              C = C[C.length - 1];
              var f = t.getParentObject(u.PACKAGE_NAME);
              f[C] =
                  null;
              delete f[C]
          }
          function c(u) {
              var C = t.getParentObject(u.PACKAGE_NAME);
              var f = C.ClientConfig;
              if (f && f.clientId) return f.clientId;
              else {
                  var x = u.PACKAGE_NAME.split(".");
                  return x[x.length - 2].substring(1)
              }
          }
          var r = A.Cookie;
          var t = A.opentag.Utils;
          A.opentag.Log.setLevelFromCookie();
          a.run = function() {
              var u = false;
              if (v()) return;
              var C = false;
              var f = p();
              var x = f || w();
              if (!C && x) {
                  if (!A.DEBUG_MODE) K.TAGSDK_NS_OVERRIDE = true;
                  else K.TAGSDK_NS_OVERRIDE = false;
                  u = true
              }
              if (A.DEBUG_MODE) K.TAGSDK_NS_OVERRIDE = false;
              try {
                  H.html.UVListener.init()
              } catch (z) {}
              a.runAllContainers(u)
          };
          a.runAllContainers = function(u) {
              try {
                  var C = A.opentag.Container.getContainers();
                  for (var f = 0; f < C.length; f++) {
                      var x = C[f];
                      var z = x.config;
                      if (!x.runningStarted && !x.configuredInMain) {
                          z.scanTags = true;
                          var B = t.getParentObject(x.PACKAGE_NAME).ClientConfig;
                          if (B) e(x.config, "clientId", B.id);
                          var l = t.getParentObject(x.PACKAGE_NAME).SystemDefaults;
                          if (l) {
                              e(z, "pingServerUrl", l.pingServerUrl);
                              e(z, "tellLoadTimesProbability", l.tellLoadTimesProbability)
                          }
                          x.configuredInMain = true;
                          if (u) {
                              x.destroy(true);
                              n(x);
                              a.loadDebugVersion(x)
                          } else if (!K.QUBIT_OPENTAG_STOP_MAIN_EXECUTION) x.run();
                          else(function() {
                              var k = A.opentag.RUN_STOPPED_EXECUTON;
                              A.opentag.RUN_STOPPED_EXECUTON = function() {
                                  try {
                                      if (k) k()
                                  } finally {
                                      x.run()
                                  }
                              }
                          })(x)
                      }
                  }
              } catch (k) {}
          };
          a.loadDebugVersion = function(u) {
              var C = document.createElement("script");
              var f;
              var x = u.config.scriptURL;
              var z = c(u);
              var B = u.getContainerId();
              if (x) {
                  f = x;
                  var l = "-debug.js".length;
                  if (f.lastIndexOf("-debug.js") !== f.length - l) {
                      f = f.substring(0, f.length - ".js".length);
                      f += "-debug.js"
                  }
              }
              var k = "//d3c3cq33003psk.cloudfront.net/opentag-" + z + "-" + B + "-debug.js";
              if (f && k !== f) C.src =
                  f;
              else C.src = "//s3-eu-west-1.amazonaws.com/opentag/opentag-" + z + "-" + B + "-debug.js";
              document.getElementsByTagName("head")[0].appendChild(C)
          };
          A.Define.namespace("qubit.opentag.Main", a)
      })()
  })()
})();
(function() {})();
(function() {
  var M = qubit.opentag.CustomTag;
  var J = new M({
      "usesDocumentWrite": true,
      "reRunOnVariableChange": true,
      "name": "banner2",
      "description": "this is a banner _ name banner2",
      "url": "media1.admicro.vn/ads_codes/ads_box_222.ads"
  });
  var L = "admicrovn.tags.banner2.Tag";
  qubit.Define.clientNamespace(L, J);
  J.htmlContent = "";
  J.addClientFiltersList([]);
  J.addClientVariablesMap({});
  J.addParameters([]);
  J.genericDependencies = [];
  J.addClientDependenciesList(["admicrovn.tags.core.Tag"]);
  J.pre = function() {};
  J.script = function() {};
  J.post = function() {}
})();
(function() {
  var M = qubit.opentag.Container;
  var J = new M({
      "delayDocWrite": false,
      "name": "admicro.vn"
  });
  var L = "admicrovn.Container";
  qubit.Define.clientNamespace(L, J)
})();
(function() {
  var M = qubit.opentag.pagevariable.BaseVariable;
  var J = new M({
      "name": "ADS_Channel"
  });
  var L = "admicrovn.variables.ADSChannel";
  qubit.Define.clientNamespace(L, J);
  J.value = "home"
})();
(function() {
  var M = qubit.opentag.filter.Filter;
  var J = new M({
      "include": false,
      "name": "HomePage",
      "pattern": "http://demo.admicro.vn/createdemoadx/testscript/"
  });
  var L = "admicrovn.filters.HomePage";
  qubit.Define.clientNamespace(L, J);
  J.customStarter = null;
  J.customScript = function(K) {
      return K.getCookie("_pls") || ""
  };
  J.lexicalPattern = '{"rows":[{"operators":[],"starters":[],"variables":[{"type":"cookie","comparator":"0","value":["_pls",""]}]}],"operators":[]}'
})();
(function() {
  try {
      qubit.opentag.Main.run()
  } catch (M) {
      try {
          console.error("Errors during opentag main runtime.", M)
      } catch (J) {}
  }
})();

for (var b = 0; b < d.length; b++) {
VIETNAMESE
cho (var b = 0; b <d.length; b ++) {
EXTENSION OPTIONSMORE »
