function resolveAfter2Seconds(x) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(x);
    }, 2000);
  });
}

async function add1(x) {
  const a = resolveAfter2Seconds(20);
  const b = resolveAfter2Seconds(30);
  return `first: ${await a + await b + x}`;
}

console.time('start');
add1(10).then((v) => {
  console.log(v); // prints 60 after 4 seconds.
  console.timeEnd('start');
});
