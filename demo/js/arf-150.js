!function(e) {
  var n = {};
  function t(o) {
      if (n[o])
          return n[o].exports;
      var r = n[o] = {
          i: o,
          l: !1,
          exports: {}
      };
      return e[o].call(r.exports, r, r.exports, t),
      r.l = !0,
      r.exports
  }
  t.m = e,
  t.c = n,
  t.d = function(e, n, o) {
      t.o(e, n) || Object.defineProperty(e, n, {
          configurable: !1,
          enumerable: !0,
          get: o
      })
  }
  ,
  t.r = function(e) {
      Object.defineProperty(e, "__esModule", {
          value: !0
      })
  }
  ,
  t.n = function(e) {
      var n = e && e.__esModule ? function() {
          return e.default
      }
      : function() {
          return e
      }
      ;
      return t.d(n, "a", n),
      n
  }
  ,
  t.o = function(e, n) {
      return Object.prototype.hasOwnProperty.call(e, n)
  }
  ,
  t.p = "",
  t(t.s = 1)
}([function(e, n, t) {
  "use strict";
  Object.defineProperty(n, "__esModule", {
      value: !0
  }),
  n.encode = function(e, n, t) {
      return function(e) {
          return btoa(encodeURIComponent(e).replace(/%([0-9A-F]{2})/g, function(e, n) {
              return String.fromCharCode("0x" + n)
          }))
      }(c(e, n && t ? c(r(n), t) : c(r(o), 90)))
  }
  ,
  n.decode = function(e, n, t) {
      return c(r(e), n && t ? c(r(n), t) : c(r(o), 90))
  }
  ;
  var o = "a25qbmtjY2g=";
  function r(e) {
      return decodeURIComponent(atob(e).split("").map(function(e) {
          return "%" + ("00" + e.charCodeAt(0).toString(16)).slice(-2)
      }).join(""))
  }
  function c(e, n) {
      var t = ""
        , o = "";
      o = e.toString();
      for (var r = 0; r < o.length; r += 1) {
          var c = o.charCodeAt(r) ^ n;
          t += String.fromCharCode(c)
      }
      return t
  }
}
, function(e, n, t) {
  "use strict";
  var o = function(e) {
      if (e && e.__esModule)
          return e;
      var n = {};
      if (null != e)
          for (var t in e)
              Object.prototype.hasOwnProperty.call(e, t) && (n[t] = e[t]);
      return n.default = e,
      n
  }(t(0));
  var r = {};
  location.search.replace(new RegExp("([^?=&]+)(=([^&]*))?","g"), function(e, n, t, o) {
      r[n] = o
  });
  var c = void 0;
  try {
      c = "dev" === o.decode("4bCG4bCH4bCU", "377fvt+6377fut++", r.corejs_env_key) ? "" : ".min",
      document.getElementById("150") || document.getElementById("admzone150") || document.getElementById("adm-slot-150") || document.write('<zone id="150"></zone>')
  } catch (e) {
      c = ".min"
  }
  var d = {
      el: document.getElementById("150") || document.getElementById("admzone150") || document.getElementById("adm-slot-150"),
      propsData: {
          model: {
              "id": "150",
              "name": "Mixing_300x385_Afamily",
              "height": 0,
              "width": 0,
              "css": "    margin-bottom: 8px;",
              "outputCss": "#zone-150 {\n  margin-bottom: 8px;\n}\n",
              "groupSSP": "",
              "isResponsive": true,
              "isMobile": false,
              "isTemplate": true,
              "template": "%%ARF_CONTENT%%\n<div id=\"advZoneBalloon300x385\" style=\"clear:both;\"></div>",
              "totalShare": 3,
              "status": "active",
              "isPageLoad": true,
              "isZoneVideo": false,
              "siteId": "27",
              "shares": [{
                  "id": "jvrkmgnw",
                  "css": "",
                  "outputCss": "",
                  "width": 0,
                  "height": 0,
                  "classes": "",
                  "isRotate": false,
                  "rotateTime": 0,
                  "type": "single",
                  "format": "",
                  "zoneId": "150",
                  "isTemplate": false,
                  "template": "",
                  "offset": "",
                  "isAdPod": false,
                  "duration": null,
                  "sharePlacements": [{
                      "id": "jvrkujm0",
                      "positionOnShare": 0,
                      "placementId": "565622",
                      "shareId": "jvrkmgnw",
                      "time": "[{\"startTime\":\"1989-12-31T17:00:00.000Z\",\"endTime\":null}]",
                      "placement": {
                          "id": "565622",
                          "width": 0,
                          "height": 0,
                          "startTime": "1989-12-31T17:00:00.000Z",
                          "endTime": null,
                          "weight": 1,
                          "revenueType": "cpm",
                          "cpdPercent": 0,
                          "isRotate": false,
                          "rotateTime": 0,
                          "relative": 0,
                          "campaignId": "1298661",
                          "campaign": {
                              "id": "1298661",
                              "startTime": "1989-12-31T17:00:00.000Z",
                              "endTime": null,
                              "weight": 0,
                              "revenueType": "cpm",
                              "pageLoad": 0,
                              "totalCPM": 0,
                              "isRunMonopoly": false,
                              "optionMonopoly": "",
                              "isRunBudget": false
                          },
                          "banners": [{
                              "id": "565622",
                              "html": "<div id=\"ssppagebid_1201\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1201,w:0,h:0, group:\"1200,1201,1202,1203\"}); });} else {admsspPosition({sspid:1201,w:0,h:0, group:\"1200,1201,1202,1203\"});}</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "565622",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkwfbe",
                                  "logical": "or",
                                  "bannerId": "565622",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwxn5",
                                      "optionBannerId": "jvrkwfbe",
                                      "channelId": "1778",
                                      "channel": {
                                          "id": "1778",
                                          "name": "other-tag",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jvrkoqcp",
                                              "name": "Site Page-URL",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/tag",
                                              "globalVariables": "",
                                              "channelId": "1778",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                  "name": "Site Page-URL",
                                                  "isInputLink": true,
                                                  "isSelectOption": false,
                                                  "isVariable": false,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "jvrkoqcr",
                                              "name": "Site Page-URL",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "toi-nay-nha-minh-an-gi",
                                              "globalVariables": "",
                                              "channelId": "1778",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                  "name": "Site Page-URL",
                                                  "isInputLink": true,
                                                  "isSelectOption": false,
                                                  "isVariable": false,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "jytggder",
                                              "name": "tag",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/tag/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "1778",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "jz120qem",
                                              "name": "Site Page-URL",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/bi-kip-mua-cuoi",
                                              "globalVariables": "",
                                              "channelId": "1778",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                  "name": "Site Page-URL",
                                                  "isInputLink": true,
                                                  "isSelectOption": false,
                                                  "isVariable": false,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "565623",
                              "html": "<div id=\"ssppagebid_1206\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1206,w:0,h:0, group:\"1205,1206,1207,1208\"}); });} else {admsspPosition({sspid:1206,w:0,h:0, group:\"1205,1206,1207,1208\"});}</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "565622",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkwffn",
                                  "logical": "or",
                                  "bannerId": "565623",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwxn6",
                                      "optionBannerId": "jvrkwffn",
                                      "channelId": "491",
                                      "channel": {
                                          "id": "491",
                                          "name": "Đời Sống",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqpkf3w",
                                              "name": "doi-song",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/doi-song/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "491",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "k06jd4fw",
                                              "name": "lifestyle-detail",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/lifestyle/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "491",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "k092bsnt",
                                              "name": "cong-so",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/cong-so/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "491",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "565626",
                              "html": "<div id=\"ssppagebid_1226\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1226,w:0,h:0, group:\"1225,1226,1227,1228\"}); });} else {admsspPosition({sspid:1226,w:0,h:0, group:\"1225,1226,1227,1228\"});}</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "565622",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkwffq",
                                  "logical": "or",
                                  "bannerId": "565626",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwxsc",
                                      "optionBannerId": "jvrkwffq",
                                      "channelId": "290",
                                      "channel": {
                                          "id": "290",
                                          "name": "Afamily_home",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyr01dyb",
                                              "name": "home",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/home/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "290",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "565627",
                              "html": "<div id=\"ssppagebid_1233\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1233,w:0,h:0, group:\"1232,1233,1234,1235\"}); });} else {admsspPosition({sspid:1233,w:0,h:0, group:\"1232,1233,1234,1235\"});}</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "565622",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkwffr",
                                  "logical": "or",
                                  "bannerId": "565627",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwxsd",
                                      "optionBannerId": "jvrkwffr",
                                      "channelId": "492",
                                      "channel": {
                                          "id": "492",
                                          "name": "Anngon-Kheotay",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqpm0cy",
                                              "name": "an-ngon",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/an-ngon-kheo-tay/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "492",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "565630",
                              "html": "<div id=\"ssppagebid_1247\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1247,w:0,h:0, group:\"1248,1249,1246,1247\"}); });} else {admsspPosition({sspid:1247,w:0,h:0, group:\"1248,1249,1246,1247\"});}</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "565622",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkwfkd",
                                  "logical": "or",
                                  "bannerId": "565630",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwxxq",
                                      "optionBannerId": "jvrkwfkd",
                                      "channelId": "493",
                                      "channel": {
                                          "id": "493",
                                          "name": "TinhYeu-HonNhan",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqpo5wg",
                                              "name": "tinh-yeu-hon-nhan",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/tinh-yeu-hon-nhan/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "493",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "k06ja1pf",
                                              "name": "yeu-detail",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/yeu/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "493",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "565633",
                              "html": "<div id=\"ssppagebid_1262\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1262,w:0,h:0, group:\"1264,1261,1262,1263\"}); });} else {admsspPosition({sspid:1262,w:0,h:0, group:\"1264,1261,1262,1263\"});}</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "565622",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkwfok",
                                  "logical": "or",
                                  "bannerId": "565633",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwxxt",
                                      "optionBannerId": "jvrkwfok",
                                      "channelId": "494",
                                      "channel": {
                                          "id": "494",
                                          "name": "SucKhoe",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqpt534",
                                              "name": "suc-khoe",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/suc-khoe/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "494",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "565636",
                              "html": "<div id=\"ssppagebid_1277\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1277,w:0,h:0, group:\"1276,1277,1278,1279\"}); });} else {admsspPosition({sspid:1277,w:0,h:0, group:\"1276,1277,1278,1279\"});}</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "565622",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkwfon",
                                  "logical": "or",
                                  "bannerId": "565636",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwy33",
                                      "optionBannerId": "jvrkwfon",
                                      "channelId": "495",
                                      "channel": {
                                          "id": "495",
                                          "name": "TamSu",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqpvlxy",
                                              "name": "tam-su",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/tam-su/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "495",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "565639",
                              "html": "<div id=\"ssppagebid_1292\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1292,w:0,h:0, group:\"1291,1292,1293,1294\"}); });} else {admsspPosition({sspid:1292,w:0,h:0, group:\"1291,1292,1293,1294\"});}</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "565622",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkwfss",
                                  "logical": "or",
                                  "bannerId": "565639",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwy36",
                                      "optionBannerId": "jvrkwfss",
                                      "channelId": "496",
                                      "channel": {
                                          "id": "496",
                                          "name": "MeVaBe",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqxqbkt",
                                              "name": "me-va-be",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/me-va-be/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "496",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "565642",
                              "html": "<div id=\"ssppagebid_1312\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1312,w:0,h:0, group:\"1312,1313,1314,1311\"}); });} else {admsspPosition({sspid:1312,w:0,h:0, group:\"1312,1313,1314,1311\"});}</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "565622",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkwfsv",
                                  "logical": "or",
                                  "bannerId": "565642",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwy8p",
                                      "optionBannerId": "jvrkwfsv",
                                      "channelId": "490",
                                      "channel": {
                                          "id": "490",
                                          "name": "Đẹp",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyr040lh",
                                              "name": "dep",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/dep/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "490",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "565645",
                              "html": "<div id=\"ssppagebid_1322\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1322,w:0,h:0, group:\"1321,1322,1323,1324\"}); });} else {admsspPosition({sspid:1322,w:0,h:0, group:\"1321,1322,1323,1324\"});}</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "565622",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkwfx5",
                                  "logical": "or",
                                  "bannerId": "565645",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwydp",
                                      "optionBannerId": "jvrkwfx5",
                                      "channelId": "497",
                                      "channel": {
                                          "id": "497",
                                          "name": "HauTruong",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqy4gd9",
                                              "name": "hau-truong",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/hau-truong/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "497",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "k06j6ugn",
                                              "name": "showbiz-detail",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/showbiz/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "497",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "565648",
                              "html": "<div id=\"ssppagebid_1337\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1337,w:0,h:0, group:\"1336,1337,1338,1339\"}); });} else {admsspPosition({sspid:1337,w:0,h:0, group:\"1336,1337,1338,1339\"});}</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "565622",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkwg1a",
                                  "logical": "or",
                                  "bannerId": "565648",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwyds",
                                      "optionBannerId": "jvrkwg1a",
                                      "channelId": "498",
                                      "channel": {
                                          "id": "498",
                                          "name": "GiaiTri",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqy5tz0",
                                              "name": "giai-tri",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/giai-tri/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "498",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "565651",
                              "html": "<div id=\"ssppagebid_1352\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1352,w:0,h:0, group:\"1351,1352,1353,1354\"}); });} else {admsspPosition({sspid:1352,w:0,h:0, group:\"1351,1352,1353,1354\"});}</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "565622",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkwg1e",
                                  "logical": "or",
                                  "bannerId": "565651",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwyiz",
                                      "optionBannerId": "jvrkwg1e",
                                      "channelId": "665",
                                      "channel": {
                                          "id": "665",
                                          "name": "Afamily-Nhahay",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jz4y43si",
                                              "name": "mua-sam-nha-hay",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/mua-sam-nha-hay/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "665",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "k06jcg63",
                                              "name": "mua-sam-nha-hay-detail",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/mua-sam-nha-hay/detail/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "665",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "k06jul2k",
                                              "name": "nha-hay-mua-sam",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/nha-hay-mua-sam/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "665",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "565654",
                              "html": "<div id=\"ssppagebid_1367\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1367,w:0,h:0, group:\"1366,1367,1368,1375\"}); });} else {admsspPosition({sspid:1367,w:0,h:0, group:\"1366,1367,1368,1375\"});}</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "565622",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkwg5h",
                                  "logical": "or",
                                  "bannerId": "565654",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwyj2",
                                      "optionBannerId": "jvrkwg5h",
                                      "channelId": "502",
                                      "channel": {
                                          "id": "502",
                                          "name": "Quiz",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jvrkoqae",
                                              "name": "Site Page-URL",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/quiz",
                                              "globalVariables": "",
                                              "channelId": "502",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                  "name": "Site Page-URL",
                                                  "isInputLink": true,
                                                  "isSelectOption": false,
                                                  "isVariable": false,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "jvrkoqag",
                                              "name": "Site Page-URL",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/xem-bai-tarot",
                                              "globalVariables": "",
                                              "channelId": "502",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                  "name": "Site Page-URL",
                                                  "isInputLink": true,
                                                  "isSelectOption": false,
                                                  "isVariable": false,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "jz0mhd18",
                                              "name": "quiz",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/quiz/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "502",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "565657",
                              "html": "<div id=\"ssppagebid_1382\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1382,w:0,h:0, group:\"1381,1382,1383,1385\"}); });} else {admsspPosition({sspid:1382,w:0,h:0, group:\"1381,1382,1383,1385\"});}</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "565622",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkwg5k",
                                  "logical": "or",
                                  "bannerId": "565657",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwyoc",
                                      "optionBannerId": "jvrkwg5k",
                                      "channelId": "1032",
                                      "channel": {
                                          "id": "1032",
                                          "name": "Xa hoi",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqzudrf",
                                              "name": "xa hoi",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/xa-hoi/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "1032",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "565660",
                              "html": "<div id=\"ssppagebid_1397\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:1397,w:0,h:0, group:\"1396,1397,1398,1399\"}); });} else {admsspPosition({sspid:1397,w:0,h:0, group:\"1396,1397,1398,1399\"});}</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "565622",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkwg9w",
                                  "logical": "or",
                                  "bannerId": "565660",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwyu6",
                                      "optionBannerId": "jvrkwg9w",
                                      "channelId": "1776",
                                      "channel": {
                                          "id": "1776",
                                          "name": "thế giới",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqoh9ju",
                                              "name": "the-gioi",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/the-gioi/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "1776",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }]
                      }
                  }, {
                      "id": "k07vrnsx",
                      "positionOnShare": 0,
                      "placementId": "k07vrns6",
                      "shareId": "jvrkmgnw",
                      "time": "[{\"startTime\":\"2019-09-06T08:51:43.809Z\",\"endTime\":null}]",
                      "placement": {
                          "id": "k07vrns6",
                          "width": 300,
                          "height": 600,
                          "startTime": "1989-12-31T17:00:00.000Z",
                          "endTime": null,
                          "weight": 1,
                          "revenueType": "cpm",
                          "cpdPercent": 0,
                          "isRotate": false,
                          "rotateTime": 0,
                          "relative": 0,
                          "campaignId": "1298661",
                          "campaign": {
                              "id": "1298661",
                              "startTime": "1989-12-31T17:00:00.000Z",
                              "endTime": null,
                              "weight": 0,
                              "revenueType": "cpm",
                              "pageLoad": 0,
                              "totalCPM": 0,
                              "isRunMonopoly": false,
                              "optionMonopoly": "",
                              "isRunBudget": false
                          },
                          "banners": [{
                              "id": "k07vrnsk",
                              "html": "<div id=\"ssppagebid_4026\"></div><script>if (typeof(admsspPosition) == \"undefined\") {_admloadJs(\"//media1.admicro.vn/core/ssppage.js\", function () {admsspPosition({sspid:4026,w:300,h:600, group:\"4025,4026,4027,4028\"}); });} else {admsspPosition({sspid:4026,w:300,h:600, group:\"4025,4026,4027,4028\"});}</script>",
                              "width": 300,
                              "height": 600,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "k07vrns6",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": false,
                              "optionBanners": [{
                                  "id": "k090m8sj",
                                  "logical": "and",
                                  "bannerId": "k07vrnsk",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "k4m8zzw9",
                                      "optionBannerId": "k090m8sj",
                                      "channelId": "k4m7pdwm",
                                      "channel": {
                                          "id": "k4m7pdwm",
                                          "name": "GiaoDuc_Afamily",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "k4m7pz3v",
                                              "name": "giao-duc",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/giao-duc/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "k4m7pdwm",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }]
                      }
                  }]
              }, {
                  "id": "jyxuurm1",
                  "css": "    margin-bottom: 8px;\n> :first-child {\nmargin-bottom: 8px;\n}\n",
                  "outputCss": "#share-jyxuurm1 {\n  margin-bottom: 8px;\n}\n#share-jyxuurm1 > :first-child {\n  margin-bottom: 8px;\n}\n",
                  "width": 300,
                  "height": 600,
                  "classes": "",
                  "isRotate": false,
                  "rotateTime": 0,
                  "type": "multiple",
                  "format": "1,1",
                  "zoneId": "150",
                  "isTemplate": false,
                  "template": "",
                  "offset": "",
                  "isAdPod": false,
                  "duration": null,
                  "sharePlacements": [{
                      "id": "jyxuwhkq",
                      "positionOnShare": 1,
                      "placementId": "363156",
                      "shareId": "jyxuurm1",
                      "time": "[{\"startTime\":\"1989-12-31T17:00:00.000Z\",\"endTime\":null}]",
                      "placement": {
                          "id": "363156",
                          "width": 0,
                          "height": 0,
                          "startTime": "1989-12-31T17:00:00.000Z",
                          "endTime": null,
                          "weight": 1,
                          "revenueType": "pb",
                          "cpdPercent": 0,
                          "isRotate": false,
                          "rotateTime": 0,
                          "relative": 0,
                          "campaignId": "1066388",
                          "campaign": {
                              "id": "1066388",
                              "startTime": "1989-12-31T17:00:00.000Z",
                              "endTime": null,
                              "weight": 0,
                              "revenueType": "cpm",
                              "pageLoad": 0,
                              "totalCPM": 0,
                              "isRunMonopoly": false,
                              "optionMonopoly": "",
                              "isRunBudget": false
                          },
                          "banners": [{
                              "id": "395231",
                              "html": "<div id=\"sspbid_1050\"></div><script type=\"text/javascript\">admsspreg({sspid:1050,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363156",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw6vk",
                                  "logical": "or",
                                  "bannerId": "395231",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwn7j",
                                      "optionBannerId": "jvrkw6vk",
                                      "channelId": "290",
                                      "channel": {
                                          "id": "290",
                                          "name": "Afamily_home",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyr01dyb",
                                              "name": "home",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/home/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "290",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "395237",
                              "html": "<div id=\"sspbid_1058\"></div><script type=\"text/javascript\">admsspreg({sspid:1058,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363156",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw6zu",
                                  "logical": "or",
                                  "bannerId": "395237",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jytmteii",
                                      "optionBannerId": "jvrkw6zu",
                                      "channelId": "490",
                                      "channel": {
                                          "id": "490",
                                          "name": "Đẹp",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyr040lh",
                                              "name": "dep",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/dep/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "490",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "395243",
                              "html": "<div id=\"sspbid_1067\"></div><script type=\"text/javascript\">admsspreg({sspid:1067,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363156",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw743",
                                  "logical": "or",
                                  "bannerId": "395243",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwnna",
                                      "optionBannerId": "jvrkw743",
                                      "channelId": "1032",
                                      "channel": {
                                          "id": "1032",
                                          "name": "Xa hoi",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqzudrf",
                                              "name": "xa hoi",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/xa-hoi/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "1032",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "395249",
                              "html": "<div id=\"sspbid_1074\"></div><script type=\"text/javascript\">admsspreg({sspid:1074,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363156",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw7cg",
                                  "logical": "or",
                                  "bannerId": "395249",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jytn0lro",
                                      "optionBannerId": "jvrkw7cg",
                                      "channelId": "491",
                                      "channel": {
                                          "id": "491",
                                          "name": "Đời Sống",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqpkf3w",
                                              "name": "doi-song",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/doi-song/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "491",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "k06jd4fw",
                                              "name": "lifestyle-detail",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/lifestyle/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "491",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "k092bsnt",
                                              "name": "cong-so",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/cong-so/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "491",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "395255",
                              "html": "<div id=\"sspbid_1082\"></div><script type=\"text/javascript\">admsspreg({sspid:1082,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363156",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw7hb",
                                  "logical": "or",
                                  "bannerId": "395255",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jytrb0ha",
                                      "optionBannerId": "jvrkw7hb",
                                      "channelId": "492",
                                      "channel": {
                                          "id": "492",
                                          "name": "Anngon-Kheotay",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqpm0cy",
                                              "name": "an-ngon",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/an-ngon-kheo-tay/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "492",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "395261",
                              "html": "<div id=\"sspbid_1090\"></div><script type=\"text/javascript\">admsspreg({sspid:1090,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363156",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw7p0",
                                  "logical": "or",
                                  "bannerId": "395261",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jytruyi2",
                                      "optionBannerId": "jvrkw7p0",
                                      "channelId": "493",
                                      "channel": {
                                          "id": "493",
                                          "name": "TinhYeu-HonNhan",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqpo5wg",
                                              "name": "tinh-yeu-hon-nhan",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/tinh-yeu-hon-nhan/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "493",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "k06ja1pf",
                                              "name": "yeu-detail",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/yeu/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "493",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "395267",
                              "html": "<div id=\"sspbid_1098\"></div><script type=\"text/javascript\">admsspreg({sspid:1098,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363156",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw7tc",
                                  "logical": "or",
                                  "bannerId": "395267",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jyts4j3j",
                                      "optionBannerId": "jvrkw7tc",
                                      "channelId": "494",
                                      "channel": {
                                          "id": "494",
                                          "name": "SucKhoe",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqpt534",
                                              "name": "suc-khoe",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/suc-khoe/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "494",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "395273",
                              "html": "<div id=\"sspbid_1106\"></div><script type=\"text/javascript\">admsspreg({sspid:1106,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363156",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw7xm",
                                  "logical": "or",
                                  "bannerId": "395273",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jytsdpou",
                                      "optionBannerId": "jvrkw7xm",
                                      "channelId": "495",
                                      "channel": {
                                          "id": "495",
                                          "name": "TamSu",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqpvlxy",
                                              "name": "tam-su",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/tam-su/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "495",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "395279",
                              "html": "<div id=\"sspbid_1114\"></div><script type=\"text/javascript\">admsspreg({sspid:1114,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363156",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw860",
                                  "logical": "or",
                                  "bannerId": "395279",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jytslrvc",
                                      "optionBannerId": "jvrkw860",
                                      "channelId": "496",
                                      "channel": {
                                          "id": "496",
                                          "name": "MeVaBe",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqxqbkt",
                                              "name": "me-va-be",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/me-va-be/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "496",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "395285",
                              "html": "<div id=\"sspbid_1123\"></div><script type=\"text/javascript\">admsspreg({sspid:1123,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363156",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw8ag",
                                  "logical": "or",
                                  "bannerId": "395285",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwox6",
                                      "optionBannerId": "jvrkw8ag",
                                      "channelId": "665",
                                      "channel": {
                                          "id": "665",
                                          "name": "Afamily-Nhahay",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jz4y43si",
                                              "name": "mua-sam-nha-hay",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/mua-sam-nha-hay/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "665",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "k06jcg63",
                                              "name": "mua-sam-nha-hay-detail",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/mua-sam-nha-hay/detail/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "665",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "k06jul2k",
                                              "name": "nha-hay-mua-sam",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/nha-hay-mua-sam/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "665",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "395291",
                              "html": "<div id=\"sspbid_1130\"></div><script type=\"text/javascript\">admsspreg({sspid:1130,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363156",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw8em",
                                  "logical": "or",
                                  "bannerId": "395291",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jytt1aap",
                                      "optionBannerId": "jvrkw8em",
                                      "channelId": "497",
                                      "channel": {
                                          "id": "497",
                                          "name": "HauTruong",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqy4gd9",
                                              "name": "hau-truong",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/hau-truong/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "497",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "k06j6ugn",
                                              "name": "showbiz-detail",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/showbiz/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "497",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "395297",
                              "html": "<div id=\"sspbid_1138\"></div><script type=\"text/javascript\">admsspreg({sspid:1138,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363156",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw8ix",
                                  "logical": "or",
                                  "bannerId": "395297",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jytt8ved",
                                      "optionBannerId": "jvrkw8ix",
                                      "channelId": "498",
                                      "channel": {
                                          "id": "498",
                                          "name": "GiaiTri",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqy5tz0",
                                              "name": "giai-tri",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/giai-tri/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "498",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "395303",
                              "html": "<div id=\"sspbid_1146\"></div><script type=\"text/javascript\">admsspreg({sspid:1146,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363156",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw8n7",
                                  "logical": "or",
                                  "bannerId": "395303",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jz7xj1b9",
                                      "optionBannerId": "jvrkw8n7",
                                      "channelId": "500",
                                      "channel": {
                                          "id": "500",
                                          "name": "ChuyenLa",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqysvel",
                                              "name": "chuyen-la",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/chuyen-la/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "500",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "395310",
                              "html": "<div id=\"sspbid_1155\"></div><script type=\"text/javascript\">admsspreg({sspid:1155,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363156",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw8w5",
                                  "logical": "or",
                                  "bannerId": "395310",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "k2vlqzd7",
                                      "optionBannerId": "jvrkw8w5",
                                      "channelId": "502",
                                      "channel": {
                                          "id": "502",
                                          "name": "Quiz",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jvrkoqae",
                                              "name": "Site Page-URL",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/quiz",
                                              "globalVariables": "",
                                              "channelId": "502",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                  "name": "Site Page-URL",
                                                  "isInputLink": true,
                                                  "isSelectOption": false,
                                                  "isVariable": false,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "jvrkoqag",
                                              "name": "Site Page-URL",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/xem-bai-tarot",
                                              "globalVariables": "",
                                              "channelId": "502",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                  "name": "Site Page-URL",
                                                  "isInputLink": true,
                                                  "isSelectOption": false,
                                                  "isVariable": false,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "jz0mhd18",
                                              "name": "quiz",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/quiz/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "502",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "556315",
                              "html": "<div id=\"sspbid_3557\"></div><script type=\"text/javascript\">admsspreg({sspid:3557,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363156",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkwbeb",
                                  "logical": "or",
                                  "bannerId": "556315",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwsu6",
                                      "optionBannerId": "jvrkwbeb",
                                      "channelId": "1778",
                                      "channel": {
                                          "id": "1778",
                                          "name": "other-tag",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jvrkoqcp",
                                              "name": "Site Page-URL",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/tag",
                                              "globalVariables": "",
                                              "channelId": "1778",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                  "name": "Site Page-URL",
                                                  "isInputLink": true,
                                                  "isSelectOption": false,
                                                  "isVariable": false,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "jvrkoqcr",
                                              "name": "Site Page-URL",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "toi-nay-nha-minh-an-gi",
                                              "globalVariables": "",
                                              "channelId": "1778",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                  "name": "Site Page-URL",
                                                  "isInputLink": true,
                                                  "isSelectOption": false,
                                                  "isVariable": false,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "jytggder",
                                              "name": "tag",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/tag/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "1778",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "jz120qem",
                                              "name": "Site Page-URL",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/bi-kip-mua-cuoi",
                                              "globalVariables": "",
                                              "channelId": "1778",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                  "name": "Site Page-URL",
                                                  "isInputLink": true,
                                                  "isSelectOption": false,
                                                  "isVariable": false,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "557054",
                              "html": "<div id=\"sspbid_2530\"></div><script type=\"text/javascript\">admsspreg({sspid:2530,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363156",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkwbix",
                                  "logical": "or",
                                  "bannerId": "557054",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwszd",
                                      "optionBannerId": "jvrkwbix",
                                      "channelId": "1776",
                                      "channel": {
                                          "id": "1776",
                                          "name": "thế giới",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqoh9ju",
                                              "name": "the-gioi",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/the-gioi/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "1776",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }]
                      }
                  }, {
                      "id": "jyxuwwdt",
                      "positionOnShare": 2,
                      "placementId": "363158",
                      "shareId": "jyxuurm1",
                      "time": "[{\"startTime\":\"1989-12-31T17:00:00.000Z\",\"endTime\":null}]",
                      "placement": {
                          "id": "363158",
                          "width": 0,
                          "height": 0,
                          "startTime": "1989-12-31T17:00:00.000Z",
                          "endTime": null,
                          "weight": 1,
                          "revenueType": "pb",
                          "cpdPercent": 0,
                          "isRotate": false,
                          "rotateTime": 0,
                          "relative": 0,
                          "campaignId": "1066388",
                          "campaign": {
                              "id": "1066388",
                              "startTime": "1989-12-31T17:00:00.000Z",
                              "endTime": null,
                              "weight": 0,
                              "revenueType": "cpm",
                              "pageLoad": 0,
                              "totalCPM": 0,
                              "isRunMonopoly": false,
                              "optionMonopoly": "",
                              "isRunBudget": false
                          },
                          "banners": [{
                              "id": "395232",
                              "html": "<div id=\"sspbid_1051\"></div><script type=\"text/javascript\">admsspreg({sspid:1051,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363158",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw6vm",
                                  "logical": "or",
                                  "bannerId": "395232",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwncp",
                                      "optionBannerId": "jvrkw6vm",
                                      "channelId": "290",
                                      "channel": {
                                          "id": "290",
                                          "name": "Afamily_home",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyr01dyb",
                                              "name": "home",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/home/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "290",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "395238",
                              "html": "<div id=\"sspbid_1059\"></div><script type=\"text/javascript\">admsspreg({sspid:1059,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363158",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw6zv",
                                  "logical": "or",
                                  "bannerId": "395238",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jytmukz5",
                                      "optionBannerId": "jvrkw6zv",
                                      "channelId": "490",
                                      "channel": {
                                          "id": "490",
                                          "name": "Đẹp",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyr040lh",
                                              "name": "dep",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/dep/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "490",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }, {
                                  "id": "jvrkw6zw",
                                  "logical": "or",
                                  "bannerId": "395238",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwnhz",
                                      "optionBannerId": "jvrkw6zw",
                                      "channelId": "1777",
                                      "channel": {
                                          "id": "1777",
                                          "name": "đẹp - only http://afamily.vn/dep.chn",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jvrkoqcl",
                                              "name": "Site Page-URL",
                                              "logical": "or",
                                              "comparison": "==",
                                              "value": "http://afamily.vn/dep.chn",
                                              "globalVariables": "",
                                              "channelId": "1777",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                  "name": "Site Page-URL",
                                                  "isInputLink": true,
                                                  "isSelectOption": false,
                                                  "isVariable": false,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "jyqomv2r",
                                              "name": "dep",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/dep/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "1777",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "395242",
                              "html": "<div id=\"sspbid_1065\"></div><script type=\"text/javascript\">admsspreg({sspid:1065,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363158",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw742",
                                  "logical": "or",
                                  "bannerId": "395242",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwnn9",
                                      "optionBannerId": "jvrkw742",
                                      "channelId": "1032",
                                      "channel": {
                                          "id": "1032",
                                          "name": "Xa hoi",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqzudrf",
                                              "name": "xa hoi",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/xa-hoi/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "1032",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "395250",
                              "html": "<div id=\"sspbid_1075\"></div><script type=\"text/javascript\">admsspreg({sspid:1075,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363158",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw7ch",
                                  "logical": "or",
                                  "bannerId": "395250",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jytn0ydj",
                                      "optionBannerId": "jvrkw7ch",
                                      "channelId": "491",
                                      "channel": {
                                          "id": "491",
                                          "name": "Đời Sống",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqpkf3w",
                                              "name": "doi-song",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/doi-song/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "491",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "k06jd4fw",
                                              "name": "lifestyle-detail",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/lifestyle/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "491",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "k092bsnt",
                                              "name": "cong-so",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/cong-so/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "491",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "395256",
                              "html": "<div id=\"sspbid_1083\"></div><script type=\"text/javascript\">admsspreg({sspid:1083,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363158",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw7hc",
                                  "logical": "or",
                                  "bannerId": "395256",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jytrpuga",
                                      "optionBannerId": "jvrkw7hc",
                                      "channelId": "492",
                                      "channel": {
                                          "id": "492",
                                          "name": "Anngon-Kheotay",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqpm0cy",
                                              "name": "an-ngon",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/an-ngon-kheo-tay/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "492",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "395262",
                              "html": "<div id=\"sspbid_1091\"></div><script type=\"text/javascript\">admsspreg({sspid:1091,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363158",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw7p1",
                                  "logical": "or",
                                  "bannerId": "395262",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jytrv9js",
                                      "optionBannerId": "jvrkw7p1",
                                      "channelId": "493",
                                      "channel": {
                                          "id": "493",
                                          "name": "TinhYeu-HonNhan",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqpo5wg",
                                              "name": "tinh-yeu-hon-nhan",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/tinh-yeu-hon-nhan/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "493",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "k06ja1pf",
                                              "name": "yeu-detail",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/yeu/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "493",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "395268",
                              "html": "<div id=\"sspbid_1099\"></div><script type=\"text/javascript\">admsspreg({sspid:1099,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363158",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw7te",
                                  "logical": "or",
                                  "bannerId": "395268",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jyts4w4o",
                                      "optionBannerId": "jvrkw7te",
                                      "channelId": "494",
                                      "channel": {
                                          "id": "494",
                                          "name": "SucKhoe",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqpt534",
                                              "name": "suc-khoe",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/suc-khoe/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "494",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "395274",
                              "html": "<div id=\"sspbid_1107\"></div><script type=\"text/javascript\">admsspreg({sspid:1107,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363158",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw81p",
                                  "logical": "or",
                                  "bannerId": "395274",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jytsfiiw",
                                      "optionBannerId": "jvrkw81p",
                                      "channelId": "495",
                                      "channel": {
                                          "id": "495",
                                          "name": "TamSu",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqpvlxy",
                                              "name": "tam-su",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/tam-su/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "495",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "395280",
                              "html": "<div id=\"sspbid_1115\"></div><script type=\"text/javascript\">admsspreg({sspid:1115,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363158",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw861",
                                  "logical": "or",
                                  "bannerId": "395280",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jytsnz36",
                                      "optionBannerId": "jvrkw861",
                                      "channelId": "496",
                                      "channel": {
                                          "id": "496",
                                          "name": "MeVaBe",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqxqbkt",
                                              "name": "me-va-be",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/me-va-be/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "496",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "395284",
                              "html": "<div id=\"sspbid_1121\"></div><script type=\"text/javascript\">admsspreg({sspid:1121,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363158",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw8af",
                                  "logical": "or",
                                  "bannerId": "395284",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwox5",
                                      "optionBannerId": "jvrkw8af",
                                      "channelId": "665",
                                      "channel": {
                                          "id": "665",
                                          "name": "Afamily-Nhahay",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jz4y43si",
                                              "name": "mua-sam-nha-hay",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/mua-sam-nha-hay/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "665",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "k06jcg63",
                                              "name": "mua-sam-nha-hay-detail",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/mua-sam-nha-hay/detail/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "665",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "k06jul2k",
                                              "name": "nha-hay-mua-sam",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/nha-hay-mua-sam/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "665",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "395292",
                              "html": "<div id=\"sspbid_1131\"></div><script type=\"text/javascript\">admsspreg({sspid:1131,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363158",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw8en",
                                  "logical": "or",
                                  "bannerId": "395292",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jytt402m",
                                      "optionBannerId": "jvrkw8en",
                                      "channelId": "497",
                                      "channel": {
                                          "id": "497",
                                          "name": "HauTruong",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqy4gd9",
                                              "name": "hau-truong",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/hau-truong/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "497",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "k06j6ugn",
                                              "name": "showbiz-detail",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/showbiz/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "497",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "395298",
                              "html": "<div id=\"sspbid_1139\"></div><script type=\"text/javascript\">admsspreg({sspid:1139,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363158",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw8iy",
                                  "logical": "or",
                                  "bannerId": "395298",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jz7xh1yk",
                                      "optionBannerId": "jvrkw8iy",
                                      "channelId": "498",
                                      "channel": {
                                          "id": "498",
                                          "name": "GiaiTri",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqy5tz0",
                                              "name": "giai-tri",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/giai-tri/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "498",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "395304",
                              "html": "<div id=\"sspbid_1147\"></div><script type=\"text/javascript\">admsspreg({sspid:1147,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363158",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw8rq",
                                  "logical": "or",
                                  "bannerId": "395304",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwpik",
                                      "optionBannerId": "jvrkw8rq",
                                      "channelId": "500",
                                      "channel": {
                                          "id": "500",
                                          "name": "ChuyenLa",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqysvel",
                                              "name": "chuyen-la",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/chuyen-la/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "500",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "395311",
                              "html": "<div id=\"sspbid_1156\"></div><script type=\"text/javascript\">admsspreg({sspid:1156,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363158",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkw8w6",
                                  "logical": "or",
                                  "bannerId": "395311",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "k2vlragj",
                                      "optionBannerId": "jvrkw8w6",
                                      "channelId": "502",
                                      "channel": {
                                          "id": "502",
                                          "name": "Quiz",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jvrkoqae",
                                              "name": "Site Page-URL",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/quiz",
                                              "globalVariables": "",
                                              "channelId": "502",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                  "name": "Site Page-URL",
                                                  "isInputLink": true,
                                                  "isSelectOption": false,
                                                  "isVariable": false,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "jvrkoqag",
                                              "name": "Site Page-URL",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/xem-bai-tarot",
                                              "globalVariables": "",
                                              "channelId": "502",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                  "name": "Site Page-URL",
                                                  "isInputLink": true,
                                                  "isSelectOption": false,
                                                  "isVariable": false,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "jz0mhd18",
                                              "name": "quiz",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/quiz/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "502",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "556316",
                              "html": "<div id=\"sspbid_3558\"></div><script type=\"text/javascript\">admsspreg({sspid:3558,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363158",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkwbec",
                                  "logical": "or",
                                  "bannerId": "556316",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwsu7",
                                      "optionBannerId": "jvrkwbec",
                                      "channelId": "1778",
                                      "channel": {
                                          "id": "1778",
                                          "name": "other-tag",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jvrkoqcp",
                                              "name": "Site Page-URL",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/tag",
                                              "globalVariables": "",
                                              "channelId": "1778",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                  "name": "Site Page-URL",
                                                  "isInputLink": true,
                                                  "isSelectOption": false,
                                                  "isVariable": false,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "jvrkoqcr",
                                              "name": "Site Page-URL",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "toi-nay-nha-minh-an-gi",
                                              "globalVariables": "",
                                              "channelId": "1778",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                  "name": "Site Page-URL",
                                                  "isInputLink": true,
                                                  "isSelectOption": false,
                                                  "isVariable": false,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "jytggder",
                                              "name": "tag",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/tag/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "1778",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "jz120qem",
                                              "name": "Site Page-URL",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/bi-kip-mua-cuoi",
                                              "globalVariables": "",
                                              "channelId": "1778",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                  "name": "Site Page-URL",
                                                  "isInputLink": true,
                                                  "isSelectOption": false,
                                                  "isVariable": false,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "557055",
                              "html": "<div id=\"sspbid_2531\"></div><script type=\"text/javascript\">admsspreg({sspid:2531,w:300,h:250});</script>",
                              "width": 0,
                              "height": 0,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "363158",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jvrkwbiy",
                                  "logical": "or",
                                  "bannerId": "557055",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jvrkwsze",
                                      "optionBannerId": "jvrkwbiy",
                                      "channelId": "1776",
                                      "channel": {
                                          "id": "1776",
                                          "name": "thế giới",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyqoh9ju",
                                              "name": "the-gioi",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/the-gioi/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "1776",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }]
                      }
                  }, {
                      "id": "jyy2c7fy",
                      "positionOnShare": 2,
                      "placementId": "276879",
                      "shareId": "jyxuurm1",
                      "time": "[{\"startTime\":\"1989-12-31T17:00:00.000Z\",\"endTime\":null}]",
                      "placement": {
                          "id": "276879",
                          "width": 300,
                          "height": 250,
                          "startTime": "1989-12-31T17:00:00.000Z",
                          "endTime": null,
                          "weight": 1,
                          "revenueType": "pr",
                          "cpdPercent": 1,
                          "isRotate": false,
                          "rotateTime": 0,
                          "relative": 0,
                          "campaignId": "1042597",
                          "campaign": {
                              "id": "1042597",
                              "startTime": "1989-12-31T17:00:00.000Z",
                              "endTime": null,
                              "weight": 0,
                              "revenueType": "cpd",
                              "pageLoad": 0,
                              "totalCPM": 0,
                              "isRunMonopoly": false,
                              "optionMonopoly": "",
                              "isRunBudget": false
                          },
                          "banners": [{
                              "id": "276879",
                              "html": "<script type=\"text/javascript\">\r\ngoogle_ad_client = \"ca-pub-6366951472589375\";\r\n\r\ngoogle_ad_slot = \"5426976522\";\r\ngoogle_ad_width = 300;\r\ngoogle_ad_height = 250;\r\n\r\n</script>\r\n<script type=\"text/javascript\"\r\nsrc=\"//pagead2.googlesyndication.com/pagead/show_ads.js\">\r\n</script>",
                              "width": 300,
                              "height": 250,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "276879",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": true,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jys30mym",
                                  "logical": "and",
                                  "bannerId": "276879",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jys30niz",
                                      "optionBannerId": "jys30mym",
                                      "channelId": "jys2nivx",
                                      "channel": {
                                          "id": "jys2nivx",
                                          "name": "location_nuocngoai",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jys2vc6b",
                                              "name": "Location",
                                              "logical": "and",
                                              "comparison": "==",
                                              "value": "101",
                                              "globalVariables": "__RC",
                                              "channelId": "jys2nivx",
                                              "valueSelect": "__RC>=101",
                                              "logicalSelect": ">=",
                                              "optionChannelType": {
                                                  "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
                                                  "name": "Location",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": false,
                                                  "status": "active",
                                                  "isMultiSelect": true,
                                                  "isGlobal": true,
                                                  "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]"
                                              }
                                          }, {
                                              "id": "jys2vc7t",
                                              "name": "Location",
                                              "logical": "and",
                                              "comparison": "!=",
                                              "value": "undefined",
                                              "globalVariables": "__RC",
                                              "channelId": "jys2nivx",
                                              "valueSelect": "__RC==undefined",
                                              "logicalSelect": "==",
                                              "optionChannelType": {
                                                  "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
                                                  "name": "Location",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": false,
                                                  "status": "active",
                                                  "isMultiSelect": true,
                                                  "isGlobal": true,
                                                  "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]"
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }]
                      }
                  }, {
                      "id": "jyy2co67",
                      "positionOnShare": 1,
                      "placementId": "276875",
                      "shareId": "jyxuurm1",
                      "time": "[{\"startTime\":\"1989-12-31T17:00:00.000Z\",\"endTime\":null}]",
                      "placement": {
                          "id": "276875",
                          "width": 300,
                          "height": 250,
                          "startTime": "1989-12-31T17:00:00.000Z",
                          "endTime": null,
                          "weight": 1,
                          "revenueType": "pr",
                          "cpdPercent": 1,
                          "isRotate": false,
                          "rotateTime": 0,
                          "relative": 0,
                          "campaignId": "1042597",
                          "campaign": {
                              "id": "1042597",
                              "startTime": "1989-12-31T17:00:00.000Z",
                              "endTime": null,
                              "weight": 0,
                              "revenueType": "cpd",
                              "pageLoad": 0,
                              "totalCPM": 0,
                              "isRunMonopoly": false,
                              "optionMonopoly": "",
                              "isRunBudget": false
                          },
                          "banners": [{
                              "id": "276875",
                              "html": "<script type=\"text/javascript\">\r\ngoogle_ad_client = \"ca-pub-6366951472589375\";\r\n\r\ngoogle_ad_slot = \"5426976522\";\r\ngoogle_ad_width = 300;\r\ngoogle_ad_height = 250;\r\n\r\n</script>\r\n<script type=\"text/javascript\"\r\nsrc=\"//pagead2.googlesyndication.com/pagead/show_ads.js\">\r\n</script>",
                              "width": 300,
                              "height": 250,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "276875",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": true,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jys303l3",
                                  "logical": "and",
                                  "bannerId": "276875",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jys30443",
                                      "optionBannerId": "jys303l3",
                                      "channelId": "jys2nivx",
                                      "channel": {
                                          "id": "jys2nivx",
                                          "name": "location_nuocngoai",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jys2vc6b",
                                              "name": "Location",
                                              "logical": "and",
                                              "comparison": "==",
                                              "value": "101",
                                              "globalVariables": "__RC",
                                              "channelId": "jys2nivx",
                                              "valueSelect": "__RC>=101",
                                              "logicalSelect": ">=",
                                              "optionChannelType": {
                                                  "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
                                                  "name": "Location",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": false,
                                                  "status": "active",
                                                  "isMultiSelect": true,
                                                  "isGlobal": true,
                                                  "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]"
                                              }
                                          }, {
                                              "id": "jys2vc7t",
                                              "name": "Location",
                                              "logical": "and",
                                              "comparison": "!=",
                                              "value": "undefined",
                                              "globalVariables": "__RC",
                                              "channelId": "jys2nivx",
                                              "valueSelect": "__RC==undefined",
                                              "logicalSelect": "==",
                                              "optionChannelType": {
                                                  "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
                                                  "name": "Location",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": false,
                                                  "status": "active",
                                                  "isMultiSelect": true,
                                                  "isGlobal": true,
                                                  "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]"
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }]
                      }
                  }, {
                      "id": "jyy2d5sx",
                      "positionOnShare": 2,
                      "placementId": "171189",
                      "shareId": "jyxuurm1",
                      "time": "[{\"startTime\":\"1989-12-31T17:00:00.000Z\",\"endTime\":\"2019-08-05T07:19:07.591Z\"},{\"startTime\":\"2019-08-05T07:19:07.591Z\",\"endTime\":null}]",
                      "placement": {
                          "id": "171189",
                          "width": 300,
                          "height": 250,
                          "startTime": "1989-12-31T17:00:00.000Z",
                          "endTime": null,
                          "weight": 1,
                          "revenueType": "pr",
                          "cpdPercent": 0,
                          "isRotate": false,
                          "rotateTime": 0,
                          "relative": 0,
                          "campaignId": "1027589",
                          "campaign": {
                              "id": "1027589",
                              "startTime": "1989-12-31T17:00:00.000Z",
                              "endTime": null,
                              "weight": 0,
                              "revenueType": "cpm",
                              "pageLoad": 0,
                              "totalCPM": 0,
                              "isRunMonopoly": false,
                              "optionMonopoly": "",
                              "isRunBudget": false
                          },
                          "banners": [{
                              "id": "171189",
                              "html": "<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script>\r\n<ins class=\"adsbygoogle\"\r\n    style=\"display:inline-block;width:300px;height:250px\"\r\n    data-ad-client=\"ca-pub-0204890140576859\"\r\n    data-ad-slot=\"5143177846\"></ins>\r\n<script>\r\n(adsbygoogle = window.adsbygoogle || []).push({});\r\n</script>",
                              "width": 300,
                              "height": 250,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "171189",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": true,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jys6v5ji",
                                  "logical": "and",
                                  "bannerId": "171189",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jys6v61h",
                                      "optionBannerId": "jys6v5ji",
                                      "channelId": "jys2nivx",
                                      "channel": {
                                          "id": "jys2nivx",
                                          "name": "location_nuocngoai",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jys2vc6b",
                                              "name": "Location",
                                              "logical": "and",
                                              "comparison": "==",
                                              "value": "101",
                                              "globalVariables": "__RC",
                                              "channelId": "jys2nivx",
                                              "valueSelect": "__RC>=101",
                                              "logicalSelect": ">=",
                                              "optionChannelType": {
                                                  "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
                                                  "name": "Location",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": false,
                                                  "status": "active",
                                                  "isMultiSelect": true,
                                                  "isGlobal": true,
                                                  "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]"
                                              }
                                          }, {
                                              "id": "jys2vc7t",
                                              "name": "Location",
                                              "logical": "and",
                                              "comparison": "!=",
                                              "value": "undefined",
                                              "globalVariables": "__RC",
                                              "channelId": "jys2nivx",
                                              "valueSelect": "__RC==undefined",
                                              "logicalSelect": "==",
                                              "optionChannelType": {
                                                  "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
                                                  "name": "Location",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": false,
                                                  "status": "active",
                                                  "isMultiSelect": true,
                                                  "isGlobal": true,
                                                  "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]"
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }, {
                              "id": "171191",
                              "html": "<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script>\r\n<ins class=\"adsbygoogle\"\r\n    style=\"display:inline-block;width:300px;height:250px\"\r\n    data-ad-client=\"ca-pub-0204890140576859\"\r\n    data-ad-slot=\"9573377442\"></ins>\r\n<script>\r\n(adsbygoogle = window.adsbygoogle || []).push({});\r\n</script>",
                              "width": 300,
                              "height": 250,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "171189",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": true,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "jysec6d9",
                                  "logical": "and",
                                  "bannerId": "171191",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "jysec727",
                                      "optionBannerId": "jysec6d9",
                                      "channelId": "jys2nivx",
                                      "channel": {
                                          "id": "jys2nivx",
                                          "name": "location_nuocngoai",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jys2vc6b",
                                              "name": "Location",
                                              "logical": "and",
                                              "comparison": "==",
                                              "value": "101",
                                              "globalVariables": "__RC",
                                              "channelId": "jys2nivx",
                                              "valueSelect": "__RC>=101",
                                              "logicalSelect": ">=",
                                              "optionChannelType": {
                                                  "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
                                                  "name": "Location",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": false,
                                                  "status": "active",
                                                  "isMultiSelect": true,
                                                  "isGlobal": true,
                                                  "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]"
                                              }
                                          }, {
                                              "id": "jys2vc7t",
                                              "name": "Location",
                                              "logical": "and",
                                              "comparison": "!=",
                                              "value": "undefined",
                                              "globalVariables": "__RC",
                                              "channelId": "jys2nivx",
                                              "valueSelect": "__RC==undefined",
                                              "logicalSelect": "==",
                                              "optionChannelType": {
                                                  "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
                                                  "name": "Location",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": false,
                                                  "status": "active",
                                                  "isMultiSelect": true,
                                                  "isGlobal": true,
                                                  "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]"
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }]
                      }
                  }, {
                      "id": "k07vu8zb",
                      "positionOnShare": 1,
                      "placementId": "k07vu8yh",
                      "shareId": "jyxuurm1",
                      "time": "[{\"startTime\":\"2019-09-06T08:53:44.567Z\",\"endTime\":null}]",
                      "placement": {
                          "id": "k07vu8yh",
                          "width": 300,
                          "height": 250,
                          "startTime": "1989-12-31T17:00:00.000Z",
                          "endTime": null,
                          "weight": 1,
                          "revenueType": "pb",
                          "cpdPercent": 0,
                          "isRotate": false,
                          "rotateTime": 0,
                          "relative": 0,
                          "campaignId": "1298661",
                          "campaign": {
                              "id": "1298661",
                              "startTime": "1989-12-31T17:00:00.000Z",
                              "endTime": null,
                              "weight": 0,
                              "revenueType": "cpm",
                              "pageLoad": 0,
                              "totalCPM": 0,
                              "isRunMonopoly": false,
                              "optionMonopoly": "",
                              "isRunBudget": false
                          },
                          "banners": [{
                              "id": "k07vu8yt",
                              "html": "<div id=\"sspbid_2012300\"></div><script type=\"text/javascript\">admsspreg({sspid:2012300,w:300,h:250});</script>",
                              "width": 300,
                              "height": 250,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "k07vu8yh",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": false,
                              "optionBanners": [{
                                  "id": "k090m2ve",
                                  "logical": "and",
                                  "bannerId": "k07vu8yt",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "k4m8zsez",
                                      "optionBannerId": "k090m2ve",
                                      "channelId": "k4m7pdwm",
                                      "channel": {
                                          "id": "k4m7pdwm",
                                          "name": "GiaoDuc_Afamily",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "k4m7pz3v",
                                              "name": "giao-duc",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/giao-duc/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "k4m7pdwm",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }]
                      }
                  }, {
                      "id": "k07vu8zd",
                      "positionOnShare": 2,
                      "placementId": "k07vu8yj",
                      "shareId": "jyxuurm1",
                      "time": "[{\"startTime\":\"2019-09-06T08:53:44.569Z\",\"endTime\":null}]",
                      "placement": {
                          "id": "k07vu8yj",
                          "width": 300,
                          "height": 250,
                          "startTime": "1989-12-31T17:00:00.000Z",
                          "endTime": null,
                          "weight": 1,
                          "revenueType": "pb",
                          "cpdPercent": 0,
                          "isRotate": false,
                          "rotateTime": 0,
                          "relative": 0,
                          "campaignId": "1298661",
                          "campaign": {
                              "id": "1298661",
                              "startTime": "1989-12-31T17:00:00.000Z",
                              "endTime": null,
                              "weight": 0,
                              "revenueType": "cpm",
                              "pageLoad": 0,
                              "totalCPM": 0,
                              "isRunMonopoly": false,
                              "optionMonopoly": "",
                              "isRunBudget": false
                          },
                          "banners": [{
                              "id": "k07vu8yx",
                              "html": "<div id=\"sspbid_2012301\"></div><script type=\"text/javascript\">admsspreg({sspid:2012301,w:300,h:250});</script>",
                              "width": 300,
                              "height": 250,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "k07vu8yj",
                              "imageUrl": "",
                              "url": "",
                              "target": "_blank",
                              "isIFrame": false,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": false,
                              "optionBanners": [{
                                  "id": "k090m616",
                                  "logical": "and",
                                  "bannerId": "k07vu8yx",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "k4m8zy3h",
                                      "optionBannerId": "k090m616",
                                      "channelId": "k4m7pdwm",
                                      "channel": {
                                          "id": "k4m7pdwm",
                                          "name": "GiaoDuc_Afamily",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "k4m7pz3v",
                                              "name": "giao-duc",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/giao-duc/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "k4m7pdwm",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }]
                      }
                  }, {
                      "id": "k4hxm884",
                      "positionOnShare": 1,
                      "placementId": "k4hxf7ly",
                      "shareId": "jyxuurm1",
                      "time": "[{\"startTime\":\"2020-01-08T17:00:00.000Z\",\"endTime\":\"2020-01-09T16:59:59.000Z\"}]",
                      "placement": {
                          "id": "k4hxf7ly",
                          "width": 1040,
                          "height": 90,
                          "startTime": "2020-01-08T17:00:00.000Z",
                          "endTime": "2020-01-09T16:59:59.000Z",
                          "weight": 1,
                          "revenueType": "cpd",
                          "cpdPercent": 1,
                          "isRotate": false,
                          "rotateTime": 0,
                          "relative": 0,
                          "campaignId": "k4hwpwf1",
                          "campaign": {
                              "id": "k4hwpwf1",
                              "startTime": "2019-12-22T17:00:00.000Z",
                              "endTime": null,
                              "weight": 0,
                              "revenueType": "cpd",
                              "pageLoad": 1,
                              "totalCPM": 0,
                              "isRunMonopoly": false,
                              "optionMonopoly": "",
                              "isRunBudget": false
                          },
                          "banners": [{
                              "id": "k4hxf813",
                              "html": "<script>\n  (function() {\n    var admid=window.frameElement.id;\n    var b = parent.document.getElementById(admid);\n    b.style.width = \"100%\";\n    var f = b.parentNode.id;\n    var a = parent.wPrototype.getElementWidth(f);\n    \n\tvar image_url = 'https://adi.admicro.vn/adt/banners/nam2015/4043/min_html5/huyphanquoc/2019_12_23/neptune-1/screenshot/neptune_0.jpg';\n\tvar banner_url = 'https://adi.admicro.vn/adt/banners/nam2015/4043/min_html5/lamlethi/2019_12_18/300x250-3/300x250/300x250.html';\n    var banner_width = 300;\n    var banner_height = 250;\n\t\n    var b = document,\n        c = navigator.userAgent + \"\";\n    var d = '<div id=\"adstop\" style=\"position:relative;overflow:hidden\">',\n        d = -1 != c.indexOf(\"Android\") || -1 != c.indexOf(\"iPad\") || -1 != c.indexOf(\"iPhone\") ? d + ('<img src=\"' + image_url + '\" border=\"0\"/><a href=\"%%CLICK_URL_UNESC%%\" target=\"_blank\" style=\"position:absolute;top:0;left:0;width:' + banner_width + 'px;height:' + banner_height + 'px;display:block;z-index:9999;\"><span></span></a>') : d + ('<iframe id=\"demo_iframe\" src=\"'+banner_url+'?url=%%CLICK_URL_ESC%%\" width=\"'+banner_width+'\" frameborder=\"0\" scrolling=\"no\" height=\"'+banner_height+'\"></iframe>');\n    b.write(d + \"</div>\");\n  }\n  )();\n</script>",
                              "width": 300,
                              "height": 250,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "k4hxf7ly",
                              "imageUrl": "",
                              "url": "https://tiki.vn/hop-qua-tet-neptune-2-chai-neptune-gold-1l-1-goi-hat-nem-vi-heo-250g-p7954228.html?utm_source=AdMicro&utm_medium=CPD&utm_campaign=Neptune",
                              "target": "",
                              "isIFrame": true,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "k5625ngt",
                                  "logical": "and",
                                  "bannerId": "k4hxf813",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "k5625oad",
                                      "optionBannerId": "k5625ngt",
                                      "channelId": "290",
                                      "channel": {
                                          "id": "290",
                                          "name": "Afamily_home",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyr01dyb",
                                              "name": "home",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/home/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "290",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }]
                      }
                  }, {
                      "id": "k4hxsifd",
                      "positionOnShare": 2,
                      "placementId": "k4hxhc0t",
                      "shareId": "jyxuurm1",
                      "time": "[{\"startTime\":\"2020-01-08T17:00:00.000Z\",\"endTime\":\"2020-01-09T16:59:59.000Z\"}]",
                      "placement": {
                          "id": "k4hxhc0t",
                          "width": 1040,
                          "height": 90,
                          "startTime": "2020-01-08T17:00:00.000Z",
                          "endTime": "2020-01-09T16:59:59.000Z",
                          "weight": 1,
                          "revenueType": "cpd",
                          "cpdPercent": 1,
                          "isRotate": false,
                          "rotateTime": 0,
                          "relative": 0,
                          "campaignId": "k4hwpwf1",
                          "campaign": {
                              "id": "k4hwpwf1",
                              "startTime": "2019-12-22T17:00:00.000Z",
                              "endTime": null,
                              "weight": 0,
                              "revenueType": "cpd",
                              "pageLoad": 1,
                              "totalCPM": 0,
                              "isRunMonopoly": false,
                              "optionMonopoly": "",
                              "isRunBudget": false
                          },
                          "banners": [{
                              "id": "k4hxhcdu",
                              "html": "<script>\n  (function() {\n    var admid=window.frameElement.id;\n    var b = parent.document.getElementById(admid);\n    b.style.width = \"100%\";\n    var f = b.parentNode.id;\n    var a = parent.wPrototype.getElementWidth(f);\n    \n\tvar image_url = 'https://adi.admicro.vn/adt/banners/nam2015/4043/min_html5/huyphanquoc/2019_12_23/neptune-2/screenshot/neptune_0.jpg';\n\tvar banner_url = 'https://adi.admicro.vn/adt/banners/nam2015/4043/min_html5/lamlethi/2019_12_18/300x385/300x385/300x385.html';\n    var banner_width = 300;\n    var banner_height = 385;\n\t\n    var b = document,\n        c = navigator.userAgent + \"\";\n    var d = '<div id=\"adstop\" style=\"position:relative;overflow:hidden\">',\n        d = -1 != c.indexOf(\"Android\") || -1 != c.indexOf(\"iPad\") || -1 != c.indexOf(\"iPhone\") ? d + ('<img src=\"' + image_url + '\" border=\"0\"/><a href=\"%%CLICK_URL_UNESC%%\" target=\"_blank\" style=\"position:absolute;top:0;left:0;width:' + banner_width + 'px;height:' + banner_height + 'px;display:block;z-index:9999;\"><span></span></a>') : d + ('<iframe id=\"demo_iframe\" src=\"'+banner_url+'?url=%%CLICK_URL_ESC%%\" width=\"'+banner_width+'\" frameborder=\"0\" scrolling=\"no\" height=\"'+banner_height+'\"></iframe>');\n    b.write(d + \"</div>\");\n  }\n  )();\n</script>",
                              "width": 300,
                              "height": 385,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "k4hxhc0t",
                              "imageUrl": "",
                              "url": "https://tiki.vn/hop-qua-tet-neptune-2-chai-neptune-gold-1l-1-goi-hat-nem-vi-heo-250g-p7954228.html?utm_source=AdMicro&utm_medium=CPD&utm_campaign=Neptune",
                              "target": "",
                              "isIFrame": true,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "k4hxsyr3",
                                  "logical": "and",
                                  "bannerId": "k4hxhcdu",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "k4hxszm0",
                                      "optionBannerId": "k4hxsyr3",
                                      "channelId": "290",
                                      "channel": {
                                          "id": "290",
                                          "name": "Afamily_home",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyr01dyb",
                                              "name": "home",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/home/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "290",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }]
                      }
                  }, {
                      "id": "k4tcgjsl",
                      "positionOnShare": 2,
                      "placementId": "k4tcebkf",
                      "shareId": "jyxuurm1",
                      "time": "[{\"startTime\":\"2019-12-30T17:00:00.000Z\",\"endTime\":\"2020-01-27T16:59:59.000Z\"}]",
                      "placement": {
                          "id": "k4tcebkf",
                          "width": 300,
                          "height": 385,
                          "startTime": "2019-12-30T17:00:00.000Z",
                          "endTime": "2020-01-27T16:59:59.000Z",
                          "weight": 1,
                          "revenueType": "cpd",
                          "cpdPercent": 1,
                          "isRotate": false,
                          "rotateTime": 0,
                          "relative": 0,
                          "campaignId": "k4tbr0gl",
                          "campaign": {
                              "id": "k4tbr0gl",
                              "startTime": "2019-12-30T17:00:00.000Z",
                              "endTime": "2020-01-27T16:59:59.000Z",
                              "weight": 0,
                              "revenueType": "cpd",
                              "pageLoad": 0,
                              "totalCPM": 0,
                              "isRunMonopoly": false,
                              "optionMonopoly": "",
                              "isRunBudget": false
                          },
                          "banners": [{
                              "id": "k4tcecev",
                              "html": "<script>\n  (function() {\n    var admid=window.frameElement.id;\n    var b = parent.document.getElementById(admid);\n    b.style.width = \"100%\";\n    var f = b.parentNode.id;\n    var a = parent.wPrototype.getElementWidth(f);\n    \n\tvar image_url = 'https://adi.admicro.vn/adt/banners/nam2015/4043/min_html5/vanphamhoang/2020_01_08/caryn/screenshot/caryn__bg_replay.jpg';\n\tvar banner_url = '//adi.admicro.vn/adt/banners/nam2015/4043/min_html5/mynguyenthitra/2020_01_07/Caryn_300x385/Caryn_300x385/Caryn_300x385.html';\n    var banner_width = 300;\n    var banner_height = 385;\n\t\n    var b = document,\n        c = navigator.userAgent + \"\";\n    var d = '<div id=\"adstop\" style=\"position:relative;overflow:hidden\">',\n        d = -1 != c.indexOf(\"Android\") || -1 != c.indexOf(\"iPad\") || -1 != c.indexOf(\"iPhone\") ? d + ('<img src=\"' + image_url + '\" border=\"0\"/><a href=\"%%CLICK_URL_UNESC%%\" target=\"_blank\" style=\"position:absolute;top:0;left:0;width:' + banner_width + 'px;height:' + banner_height + 'px;display:block;z-index:9999;\"><span></span></a>') : d + ('<iframe id=\"demo_iframe\" src=\"'+banner_url+'?url=%%CLICK_URL_ESC%%\" width=\"'+banner_width+'\" frameborder=\"0\" scrolling=\"no\" height=\"'+banner_height+'\"></iframe>');\n    b.write(d + \"</div>\");\n  }\n  )();\n</script>",
                              "width": 300,
                              "height": 385,
                              "keyword": "",
                              "isMacro": false,
                              "weight": 1,
                              "placementId": "k4tcebkf",
                              "imageUrl": "",
                              "url": "http://debametulam.com/hanh-trinh-de-ba-me-tu-lam",
                              "target": "",
                              "isIFrame": true,
                              "isDefault": false,
                              "isRelative": false,
                              "vastTagsUrl": "",
                              "thirdPartyTracking": "",
                              "thirdPartyUrl": "",
                              "isDocumentWrite": true,
                              "optionBanners": [{
                                  "id": "k4tchgit",
                                  "logical": "and",
                                  "bannerId": "k4tcecev",
                                  "comparison": "==",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "k4tchhgw",
                                      "optionBannerId": "k4tchgit",
                                      "channelId": "290",
                                      "channel": {
                                          "id": "290",
                                          "name": "Afamily_home",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jyr01dyb",
                                              "name": "home",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "/home/",
                                              "globalVariables": "_ADM_Channel",
                                              "channelId": "290",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "334288f0-8a58-4372-b262-2fba4fffabf9",
                                                  "name": "Variable",
                                                  "isInputLink": false,
                                                  "isSelectOption": false,
                                                  "isVariable": true,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }, {
                                  "id": "k54pmz1z",
                                  "logical": "and",
                                  "bannerId": "k4tcecev",
                                  "comparison": "!=",
                                  "value": "",
                                  "type": "channel",
                                  "optionBannerChannels": [{
                                      "id": "k54pmzme",
                                      "optionBannerId": "k54pmz1z",
                                      "channelId": "280",
                                      "channel": {
                                          "id": "280",
                                          "name": "Afamily - BETA",
                                          "siteId": "27",
                                          "optionChannels": [{
                                              "id": "jvrkoq7h",
                                              "name": "Site Page-URL",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": ".channelvn.net",
                                              "globalVariables": "",
                                              "channelId": "280",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                  "name": "Site Page-URL",
                                                  "isInputLink": true,
                                                  "isSelectOption": false,
                                                  "isVariable": false,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }, {
                                              "id": "jvrkoq7j",
                                              "name": "Site Page-URL",
                                              "logical": "or",
                                              "comparison": "=~",
                                              "value": "cnnd.vn",
                                              "globalVariables": "",
                                              "channelId": "280",
                                              "valueSelect": "",
                                              "logicalSelect": "",
                                              "optionChannelType": {
                                                  "id": "3b8cc1bc-aff2-436f-9829-4e4088a6fc7e",
                                                  "name": "Site Page-URL",
                                                  "isInputLink": true,
                                                  "isSelectOption": false,
                                                  "isVariable": false,
                                                  "status": "active",
                                                  "isMultiSelect": false,
                                                  "isGlobal": false,
                                                  "storageType": null
                                              }
                                          }]
                                      }
                                  }]
                              }],
                              "bannerType": {
                                  "name": "Script",
                                  "value": "script",
                                  "isUpload": false,
                                  "isVideo": false
                              },
                              "bannerHtmlType": {
                                  "name": "PR Tracking",
                                  "value": "pr-tracking",
                                  "weight": 99
                              }
                          }]
                      }
                  }]
              }],
              "site": {
                  "id": "27",
                  "domain": "http://afamily.vn",
                  "pageLoad": {
                      "totalPageload": 3,
                      "campaigns": [{
                          "campaignId": "k4hwpwf1",
                          "pageLoad": 1
                      }]
                  },
                  "isRunBannerDefault": false,
                  "isNoBrand": false,
                  "globalFilters": [],
                  "channels": [{
                      "id": "d045cfc5-c489-4e9c-8d74-423063db496b",
                      "name": "Location",
                      "isGlobal": true,
                      "storageType": "[{\"valueName\":\"__RC\",\"type\":\"cookie\"},{\"valueName\":\"__R\",\"type\":\"cookie\"}]"
                  }],
                  "campaignMonopoly": {
                      "campaigns": []
                  }
              }
          }
      }
  };
  if (!document.getElementById("arf-core-js")) {
      var u = document.createElement("script");
      u.id = "arf-core-js",
      u.type = "application/javascript",
      u.src = "//media1.admicro.vn/cms/Arf" + c + ".js",
      document.getElementsByTagName("body")[0].appendChild(u)
  }
  window.arfZonesQueue = window.arfZonesQueue || [],
  d.propsData.model && window.arfZonesQueue.push(d)
}
]);
//# sourceMappingURL=Template.min.js.map
